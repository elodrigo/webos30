var fs = require('fs');
var api = require('./api');
var lunaAPI = require('./luna');
var child_process = require('child_process');
var exec = require('child_process').exec;

var execFile = require('child_process').execFile;

var configFile = '/var/outdoorweb/ledConfig.json';
var firstUseFile = '/var/outdoorweb/firstUse.json';
var ledTypeFile = '/var/outdoorweb/ledType.json';
var updateDir = "/media/update/";
var updateDestDir = "/home/root/";
var siAppName = "com.lg.app.signage";
var downloadAppDir = "/media/internal/downloads/";
var copyAppDir = "/mnt/lg/appstore/signage/";

var command = {};
var ledOption;

function init() {
	initCommand();

	var envStr = fs.readFileSync(__dirname + '/env/ledOption.json');
	ledOption = JSON.parse(envStr);
}

function commandHandler(socket, msg) {
	var handler = command[msg.command];
	if (!handler) {
		console.log('No matching handler for ' + msg.command);
		return;
	}

	handler(socket, msg);
}

function getUnitResolutionList(socket, msg) {
	socket.emit("config" + msg.eventID, ledOption);
}

function getUnitConfigInternal(callback) {
	fs.readFile(configFile, function(err, data) {
		var config = {
			resolution: '32 x 32',
			size : {
				h: 0,
				v: 0
			},
			from: 0,
			direction: 0,
			regular: 0,
			onUnits: {
				numOn: 0
			}
		}

		if (!err) {
			config = JSON.parse(String(data));
		}

		callback(config);
	});
}

function getUnitConfig(socket, msg) {
	getUnitConfigInternal(function(config) {
		socket.emit("config" + msg.eventID, config);
	});
}

function setUnitConfig(socket, msg) {
	var config = {
		resolution: msg.resolution,
		size: {
			h: msg.size.h,
			v: msg.size.v
		},
		from: msg.from,
		direction: msg.direction,
		onUnits: msg.onUnits,
		regular: msg.regular
	};

	fs.writeFile(configFile, JSON.stringify(config, null, 2), function(err) {
		if (err) {
			console.log(err);
		}
		api.sendReturn(socket, err !== undefined, "config");
	});


	var resolution = msg.resolution.split('x');
	var settings = {
		//moduleWidth: Number(resolution[0]) / 2,
		//moduleHeight: Number(resolution[1]) / 2,
		rowOfModule: 2,
		columnOfModule: 2,
		//rowOfUnit: msg.size.v,
		//columnOfUnit: msg.size.h,
		numOfUnits: msg.onUnits.numOn
	};

	lunaAPI.setSystemSettings('commercial', settings, function(ret) {
		if (!ret.payload.returnValue) {
			console.log(ret.payload);
		}
	});
}

function getDiskSpace(socket, msg) {
	var ret = [0, 0];
	try {
		var str = '' + child_process.execSync("df -B 1 | grep media$ | awk '{print $2 \" \" $4}'");
		console.log(str);
		var spaceArr = str.split(' ');
		console.log(spaceArr);
		ret[0] = Number(spaceArr[0]);
		ret[1] = Number(spaceArr[1]);
		console.log(ret);
		socket.emit("getDiskSpace" + msg.eventID, ret);
	} catch (e) {
		console.log(e);
		socket.emit("getDiskSpace" + msg.eventID, e);
	}
}

function getNewLog(socket, msg) {
	var dir = "/var/sdmlog/";
	var downDir = "/var/sdmlog/download/";

	var files = fs.readdirSync(dir);
	var re = /^log\d{5}\.txt$/;
	files = files.filter(function(name) {
		return re.test(name);
	});
	files.sort();

	var fileName = files[files.length - 1];
	var cmd = 'cat ' + dir + fileName
	var log = "";

	try {
		console.log('======log======');
		log = child_process.execSync(cmd, {
			timeout: 2000
		});
		log = String(log);
		log = log.replace(/Please, Set a time/g, "Please Set a time");
		socket.emit("getNewLog" + msg.eventID, log);
	} catch (e) {
		console.log('======error======');
		console.log(e);
		log = "";
	}

}

function listDevices(socket, msg) {
	api.luna().call('luna://com.webos.service.attachedstoragemanager/listDevices', {}, function(ret) {
        socket.emit('listDevices' + msg.eventID, ret.payload);
    });
}

function installApplication(socket, msg) {
	var installIPK = api.luna().subscribe('luna://com.webos.appInstallService/commercial/installInternal', {
		id: siAppName,
		ipkUrl: msg.ipkUrl,
		subscribe: true
	});

	installIPK.on("response", function (ret) {
		if (ret.payload.hasOwnProperty("details") && (ret.payload.details.state === "installed" || ret.payload.details.state === "install failed")) {
			api.io.emit("installIPK", ret.payload);
			installIPK.cancel();
		}
	});
}

function downloadApplication(socket, msg) {
	var downloadIPK = api.luna().subscribe('palm://com.palm.downloadmanager/download', {
		target: msg.targetURL,
		targetDir: downloadAppDir,
		targetFilename: siAppName + '.ipk',
		subscribe: true
	});

	downloadIPK.on('response', function (ret) {
		if (ret.payload.hasOwnProperty("completionStatusCode")) {
			api.io.emit('downloadIPK', ret.payload);
			downloadIPK.cancel();
		}
	});
}

function upgradeApplication(socket, msg) {
	api.luna().call('luna://com.webos.service.commercial.signage.storageservice/upgradeApplication', {
		from: msg.from,
		to: msg.to
	}, function (ret) {
		socket.emit('upgradeApplication' + msg.eventID, ret.payload);
	});
}

function copyApplication(socket, msg) {
	api.luna().call('luna://com.webos.service.attachedstoragemanager/copy', {
		sourceDeviceId: msg.deviceId,
		sourceSubDeviceId: msg.subDevices[0].deviceId,
		destinationDeviceId: 'INTERNAL_STORAGE_SIGNAGE',
		sourcePath: msg.subDevices[0].deviceUri + '/application/' + siAppName + '.ipk',
		destinationPath: copyAppDir + siAppName + '.ipk',
		itemType: 'unknown',
		overwrite: true
	}, function (ret) {
		if(ret.payload.returnValue){
			ret.payload.destinationPath = copyAppDir + siAppName + '.ipk';
		}
		socket.emit('copyApplication' + msg.eventID, ret.payload);
	});
}

var ledfpgaURL = 'luna://com.webos.service.tv.signage/ledfpga/';

function lunaCallConfig(url, command, param, callback) {
	api.luna().call(url, {
		command: command,
		parameter: param
	}, callback);
}

function lunaSetConfig(command, param, callback) {
	var setConfigURL = ledfpgaURL + 'setConfig';
	lunaCallConfig(setConfigURL, command, param, callback);
}

function lunaGetConfig(command, param, callback) {
	var getConfigURL = ledfpgaURL + 'getConfig';
	lunaCallConfig(getConfigURL, command, param, callback);
}

function saveConfigInternal(slave, callback) {
	setTimeout(function() {
		lunaSetConfig('showPattern', {
			slave: slave,
			on: false,
			r: 0,
			g: 0,
			b: 0
		}, function(ret) {
			lunaSetConfig('saveConfigData', {
				slave: slave
			}, function(ret) {
				callback(ret);
			});
		});
	}, 100);
}

function saveConfig(socket, msg) {
	saveConfigInternal(msg.slave, function(ret) {
		socket.emit("config" + msg.eventID, ret.payload.returnValue);
	});
}

function loadConfig(socket, msg) {
	lunaSetConfig('loadConfigData', {
		slave: msg.slave
	}, function(ret) {
		socket.emit("config" + msg.eventID, ret.payload.returnValue);
	});
}


function saveConfigAllFpga(socket, msg) {
	lunaGetConfig('saveConfigAllFpga', {}, function(ret) {
		socket.emit("ConfigAllFpga" + msg.eventID, ret.payload.returnValue);
	});
}

function setSlaveAddr(socket, msg) {
	lunaSetConfig('setSlaveAddr', {
		numUnits: msg.numUnits
	}, function(ret) {
		saveConfigInternal(0, function(ret2) {
			socket.emit("config" + msg.eventID, ret.payload.returnValue);
		});
	});
}

function setPosition(socket, msg) {
	lunaSetConfig('setPosition', {
		slave: msg.slave,
		x: msg.x,
		y: msg.y
	}, function(ret) {
		saveConfigInternal(msg.slave, function(ret2) {
			socket.emit("config" + msg.eventID, ret.payload.returnValue);
		});
	});
}

function getPosition(socket, msg) {
	lunaGetConfig('getPosition', {
		slave: msg.slave
	}, function(ret) {
		socket.emit("getPosition" + msg.eventID, ret.payload.result);
	});
}

function showPatternInternal(msg, callback) {
	lunaSetConfig('showPattern', {
		slave: msg.slave,
		on: msg.on,
		r: msg.r,
		g: msg.g,
		b: msg.b
	}, callback);
}

function showPattern(socket, msg) {
	showPatternInternal(msg, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'pattern');
	});
}

function setGamma(socket, msg) {
	lunaSetConfig('setGamma', {
		slave: msg.slave,
		gamma: msg.gamma
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'unit');
	});
}

function getGamma(socket, msg) {
	lunaGetConfig('getGamma', {
		slave: msg.slave
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload);
	});
}

function setGamut(socket, msg) {
	lunaSetConfig('setGamut', {
		slave: msg.slave,
		array: msg.array
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'unit');
	});
}

function getGamut(socket, msg) {
	lunaGetConfig('getGamut', {
		slave: msg.slave
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result.gamut);
	});
}

function setGlobalBC(socket, msg) {
	lunaSetConfig('setGlobalBC', {
		slave: msg.slave,
		bc: msg.bc
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'unit');
	});
}

function getGlobalBC(socket, msg) {
	lunaGetConfig('getGlobalBC', {
		slave: msg.slave
	}, function(ret) {
		socket.emit("globalBC" + msg.eventID, ret.payload.result);
	});
}

function setBC_CC(socket, msg) {
	lunaSetConfig('setBC_CC', {
		slave: msg.slave,
		bank: msg.bank,
		bc: msg.bc,
		r: msg.r,
		g: msg.g,
		b: msg.b
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'module');
	});
}

function getBC_CC(socket, msg) {
	lunaGetConfig('getBC_CC', {
		slave: msg.slave,
		bank: msg.bank
	}, function(ret) {
		socket.emit("module" + msg.eventID, ret.payload.result);
	});
}

function getTemperature(socket, msg) {
	lunaGetConfig('getTemperature', {
		slave: msg.slave,
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function getTempHumidity(socket, msg) {
	lunaGetConfig('getTempHumidity', {
		fpgaNo: msg.fpgaNo,
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function getCurrent(socket, msg) {
	lunaGetConfig('getCurrent', {
		slave: msg.slave,
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function getSignalStatus(socket, msg) {
	lunaGetConfig('getSignalStatus', {
		slave: msg.slave,
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function getSignalState(socket, msg) {
	lunaGetConfig('getSignalState', {
		fpgaNo: msg.fpgaNo,
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function getPowerState(socket, msg) {
	lunaGetConfig('getPowerState', {
		fpgaNo: msg.fpgaNo,
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function getPowerStatus(socket, msg) {
	lunaGetConfig('getPowerStatus', {
		slave: msg.slave,
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function getLOD(socket, msg) {
	lunaGetConfig('getLOD', {
		slave: msg.slave,
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function getLodDataInternal(param, callback) {
	lunaGetConfig('getLodData', {
		slave: param.slave
	}, callback);
}

function getLodData(socket, msg) {
	getLodDataInternal(msg, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function getOverallStatus(socket, msg) {
	lunaGetConfig('getOverallStatus', {
		slave: msg.slave,
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function setModuleCalibration(socket, msg) {
	lunaSetConfig('tlfCalibMakeMemory', {
		slave: msg.slave,
		row : msg.row,
		col : msg.col,
		data: msg.data
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'module');
	});
}

function saveModuleCalibration(socket, msg) {
	lunaSetConfig('tlfCalibSaveApply', {
		slave: msg.slave
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'module');
	});
}

function reloadModuleCalibration(socket, msg) {
	lunaSetConfig('tlfCalibLoadApply', {
		slave: msg.slave
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'module');
	});
}

function getModuleCalibration(socket, msg) {
	lunaGetConfig('getTLFModuleGainTableIdx0', {
		slave: msg.slave,
		row : msg.row,
		col : msg.col
	}, function(ret) {
		socket.emit("module" + msg.eventID, ret.payload.result);
	});
}

function saveCalibrationData(socket, msg) {
	lunaSetConfig('saveCalibrationData', {
		slave: msg.slave,
		bank: msg.bank
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'module');
	});
}

function loadCalibrationData(socket, msg) {
	lunaSetConfig('loadCalibrationData', {
		slave: msg.slave,
		from: msg.from
	}, function(ret) {
		socket.emit("module" + msg.eventID, ret.payload.result);
	});
}

function ledFpgaUpdate(socket, msg) {
	api.luna().call(ledfpgaURL + 'fpgaUpdate', {
		filePath: updateDir + msg.fileName
	}, function(ret) {
		socket.emit("ledFpga" + msg.eventID, ret.payload);
	});
}

function getFpgaVersion(socket, msg) {
	lunaGetConfig('getFpgaVersion', {
		numUnits: msg.numUnits
	}, function(ret) {
		socket.emit("ledFpga" + msg.eventID, ret.payload);
	});
}

function getFpgaUpdatePercent(socket, msg) {
	api.luna().call(ledfpgaURL + 'getFPGAUpdatePercent', {
	}, function(ret) {
		socket.emit("ledFpga" + msg.eventID, ret.payload);
	});
}

function getFPGAUpdateErrorStatus(socket, msg) {
	api.luna().call(ledfpgaURL + 'getFPGAUpdateErrorStatus', {
	}, function(ret) {
		socket.emit("ledFpga" + msg.eventID, ret.payload);
	});
}

function initConfigFpga(socket, msg) {
	lunaSetConfig('initConfigFpga', {
		slave: msg.slave,
	}, function(ret) {
		socket.emit("ledFpga" + msg.eventID, ret.payload.returnValue);
	});
}

function rebootFpga(socket, msg) {
	lunaSetConfig('rebootFpga', {
		slave: msg.slave,
	}, function(ret) {
		socket.emit("ledFpga" + msg.eventID, ret.payload.returnValue);
	});
}

function getFpgaCheckSum(socket, msg) {
	var path = '/media/update/' + msg.name;
	if (msg.name == 'NULL') {
		path = 'NULL';
	}
	api.luna().call(ledfpgaURL + 'getFPGACheckSum', {
		filePath: path
	}, function(ret) {
		socket.emit("ledFpga" + msg.eventID, ret.payload);
	});
}

function setPMControl(socket, msg) {
	lunaSetConfig('setPMControl', {
		onOff: msg.onOff,
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'factory');
	});
}

function setInputDirection(socket, msg) {
	lunaSetConfig('setInputDirection', {
		slave: msg.slave,
		inputDirection: msg.inputDirection
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'inputDirection');
	});
}

function getInputDirection(socket, msg) {
	lunaGetConfig('getInputDirection', {
		slave: msg.slave
	}, function(ret) {
		socket.emit("getInputDirection" + msg.eventID, ret.payload.result);
	});
}

function setWidthHeight(socket, msg) {
	lunaSetConfig('setWidthHeight', {
		slave: msg.slave,
		width: msg.width,
		height: msg.height
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'setWidthHeight');
	});
}

function getWidthHeight(socket, msg) {
	lunaGetConfig('getWidthHeight', {
		slave: msg.slave
	}, function(ret) {
		socket.emit("getWidthHeight" + msg.eventID, ret.payload.result);
	});
}

function setDisplayPortSel(socket, msg) {
	lunaSetConfig('setDisplayPortSel', {
		slave: msg.slave,
		displayPort: msg.displayPort
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'setDisplayPortSel');
	});
}

function getDisplayPortSel(socket, msg) {
	lunaGetConfig('getDisplayPortSel', {
		slave: msg.slave
	}, function(ret) {
		socket.emit("getDisplayPortSel" + msg.eventID, ret.payload.result);
	});
}

function getTrim(socket, msg) {
	lunaGetConfig('getTrim', {
		slave: msg.slave
	}, function(ret) {
		socket.emit("getTrim" + msg.eventID, ret.payload.result);
	});
}

function setTrim(socket, msg) {
	lunaSetConfig('setTrim', {
		slave: msg.slave,
		width: msg.width,
		height: msg.height
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'setTrim');
	});
}

function getAutoDetect(socket, msg) {
	lunaGetConfig('getTotalUnit', {
		slave: msg.slave
	}, function(ret) {
		socket.emit("getAutoDetect" + msg.eventID, ret.payload.result);
	});
}


function firstUseDone(socket, msg) {
	fs.readFile(firstUseFile, function(err, data) {
		var exist = true;

		if(err){
			if(err.code == 'ENOENT')
				exist = false;
			console.log(err);
		}
		socket.emit("firstUseDone" + msg.eventID, exist);
	});
}

function ledTypeDone(socket, msg) {
	fs.readFile(ledTypeFile, function(err, data) {
		var exist = true;

		if(err){
			if(err.code == 'ENOENT')
				exist = false;
			console.log(err);
		}
		socket.emit("ledTypeDone" + msg.eventID, exist);
	});
}

function saveFirstUseFile(socket, msg) {
	fs.writeFile(firstUseFile, JSON.stringify(msg.selectVal, null, 2), function(err) {
		if (err) {
			console.log(err);
		}
		api.sendReturn(socket, err !== undefined, "saveFirstUseFile");
	});
}

function saveLEDTypeFile(socket, msg) {
	fs.writeFile(ledTypeFile, JSON.stringify(msg.ledType, null, 2), function(err) {
		if (err) {
			console.log(err);
		}
		api.sendReturn(socket, err !== undefined, "saveLEDTypeFile");
	});
}

var lodTimer = undefined;

var colorCode = ['-', 'R', 'G', 'RG', 'B', 'RB', 'GB', 'RGB'];
function mergeColor(c1, c2) {
	if (c1 == c2) {
		return c1;
	}
	var i1 = colorCode.indexOf(c1);
	var i2 = colorCode.indexOf(c2);

	var newIndex = i1 | i2;

	return colorCode[newIndex];
}

function packLodData(data) {
	var keys = Object.keys(data);
	var ret = {};
	keys.forEach(function(key) {
		var comma = key.lastIndexOf(',');
		var location = key.substring(0, comma);
		var color = key.substring(comma + 1, key.length);

		if (ret[location]) {
			color = mergeColor(ret[location], color);
		}

		ret[location] = color;
	});

	return ret;
}

function liveLOD(socket, msg) {
	var ret = {
		isRunning: lodTimer !== undefined
	};

	if (msg.start === undefined) {
		if (lodTimer) {
			ret.data = packLodData(LodData.data);
		}
		socket.emit('liveLOD' + msg.eventID, ret);
		return;
	}

	if (msg.start === false) {
		if (lodTimer) {
			LodData.stopDetection();
		}

		ret.isRunning = false;
		socket.emit('liveLOD' + msg.eventID, ret);
		return;
	}

	getUnitConfigInternal(function(config) {
		LodData.config = config;

		if (LodData.config.onUnits.numOn == 0) {
			return;
		}
	
		lodTimer = setInterval(LodData.detectionTimerCallback, 10 * 1000);

		ret.isRunning = true;
		socket.emit('liveLOD' + msg.eventID, ret);
	});
}







// for Color Film
function getReceivedMac(socket, msg) {
	lunaGetConfig('getReceivedMAC', {slave: msg.slave}, function(ret) {
		console.log(ret);
		socket.emit("getReceivedMac" + msg.eventID, ret.payload);
	});
}

function getSendingMAC(socket, msg) {
	lunaGetConfig('getSendingMAC', {slave: msg.slave}, function(ret) {
		console.log(ret);
		socket.emit("getSendingMAC" + msg.eventID, ret.payload);
	});
}

function getTempHumidityColor(socket, msg) {
	lunaGetConfig('getTempHumidityColor', {
		senderIdx: msg.senderIdx,
		receiverIdx : msg.receiverIdx
	}, function(ret) {
		socket.emit("getTempHumidityColor" + msg.eventID, ret.payload);
	});
}

function getFpgaStatus(socket, msg) {
	lunaGetConfig('getFpgaStatus', {
		index: msg.index
	}, function(ret) {
		socket.emit("getFpgaStatus" + msg.eventID, ret.payload);
	});
}

function getReceiverStatus(socket, msg) {
	lunaGetConfig('getReceiverStatus', {
		slave : msg.slave,
		rcvIdx: msg.rcvIdx
	}, function(ret) {
		socket.emit("getReceiverStatus" + msg.eventID, ret.payload);
	});
}

function getFilmGain(socket, msg) {
	lunaGetConfig('getFilmGain', {
		slave: msg.slave,
		rcvIdx: msg.rcvIdx,
		filmIdx: msg.filmIdx
	}, function(ret) {
		socket.emit("getFilmGain" + msg.eventID, ret.payload);
	});
}

function setSendingMAC(socket, msg) {
	lunaSetConfig('setSendingMAC', {
		slave: msg.slave,
		rcvIdx: msg.rcvIdx,
		mac5th: msg.mac5th,
		mac6th: msg.mac6th
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'setSendingMAC');
	});
}

function clearSendingMAC(socket, msg) {
	lunaSetConfig('clearSendingMAC', {
		slave: msg.slave
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'clearSendingMAC');
	});
}

function setSenderPos(socket, msg) {
	lunaSetConfig('setSenderPos', {
		slave: msg.slave,
		hValue: msg.hValue,
		vValue: msg.vValue
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'setSenderPos');
	});
}

function setXYPos(socket, msg) {

	if( -1 < msg.rcvIdx ){
		lunaSetConfig('setXYPos', {
			slave: msg.slave,
			rcvIdx: msg.rcvIdx,
			xValue: msg.xValue,
			yValue: msg.yValue
		}, function(ret) {
			api.sendReturn(socket, ret.payload.returnValue, 'setXYPos');
		});
	}else{
		lunaSetConfig('setXYPos', {
			slave: msg.slave,
			xValue: msg.xValue,
			yValue: msg.yValue
		}, function(ret) {
			api.sendReturn(socket, ret.payload.returnValue, 'setXYPos');
		});
	}
}

function setInputDirectionColor(socket, msg) {
	lunaSetConfig('setInputDirectionColor', {
		slave: msg.slave,
		inputDirection: msg.inputDirection
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'setInputDirectionColor');
	});
}

function setDisplayPort(socket, msg) {
	lunaSetConfig('setDisplayPort', {
		slave: msg.slave,
		rcvIdx : msg.rcvIdx,
		value: msg.value
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'setDisplayPort');
	});
}

function setTrimColor(socket, msg) {
	lunaSetConfig('setTrimColor', {
		slave: msg.slave,
		rcvIdx : msg.rcvIdx,
		left: msg.left,
		right: msg.right
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'setTrimColor');
	});
}

function setTestPattern(socket, msg) {
	lunaSetConfig('setTestPattern', {
		slave: msg.slave,
		onOff : msg.onOff,
		patternR : msg.patternR,
		patternG : msg.patternG,
		patternB : msg.patternB,
		mac5th : msg.mac5th,
		mac6th : msg.mac6th
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'setTestPattern');
	});
}

function setHVNum(socket, msg) {
	lunaSetConfig('setHVNum', {
		slave: msg.slave,
		rcvIdx : msg.rcvIdx,
		hValue : msg.hValue,
		vValue : msg.vValue
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'setHVNum');
	});
}

function setReceiverNum(socket, msg) {
	lunaSetConfig('setReceiverNum', {
		slave: msg.slave,
		receiverNum : msg.receiverNum
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'setReceiverNum');
	});
}

function setIP(socket, msg) {
	lunaSetConfig('setIP', {
		slave: msg.slave,
		senderIP1st : msg.ip1st,
		senderIP2nd : msg.ip2nd,
		senderIP3th : msg.ip3th,
		senderIP4th : msg.ip4th,
		serverIP1st : msg.serverIP[0],
		serverIP2nd : msg.serverIP[1],
		serverIP3th : msg.serverIP[2],
		serverIP4th : msg.serverIP[3]
	}, function(ret) {
		socket.emit("setIP" + msg.eventID, ret.payload.returnValue);
	});
}

function setFpgaControl(socket, msg) {
	lunaSetConfig('setFpgaControl', {
		slave: msg.slave,
		index : msg.index,
		value : msg.value
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'setFpgaControl');
	});
}

function setFilmGain(socket, msg) {
	lunaSetConfig('setFilmGain', {
		slave: msg.slave,
		rcvIdx : msg.rcvIdx,
		filmIdx : msg.filmIdx,
		valueR : msg.valueR,
		valueG : msg.valueG,
		valueB : msg.valueB
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'setFilmGain');
	});
}

function saveFPGAConfig(socket, msg) {
	lunaSetConfig('setFpgaControl', {
		slave: msg.slave,
		index : msg.index,
		value : msg.value
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'saveFPGAConfig');
	});
}

function FPGAReset(socket, msg) {
	console.log('FPGAReset');
	lunaSetConfig('setFpgaControl', {
		slave: msg.slave,
		index : msg.index,
		value : msg.value
	}, function(ret) {
		socket.emit("FPGAReset" + msg.eventID, ret.payload.returnValue);
	});
}

function execTFTP(socket, msg){
	var child = exec('/usr/bin/udpsvd -vE 0.0.0.0 69 /usr/sbin/tftpd /home/root/ > /dev/null 2>&1 &', function(error, stdout, stderr){
		console.log(error);
		console.log(stderr);
		console.log(stdout);
	});
	socket.emit("execTFTP" + msg.eventID, child.pid);
}

function killTFTP(socket, msg){
	var child = exec('kill '+msg.pid);
}

function moveUpdateFile(socket, msg){
	fs.createReadStream(updateDir + msg.fileName).pipe(fs.createWriteStream(updateDestDir + 'tled_ota.img'));
}

function getFpgaVersionColor(socket, msg) {
	lunaGetConfig('getFpgaVersionColor', {slave: msg.slave}, function(ret) {
		socket.emit("getFpgaVersionColor" + msg.eventID, ret.payload);
	});
}

function getPowerStatusColor(socket, msg) {
	lunaGetConfig('getPowerStatusColor', {index: msg.index}, function(ret) {
		socket.emit("getPowerStatusColor" + msg.eventID, ret.payload);
	});
}


function getUpgradeStatus(socket, msg) {
	lunaGetConfig('getUpgradeStatus', {slave: msg.slave}, function(ret) {
		socket.emit("getUpgradeStatus" + msg.eventID, ret.payload);
	});
}

function getP14Status(socket, msg) {
	lunaGetConfig('getP14Status', {}, function(ret) {
		socket.emit("getP14Status" + msg.eventID, ret.payload);
	});
}

function initCommand() {
	command['getUnitResolutionList'] = getUnitResolutionList;

	command['getUnitConfig'] = getUnitConfig;
	command['setUnitConfig'] = setUnitConfig;

	command['getDiskSpace'] = getDiskSpace;
	command['getNewLog'] = getNewLog;

	command['setSlaveAddr'] = setSlaveAddr;
	command['setPosition'] = setPosition;
	command['getPosition'] = getPosition;

	command['showPattern'] = showPattern;

	command['getGamma'] = getGamma;
	command['setGamma'] = setGamma;
	command['getGamut'] = getGamut;
	command['setGamut'] = setGamut;
	command['setGlobalBC'] = setGlobalBC;
	command['getGlobalBC'] = getGlobalBC;
	command['getBC_CC'] = getBC_CC;
	command['setBC_CC'] = setBC_CC;

	command['getTempHumidity'] = getTempHumidity;
	command['getTemperature'] = getTemperature;
	command['getCurrent'] = getCurrent;
	command['getSignalStatus'] = getSignalStatus;
	command['getSignalState'] = getSignalState;
	command['getPowerState'] = getPowerState;
	command['getPowerStatus'] = getPowerStatus;
	command['getLOD'] = getLOD;
	command['getOverallStatus'] = getOverallStatus;

	command['getLodData'] = getLodData;

	command['saveConfig'] = saveConfig;
	command['loadConfig'] = loadConfig;

	command['setModuleCalibration'] = setModuleCalibration;
	command['saveModuleCalibration'] = saveModuleCalibration;
	command['reloadModuleCalibration'] = reloadModuleCalibration;
	command['getModuleCalibration'] = getModuleCalibration;

	command['saveCalibrationData'] = saveCalibrationData;
	command['loadCalibrationData'] = loadCalibrationData;

	command['saveConfigAllFpga'] = saveConfigAllFpga;

	command['ledFpgaUpdate'] = ledFpgaUpdate;
	command['getFpgaVersion'] = getFpgaVersion;
	command['getFpgaUpdatePercent'] = getFpgaUpdatePercent;
	command['getFPGAUpdateErrorStatus'] = getFPGAUpdateErrorStatus;
	command['initConfigFpga'] = initConfigFpga;
	command['rebootFpga'] = rebootFpga;
	command['getFpgaCheckSum'] = getFpgaCheckSum;

	command['setPMControl'] = setPMControl;
	command['setInputDirection'] = setInputDirection;
	command['setWidthHeight'] = setWidthHeight;
	command['setDisplayPortSel'] = setDisplayPortSel;
	command['getAutoDetect'] = getAutoDetect;
	command['getInputDirection'] = getInputDirection;
	command['getWidthHeight'] = getWidthHeight;
	command['getDisplayPortSel'] = getDisplayPortSel;
	command['getTrim'] = getTrim;
	command['setTrim'] = setTrim;

	command['firstUseDone'] = firstUseDone;
	command['ledTypeDone'] = ledTypeDone;
	command['saveFirstUseFile'] = saveFirstUseFile;
	command['saveLEDTypeFile'] = saveLEDTypeFile;

	command['liveLOD'] = liveLOD;

	//for Color Film
	command['getReceivedMac'] = getReceivedMac;
	command['getSendingMAC'] = getSendingMAC;
	command['getTempHumidityColor'] = getTempHumidityColor;
	command['getFpgaStatus'] = getFpgaStatus;
	command['getReceiverStatus'] = getReceiverStatus;
	command['getFilmGain'] = getFilmGain;

	command['setSendingMAC'] = setSendingMAC;
	command['clearSendingMAC'] = clearSendingMAC;
	command['setSenderPos'] = setSenderPos;
	command['setXYPos'] = setXYPos;
	command['setInputDirectionColor'] = setInputDirectionColor;
	command['setDisplayPort'] = setDisplayPort;
	command['setTrimColor'] = setTrimColor;
	command['setTestPattern'] = setTestPattern;
	command['setHVNum'] = setHVNum;
	command['setReceiverNum'] = setReceiverNum;
	command['setFilmGain'] = setFilmGain;
	command['saveFPGAConfig'] = saveFPGAConfig;
	command['FPGAReset'] = FPGAReset;

	command['execTFTP'] = execTFTP;
	command['killTFTP'] = killTFTP;
	command['setIP'] = setIP;
	command['moveUpdateFile'] = moveUpdateFile;
	command['getFpgaVersionColor'] = getFpgaVersionColor;
	command['getPowerStatusColor'] = getPowerStatusColor;
	command['getUpgradeStatus'] = getUpgradeStatus;
	command['setFpgaControl'] = setFpgaControl;
	command['getP14Status'] = getP14Status;

	// SI server Setting
	command['listDevices'] = listDevices;
	command['upgradeApplication'] = upgradeApplication;
	command['downloadApplication'] = downloadApplication;
	command['installApplication'] = installApplication;
	command['copyApplication'] = copyApplication;
}

var LodData = {
	slave: 2,
	bank: 0,
	config: undefined,
	data: {},

	collectData: function (slave, now) {
		getLodDataInternal({
			slave: slave,
		}, function(ret) {
			ret.payload.result.defects.forEach(function(defect) {
				for (var i = 0; i != defect.color.length; ++i) {
					var key = '' + slave + ',' + defect.ldm + ',' + defect.v + ',' + defect.h + ',' + defect.color[i];
					LodData.data[key] = now;
				}
			});
		})
	},

	moveDetectionWindow: function (data, now) {
		for (var key in data) {
			if (data[key] < (now - 60 * 60 * 1000)) { // one hour
				delete data[key];
			}
		}
	},

	detectionTimerCallback: function () {
		var now = Date.now();

		LodData.moveDetectionWindow(LodData.data, now);
		LodData.collectData(LodData.slave, now);

		LodData.slave = ((LodData.slave / 2) % LodData.config.onUnits.numOn + 1) * 2;
	},

	stopDetection: function() {
		clearInterval(lodTimer);
		lodTimer = undefined;
		LodData.data = {};
		LodData.slave = 2;
	}
}

exports.commandHandler = commandHandler;
exports.getUnitConfig = getUnitConfigInternal;
exports.ledSetConfig = lunaSetConfig;
exports.ledGetConfig = lunaGetConfig;
exports.getLodData = getLodDataInternal;
exports.showPattern = showPatternInternal;
exports.init = init;
