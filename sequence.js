/*
 * by bc.cha@lge.com (ByungChang Cha)
 *
 * usage)
 * sequence(function(next) {
 *     firstTask();
 *     next();
 * }, function(next) 
 *     secondTask();
 *     next();
 * }, ...
 * ...
 * }, function() {
 *     sumUpTasks();
 * }
 * 
 * Example)
	sequence(function(next) {
    	console.log('first');
    	next();
	}, function(next) {
    	console.log('second');
    	next();
	}, function() {
    	console.log('finally'); 
	});
 *
 * Result - prints in sequencial order.
   first
   second
   finally
 *
 */

function sequence() {
	var allFns = arguments;

	function createWrapFn(i) {
		if (i == allFns.length - 1) {
			return allFns[i];
		}

		return function() {
			allFns[i](createWrapFn(i + 1));
		}
	}

	if (allFns.length == 1) {
		allFns[0](function(){});
		return;
	}

	createWrapFn(0)();
}

/*
 * When all tasks end, final handler is called.
 * Same usage as sequence API.
 */

function join() {
	var allFns = arguments;
	var finished = 0;

	if (allFns.length == 1) {
		allFns[0](function(){});
		return;
	}

	function done() {
		++finished;
		if (finished == allFns.length - 1) {
			allFns[allFns.length - 1]();
			return;
		}
	}

	for (var i = 0; i != allFns.length - 1; ++i) {
		allFns[i](done);
	}   
}

exports.sequence = sequence;
exports.join = join;
