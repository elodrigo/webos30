var app = require('./app').app;
var restApp = require('./restApp').app;
var session = require('./app').session;
var api = require("./api");
var lunaAPI = require("./luna");
var ledAPI = require('./ledAPI');
var ledFilmAPI = require('./ledFilmAPI');
var fs = require('fs');
var http = require('http');
var https = require('https');
var constants = require('constants');

var envStr = fs.readFileSync(__dirname + '/env/env.json');
var env = JSON.parse(envStr);
var modelStr = fs.readFileSync(__dirname + "/env/model.json");
var model = JSON.parse(modelStr);

var languageTable = undefined;
var languageStr = '';

var server;
var Service = require('webos-service');
var service = new Service("com.webos.service.outdoorwebcontrol");

var forceStartStr = '--force-start';
var supportLedSignageStr = '--support-led-signage';
var supportLedSignage20Str = '--support-led-signage-20';
var support365CareStr = '--support-365-care';

lunaAPI.init(service);

lunaAPI.getWebOSInfo(function(m) {
	var webOSVer = m.payload.core_os_release;

	if (webOSVer.indexOf('3.') == 0) {
		startServerFor_3_0();
		return;
	}

	if (webOSVer.indexOf('2.') == 0 && process.argv.indexOf(forceStartStr) >= 0) {
		initEnv('86BH5C');
		lunaAPI.createRemoCodeMap();

		startServer();
		api.init(service, server, session);
		return;
	}

	console.log("This model does not support outdoorwebcontrol");
	process.exit(0);
});

var isFirstBootup = false;

function startServerFor_3_0() {
	service.call('luna://com.webos.service.config/getConfigs', {
		'configNames': [
			"tv.model.*",
			"commercial.network.disableWifi",
			"tv.model.commerAddLanguageType",
			"commercial.hw.sensor.fanInfo"
		]
	}, function(msg) {
		if (!msg) {
			return;
		}

		// check whether property exists.
		var modelName = msg.payload.configs["tv.model.commerModelOptType"];
		if (!modelName) {
			console.log("Cannot read Micom model opt type!");
			process.exit(0);
			return;
		}

		if (!(modelName in model.supportModels) && process.argv.indexOf(forceStartStr) < 0) {
			console.log("Does not support " + modelName);
			process.exit(0);
			return;
		}

		env.disableWifi = msg.payload.configs["commercial.network.disableWifi"];
		env.toolType = msg.payload.configs["tv.model.toolType"];
		env.addLanguage = msg.payload.configs["tv.model.commerAddLanguageType"];
		env.moduleInchType = msg.payload.configs["tv.model.moduleInchType"];
		var fanInfo = msg.payload.configs["commercial.hw.sensor.fanInfo"];
		env.fanControl = parsefanInfo(fanInfo, Number(env.moduleInchType));

		lunaAPI.getSystemSettings('commercial', [
			"signage365CareAccountNumber", "signage365CareAccountName", "care365Enable"
		], function(m) {
			env.support365Care = (env.supportLedSignage && m.payload.returnValue) || process.argv.indexOf(support365CareStr) >=0;
		});

		lunaAPI.getInputList(function (m) {
			env.numOfExtInputs = m.payload.totalCount ? m.payload.totalCount : 0
		});

		initEnv(modelName);
		lunaAPI.createRemoCodeMap();

		startServer();
		api.init(service, server, session);
		if (isFirstBootup && env.supportLedSignage) {
			api.setDefaultTimezone();
		}

		ledAPI.init();
		ledFilmAPI.init();
	});
}

function initEnv(modelName) {
	if (!fs.existsSync('/var/outdoorweb')) {
		fs.mkdirSync('/var/outdoorweb');
		isFirstBootup = true;
	}

	service.call('luna://com.webos.settingsservice/getSystemSettings', {
		category : 'commercial',
		keys: ['ledFilmType']
	}, function(msg) {
		console.log('ledType::'+msg.payload.settings.ledFilmType);
		var ledType = msg.payload.settings.ledFilmType;
		env.ledFilmType = msg.payload.settings.ledFilmType;
	});

	if (env.toolType == 'XE3D') {
		env.model = model.supportModels[env.moduleInchType + env.toolType];
	} else if (env.toolType === 'XS4F') { // window facing model
		env.model = model.supportModels['XS4F'];
	} else {
		env.model = model.supportModels[modelName];
	}

	if (!env.model) {
		env.model = model.supportModels["default"];
	}

	if( env.toolType == 'XS2D' || env.toolType == 'XE3D' ){
		env.model.name = env.moduleInchType + env.toolType;
	} else {
		env.model.name = modelName;
	}

	env.fan = model.fan;
	env.emergencyTemp = model.emergencyTemp;
	env.curTempSensor = env.model.tempSensor;
	env.support49XE = env.toolType == 'XE3D';
	env.support75XE = env.model.name.substring(0, 4) === '75XE';
	env.supportXE = env.model.name.substring(2, 4) === 'XE';
	env.supportLedSignage = env.model.supportLedSignage || process.argv.indexOf(supportLedSignageStr) >= 0;
	env.supportLedFilm = env.supportLedSignage && (env.toolType == 'LCLG002');
	env.supportLedSignage20 = (env.supportLedSignage && process.argv.indexOf(supportLedSignage20Str) >= 0) || env.supportLedFilm;
	env.supportLocale = true;

	exports.env = env;

	service.call('luna://com.webos.service.tv.systemproperty/getProperties', {
		keys: ['displayResolution', 'powerOnlyMode', 'commer365CareServiceMode']
	}, function(msg) {
		var resolution = msg.payload.displayResolution;
		var resolArray = resolution.split('x');
		env.screen.width = Number(resolArray[0]);
		env.screen.height = Number(resolArray[1]);
		env.powerOnlyMode = msg.payload.powerOnlyMode == 'true';
		env.commer365CareServiceMode = msg.payload.commer365CareServiceMode;
	});

	getConfigs();
	getSDMInfo();
	setSupportPsuStatus();
	setSupportMinMaxBacklight();
	setSupportHumid();
}

function startServer() {
	if (env.secured) {
		var ciphers = [
			'ECDHE-RSA-AES128-GCM-SHA256',
			'ECDHE-RSA-AES256-GCM-SHA384',
			'ECDHE-RSA-AES128-SHA256',
			'ECDHE-RSA-AES256-SHA384',
			'!ECDHE-RSA-AES256-SHA',
			'AES256-GCM-SHA384',
			'AES256-SHA256',
			'!AES256-SHA',
			'!ECDHE-RSA-AES128-SHA',
			'AES128-GCM-SHA256',
			'AES128-SHA256',
			'!AES128-SHA'
		].join(':');

		var options = {
			key: fs.readFileSync('https/key.pem'),
			cert: fs.readFileSync('https/cert.pem'),
			secureOptions: constants.SSL_OP_NO_TLSv1 | constants.SSL_OP_NO_TLSv1_1,
			ciphers: ciphers
		};
		server = https.createServer(options, app).listen(env.port);
	} else {
		server = app.listen(env.port, function() {
			console.log("server started");
		});
	}

	service.register("serverURL", function(message) {
		service.call("luna://com.palm.connectionmanager/getstatus", {},
		function(m) {
			var url = m.payload.wired.ipAddress;
			url = (env.secured ? "https://" : "http://") + url;
			url += ":" + env.port;

			message.respond({
				url: url
			});
		});
	});
}

service.call('luna://com.webos.settingsservice/getSystemSettings', {
	keys: ['localeInfo']
}, function(msg) {
	var settings = msg.payload.settings;
	env.locale = settings.localeInfo.locales.UI;
});

process.on('uncaughtException', function(e) {
	var str = e.stack
	fs.writeFileSync('/var/outdoorweb/log' + Date.now(), str);
	console.log(e.stack);
});

function getConfigs() {
	var api = 'luna://com.webos.service.config/getConfigs';
	var params = {
		configNames: [
			'commercial.network.supportIPv6',
			'commercial.sensor.list',
			'commercial.video.bluControl',
			'commercial.hw.sensor.extraInfo'
		]
	}
	var callback = function (res) {
		setSupportIPv6(res);
		setSupportBrighness(res);
		setSupportAcCurrent(res);
		setCheckScreenInfo(res);
	}
	service.call(api, params, callback);
}

function setCheckScreenInfo(data) {
	env.checkscreen = {};
	if (data.payload.returnValue && data.payload.configs) {
		var extraInfo = data.payload.configs['commercial.hw.sensor.extraInfo'];
		if (extraInfo) {
			env.checkscreen = extraInfo.checkscreen;
		}
	}
}

function setSupportIPv6(data) {
	env.supportIPv6 = true;
	if (data.payload.returnValue && data.payload.configs) {
		var supportIPv6 = data.payload.configs['commercial.network.supportIPv6'];
		if (supportIPv6) {
			env.supportIPv6 = supportIPv6;
		}
	}
}

function setSupportBrighness(data) {
	env.supportBrighness = false;
	if (data.payload.returnValue && data.payload.configs) {
		var blueControl = data.payload.configs['commercial.video.bluControl'];
		if (blueControl) {
			env.supportBrighness = blueControl.support;
		}
	}
}

function setSupportAcCurrent(data) {
	env.supportAcCurrent = false;
	if (data.payload.returnValue && data.payload.configs) {
		var sensorList = data.payload.configs['commercial.sensor.list'];
		if (sensorList) {
			if (sensorList.indexOf('powerCurrent') > -1) {
				env.supportAcCurrent = true;
			}
		}
	}
}

function getSDMInfo() {
	var api = 'luna://com.webos.service.tv.outdoor/getSDMInfo';
	var params = {
		subscribe: false
	}
	var callback = function (res) {
		setTemperatureSensors(res);
		setFanMicomNum(res);
		setPsuNum(res);
	}
	service.call(api, params, callback);
}

function setTemperatureSensors(data) {
	var shouldChangeID = env.supportXE || env.model.name === '86BH5C';
	var temperatureSensorsInfo = {
		'B': { name: 'Bottom', id: shouldChangeID ? 'lcm' : 'bottom' },
		'T': { name: 'Top', id: shouldChangeID ? 'panel' : 'top' },
		'M': { name: 'Main', id: 'main' },
		'E': { name: 'External', id: 'external' },
		'P': { name: 'PSU', id: 'psu' }
	}

	var temperatureSensors = [];

	if (data.payload.TemperatureName) {
		data.payload.TemperatureName.split(', ').map(function (sensor) {
			temperatureSensors.push(temperatureSensorsInfo[sensor]);
		});
	} else {
		temperatureSensors.push(temperatureSensorsInfo['M']);
	}

	env.curTempSensor = temperatureSensors;
}

function setFanMicomNum(data) {
	env.fanMicomNum = 1;
	if (data.payload.FanMicom) {
		env.fanMicomNum = data.payload.FanMicom;
	}
}

function setPsuNum(data) {
	env.psuNum = 2;
	if (data.payload.PSUStatus) {
		env.psuNum = data.payload.PSUStatus.length;
	}
}

function setSupportPsuStatus() {
	env.supportPsuStatus = env.support75XE;
}

function setSupportMinMaxBacklight() {
	env.supportMinMaxBacklight = (env.toolType == 'XE3D') || (env.toolType == 'XS2D') || (env.toolType == 'XF3D') ||
		(env.toolType === 'XE3E') || (env.toolType === 'XF3E') || (env.toolType === 'XS4F') || env.supportXE || (env.model.name === '86XF3D');
}

function setSupportHumid() {
	env.supportHumid = env.support49XE || (env.toolType === 'XE3E');
}

function parsefanInfo(data, inch){
	var result = "none";
	if(data.support){
		var scenario = "";
		if(data.inchVariation && data.inchType){
			data.inchType.forEach(function(inchType) {
                if (inchType.inch === inch) {
                    scenario = inchType.scenario;
                }
            });
		} else {
			scenario = data.scenario;
		}
		var re = /^[0-9]*/;
		if (scenario) {
			if (re.test(scenario)) {
				scenario = scenario.substring(scenario.indexOf('_') + 1, scenario.length);
			}

			scenario = scenario.toLowerCase();
			result = scenario;
		}
	}
	return result;
}