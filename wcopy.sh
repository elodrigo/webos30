#!/bin/bash
mkdir /media/developer
cp -r /usr/palm/services/com.webos.service.outdoorwebcontrol /media/developer/
cp ~/sources/run-web-service /media/developer/com.webos.service.outdoorwebcontrol/
# cp ~/sources/app.js /media/developer/com.webos.service.outdoorwebcontrol/
cp -r ~/sources/public /media/developer/com.webos.service.outdoorwebcontrol/
cp -r ~/sources/resources /media/developer/com.webos.service.outdoorwebcontrol/
mkdir /media/developer/com.webos.service.outdoorwebcontrol/public/js/design
mkdir /media/developer/com.webos.service.outdoorwebcontrol/public/js/design/ledSignage
mkdir /media/developer/com.webos.service.outdoorwebcontrol/public/js/design/modal