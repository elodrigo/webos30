var express = require('express');
var router = express.Router();
var lunaAPI = require('../luna');
var ledAPI = require('../ledAPI');
var fs = require('fs');
var passwd = require('../passwd');
var web = require('../web');
var group = require('../group');
var crypto = require('crypto');
var sequence = require('../sequence');

var salt = 'restfulOutdoorwebcontrol';

var tokenList = [];
var tokenChecker = setInterval(function() {
	var cur = new Date().valueOf();
	var delEnd = -1;

	for (var i = 0; i != tokenList.length; ++i) {
		if (cur <= tokenList[i].expire) {
			break;
		}
		delEnd = i;
	}

	if (delEnd >= 0) {
		tokenList.splice(0, delEnd + 1);
	}
}, 60 * 1000);

function createToken(req) {
	var date = new Date();
	var token = salt + date.valueOf() + req.connection.remoteAddress;
	var genHash = crypto.createHash('sha256').update(token).digest('hex');
	return genHash;
}

function isValidToken(token) {
	if (token === group.secureKey()) {
		return true;
	}

	for (var i = 0; i != tokenList.length; ++i) {
		if (tokenList[i].token === token) {
			return true;
		}
	}
	return false;
}

function errorReturn(message, res) {
	res.json({
		returnValue: false,
		message: message
	});
}

function isFromLocal(client) {
	return true;
}

function isDbcFunction(url) {
	return url.indexOf('getTargetPmValue') > 0 || url.indexOf('setSharedPmValue') > 0;
}

function checkToken(req, res, next) {
	if (web.env.supportLedSignage) {
		var ip = req.headers['x-forwarded-for'] || 
			req.connection.remoteAddress || 
			req.socket.remoteAddress ||
			req.connection.socket.remoteAddress;
		ip = ip.substring(ip.lastIndexOf(':') + 1);
		ledAPI.readGroupFile(isDbcFunction(req.originalUrl) ? 'dbc' : 'control', function(ret) {
			if (ret.master === ip) {
				next();
				return;
			}

			if (!req.query.token || !isValidToken(req.query.token)) {
				errorReturn("invalid or timed out token.", res);
				return;
			}
			next();
		});
	} else {
		if (!req.query.token || !isValidToken(req.query.token)) {
			errorReturn("invalid or timed out token.", res);
			return;
		}
		next();
	}
}

router.get('/login', function(req, res) {
	var password = '' + req.query.passwd;

	if (!passwd.checkPasswd(password)) {
		errorReturn('Password is not correct', res);
		return;
	}
	var ret = {
		token: createToken(req),
		expire: new Date().valueOf() + (60 * 60 * 1000), // for 1 hour.
		returnValue: true
	};
	tokenList.push(ret);
	res.json(ret);
});

router.all('*', function(req, res, next) {
	checkToken(req, res, next);
});

router.get('/incidents', function(req, res) {
	lunaAPI.getDowntimeIncident(function(m) {
		res.json(m.payload);
	});
});

router.get('/status', function(req, res) {
	var json = {};
	sequence.join(function(next) {
		lunaAPI.getCurrentTime(function(ret) {
			json.time = ret;
			next();
		});
	}, function(next) {
		lunaAPI.getAllTemperature(function(m) {
			json.temperature = m;
			next();
		});
	}, function(next) {
		lunaAPI.getNetworkStatus(function(m) {
			m.payload.wired.returnValue = m.payload.returnValue;
			json.wired = m.payload.wired;
			next();
		});
	}, function(next) {
		lunaAPI.getDoorStatus(function(m) {
			json.door = m.payload;
			next();
		});
	}, function(next) {
		lunaAPI.getFanStatus(function(m) {
			json.fan = m.payload;
			next();
		});
	}, function(next) {
		lunaAPI.getCheckScreenOn(function(m) {
			var ret = {
				checkScreen: m.payload.settings.checkScreen
			};

			if (ret.checkScreen == 'off') {
				json.checkScreen = ret;
				next();
				return;
			}

			lunaAPI.getCheckScreenInfo(function(info) {
				ret.info = info.payload;
				ret.returnValue = info.payload.returnValue;
				delete ret.info.returnValue;
				json.checkScreen = ret;
				next();
			});
		});
	}, function(next) {
		lunaAPI.isNoSignal(function(signal) {
			var ret = {};

			ret.signal = !signal.payload.noSignal;
			ret.returnValue = signal.payload.returnValue;

			lunaAPI.getVideoSize(function(size) {
				ret.videoSize = size.payload.videoSize;
				ret.returnValue = ret.returnValue && size.payload.returnValue;

				lunaAPI.getVideoStillStatus(function(stall) {
					ret.stillStatus = stall.payload;
					ret.returnValue = ret.returnValue && stall.payload.returnValue;
					delete ret.stillStatus.returnValue;
					
					json.signal = ret;
					next();
				});
			});
		});
	}, function(next) {
		lunaAPI.getBasicInfo(function(info) {
			json.info = info.payload;
			next();
		});
	}, function() {
		json.returnValue = true;
		res.json(json);
	});
});

router.get('/currentTime', function(req, res) {
	lunaAPI.getCurrentTime(function(ret) {
		res.json(ret);
	});
});

var lastShotDate = 0;
router.get('/screenShot', function(req, res) {
	var curDate = new Date().valueOf();
	var height = 480;

	if (curDate - lastShotDate < 500) {
		var fileName = '/tmp/outdoorweb/rest' + lastShotDate + '.jpg';
		setTimeout(function() {
			res.sendFile(fileName);
		}, 300);
		return;
	}

	lastShotDate = curDate;

	if (req.query.height) {
		var num = Number(req.query.height);
		if (num) {
			height = num;
		}
	}

	var fileName = '/tmp/outdoorweb/rest' + curDate + '.jpg';
	lunaAPI.screenShot(fileName, height, function(m) {
		res.sendFile(fileName);
	}, 'jpg');

	setTimeout(function() {
		fs.unlink(fileName, function(err) {
			if (err) {
				console.log(err);
			}
		});
	}, 5 * 1000);
});

router.get('/temperature', function(req, res) {
	var id = req.query.id;
	if (id) {
		lunaAPI.getTemperature(id, function(m) {
			res.json(m.payload);
		});
	} else {
		lunaAPI.getAllTemperature(function(m) {
			res.json(m);
		});
	}
});

router.get('/network', function(req, res) {
	lunaAPI.getNetworkStatus(function(m) {
		m.payload.wired.returnValue = m.payload.returnValue;
		res.json(m.payload.wired);
	});
});

router.get('/door', function(req, res) {
	lunaAPI.getDoorStatus(function(m) {
		var ret = m.payload;
		ret.opened = ret.status;
		delete ret.status;
		res.json(ret);
	});
});

router.get('/fan', function(req, res) {
	lunaAPI.getFanStatus(function(m) {
		res.json(m.payload);
	});
});

router.get('/checkScreen', function(req, res) {
	lunaAPI.getCheckScreenOn(function(m) {
		if (!m.payload.returnValue) {
			errorReturn('fail to call luna api', res);
			return;
		}

		var ret = {
			checkScreen: m.payload.settings.checkScreen
		};

		if (ret.checkScreen == 'off') {
			res.json(ret);
			return;
		}

		lunaAPI.getCheckScreenInfo(function(info) {
			ret.info = info.payload;
			ret.returnValue = info.payload.returnValue;
			delete ret.info.returnValue;
			res.json(ret);
		});
	});
});

router.get('/signal', function(req, res) {
	lunaAPI.isNoSignal(function(signal) {
		var ret = {};

		ret.signal = !signal.payload.noSignal;
		ret.returnValue = signal.payload.returnValue;

		lunaAPI.getVideoSize(function(size) {
			ret.videoSize = size.payload.videoSize;
			ret.returnValue = ret.returnValue && size.payload.returnValue;

			lunaAPI.getVideoStillStatus(function(stall) {
				ret.stillStatus = stall.payload;
				ret.returnValue = ret.returnValue && stall.payload.returnValue;
				delete ret.stillStatus.returnValue;

				res.json(ret);
			});
		});
	});
});

router.get('/version', function(req, res) {
	lunaAPI.getBasicInfo(function(info) {
		res.json(info.payload);
	});
});

router.get('/input', function(req, res) {
	lunaAPI.getInputList(function(list) {
		var devices = list.payload.devices;
		var ret = {
			list: [],
			returnValue: list.payload.returnValue
		};
		devices.forEach(function(dev) {
			ret.list.push({
				id: dev.id,
				label: dev.label,
				deviceName: dev.deviceName
			});
		});

		lunaAPI.getCurrentInput(function(cur) {
			ret.currentInputID = cur.payload.deviceID;
			ret.returnValue = ret.returnValue && cur.payload.returnValue;
			res.json(ret);
		});
	});
});

router.put('/input', function(req, res) {
	if (!req.body || !req.body.id) {
		errorReturn('invalid input id', res);
		return;
	}
	lunaAPI.setCurrentInput(req.body.id, function(m) {
		res.json(m.payload);
	});
});

router.put('/mute', function(req, res) {
	if (!req.body || req.body.mute === undefined) {
		errorReturn('invalid mute parameter', res);
		return;
	}

	if (req.body.mute === true) {
		lunaAPI.turnOffScreen(function(m) {
			res.json(m.payload);
		});
	} else {
		lunaAPI.turnOnScreen(function(m) {
			res.json(m.payload);
		});
	}
});


router.get('/volume', function(req, res) {
	lunaAPI.getVolume(function(m) {
		res.json(m.payload);
	});
});

router.put('/volume', function(req, res) {
	if (!req.body) {
		errorReturn('invalid volume value', res);
		return;
	}

	var ret = {
		returnValue: true
	};

	function setVolume() {
		if (!isNaN(req.body.volume)) {
			lunaAPI.setVolume(Number(req.body.volume), function(m) {
				ret.returnValue = ret.returnValue && m.payload.returnValue;
				res.json(ret);
			});
		} else {
			res.json(ret);
		}
	}

	if (typeof(req.body.muted) === 'boolean') {
		lunaAPI.setMuted(req.body.muted, function(mute) {
			ret.returnValue = ret.returnValue && mute.payload.returnValue;
			setVolume();
		});
		return;
	}

	setVolume();
});

router.get('/ledConfig', function(req, res) {
	ledAPI.getUnitConfig(function(config) {
		config.regular = (config.regualr == 0);
		res.json(config);
	});
});

var numOn = 0;

router.get('/ledStatus', function(req, res) {
	ledAPI.getUnitConfig(function(config) {
		var retVal = {};
		numOn = config.onUnits.numOn;
		retVal.numOn = numOn;
		for (var i = 1; i <= numOn; ++i) {
			(function() {
				var id = i;
				var slave = i * 2;
				ledAPI.ledGetConfig('getOverallStatus', {
					slave: slave
				}, function(ret) {
					retVal[id] = ret.payload.result.isOverallOk;
					if (id == numOn) {
						res.json(retVal);
					}
				});
			})();
		}
	});
});

router.get('/ledDetailStatus', function(req, res) {
	var id = req.query.id;
	var retVal = {};
	ledAPI.ledGetConfig('getPowerStatus', {
		slave: id * 2
	}, function(ret1) {
		retVal.isPowerOk = ret1.payload.result.isPowerOk;

		if (!retVal.isPowerOk) {
			res.json(retVal);
			return;
		}

		ledAPI.ledGetConfig('getOverallStatus', {
			slave: id * 2
		}, function(ret2) {
			retVal.isOk = ret2.payload.result.isOverallOk;

			ledAPI.ledGetConfig('getSignalStatus', {
				slave: id * 2
			}, function(ret3) {
				retVal.isSignalOk = ret3.payload.result.isSignalOk;

				ledAPI.ledGetConfig('getTemperature', {
					slave: id * 2
				}, function(ret4) {
					retVal.unitTemperature = ret4.payload.result.temperature[0];
					retVal.isUnitTempOk = ret4.payload.result.isOk[0];
					retVal.moduleTemperature = ret4.payload.result.temperature.slice(1);
					retVal.isModuleTempOk = ret4.payload.result.isOk.slice(1);

					ledAPI.ledGetConfig('getCurrent', {
						slave: id * 2
					}, function(ret5) {
						retVal.moduleCurrent = ret5.payload.result.current;
						retVal.isModuleCurrentOk = ret5.payload.result.isOk;

						ledAPI.ledGetConfig('getLOD', {
							slave: id * 2
						}, function(ret6) {
							retVal.moduleLOD = ret6.payload.result.isOk;
							res.json(retVal);
						});
					});
				});

			});
		});
	});
});

router.get('/getTargetPmValue', function(req, res) {
	ledAPI.getTargetPmValue(function(ret) {
		res.json(ret.payload);
	});
});

router.put('/picture', function(req, res) {
	if (!req.body) {
		errorReturn('invalid picture settings', res);
		return;
	}

	if ('pictureSettingModified' in req.body.settings) {
		lunaAPI.setSystemSettings('picture', {
			pictureSettingModified: req.body.settings.pictureSettingModified
		}, function(ret) {
		});
		req.body.settings.pictureSettingModified = undefined;
	}

	lunaAPI.setSystemSettingsByDimension('picture', req.body.dimension, req.body.settings, function(ret) {
		res.json(ret.payload);
	})
});

router.put('/colorTemp', function(req, res) {
	if (!req.body) {
		errorReturn('invalid colorTemp', res);
		return;
	}
	
	lunaAPI.setSystemSettings('commercial', {
		ledColorTempDegree: req.body.temp
	}, function(ret) {
		res.json(ret.payload);
	})
});

router.put('/ledPower', function(req, res) {
	if (!req.body) {
		errorReturn('invalid ledPower', res);
		return;
	}
	ledAPI.setCtrlDCDC(0, req.body.ledPower, function(ret) {
		res.json(ret.payload);
	});
});

router.put('/setSharedPmValue', function(req, res) {
	if (!req.body) {
		errorReturn('invalid shared pm value', res);
		return;
	}
	ledAPI.setSharedPmValue(req.body.sharedPmVal, function(ret) {
		res.json(ret.payload);
	});
});

module.exports = router;
