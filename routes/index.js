var fs = require('fs');
var express = require('express');
var router = express.Router();
var password = require('../passwd');
var child_process = require('child_process');
var web = require('../web');
var multer = require('multer');
var path = require('path');
var lunaAPI = require('../luna');
var contentDisposition = require('content-disposition');
var osCommandFilter = /`|\$|\'|"|;|\||&|\\|<|>|\n|%27|%22|%3B|%7C|%26|%5c|%60|%24|%0a|%0d/;

var FIRST_LOGIN_CHECK_FILE = "/var/outdoorweb/first_login_check.json";
var MAX_LOGIN_TIMES = 5;
var RESTRICT_LOGIN_TIME = 5 * 60 * 1000;

var loginTryTimes = 0;
var loginRestrictedTime = 0;
var loginRestrictedInterval = undefined;

function reduceLoginRestrictedSeconds() {
	if (loginRestrictedTime > 0) {
		loginRestrictedTime = loginRestrictedTime - 1000;
	} else {
		loginTryTimes = 0;
		loginRestrictedTime = 0;
		clearInterval(loginRestrictedInterval);
		loginRestrictedInterval = undefined;
	}
}

function processLogin(passwd, req, res) {
	var status = 'fail'
	if (loginRestrictedTime <= 0) {
		if (password.checkPasswd(passwd)) {
			// process session
			req.session.login = true;
			// update login try info
			status = 'success';
			loginTryTimes = 0;
			loginRestrictedTime = 0;
			if (isFirstLogin()) {
				status = 'first_login';
			}
		} else {
			// update login try info
			status = 'fail';
			++loginTryTimes;
			if (loginTryTimes >= MAX_LOGIN_TIMES) {
				loginRestrictedTime = RESTRICT_LOGIN_TIME;
				loginRestrictedInterval = setInterval(reduceLoginRestrictedSeconds, 1000);
			}
		}
	}

	res.json({
		status: status,
		loginTryTimes : loginTryTimes,
		loginRestrictedTime: loginRestrictedTime
	});
}

router.get('/loginStatus', function (req, res, next) {
	res.json({
		status: loginTryTimes > 0 ? 'fail' : 'none',
		loginTryTimes: loginTryTimes,
		loginRestrictedTime: loginRestrictedTime
	});
})

router.get('/login.html', function(req, res, next) {
	res.render('loginPage', {
		env: web.env,
		layout: false
	});
});

router.get('/login', function(req, res, next) {
	var passwd = req.query.password;
	processLogin(passwd, req, res);
});

router.post('/login', function(req, res, next) {
	var passwd = req.body.password;
	processLogin(passwd, req, res);
});

router.get('/logout', function(req, res, next) {
	req.session.destroy();
	res.redirect('login.html');
});

router.all('*', function(req, res, next) {
	checkLogin(req, res, next, false);
});

/* GET home page. */
router.get('/', function(req, res, next) {
	if (web.env.supportLedSignage20) {
		res.redirect('/ledSignage/ledDashboard');
		return;
	}
	res.render('dashBoard', req.parameters);
});

router.get('/dashBoard', function(req, res, next) {
	if (web.env.supportLedSignage20) {
		res.redirect('/ledSignage/ledDashboard');
		return;
	}
	res.render('dashBoard', req.parameters);
});

router.get('/device', function(req, res, next) {
	res.render('device', req.parameters);
});

router.get('/system', function(req, res, next) {
	res.render('system', req.parameters);
});

router.get('/picture', function(req, res, next) {
	res.render('picture', req.parameters);
});

router.get('/sound', function(req, res, next) {
	res.render('sound', req.parameters);
});

router.get('/input', function(req, res, next) {
	res.render('input', req.parameters);
});

router.get('/network', function(req, res, next) {
	res.render('network', req.parameters);
});

router.get('/time', function(req, res, next) {
	res.render('time', req.parameters);
});

router.get('/door', function(req, res, next) {
	res.render('door', req.parameters);
});

router.get('/power', function(req, res, next) {
	res.render('power', req.parameters);
});

router.get('/bright', function(req, res, next) {
	res.render('bright', req.parameters);
});

router.get('/checkScreen', function(req, res, next) {
	res.render('checkScreen', req.parameters);
});

router.get('/fan', function(req, res, next) {
	res.render('fan', req.parameters);
});

router.get('/emergency', function(req, res, next) {
	res.render('emergency', req.parameters);
});

router.get('/remocon', function(req, res, next) {
	res.render('remocon', req.parameters);
});

router.get('/charts', function(req, res, next) {
	var page = req.query.page;
	if (page == undefined || osCommandFilter.test(page) || /\D/.test(page)) {
		page = 1;
	}
	req.parameters.page = page;
	res.render('charts', req.parameters);
});

router.get('/logFile', function(req, res, next) {
	getLog(req, res, false);
	res.render('log', req.parameters);
});

router.get('/media', function(req, res, next) {
	getMedia(req, res, function() {
		res.render('media', req.parameters);
	});
});

router.get('/update', function(req, res, next) {
	getUpdate(req, res, ['epk', 'txt']);
	res.render('update', req.parameters);
});

router.get('/changePasswd', function(req, res, next) {
	res.render('changePasswd', req.parameters);
});

router.post('/changePasswd', function(req, res, next) {
	var currentPasswd = req.body.current ? req.body.current : '';
	var newPasswd = req.body.passwd ? req.body.passwd : '';
	var checkNewPasswd = req.body.check ? req.body.check : '';

	if (!password.checkPasswd(currentPasswd)) {
		res.send('fail_incorrect');
	} else if (currentPasswd === newPasswd) {
		res.send('fail_same');
	} else if (newPasswd.length < 8) {
		res.send('fail_short');
	} else if (!password.validateNewPasswd(newPasswd)) {
		res.send('fail_inavlid_format');
	} else if (newPasswd !== checkNewPasswd) {
		res.send('fail_not_match');
	} else {
		if (isFirstLogin()) {
			setFirstLoginDone();
		}
		password.changePasswd(newPasswd);
		res.send('success');
	}
});

router.get('/language', function(req, res, next) {
	res.render('language', req.parameters);
});

router.get('/ledConfig', function(req, res, next) {
	res.render('ledConfig', req.parameters);
});

router.get('/ledControl', function(req, res, next) {
	res.render('ledControl', req.parameters);
});

router.get('/ledUpdate', function(req, res, next) {
	getUpdate(req, res, ['hexout']);
	res.render('ledUpdate', req.parameters);
});

router.get('/ledFilmConfig', function(req, res, next) {
	res.render('ledFilmConfig', req.parameters);
});

router.get('/ledFilmControl', function(req, res, next) {
	res.render('ledFilmControl', req.parameters);
});

router.get('/ledFilmUpdate', function(req, res, next) {
	getUpdate(req, res, ['hexout']);
	res.render('ledFilmUpdate', req.parameters);
});

function downloadFile(root, req, res, next) {
	var fileName = req.query.name;
	var fullPath = path.normalize(root + fileName);
	if (fullPath.indexOf(root) < 0) {
		res.status(404);
		res.send('Unauthorized access');
		res.end();
		return;
	}

	var content = contentDisposition(fileName);

	res.sendFile(fullPath, {
		headers: {
			'Content-Disposition': content
		},
		dotfiles: 'allow'
	});

	return;
}

router.get('/download', function(req, res, next) {
	var root = '/media/signage/';
	downloadFile(root, req, res, next);
});

router.get('/logDown', function(req, res, next) {
	var root = '/var/sdmlog/download/';
	downloadLogFile(root, req, res, next);
});

router.get('/downloadIncidents', function(req, res, next) {
	downloadFile('/tmp/outdoorweb/', req, res, next);
});

router.get('/ledSignage/ledDashboard', function(req, res, next) {
	getLog(req, res, false);
	getUpdate(req, res, ['epk', 'hexout', 'img']);
	req.parameters.layout = '../ledSignage/layout.hbs';
	var diskSpace = getDiskSpace();
	req.parameters.totalSpace = diskSpace[0];
	req.parameters.freeSpace = diskSpace[1];
	res.render('ledSignage/ledDashboard', req.parameters);
});

router.get('/ledSignage/ledControl', function(req, res, next) {
	req.parameters.layout = '../ledSignage/layout.hbs';
	res.render('ledSignage/ledControl', req.parameters);
});

router.get('/ledSignage/ledConfig', function(req, res, next) {
	req.parameters.layout = '../ledSignage/layout.hbs';
	res.render('ledSignage/ledConfig', req.parameters);
});

router.get('/ledSignage/ledSettings', function(req, res, next) {
	req.parameters.layout = '../ledSignage/layout.hbs';
	res.render('ledSignage/ledSettings', req.parameters);
});

router.get('/ledSignage/ledMedia', function(req, res, next) {
	getMedia(req, res, function() {
		var diskSpace = getDiskSpace();
		req.parameters.totalSpace = diskSpace[0];
		req.parameters.freeSpace = diskSpace[1];
		req.parameters.layout = '../ledSignage/layout.hbs';
		res.render('ledSignage/ledMedia', req.parameters);
	});
});

router.get('/ledSignage/ledSignage365Care', function(req, res, next) {
	req.parameters.layout = '../ledSignage/layout.hbs';
	res.render('ledSignage/ledSignage365Care', req.parameters);
});

router.get('/ledSignage/ledSiserver', function(req, res, next) {
	req.parameters.layout = '../ledSignage/layout.hbs';
	res.render('ledSignage/ledSiserver', req.parameters);
});

var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		var to = '/media/update/';

		if (file.fieldname == 'media') {
			var root = '/media/signage/';
			to = root;
			if (req.body.dir) {
				to += req.body.dir;
				to = path.normalize(to);
				if (to.search(new RegExp('^' + root)) < 0) {
					to = root;
				}
			}
		} else if (file.fieldname == 'ledlayout') {
			var root = '/var/outdoorweb/';
			to = root;
		}
		cb(null, to);
	},
	filename: function (req, file, cb) {
		var name = file.originalname;

		if (req.body.newname) {
			name = req.body.newname;
		}

		cb(null, name);
	}
})

var upload = multer({
	storage: storage
});

router.post('/upload', upload.any(), function (req, res, next) {
	res.end();
});

function downloadLogFile(root, req, res, next) {
	var fileName = req.query.name;
	var filePath = path.normalize(root + fileName);
	var outputPath = filePath.substring(0, filePath.lastIndexOf(".")) + ".csv";
	fs.readFile(filePath, "utf8", function (err, data) {
		if (err) {
			sendError(res, err);
			return;
		} else {
			if(!web.env.supportLedSignage20){
				data = "Category, Sensor, Event, State, Occurrence Time\n" + data;
			}
			data = data.replace(/^ */g, "").replace(/\n[ ]+/g, "\n").replace(/\t/g, " ").replace(/ {2,}/g, " ");
			data = data.replace(/Please, Set a time/g, "Please Set a time");
			fs.writeFile(outputPath, data, function (err) {
				if (err) {
					sendError(res, err);
					return;
				} else {
					res.download(outputPath, function (err) {
						fs.unlinkSync(outputPath);
					});
					return;
				}
			});
		}
	});
}

function sendError(res, err) {
	console.log(err);
	res.send(err);
	res.end();
}

function getLog(req, res, isMobile) {
	var dir = "/var/sdmlog/";
	var downDir = "/var/sdmlog/download/";
	var availableFilters = [
		' ' /* All*/ , 'TEMP', 'SIGNAL', 'TIME',
		'CHECKSCREEN', 'DOOR', 'MEMS', 'PANEL',
		'FAN', 'PSU', 'FILTER', 'LEDUNIT', 'SCREEN'
	]
	var baseFileName = path.basename('' + req.query.file);

	if (req.query.delete) {
		var delPath = path.normalize(dir + baseFileName);
		if (fs.existsSync(delPath)) {
			fs.unlinkSync(delPath);
		}
		req.query.file = undefined;
		baseFileName = undefined;
	}

	var filter = req.query.filter;
	if (!fs.existsSync(dir)) {
		fs.mkdirSync(dir);
	}

	if (!fs.existsSync(downDir)) {
		fs.mkdirSync(downDir);
	}

	var files = fs.readdirSync(dir);
	var re = /^log\d{5}\.txt$/;
	files = files.filter(function(name) {
		return re.test(name);
	});
	files.sort();

	if (osCommandFilter.test(req.query.file) || !re.test(req.query.file)) {	
		req.query.file = undefined;
	}

	var fileName = req.query.file ? req.query.file : files[files.length - 1];

	var cmd = 'cat ' + dir + fileName
	if (availableFilters.indexOf(filter) > -1) {
		if (filter === 'PSU' || filter === 'SCREEN')
			cmd = 'grep -w "' + filter + '" ' + dir + fileName;
		else
			cmd = 'grep "' + filter + '" ' + dir + fileName;
	} else {
		filter = ' '; // ALL
	}

	var log = "";
	var columnTitle = "Category, Sensor, Event, State, Occurrence Time\n";

	try {
		log = child_process.execSync(cmd, {
			timeout: 2000
		});
		log = String(log);
		log = log.replace(/Please, Set a time/g, "Please Set a time");

		fs.writeFileSync(downDir + fileName, log);
		if(!web.env.supportLedSignage20){
			log = columnTitle + log;
		}
	} catch (e) {
		log = "";
		if (!web.env.supportLedSignage20) {
			log = columnTitle;
		}
	}

	req.parameters.log = { log: log };
	req.parameters.filter = filter;
	req.parameters.files = files;
	req.parameters.curFile = fileName;
}

function getDiskSpace() {
	var ret = [0, 0];
	try {
		var str = '' + child_process.execSync("df -B 1 | grep media$ | awk '{print $2 \" \" $4}'");
		console.log(str);
		var spaceArr = str.split(' ');
		console.log(spaceArr);
		ret[0] = Number(spaceArr[0]);
		ret[1] = Number(spaceArr[1]);
		console.log(ret);
	} catch (e) {
		console.log(e);
	}
	return ret;
}

function getFreeSpace() {
	return getDiskSpace()[1];
}

function getFileSize(dir, files) {
	var size = [];
	files.forEach(function(file) {
		var stats = fs.statSync(dir + file);
		size.push(stats.size);
	});

	return size;
}

function getMedia(req, res, callback) {
	var root = "/mnt/lg/appstore/signage/";
	var dir = root;
	var curDir = '/';
	var filter = '';
	var dirs = [];
	var files = [];
	var newFiles = [];
	var allFiles = [];

	if (!fs.existsSync(root)) {
		fs.mkdirSync(root);
	}

	if (req.query.dir) {
		dir += req.query.dir + '/';
		dir = path.normalize(dir);
		curDir = dir.substring(root.length - 1);
		if (curDir.length > 1) {
			dirs.push('..');
		}

		if (dir.search(new RegExp('^' + root)) < 0) {
			dir = root;
			curDir = '/';
		}
	}

	req.parameters.filter = req.query.filter;

	var offset = 0;
	var cur = new Date();

	function getAllContents(ret) {
		if (!ret.payload.returnValue) {
			files.push('Loading Media Files... Please refresh this page after a while');
			req.parameters.curDir = curDir;
			req.parameters.files = files;
			req.parameters.dirs = dirs;
			req.parameters.newFiles = newFiles;
			req.parameters.maxSize = getFreeSpace();

			callback();
			return;
		}
		var contents = ret.payload.contents;
		var numContents = ret.payload.totalCount;
		offset += contents.length;

		contents.forEach(function(item) {
            allFiles.push(item.itemName);

			if (item.itemType == 'folder') {
				dirs.push(item.itemName);
				return;
			}
			if (req.query.filter && req.query.filter != item.itemType) {
				return;
			}

			files.push(item.itemName);

			var stat = fs.statSync(item.itemPath);
			var diffTime = cur.getTime() - stat.mtime.getTime();
			if (diffTime >=0 && diffTime <= 60 * 1000) { // one min.
				newFiles.push(item.itemName);
			}
		});

		if (offset >= numContents) {
			req.parameters.curDir = curDir;
			req.parameters.files = files;
			req.parameters.dirs = dirs;
			req.parameters.newFiles = newFiles;
			req.parameters.maxSize = getFreeSpace();
            req.parameters.allFiles = allFiles;
			callback();
			return;
		}

		lunaAPI.getMediaList(dir, offset, getAllContents);
	}

	lunaAPI.getMediaList(dir, 0, getAllContents);
}

function getUpdate(req, res, filter) {
	var dir = "/media/update/";
	if (!fs.existsSync(dir)) {
		fs.mkdirSync(dir);
	}

	var files = fs.readdirSync(dir).filter(function(name) {	
		var ext = name.substring(name.lastIndexOf('.') + 1).toLowerCase();
		return filter.indexOf(ext) >= 0;
	});

	if(!web.env.supportLedSignage20)
		req.parameters.files = files;
	else
		req.parameters.updateFiles = files;

	req.parameters.size = getFileSize(dir, files);
	req.parameters.maxSize = getFreeSpace();
}

function checkLogin(req, res, next, isMobile) {
	var noCheckUrl = ['/js/jquery.min.map', '/js/socket.io.js.map'];
	if (noCheckUrl.indexOf(req.url) >= 0) {
		next();
		return;
	}

	if (!req.session || ! req.session.login) {
		res.redirect('/login.html');
	} else {
		if (isFirstLogin() && req.url !== '/changePasswd') {
			res.redirect('/logout');
			return;
		}

		var languageManager = require("../LanguageManager");
		var languageTable = languageManager.languageTable;

		req.parameters = {
			env: web.env,
			// languageTable: web.languageTable,
			languageTable: languageTable,
			currentTime: (new Date()).toString()
		};
		if (isMobile) {
			req.parameters.layout = 'mobile/mobile_layout';
		}
		next();
	}
}

function isFirstLogin() {
	var retVal = true;
	if (web.env.powerOnlyMode) {
		return false;
	}

	try {
		fs.readFileSync(FIRST_LOGIN_CHECK_FILE);
		retVal = false;
	} catch (err) {
		console.log("first login is true");
	}
	return retVal;
}

function setFirstLoginDone() {
	if (isFirstLogin()) {
		var information = {
			isFirstLogin: false
		};
		fs.writeFileSync(FIRST_LOGIN_CHECK_FILE, JSON.stringify(information));
	}
}

module.exports = router;

module.exports.getMedia = getMedia;
module.exports.getLog = getLog;
module.exports.getUpdate = getUpdate;
module.exports.checkLogin = checkLogin;
module.exports.osCommandFilter = osCommandFilter;