var express = require('express');
var router = express.Router();
var api = require('../api');
var fs = require('fs');
var passwd = require('../passwd');
var web = require('../web');
var lunaAPI = require('../luna');
var ledAPI = require('../ledAPI');
var ledFilmAPI = require('../ledFilmAPI');
var sequence = require('../sequence');
var defaultRouter = require('./index');
var autoLedFilmHandler = require('../autoLedFilmHandler');

/* GET users listing. */
router.all('*', function(req, res, next) {
	if (!req.session || ! req.session.login) {
		res.redirect('/login.html');
	} else {
		req.parameters = {
			env: web.env,
			languageTable: web.languageTable,
			layout: 'factorymenu/factorymenu_layout'
		};
		next();
	}
});

router.get('/', function(req, res, next) {
	res.render('factorymenu/index', req.parameters);
	return;
});

router.get('/fan', function(req, res, next) {
	res.render('factorymenu/fan', req.parameters);
	return;
});

router.get('/checkScreen', function(req, res, next) {
	res.render('factorymenu/checkScreen', req.parameters);
	return;
});

router.get('/humid', function(req, res, next) {
	var page = req.query.page;
	if (page == undefined || defaultRouter.osCommandFilter.test(page) || /\D/.test(page)) {
		page = 1;
	}
	req.parameters.page = page;
	res.render('factorymenu/humid', req.parameters);
	return;
});

router.get('/emergency', function(req, res, next) {
	res.render('factorymenu/emergency', req.parameters);
	return;
});


router.get('/testsuite', function(req, res, next) {
	res.render('factorymenu/testsuite', req.parameters);
	return;
});

router.get('/ledSignage', function(req, res, next) {
	res.render('factorymenu/ledSignage', req.parameters);
	return;
});

router.get('/videoScaler', function(req, res, next) {
	res.render('factorymenu/videoScaler', req.parameters);
	return;
});

router.get('/ledOpenDetection', function(req, res, next) {
	res.render('factorymenu/ledOpenDetection', req.parameters);
	return;
});

router.get('/ledFilm', function(req, res, next) {
	res.render('factorymenu/ledFilm', req.parameters);
	return;
});

router.get('/ledBitmap', function(req, res, next) {
	res.render('factorymenu/ledBitmap', req.parameters);
	return;
});

router.get('/canvas', function(req, res, next) {
	res.render('factorymenu/canvas', req.parameters);
	return;
});

if (process.env['GROUP']) {
	router.get('/group', function(req, res, next) {
		res.render('factorymenu/group', req.parameters);
		return;
	});
}

router.get('/chartRaw', function(req, res) {
	var finished = 0;

	res.set({
		"Content-Disposition": 'attachment; filename="chartRaw.csv"'
	});

	var allHistory = [];

	sequence.sequence(function(next) {
		web.env.curTempSensor.forEach(function(sensor) {
			var param = {
				id: sensor.id
			};

			lunaAPI.getTempHistory(param, function(m) {
				var history = m.payload.history;
				history.push(sensor.name);
				history = history.reverse();

				allHistory.push(history);

				++finished;
				if (finished == web.env.curTempSensor.length) {
					next();
				}
			});
		});
	}, function(next) {
		if (web.env.fanControl === 'none') {
			next();
			return;
		}

		lunaAPI.getNumFans(function(msg) {
			var fans = msg.payload;
			var totalFans = 0;
			web.env.fan.forEach(function(fan) {
				totalFans += fans[fan.id];
			});

			var finished = 0;
			web.env.fan.forEach(function(fan) {
				for (var i = 0; i != fans[fan.id]; ++i) {
					(function() {
						var index = i;
						lunaAPI.getFanRpmHistory({
							id: fan.id,
							num: index
						}, function(msg) {
							var history = msg.payload.history;
							history.push(fan.name + index);
							history = history.reverse();

							allHistory.push(history);

							++finished;
							if (finished == totalFans) {
								next();
							}
						});
					})();
				}
			});
		});
	}, function(next) {
		var getLightHistory = web.env.supportLedSignage ? lunaAPI.getContrastHistory : lunaAPI.getBacklightHistory;
		getLightHistory({}, function(msg) {
			var history = msg.payload.history;
			history.push('Backlight');
			allHistory.push(history.reverse());
			next();
		});
	}, function(next) {
		lunaAPI.getEyeQSensorHistory({}, function(msg) {
			var history = msg.payload.history;
			history.push('EyeQ');
			allHistory.push(history.reverse());
			next();
		});
	}, function() {
		var kinds = allHistory.length;
		var size = allHistory[0].length;

		for (var i = 0; i != size; ++i) {
			for (var j = 0; j != kinds; ++j) {
				res.write(allHistory[j][i] + ',');
			}
			res.write('\n');
		}
		res.end();
	});
/*
	web.env.curTempSensor.forEach(function(sensor) {
		var param = {
			id: sensor.id
		};

		lunaAPI.getTempHistory(param, function(m) {
			var history = m.payload.history;
			history.push(sensor.name);
			history = history.reverse();

			allHistory.push(history);

			++finished;
			if (finished == web.env.curTempSensor.length) {
				var numHistory = allHistory[0].length;
				var numSensors = allHistory.length;
				for (var i = 0; i != numHistory; ++i) {
					for (var j = 0; j != numSensors; ++j) {
						res.write(allHistory[j][i] + ',');
					}
					res.write('\n');
				}
				res.end();
			}
		});
	});
*/
});

router.get('/deviceRawData', function(req, res) {
	res.set({
		"Content-Disposition": 'attachment; filename="deviceRawData.json"'
	});

	var json = {};
	sequence.join(function(next) {
		lunaAPI.getCurrentTime(function(ret) {
			json.time = ret;
			next();
		});
	}, function(next) {
		lunaAPI.getAllTemperature(function(m) {
			json.temperature = m;
			next();
		});
	}, function(next) {
		lunaAPI.getNetworkStatus(function(m) {
			m.payload.wired.returnValue = m.payload.returnValue;
			json.wired = m.payload.wired;
			next();
		});
	}, function(next) {
		lunaAPI.getDoorStatus(function(m) {
			json.door = m.payload;
			next();
		});
	}, function(next) {
		lunaAPI.getFanStatus(function(m) {
			json.fan = m.payload;
			next();
		});
	}, function(next) {
		lunaAPI.getCheckScreenOn(function(m) {
			var ret = {
				checkScreen: m.payload.settings.checkScreen
			};

			if (ret.checkScreen == 'off') {
				json.checkScreen = ret;
				next();
				return;
			}

			lunaAPI.getCheckScreenInfo(function(info) {
				ret.info = info.payload;
				ret.returnValue = info.payload.returnValue;
				delete ret.info.returnValue;
				json.checkScreen = ret;
				next();
			});
		});
	}, function(next) {
		lunaAPI.isNoSignal(function(signal) {
			var ret = {};

			ret.signal = !signal.payload.noSignal;
			ret.returnValue = signal.payload.returnValue;

			lunaAPI.getVideoSize(function(size) {
				ret.videoSize = size.payload.videoSize;
				ret.returnValue = ret.returnValue && size.payload.returnValue;

				lunaAPI.getVideoStillStatus(function(stall) {
					ret.stillStatus = stall.payload;
					ret.returnValue = ret.returnValue && stall.payload.returnValue;
					delete ret.stillStatus.returnValue;

					json.signal = ret;
					next();
				});
			});
		});
	}, function(next) {
		lunaAPI.getBasicInfo(function(info) {
			json.info = info.payload;
			next();
		});
	}, function() {
		json.returnValue = true;
		res.json(json);
	});
});

router.get('/lodData', function(req, res) {
	res.set({
		"Content-Disposition": 'attachment; filename="LED_LOD_Defects.csv"'
	});

	var json = {};

	ledAPI.getUnitConfig(function(config) {
		var numOn = config.onUnits.numOn;

		function getUnitLod(slave, callback) {
			ledAPI.getLodData({
				slave: slave
			}, function(ret) {
				var defects = ret.payload.result.defects;
				defects.forEach(function(lod) {
					res.write(slave + ', ' + lod.ldm + ', ' + lod.v + ', ' + lod.h + ', '
						+ lod.color + ',\n');
				});

				callback();
			});
		}

		function getAllLod(slave) {
			getUnitLod(slave, function() {
				if (slave / 2 == numOn) {
					ledAPI.showPattern({
						slave: 0,
						on: false,
						r: 0,
						g: 0,
						b: 0
					}, function() {});
					res.end();
					return;
				}
				getAllLod(slave + 2);
			});
		}

		res.write('SLAVE, BANK, V, H, COLOR,\n');
		if (numOn > 0) {
			getAllLod(2);
		}
	});
});

router.get('/ledFilm/getLedLayout', function(req, res) {
	res.set({
		"Content-Disposition": 'attachment; filename="ledLayout.json"'
	});

	var json = {};
	sequence.join(function(next) {
		lunaAPI.getSystemSettings('commercial', ['ledLayout'], function(ret) {
			json = ret.payload.settings.ledLayout;
			next();
		});
	}, function() {
		setTimeout(function () {
			res.json(json);
		}, 500);
	});
});

// input direction : 0(right), 1(left), 2(bottom), 3(top)
// port selection : 0(both), 1(left), 2(right)
router.post('/ledFilm/setLedLayout', function(req, res) {
	var layout = {};

	try {
		var data = fs.readFileSync('/var/outdoorweb/ledLayout.json');
		layout = JSON.parse(data);

	} catch (e) {
		res.json({status: 'fail', message: 'File not found or parsing error'});
		return;
	}

	var missingFields = autoLedFilmHandler.checkNecessaryFields(layout);

	if (missingFields.length > 0) {
		fs.unlinkSync('/var/outdoorweb/ledLayout.json');
		res.send({status: 'fail', message: missingFields});
		return;
	}

	sequence.sequence(function(next) {
		autoLedFilmHandler.adjustFilmLayout(layout);
		next();
	}, function (next) {
		for (var i=1; i<=4; i++) {
			ledFilmAPI.ledSetConfig('clearSendingMAC', {slave: (i * 2)});
			ledFilmAPI.ledSetConfig('setTestPattern', {slave: (i * 2), onOff: 0, patternR: 0, patternG: 0, patternB: 0, mac5th: 'FF', mac6th: 'FF'});
		}

		setTimeout(function () {
			next();
		}, 5000);
	}, function(next) {
		layout.senderList.forEach(function(sender) {
			var slaveId = parseInt(sender.senderId * 2);

			ledFilmAPI.ledSetConfig('setSenderPos', {slave: slaveId, hValue: sender.x, vValue: sender.y});
			ledFilmAPI.ledSetConfig('setInputDirectionColor', {slave: slaveId, inputDirection: sender.inputDirection});
			ledFilmAPI.ledSetConfig('setReceiverNum', {slave: slaveId, receiverNum: sender.unitList.length});

			sender.unitList.forEach(function(unit) {
				var unitId = parseInt(unit.unitId) - 1;

				// based on "input direction : bottom/top"
				var hVal = parseInt(unit.portSelection) == 0 ? 2 : 1;
				var vVal = parseInt(unit.rows) + (unit.mini ? 1 : 0);

				ledFilmAPI.ledSetConfig('setSendingMAC', {slave: slaveId, rcvIdx: unitId, mac5th: unit.mac[0], mac6th: unit.mac[1]});

				if (sender.inputDirection < 2) {
					ledFilmAPI.ledSetConfig('setHVNum', {slave: slaveId, rcvIdx: unitId, hValue: vVal, vValue: hVal});
				} else {
					ledFilmAPI.ledSetConfig('setHVNum', {slave: slaveId, rcvIdx: unitId, hValue: hVal, vValue: vVal});
				}

				ledFilmAPI.ledSetConfig('setDisplayPort', {slave: slaveId, rcvIdx: unitId, value: unit.portSelection});

				if (sender.inputDirection < 2) {
					ledFilmAPI.ledSetConfig('setXYPos', {slave: slaveId, rcvIdx: unitId, xValue: unit.x, yValue: unit.y - (unit.portSelection != 1 ? unit.trimming.right : 0)});
				} else {
					ledFilmAPI.ledSetConfig('setXYPos', {slave: slaveId, rcvIdx: unitId, xValue: unit.x - (unit.portSelection != 2 ? unit.trimming.left : 0), yValue: unit.y});
				}
			});

			ledFilmAPI.ledSetConfig('setFpgaControl', {slave: slaveId, index : 2, value : 1});
		});
		next();
	}, function() {
		lunaAPI.resetSystemSettings('commercial', ['ledLayout'], function() {
			lunaAPI.setSystemSettings('commercial', {'ledLayout' : layout });
			fs.unlinkSync('/var/outdoorweb/ledLayout.json');
			res.json({status: 'OK'});
		});
	});
});

module.exports = router;
