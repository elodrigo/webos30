var fs = require('fs');
var express = require('express');
var router = express.Router();
var password = require('../passwd');
var child_process = require('child_process');
var web = require('../web');
var defaultRouter = require('./index');

router.all('*', function(req, res, next) {
	defaultRouter.checkLogin(req, res, next, true);
});

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('mobile/dashBoard', req.parameters);
});

router.get('/dashBoard', function(req, res, next) {
	res.render('mobile/dashBoard', req.parameters);
});

router.get('/device', function(req, res, next) {
	res.render('mobile/device', req.parameters);
});

router.get('/system', function(req, res, next) {
	res.render('mobile/system', req.parameters);
});

router.get('/picture', function(req, res, next) {
	res.render('mobile/picture', req.parameters);
});

router.get('/sound', function(req, res, next) {
	res.render('mobile/sound', req.parameters);
});

router.get('/input', function(req, res, next) {
	res.render('mobile/input', req.parameters);
});

router.get('/network', function(req, res, next) {
	res.render('mobile/network', req.parameters);
});

router.get('/time', function(req, res, next) {
	res.render('mobile/time', req.parameters);
});

router.get('/door', function(req, res, next) {
	res.render('mobile/door', req.parameters);
});

router.get('/power', function(req, res, next) {
	res.render('mobile/power', req.parameters);
});

router.get('/bright', function(req, res, next) {
	res.render('mobile/bright', req.parameters);
});

router.get('/checkScreen', function(req, res, next) {
	res.render('mobile/checkScreen', req.parameters);
});

router.get('/fan', function(req, res, next) {
	res.render('mobile/fan', req.parameters);
});

router.get('/emergency', function(req, res, next) {
	res.render('mobile/emergency', req.parameters);
});

router.get('/remocon', function(req, res, next) {
	res.render('mobile/remocon', req.parameters);
});

router.get('/charts', function(req, res, next) {
	var page = req.query.page;
	if (page == undefined || defaultRouter.osCommandFilter.test(page) || /\D/.test(page)) {
		page = 1;
	}
	req.parameters.page = page;
	res.render('mobile/charts', req.parameters);
});

router.get('/logFile', function(req, res, next) {
	defaultRouter.getLog(req, res, true);
	res.render('mobile/log', req.parameters);
});

router.get('/media', function(req, res, next) {
	defaultRouter.getMedia(req, res, function(){
		res.render('mobile/media', req.parameters);
	});
});

router.get('/update', function(req, res, next) {
	defaultRouter.getUpdate(req, res, ['epk', 'txt']);
	res.render('mobile/update', req.parameters);
});

router.get('/language', function(req, res, next) {
	res.render('mobile/language', req.parameters);
});

router.get('/changePasswd', function(req, res, next) {
	res.render('mobile/changePasswd', req.parameters);
});

module.exports = router;

