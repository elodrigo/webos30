echo off
set arg1=%1
set default=242
set pre=192.168.1.
set var=%pre%%default%

IF "%arg1%" == "" (
    set var=%pre%%default%
) else (
    set var=%pre%%arg1%
)

scp -r views root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/
scp -r public/js/design root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/public/js/
scp public/js/page/loginPage.js root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/public/js/page/loginPage.js
scp web.js root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/web.js
scp api.js root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/api.js
scp app.js root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/app.js
scp routes/index.js root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/routes/index.js
scp LanguageManager.js root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/LanguageManager.js