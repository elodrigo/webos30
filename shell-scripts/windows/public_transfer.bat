echo off
set arg1=%1
set default=242
set pre=192.168.1.
set var=%pre%%default%

IF "%arg1%" == "" (
    set var=%pre%%default%
) else (
    set var=%pre%%arg1%
)

scp -r public/assets root@%var%:~/sources/public/
scp public/js/client-common.js root@%var%:~/sources/public/js/client-common.js
scp public/js/page/common.js root@%var%:~/sources/public/js/page/common.js
scp -r resources root@%var%:~/sources/