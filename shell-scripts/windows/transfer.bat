echo off
set arg1=%1
set default=242
set pre=192.168.1.
set var=%pre%%default%

IF "%arg1%" == "" (
    set var=%pre%%default%
) else (
    set var=%pre%%arg1%
)

@REM scp -r views/*.* root@%var%:/media/developer/com.webos.service.outdoorwebcontrol/views
scp views/ledSignage/*.* root@%var%:/media/developer/com.webos.service.outdoorwebcontrol/views/ledSignage/
@REM scp public/js/design/ledSignage/*.* root@%var%:/media/developer/com.webos.service.outdoorwebcontrol/public/js/design/ledSignage/
@REM scp public/js/design/modal/*.* root@%var%:/media/developer/com.webos.service.outdoorwebcontrol/public/js/design/modal/
scp -r public/js/design root@%var%:/media/developer/com.webos.service.outdoorwebcontrol/public/js/
scp -r views/partials root@%var%:/media/developer/com.webos.service.outdoorwebcontrol/views/
scp views/loginPage.hbs root@%var%:/media/developer/com.webos.service.outdoorwebcontrol/views/loginPage.hbs
@REM scp public/js/design/loginPage.js root@%var%:/media/developer/com.webos.service.outdoorwebcontrol/public/js/design/loginPage.js
scp web.js root@%var%:/media/developer/com.webos.service.outdoorwebcontrol/web.js
scp api.js root@%var%:/media/developer/com.webos.service.outdoorwebcontrol/api.js
scp app.js root@%var%:/media/developer/com.webos.service.outdoorwebcontrol/app.js
scp routes/index.js root@%var%:/media/developer/com.webos.service.outdoorwebcontrol/routes/index.js
scp LanguageManager.js root@%var%:/media/developer/com.webos.service.outdoorwebcontrol/LanguageManager.js
@REM scp views/layout.hbs root@%var%:/media/developer/com.webos.service.outdoorwebcontrol/views/layout.hbs