var fs = require('fs');
var https = require('https');

var password = require('./passwd');
var api = require('./api');

var command = {};
var secureKey = '';
var peers = [];

var secureKeyFileName = '/var/outdoorweb/groupSecureKey';
var peersFileName = '/var/outdoorweb/groupPeers';

init();

function init() {
	fs.readFile(secureKeyFileName, function(err, data) {
		if (!err) {
			secureKey = String(data);
		}
	});

	fs.readFile(peersFileName, function(err, data) {
		if (!err) {
			peers = JSON.parse(String(data));
		}
	});
}

function createSecureKey(socket, msg) {
	var salt = Math.round((new Date().valueOf() * Math.random())) + '';
	var key = password.createPasswd(msg.word, salt, 'sha256');
	console.log(key);
	socket.emit("secureKey" + msg.eventID, key);
}

function getSecureKey(socket, msg) {
	socket.emit('secureKey' + msg.eventID, secureKey);
}

function setSecureKey(socket, msg) {
	secureKey = msg.key;
	fs.writeFile(secureKeyFileName, msg.key, function(err) {
		if (err) {
			console.log(err);
		}
		api.sendReturn(socket, err !== undefined, "secureKey");
	});
}

function commandHandler(socket, msg) {
	var handler = command[msg.command];
	if (!handler) {
		console.log('No matching handler for ' + msg.command);
		return;
	}

	handler(socket, msg);
}

function getPeers(socket, msg) {
	socket.emit('peer' + msg.eventID, peers);
}

function checkPeer(peer) {
	if (!peer) {
		return false;
	}
	if (peer.https === undefined || !peer.ip || !peer.port) {
		return false;
	}

	return true;
}

function updatePeerFile(callback) {
	fs.writeFile(peersFileName, JSON.stringify(peers, null, 2), callback);
}

function addPeer(socket, msg) {
	if (!checkPeer(msg.peer)) {
		api.sendReturn(socket, false, "peer");
		return;
	}

	peers.push(msg.peer);
	updatePeerFile(function(err) {
		if (err) {
			console.log(err);
		}
		api.sendReturn(socket, true, "peer");
	});
}

function removePeer(socket, msg) {
	var index = msg.index;
	peers.splice(index, 1);
	updatePeerFile(function(err) {
		if (err) {
			console.log(err);
		}
		api.sendReturn(socket, true, "peer");
	});
}

function sendError(msg, socket) {
	socket.emit("peer" + msg.eventID, {
		returnValue: false,
		message: 'Error',
		detail: msg
	});
}

function responseHandler(res, socket, msg, jsonHandler) {
	var data = '';
	res.on('error', function(err) {
		console.log(err);
		socket.emit("peerStatus" + msg.eventID, {
			returnValue: false,
			message: 'Error',
			detail: 'Peer response error'
		});
	});
	res.on('data', function(chunk) {
		data += chunk;
	});
	res.on('end', function() {
		if (!JSON.parse(data).returnValue) {
			sendError('cannot get data', socket);			
			return;
		}
		jsonHandler(data, socket, msg)
	});
}

function getPeerData(peer, path, handler, errorHandler, paramStr) {
	var httpOption = {
		host: peer.ip,
		port: peer.port,
		path: path + '?token=' + secureKey + '&' + paramStr,
		method: 'GET',
	};

	var req = undefined;

	if (peer.https) {
		var agentOption = {
			host: peer.ip,
			port: peer.port,
			path: path,
			rejectUnauthorized: false
		};
		
		var agent = new https.Agent(agentOption);

		httpOption.agent = agent;

		req = https.get(httpOption, handler).on('error', errorHandler);
	} else {
		req = http.get(httpOption, handler).on('error', errorHandler);
	}
}

function incidentsHandler(data, socket, msg) {
	var incidents = JSON.parse(data).incident;
	var ngCount = 0;
	var retList = '';
	for (var i = 0; i != incidents.length; ++i) {
		if (incidents[i].state == 'NG') {
			++ngCount;
			retList += incidents[i].category + ':' + incidents[i].sensor + ':' + incidents[i].event + '\n';
		}
	}

	var message = ngCount + '/' + incidents.length;
	socket.emit("peer" + msg.eventID, {
		returnValue: (ngCount === 0),
		message: message,
		detail: retList
	});
}

function statusHandler(data, socket, msg) {
	socket.emit('peer' + msg.eventID, JSON.parse(data));
}

function getPeerIncidents(socket, msg) {
	var i = msg.index;

	getPeerData(peers[i], '/rest/incidents', function(res) {
		responseHandler(res, socket, msg, incidentsHandler)
	}, function(e) {
		sendError('Error while connecting to the peer\n(' + e.message + ')');
	});
}

function getPeerStatus(socket, msg) {
	var i = msg.index;

	getPeerData(peers[i], '/rest/status', function(res) {
		responseHandler(res, socket, msg, statusHandler)
	}, function(e) {
		sendError('Error while connecting to the peer\n(' + e.message + ')');
	});
}

function getPeerScreen(socket, msg) {
	var curTime = new Date().valueOf();
	var i = msg.index;

	getPeerData(peers[i], '/rest/screenShot', function(res) {
		var data = '';
		res.setEncoding('binary');

		res.on('error', function(err) {
			console.log('SCREEN ERROR : ' + err);
			socket.emit("peerScreen" + msg.eventID, {
				returnValue: false,
				message: 'Error',
				detail: 'Peer response error'
			});
		});

		res.on('data', function(chunk) {
			data += chunk;
		});

		res.on('end', function() {
			var file = 'group' + (new Date().valueOf()) + '.jpg';
			var path = '/tmp/outdoorweb/' + file;

			console.log(data.length);

			fs.writeFile(path, data, 'binary', function(err) {
				if (err) {
					console.log('WRITE FILE ERROR : ' + err);
					sendError('Error while receiving the screen shot', socket);
					return;
				}

				socket.emit("peer" + msg.eventID, {
					returnValue: true,
					url: '/tmp/' + file
				});
			});

			setTimeout(function() {
				fs.unlink(path, function(err) {
					if (err) {
						console.log(err);
					}
				});
			}, 10 * 1000);
		});
	}, function(e) {
		sendError('Error while connecting to the peer\n(' + e.message + ')', socket);
	}, 'height=90');
}

command['createSecureKey'] = createSecureKey;
command['getSecureKey'] = getSecureKey;
command['setSecureKey'] = setSecureKey;
command['getPeers'] = getPeers;
command['addPeer'] = addPeer;
command['removePeer'] = removePeer;
command['getPeerIncidents'] = getPeerIncidents;
command['getPeerScreen'] = getPeerScreen;
command['getPeerStatus'] = getPeerStatus;

exports.commandHandler = commandHandler;
exports.secureKey = function() {
	if (secureKey && secureKey !== '') {
		return secureKey;
	}
	return undefined;
}
