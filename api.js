var luna;
var server;
var io;

var fs = require('fs');
var PATH = require('path');
var web = require('./web');
var lunaAPI = require('./luna');
var groupAPI = require('./group');
var ledAPI = require('./ledAPI');
var ledFilmAPI = require('./ledFilmAPI');
var password = require('./passwd');
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
var async = require('async');
var LanguageManager = require("./LanguageManager");

var localeInfo = undefined;

var commands = {};

var updateDir = "/media/update/";
var thumbDir = "/tmp/outdoorweb/tnm/";
var mediaDir = "/media/signage/";

var nameMaxLength = 20;

function sendReturn(socket, result, from) {
	var ret = {
		result: result,
		from: from
	};
	socket.emit('return', ret);
}

function getVolume(socket, msg) {
	lunaAPI.getVolume(function(m) {
		socket.emit('volume' + msg.eventID, m.payload);
	});
}

function setVolume(socket, msg) {
	lunaAPI.setVolume(Number(msg.volume), function(m) {
		sendReturn(socket, m.payload.returnValue, "sound");
	});
}

function setMuted(socket, msg) {
	lunaAPI.setMuted(msg.muted, function(m) {
		sendReturn(socket, m.payload.returnValue, "sound");
	});
}

function getVolume(socket, msg) {
	luna.call("luna://com.webos.audio/getVolume", {},
	function(m) {
		socket.emit('volume' + msg.eventID, m.payload);
	});
}

function sendRemocon(socket, msg, from) {
	var tasks = [
		function (callback) {
			luna.call("luna://com.webos.service.update/getCurStatus", {}, function (m) {
				callback(null, m);
			});
		},
		function (callback) {
			luna.call("luna://com.webos.service.tv.csu/updateStatus", {}, function (m) {
				callback(null, m);
			});
		},
	];

	async.parallel(tasks, function (err, results) {
		var isRunningUpdate = false;
		results.forEach(function (currentValue, index, arry) {
			if (!isRunningUpdate) {
				isRunningUpdate = currentValue.payload.status !== 'idle';
			}
		});

		if (isRunningUpdate) {
			return;
		}

		var key = msg.key;

		if (!from) {
			from = 'remocon';
		}

		lunaAPI.sendRemocon(key, function (m) {
			sendReturn(socket, m.payload.returnValue, from);
		});
	});
}

function setHdmiInput(socket, msg) {
	var input = msg.input;
	var code = "";
	function setInput() {
		lunaAPI.getInputList(function (m) {
			var devices = m.payload.devices;
			for (var i = 0; i < devices.length; ++i) {
				if (devices[i].id === input) {
					lunaAPI.setInputSouce(devices[i].appId);
					ledAPI.inputChangeHandler(input);
				}
			}
		});
	}
	setInput();
	setTimeout(setInput, 500);
}

function getHdmiInput(socket, msg) {
	lunaAPI.getCurrentInput(function(m) {
		if (m.payload.returnValue == true) {
			socket.emit('input' + msg.eventID, m.payload.deviceID);
		} else {
			sendReturn(socket, m.payload.returnValue);
		}
	});
}

function setPortrait(socket, msg) {
	var settings = msg;

	lunaAPI.setSystemSettings('commercial', settings, function(m) {
		console.log(m);
		sendReturn(socket, m.payload.returnValue, "portrait");
	});
}

function getPortrait(socket, msg) {
	lunaAPI.getSystemSettings('commercial', [
		"osdPortraitMode", "contentRotation", "rotationAspectRatio", "pivotMode"
	], function(m) {
		if (!m.payload.settings) {
			sendReturn(socket, m.payload.returnValue, "portrait");
		} else {
			socket.emit('portrait' + msg.eventID, m.payload.settings);
		}
	});
}

function capture(socket, msg) {
	var curTime = new Date().valueOf();

	var dir = "/tmp/outdoorweb";
	var file = "/capture" + curTime + ".jpg";
	var retFile = "/tmp" + file;
	var path = dir + file;

	var errorFile = '/images/screenshot_noimage.png';

	lunaAPI.screenShot(path, msg.height, function(m) {
		if (!m.payload.returnValue) {
			retFile = errorFile;
		} else {
			setTimeout(function() {
				fs.unlink(path, function(err) {
					if (err) {
						console.log(err);
					}
				});
			}, 5000);
		}
		socket.emit("capture" + msg.eventID, retFile);
	});
}

function sendToast(socket, msg) {
	luna.call("luna://com.webos.notification/createToast", {
		sourceId: "web",
		message: msg.message
	},
	function(m) {
		sendReturn(socket, m.payload.returnValue, "toast");
	});
}

function sendAlert(socket, msg) {
	luna.call("luna://com.webos.notification/createAlert", {
		message: msg.message,
		buttons: [
			{label: "Close"}
		]
	}, function(m) {
		sendReturn(socket, m.payload.returnValue, "alert");
	});
}

var scrollAlertProcess;

function sendScrollAlert(socket, msg) {
	var pointY = 0;
	var lines = msg.message.split(/\n/).length;
	var textHeight = msg.textSize * 1.35;
	var height = textHeight * lines;
	var bold = msg.bold | false;
	var underLine = msg.underLine | false;
	var italic = msg.italic | false;

	if (msg.scrollDir == 'GOLEFT' || msg.scrollDir == 'GORIGHT' || msg.scrollDir == 'NONE') {
		textHeight = height;
	}

	switch (msg.location) {
		case 'Center':
			pointY = (1080 - height) / 2;
			break;
		case 'Top':
			pointY = textHeight - height;
			break;
		case 'Bottom':
			pointY = 1080 - textHeight;
			break;
	}

	var opt = {
		LineSpace: 2,
		Text_Region: {
			X: 0, Y: pointY, Name: "Text", Height: height, Z:1, Width: 1920
		},
		Window_BkColor:16777215,
		Repeat:"INFINITE",
		LineView:false,
		Space:0,
		Message: msg.message,
		launchHidden:false,
		Win_Region:{
			X:0, Y: pointY, Name:"background", Height:height, Z:1, Width:1920
		},
		Text_BkColor: msg.backgroundColor, 
		Line_clr:16711680,
		Content:{
			StartTime:0,Duration:1800
		},
		Period:{
			Name:"SuperSign",Type:"MESSAGE",Duration:60,Idx:-1
		},
		Italic: italic,
		font:"LG_Display",
		underLine: underLine,
		Effect: msg.scrollDir,
		Speed:2,
		text_clr: msg.textColor,
		nid:"com.webos.app.commercial.alert",
		Text_size: msg.textSize,
		Thickness:10,
		duration:"INFINITE",
		HAlign:"LEFT",
		bold: bold
	};

	scrollAlertProcess = spawn("/usr/bin/com.webos.app.commercial.alert",
							   [JSON.stringify(opt)]);
}

process.on('SIGTERM', function() {
	if (scrollAlertProcess) {
		scrollAlertProcess.kill('SIGTERM');
	}
	process.exit(0);
});

function sendMouseEvent(socket, msg) {
	luna.call("luna://com.lge.inputgenerator/pushMouseEvent", {
		eventtype: "setpos",
		x: Number(msg.x) * web.env.screen.width,
		y: Number(msg.y) * web.env.screen.height
	}, function(m) {
		setTimeout(function() {
			luna.call("luna://com.lge.inputgenerator/pushMouseEvent", {
				eventtype: "click"
			}, function(m2) {
				//sendReturn(socket, m.payload.returnValue);
			});
		}, 500);
	});
}

function sendKeyEvent(socket, msg) {
	var tasks = [
		function (callback) {
			luna.call("luna://com.webos.service.update/getCurStatus", {}, function (m) {
				callback(null, m);
			});
		},
		function (callback) {
			luna.call("luna://com.webos.service.tv.csu/updateStatus", {}, function (m) {
				callback(null, m);
			});
		},
	];

	async.parallel(tasks, function (err, results) {
		var isRunningUpdate = false;
		results.forEach(function (currentValue, index, arry) {
			if (!isRunningUpdate) {
				isRunningUpdate = currentValue.payload.status !== 'idle';
			}
		});

		if (isRunningUpdate) {
			return;
		}

		luna.call("luna://com.lge.inputgenerator/pushKeyEvent", {
			eventtype: "key",
			keycode: msg.keycode
		}, function (m) {});
	});
}

function deleteTnm(fileName) {
	var thumbStart = fileName.lastIndexOf('/');
	var thumbEnd = fileName.lastIndexOf('.');
	var thumbFileName = fileName.substring(thumbStart + 1, thumbEnd);
	console.log(thumbStart + '~' + thumbEnd + ':' + thumbFileName);

	var files = fs.readdirSync(thumbDir);
	files.filter(function(fileName) {
		return fileName.indexOf(thumbFileName) == 0;
	}).forEach(function(fileName) {
		fs.unlink(thumbDir + '/' + fileName, function(e) {
			if (e) {
				console.log(e);
			}
		});
	});

	luna.call("luna://com.webos.service.tnm/clean", {}, function(msg) {
		if (!msg.payload.returnValue) {
			console.log(msg.payload);
		}
	});
}

function deleteFile(socket, msg) {
	var dir = "/media/";
	if (msg.which != "update" && msg.which != "signage") {
		sendReturn(socket, false, "delete");
		return;
	}

	dir += msg.which + "/";

	if (msg.path) {
		dir += path + '/';
	}

	var filePath = PATH.normalize(dir + msg.fileName);
	if (filePath.indexOf(dir) != 0) {
		sendReturn(socket, {
			returnValue: false,
			reason: "illegal access to " + filePath
		}, "delete");
		return;
	}

	fs.unlink(filePath, function(err) {
		if (err) {
			sendReturn(socket, false, "delete");
			return;
		}
		socket.emit("deleteFile" + msg.eventID, filePath);
	});

	if (msg.which == 'signage') {
		deleteTnm(msg.fileName);
	}
}

function swupdate(socket, msg) {
	var dir = updateDir;
	var filePath = PATH.normalize(dir + msg.fileName);

	if (filePath.indexOf(dir) != 0) {
		sendReturn(socket, {
			returnValue: false,
			reason: "illegal access to " + filePath
		}, "swupdate");
		return;
	}

	luna.call("luna://com.webos.service.update/startUpdateThruUsb", {
		mode: 'expert',
		filePath: filePath,
		forceUpdateMicom: msg.micomUpdate === true
	}, function(m) {
		sendReturn(socket, m.payload.returnValue, "swupdate");
	});
}

function listUpdates(socket, msg) {
	var path = updateDir;

	luna.call("luna://com.webos.service.update/listUpdatesInUsb", {
		path: path
	}, function(m) {
		socket.emit("listUpdates" + msg.eventID, m.payload);
	});
}

function getUpdateStatus(socket, msg) {
	luna.call("luna://com.webos.service.update/getCurStatus", {}, function(m) {
		socket.emit("swupdate" + msg.eventID, m.payload);
	});
}

var sync = false;

function runThumbnail(msg, fileName, mediaType, cmd, socket) {
	var child = exec(cmd, function(error, stdout, stderr) {
		var result = JSON.parse(stdout);
		socket.emit("thumbnail", {
			id: msg.id,
			fileName: fileName,
			mediaType: mediaType,
			result: result
		});
		sync = false;
	});
}

var mediaType = {
	audio: ["mp3", "ogg", "aac"],
	image: ["jpg", "png", "bmp", "gif"]
};

function emptyThumbnail(){
	var targetDir = thumbDir;
	fs.readdir(targetDir, function(err, files){
		files.forEach(function(fileName){
			fs.unlink(PATH.join(targetDir, fileName), function(err){
				if (err) throw error;
			});
		});
	});
}

function getThumbnail(socket, msg) {
	var dir = "/media/signage/";
	var targetDir = thumbDir;

	var filePath = PATH.normalize(dir + msg.fileName);
	var fileName = filePath;
	var idx = fileName.lastIndexOf('/');
	if (idx >= 0) {
		fileName = fileName.substring(idx + 1);
	}
	var targetUri = PATH.normalize(targetDir + fileName + "_");

	if (filePath.indexOf(dir) != 0) {
		sendReturn(socket, {
			returnValue: false,
			reason: "illegal access to " + filePath
		});
		return;
	}

	var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
	var type = 'video';
	if (mediaType.audio.indexOf(ext) >= 0) {
		type = 'audio';
	} else if (mediaType.image.indexOf(ext) >= 0) {
		type = 'image'
	}

	try {
		var stat = fs.statSync(targetUri + ".jpg");
		if (stat) {
			socket.emit("thumbnail", {
				result: { returnValue: true },
				id: msg.id,
				fileName: fileName,
				mediaType: type
			});
			return;
		}
	} catch(err) {
		console.log(fileName + " does not exist");
	}

	var param = {
		sourceUri: filePath,
		targetUri: targetUri,
		mediaType: type,
		targetWidth: 320,
		targetHeight: 180,
		priority: 0,
		scaleType: "crop",
		requestId: "outdoorweb"
	};
	var strParam = JSON.stringify(param);
	strParam = strParam.replace(/'/g, '\\u0027');
	var cmd = "luna-send -n 1 -a testapp luna://com.webos.service.tnm/getThumbnail '" + strParam + "'";
	if (sync) {
		var interval = setInterval(function() {
			if (!sync) {
				sync = true;
				runThumbnail(msg, fileName, type, cmd, socket);
				clearInterval(interval);
			}
		},
		500);
	} else {
		sync = true;
		runThumbnail(msg, fileName, type, cmd, socket);
	}
}

function getCheckScreenOn(socket, msg) {
	lunaAPI.getCheckScreenOn(function(m) {
		socket.emit('isCheckScreenOn' + msg.eventID, m.payload.settings.checkScreen);
	});
}

function getNormalCheckScreenStatus(socket, msg) {
	luna.call("luna://com.webos.service.tv.signage/checkScreen", {}, function(m) {
		socket.emit('normalCheckScreen' + msg.eventID, m.payload);
	});
}

function setCheckScreenOn(socket, msg) {
	lunaAPI.setSystemSettings('commercial', { checkScreen: msg.isOn }, function(m) {
		sendReturn(socket, m.payload.returnValue, "checkScreen");
	});
}

function setCheckScreenPos(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/setCheckScreenPos", {
		position: msg.position
	}, function(m) {
		sendReturn(socket, m.payload.returnValue, "checkScreen");
	});
}

function getCheckScreenInfo(socket, msg) {
	lunaAPI.getCheckScreenInfo(function(m) {
		socket.emit('checkScreenInfo' + msg.eventID, m.payload);
	});
}

function calibrateCheckScreenEYEKey(socket, msg) {
	lunaAPI.calibrateCheckScreenEYEKey(function(m) {
		socket.emit('calibrateCheckScreenEYEKey' + msg.eventID, m.payload);
	});
}

function getCheckScreenColor(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/getCheckScreenColor", {}, function(m) {
		socket.emit('checkScreenColor' + msg.eventID, m.payload);
	});
}

function calibrateCheckScreen(socket, msg) {
	var addr = "luna://com.webos.service.tv.outdoor/calibrateCheckScreen";

	if (msg.eyekey) {
		addr += 'EYEKey';
	}

	luna.call(addr, {},	function(m) {
		socket.emit('checkScreenCalib' + msg.eventID, m.payload);
	});
}

function getNTPStatus(socket, msg) {
	luna.call("luna://com.palm.systemservice/getPreferences", {
		keys: [
			"useNetworkTime", "networkTimeSource"
		]
	}, function(m) {
		socket.emit("ntpStatus" + msg.eventID, m.payload);
	});
}

function setNTPStatus(socket, msg) {
	/*
	var param = { useNetworkTime: msg.useNTP,
		networkTimeSource: { NTPServer: msg.serverIP,
			useSNTP: msg.useSNTP
		}
	};
	*/
	var param = { useNetworkTime: msg.useNTP };

	luna.call("luna://com.palm.systemservice/setPreferences", param, function(m) {
		lunaAPI.setSystemSettings('time', {
			autoClock: (msg.useNTP ? 'on' : 'off')
		}, function(m) {
			sendReturn(socket, m.payload.returnValue, "ntp");
			socket.emit("ntp" + msg.eventID, m.payload.returnValue);
		});
	});
}

function getDoorStatus(socket, msg) {
	lunaAPI.getDoorStatus(function(m) {
		socket.emit('curDoor' + msg.eventID, m.payload);
	});
}

function changeDoorMuteOpt(socket, msg) {
	if (!msg.mute) {
		return;
	}

	lunaAPI.setSystemSettings('commercial', {
		doorMuteOpt: msg.mute
	}, function(m) {
		sendReturn(socket, m.payload.returnValue, "door");
	});
}

function resetDoorState(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/resetDoorState", {}, function(m) {
		sendReturn(socket, m.payload.returnValue, "door");
	});
}

function getInputList(socket, msg) {
	lunaAPI.getInputList(function (m) {
		var devices = m.payload.devices;
		var inputList = [];
		for (var i = 0; i < devices.length; ++i) {
			inputList.push({
				id: devices[i].id,
				name: devices[i].deviceName,
				appId: devices[i].appId,
				settingKey: devices[i].id.replace('_', '').toLowerCase()
			});
			if (inputList[i].settingKey.indexOf('rgb') >= 0) {
				inputList[i].settingKey = 'rgb';
			}
		}
		socket.emit("inputList" + msg.eventID, inputList);
	});
}

function mpPressPowerBtn(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/mpPressPowerBtn", {
		id: msg.id,
		isLong: msg.isLong
	}, function(m) {
		sendReturn(socket, m.payload.returnValue);
	});
}

function mpPressResetBtn(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/mpPressResetBtn", {
		id: msg.id,
		isLong: msg.isLong
	}, function(m) {
		sendReturn(socket, m.payload.returnValue);
	});
}

function mpCheckPowerLed(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/mpCheckPowerLed", {},
	function(m) {
		socket.emit('powerLed' + msg.eventID, m.payload);
	});
}

function getTemp(socket, msg) {
	lunaAPI.getTemperature(msg.id, function(m) {
		socket.emit('temperature' + msg.eventID, m.payload);
	});
}

function addHistoryRange(msg, param) {
	if (msg.start != undefined) {
		param.start = msg.start;
	}

	if (msg.end != undefined) {
		param.end = msg.end;
	}
}

function getTempHistory(socket, msg) {
	var param = {
		id: msg.id
	};

	addHistoryRange(msg, param);

	lunaAPI.getTempHistory(param, function(m) {
		socket.emit('tempHistory' + msg.eventID, m.payload);
	});
}

function getFanRpmHistory(socket, msg) {
	var param = {
		id: msg.which,
		num: msg.id
	};

	addHistoryRange(msg, param);

	lunaAPI.getFanRpmHistory(param,	function(m) {
		m.payload.num = msg.id;
		socket.emit('fanRpmHistory' + msg.eventID, m.payload);
	});
}

function getBacklightHistory(socket, msg) {
	var param = {};

	addHistoryRange(msg, param);

	if (web.env.supportLedSignage) {
		lunaAPI.getContrastHistory(param, function(m) {
			socket.emit('backlightHistory' + msg.eventID, m.payload);
		});
	} else {
		lunaAPI.getBacklightHistory(param, function(m) {
			socket.emit('backlightHistory' + msg.eventID, m.payload);
		});
	}
}

function getEyeQSensorHistory(socket, msg) {
	var param = {};

	addHistoryRange(msg, param);

	lunaAPI.getEyeQSensorHistory(param,	function(m) {
		socket.emit('illumHistory' + msg.eventID, m.payload);
	});
}

function getHumidityHistory(socket, msg) {
	var param = {};

	addHistoryRange(msg, param);

	lunaAPI.getHumidityHistory(param, function(m) {
		socket.emit('humidityHistory' + msg.eventID, m.payload);
	});
}

function getSystemProperties(socket, msg, method) {
	lunaAPI.getSystemProperties(msg.keys, function(m) {
		if (!method) {
			method = "systemProp";
		}
		socket.emit(method + msg.eventID, m.payload);
	});
}

function setSystemProperties(socket, msg) {
	lunaAPI.setSystemProperties(msg.properties, function(m) {
		sendReturn(socket, m.payload, "systemProp");
	});
}

function getSystemSettings(socket, msg) {
	if (msg.dimension) {
		lunaAPI.getSystemSettingsByDimension(msg.category, msg.dimension, msg.keys, function(m) {
			socket.emit('systemSettings' + msg.eventID, m.payload.settings);
		});
	} else {
		lunaAPI.getSystemSettings(msg.category, msg.keys, function(m) {
			socket.emit('systemSettings' + msg.eventID, m.payload.settings);
		});
	}
}

function setSystemSettings(socket, msg) {
	if (msg.dimension) {
		lunaAPI.setSystemSettingsByDimension(msg.category, msg.dimension, msg.settings, function(m) {
			socket.emit('systemSettings' + msg.eventID, m.payload);
		});
	} else {
		lunaAPI.setSystemSettings(msg.category, msg.settings, function(m) {
			socket.emit('systemSettings' + msg.eventID, m.payload);
		});
	}
}

function resetSystemSettings(socket, msg) {
	lunaAPI.resetSystemSettings(msg.category, msg.keys, function(m) {
		socket.emit('systemSettings' + msg.eventID, m.payload);
	});
}

function getBasicInfo(socket, msg) {
	lunaAPI.getBasicInfo(function(ret) {
		socket.emit('basicInfo' + msg.eventID, ret.payload);
	});
}

function getWebOSInfo(socket, msg) {
	lunaAPI.getWebOSInfo(function(m) {
		socket.emit("webOSInfo" + msg.eventID, m.payload);
	});
}

function getFanMicomInfo(socket, msg) {
	lunaAPI.getFanMicomInfo(function(m) {
		socket.emit("FanMicomInfo" + msg.eventID, m.payload);
	});
}

var incidentStr = '';
function getDowntimeIncident(socket, msg) {
	lunaAPI.getDowntimeIncident(function(m) {
		var newStr = JSON.stringify(m.payload, null, 2);

		if (incidentStr !== newStr) {
			incidentStr = newStr;
			try {
				fs.writeFileSync('/tmp/outdoorweb/log.json', incidentStr);
			} catch (e) {
				console.log(e);
			}
		}
		socket.emit("downtimeIncident" + msg.eventID, m.payload);
	});
}

function getFanStatus(socket, msg) {
	lunaAPI.getFanStatus(function(m) {
		socket.emit("fanStatus" + msg.eventID, m.payload);
	});
}

function getFanRPM(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/getFanRPM", {
	}, function(m) {
		socket.emit("fanRPM" + msg.eventID, m.payload);
	});
}

function getNumFans(socket, msg) {
	lunaAPI.getNumFans(function(m) {
		socket.emit("numFans" + msg.eventID, m.payload);
	});
}

function isNoSignal(socket, msg) {
	lunaAPI.isNoSignal(function(m) {
		socket.emit("isNoSignal" + msg.eventID, m.payload);
	});
}

function getVideoSize(socket, msg) {
	lunaAPI.getVideoSize(function(m) {
		socket.emit("videoSize" + msg.eventID, m.payload);
	});
}

function getVideoStillStatus(socket, msg) {
	lunaAPI.getVideoStillStatus(function(m) {
		socket.emit("videoStill" + msg.eventID, m.payload);
	});
}

function resetStalledImageSetting(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/stalledImgRestart", {
		minutes: msg.minutes
	}, function(m) {
		sendReturn(socket, m.payload, "resetStall");
	});
}

function getEmergency(socket, msg) {
	lunaAPI.getSystemSettings('commercial', [
		"lcmOffset", "panelOffset", "temperaturePowerOff", "temperatureBacklightOn", "temperatureBacklightOff"
	], function(m) {
		socket.emit("emergency" + msg.eventID, m.payload.settings);
	});
}

function setEmergency(socket, msg) {
	lunaAPI.setSystemSettings('commercial', {
		lcmOffset: msg.lcmOffset,
		panelOffset: msg.panelOffset,
		temperatureBacklightOn: msg.backlightOn,
		temperatureBacklightOff: msg.backlightOff,
		temperaturePowerOff: msg.powerOff
	}, function(m) {
		sendReturn(socket, m.payload.returnValue, "emergency");
	});
}

function testsuiteForDIL(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/testsuiteForDIL", {}, function(m) {
		socket.emit('testDIL' + msg.eventID, m.payload);
	});
}

function testsuiteForEyeQ(socket, msg) {
	luna.call("luna://com.webos.service.tv.systemproperty/getProperties", { keys:["eyecheckstatus"] }, function(m) {
		console.log("testsuiteForEyeQ"+m.payload);
		socket.emit('testEyeQ' + msg.eventID, m.payload);
	});
}

function updatePictureMode(socket, msg) {
	msg.settings = {pictureMode: msg.pictureMode};
	setPictureDBVal(socket, msg);
}

function getPictureMode(socket, msg) {
	lunaAPI.getSystemSettings('picture', [
		'pictureMode', 'pictureSettingModified'
	], function(m) {
		var ret;
		if (m.payload.settings) {
			ret = m.payload.settings;
		} else {
			ret = m.payload;
		}
		socket.emit('pictureMode' + msg.eventID, ret);
	});
}

function getPictureDBVal(socket, msg) {
	lunaAPI.getSystemSettings('picture', msg.keys, function(m) {
		var ret;
		if (m.payload.settings) {
			ret = m.payload.settings;
		} else {
			ret = m.payload;
		}
		socket.emit('pictureDB' + msg.eventID, ret);
	});
}

function setPictureDBVal(socket, msg) {
	lunaAPI.setSystemSettings('picture', msg.settings, function(m) {
		var from = 'picture';
		if (msg.from) {
			from = msg.from;
		}
		sendReturn(socket, m.payload.returnValue, from);
	});
}

function getPowerState(socket, msg) {
	luna.call("luna://com.webos.service.tvpower/power/getPowerState", {}, function(m) {
		socket.emit('powerState' + msg.eventID, m.payload);
	});
}

function turnOffScreen(socket, msg) {
	lunaAPI.turnOffScreen(function(m) {
		sendReturn(socket, m.payload.returnValue, "backlight");
	});
}

function turnOnScreen(socket, msg) {
	lunaAPI.turnOnScreen(function(m) {
		sendReturn(socket, m.payload.returnValue, "backlight");
	});
}

function rebootScreen(socket, msg) {
	luna.call("luna://com.webos.service.tv.power/reboot", {
		"reason": "reset"
	}, function(m) {
	});
}

function getFanDuty(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/getFanDuty", {
		id: msg.id,
		micomId: msg.micomId
	}, function(m) {
		socket.emit('getFanDuty' + msg.eventID, m.payload);
	});
}

function setFanDuty(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/setFanDuty", {
		id: msg.id,
		duty: Number(msg.duty),
		micomId: msg.micomId
	}, function(m) {
		sendReturn(socket, m.payload.returnValue, "fan");
	});
}

var fanTimeout;

function stopFanControl(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/setFanMode", {
		"mode": "manual"
	}, function(m) {
		sendReturn(socket, m.payload.returnValue, "fan");
		// resume fan control after 5 min.
		if (fanTimeout) {
			clearTimeout(fanTimeout);
			fanTimeout = undefined;
		}
		fanTimeout = setTimeout(function() {
			restartFanControl(null, {});
		}, web.env.restartFanSecond * 1000);
	});
}

function restartFanControl(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/setFanMode", {
		"mode": "auto"
	}, function(m) {
		if (fanTimeout) {
			clearTimeout(fanTimeout);
			fanTimeout = undefined;
		}
		if (socket) {
			socket.emit("restartFan" + msg.eventID, {});
			sendReturn(socket, m.payload.returnValue, "fan");
		}
	});
}

function getNetworkStatus(socket, msg) {
	lunaAPI.getNetworkStatus(function(m) {
		socket.emit("networkInfo" + msg.eventID, m.payload);
	});
}

function getMacAddr(socket, msg) {
	lunaAPI.getMacAddr(function(m) {
		socket.emit("networkInfo" + msg.eventID, m.payload);
	});
}

function setNetworkStatus(socket, msg) {

	var param = {
		method: msg.method
	};

	if(msg.method == 'manual'){
		param.address = msg.address;
		param.netmask = msg.netmask;
		param.gateway = msg.gateway;
	}

	if (msg.ssid) {
		param.ssid = msg.ssid;
	}

	luna.call("luna://com.webos.service.connectionmanager/setipv4", param, function(m) {
		var reason = "";

		if (!m.payload.returnValue) {
			reason = m.payload.errorText;
		}

		socket.emit('setNetworkStatus' + msg.eventID, m.payload);
		sendReturn(socket, {
			returnValue: m.payload.returnValue,
			reason: reason
		}, "network");
	});
}

function setNetworkDNS(socket, msg) {
	param = {
		dns: []
	};

	if (msg.dns && msg.dns !== '') {
		param.dns.push(msg.dns);
	}

	if (msg.ssid) {
		param.ssid = msg.ssid;
	};

	luna.call("luna://com.webos.service.connectionmanager/setdns", param, function(m) {
		sendReturn(socket, m.payload.returnValue, "network");
		socket.emit("setNetworkDNS" + msg.eventID, m.payload);
	});
}

function getSignageName(socket, msg) {
	lunaAPI.getSystemSettings('network', ['deviceName'], function(m) {
		socket.emit('signageName' + msg.eventID, m.payload.settings.deviceName);
	});
}

function setSignageName(socket, msg) {
	var newName = msg.signageName;
	if(newName.length > nameMaxLength){
		newName = newName.slice(0, nameMaxLength);
	}
	lunaAPI.setSystemSettings('network', {
		deviceName: newName
	}, function(m) {
		sendReturn(socket, m.payload.returnValue, 'network');
	});
}

function getFanControl(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/getAutoFanCtlTable", {
		id: msg.id,
		micomId: msg.micomId
	}, function(m) {
		socket.emit("controlTable" + msg.eventID, m.payload);
	});
}

function setFanControl(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/setAutoFanCtlTable", {
		id: msg.id,
		micomId: msg.micomId,
		zone: msg.zone,
		temp: msg.temp,
		level: msg.level
	}, function(m) {
		sendReturn(socket, m.payload.returnValue, "fan");
	});
}

function resetFanControl(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/resetAutoFanCtlTable", {}, function(m) {
		sendReturn(socket, m.payload.returnValue, "fan");
	});
}

function setCurrentTime(socket, msg) {
	exec('date +"GMT%z"', function(error, stdout, stderr) {
		var utc = msg.utc.year + '-' + msg.utc.month + '-' + msg.utc.day
				+ ' ' + msg.utc.hour + ':' + msg.utc.minute + ':00 ' + stdout;
		newTimeDate = new Date(utc);

		lunaAPI.setCurrentTime(newTimeDate.getTime() / 1000, function(m) {
			sendReturn(socket, m.payload.returnValue, "setCurrentTime");
		});
	});
}

function getCurrentTime(socket, msg) {
	lunaAPI.getCurrentTime(function(time) {
		socket.emit("currentTime" + msg.eventID, time);
	});
}

function getLocaleInfo(socket, msg) {
	luna.call("luna://com.webos.settingsservice/getSystemSettings", {
		key: "localeInfo"
	}, function(m) {
		socket.emit("locale" + msg.eventID, m.payload);
	});
}

function getHistoryInterval(socket, msg) {
	lunaAPI.getSystemSettings('commercial', ['historyIntervalSeconds'],	function(m) {
		socket.emit('historyInterval' + msg.eventID, m.payload.settings);
	});
}

function setHistoryInterval(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/setHistoryInterval", {
		second: msg.second
	}, function(m) {
		sendReturn(socket, m.payload.returnValue, "history");
	});
}

function clearHistory(socket, msg) {
	luna.call("luna://com.webos.service.tv.outdoor/clearHistory", {}, function(m) {
		sendReturn(socket, m.payload.returnValue, "history");
	});
}

function fanMicomUpdate(socket, msg) {
	luna.call('luna://com.webos.service.tv.outdoor/fanMicomDownload', {
		path: updateDir + msg.fileName
	}, function(m) {
		socket.emit("fanMicom" + msg.eventID, m.payload);
	});
}

function getPmMode(socket, msg) {
	lunaAPI.getSystemSettings('commercial', ['pmMode'],	function(m) {
		socket.emit('pmMode' + msg.eventID, m.payload.settings);
	});
}

function setPmMode(socket, msg) {
	lunaAPI.setSystemSettings('commercial', {
		pmMode: msg.pmMode
	}, function(m) {
		sendReturn(socket, m.payload.returnValue, 'pmMode');
	});
}

function checkPasswd(socket, msg) {
	var returnVal = password.checkPasswd(msg.currentPasswd);
	socket.emit('checkPass' + msg.eventID, returnVal);
}

function testPattern(socket, msg) {
	luna.call("luna://com.webos.service.tv.systemproperty/setTestPattern", {
		enable: msg.enable,
		category: 1,
		pattern: Number(msg.pattern)
	}, function(m) {
		sendReturn(socket, m.payload.returnValue, 'testPattern');	
	});
}

function getlocaleContinent(socket, msg){
	lunaAPI.getSystemSettings('commercial',["localeContinent"], function(m) {
		socket.emit('getlocaleContinent' + msg.eventID, m.payload.settings);
	});
}

function getTimeZone(socket, msg){
	luna.call("luna://com.palm.systemservice/getPreferences", {
		keys:["timeZone"]
	}, function(m) {
		socket.emit('getTimeZone' + msg.eventID, m.payload.timeZone);
	});
}

function setContinent(socket, msg) {
	lunaAPI.setSystemSettings('commercial', {
		localeContinent:msg.continent
	}, function(m) {
		socket.emit('setContinent' + msg.eventID, m.payload.returnValue);
	});
}

function setCountry(socket, msg) {
	lunaAPI.setSystemSettings('commercial', {
		localeCountry:msg.country
	}, function(m) {
		socket.emit('setCountry' + msg.eventID, m.payload.returnValue);
	});
}

function getlocaleCountry(socket, msg){
	lunaAPI.getSystemSettings('commercial', ["localeCountry"], function(m) {
		socket.emit('getlocaleCountry' + msg.eventID, m.payload.settings);
	});
}

function getCountryList(socket, msg) {
	var contryStr = fs.readFileSync(__dirname + '/env/countryListSignage.json');
	var contry = JSON.parse(contryStr);
	var continentList = contry.countryList;
	var contryList = undefined;

	continentList.forEach(function(item) {
		if(item.continent == msg.continent){
			contryList = item.countries;
		}
	});
	socket.emit('getCountryList' + msg.eventID, contryList);
}

function setCity(socket, msg) {
	luna.call("luna://com.palm.systemservice/setPreferences", {
		timeZone : msg.timeZone
	}, function(m) {
		lunaAPI.setSystemSettings('time', {
			timeZone : msg.timeZone
		}, function(m) {
			sendReturn(socket, m.payload.returnValue, 'setCity');
		});
	});
}

function getCityList(socket, msg) {
	luna.call("luna://com.palm.systemservice/getPreferenceValues", {
		key : "timeZone"
	}, function(m) {
		var cityList = [];
		m.payload.timeZone.forEach(function(item) {
			if(item.CountryCode == msg.country) {
				cityList.push(item);
			}
		});
		socket.emit('getCityList' + msg.eventID, cityList);
	});
}

function setDstOnOff(socket, msg) {
	lunaAPI.setSystemSettings('commercial', {
		dstMode: msg.dstOnOff
	}, function(m) {
		sendReturn(socket, m.payload.returnValue, 'setDstOnOff');
	});
}

function setDstStartTime(socket, msg) {
	lunaAPI.setSystemSettings('commercial', {
		dstStartMonth: msg.Month,
		dstStartWeek: msg.Week,
		dstStartDayOfWeek: msg.Weekday,
		dstStartHour: msg.Hour
	}, function(m) {
		sendReturn(socket, m.payload.returnValue, 'setDstStartTime');
	});
}

function setDstEndTime(socket, msg) {
	lunaAPI.setSystemSettings('commercial', {
		dstEndMonth: msg.Month,
		dstEndWeek: msg.Week,
		dstEndDayOfWeek: msg.Weekday,
		dstEndHour: msg.Hour
	}, function(m) {
		sendReturn(socket, m.payload.returnValue, 'setDstEndTime');
	});
}

function getDSTInfo(socket, msg) {
	lunaAPI.getSystemSettings('commercial', [
		'dstMode', 'dstStartMonth', 'dstStartWeek', 'dstStartDayOfWeek',
		'dstStartHour', 'dstEndMonth', 'dstEndWeek', 'dstEndDayOfWeek',
		'dstEndHour'
	], function(m) {
		socket.emit('getDSTInfo' + msg.eventID, m.payload.settings);
	});
}

function readTempType(socket, msg){
	var fileName = "/var/outdoorweb/tempType.json";
	fs.stat(fileName, function(err, stat){
		if( err == null ){
			fs.readFile(fileName, function(err, data){
				var tempType = JSON.parse(String(data));
				socket.emit("readTempType" + msg.eventID, tempType);
			});
		}else{
			var tempType = {isCelsius:'true'};
			socket.emit("readTempType" + msg.eventID, tempType);
		}
	});
}

function writeTempType(socket, msg){
	var fileName = "/var/outdoorweb/tempType.json";

	var isCelsius = {isCelsius:msg.isCelsius};

	fs.writeFile(fileName, JSON.stringify(isCelsius, null, 2), function(err) {
		if (err) {
			console.log(err);
		}
		sendReturn(socket, err !== undefined, 'writeTempType');
	});
}

function getPanelErrorOut(socket, msg) {
	lunaAPI.getPanelErrorOut(function(m) {
		socket.emit('getPanelErrorOut' + msg.eventID, m.payload);
	});
}

function getBacklight(socket, msg){
	lunaAPI.getSystemSettings('commercial', ["minBacklight", "maxBacklight"], function(m){
		socket.emit('getBacklight' + msg.eventID, m.payload);
	});
}

function setBacklight(socket, msg){
	var settings = (msg.id=="maxBacklight") ? {maxBacklight:msg.val} : {minBacklight:msg.val};
	lunaAPI.setSystemSettings('commercial', settings, function(m){
		sendReturn(socket, m.payload.returnValue, "picture");
		socket.emit('setBacklight' + msg.eventID, m.payload);
	});
}

function getForegroundAppInfo(socket, msg){
	lunaAPI.getForegroundAppInfo(function(m) {
		socket.emit('getForegroundAppInfo' + msg.eventID, m.payload);
	});
}

function getRunningAppInfo(socket, msg){
	lunaAPI.getRunningAppInfo(function(m) {
		socket.emit('getRunningAppInfo' + msg.eventID, m.payload);
	});
}

function getRecentsAppInfo(socket, msg){
	lunaAPI.getRecentsAppInfo(function(m) {
		socket.emit('getRecentsAppInfo' + msg.eventID, m.payload);
	});
}

function getMenuLanguage(socket, msg) {
	socket.emit('language' + msg.eventID, localeInfo.locales.UI);
}

function setMenuLanguage(socket, msg) {
	var langMap = {
		cs: 'CZ',
		da: 'DK',
		de: 'DE',
		en: 'US',
		es: 'ES',
		el: 'GR',
		fr: 'FR',
		it: 'IT',
		nl: 'NL',
		nb: 'NO',
		pt: 'PT',
		ru: 'RU',
		fi: 'FI',
		sv: 'SE',
		ko: 'KR',
		'zh-Hans': 'CN',
		ja: 'JP',
		'zh-Hant': 'HK',
		ar: 'SA'
	}

	var langCode = msg.langCode;
	if (langMap[msg.langCode]) {
		langCode += '-' + langMap[msg.langCode];
	}

	localeInfo.locales.FMT = localeInfo.locales.TV = localeInfo.locales.UI = langCode

	luna.call("luna://com.webos.settingsservice/setSystemSettings", {
		settings: {
			localeInfo: localeInfo
		}
	}, function(m) {
		socket.emit("language" + msg.eventID, m.payload);
	});
}

function getOSDLockMode(socket, msg) {
	lunaAPI.getSystemSettings('hotelMode', ["enableOsdVisibility"], function(m) {
		socket.emit('OSDLockMode' + msg.eventID, m.payload);
	});
}

function getSDMInfo(socket, msg) {
	lunaAPI.getSDMInfo(function(m) {
		socket.emit('getSDMInfo' + msg.eventID, m.payload);
	});
}

function getVideoInfo(socket, msg) {
	var videoPath = mediaDir + msg.fileName;
	lunaAPI.getVideoInfo(videoPath, function(m) {
		socket.emit('getVideoInfo' + msg.eventID, m.payload);
	});
}

function resetPictureDB(socket, msg) {
	lunaAPI.getSystemSettings('picture', ["contrast"], function(ret) {
		lunaAPI.resetSystemSettingsByDimension('picture', ret.payload.dimension, function(ret2) {
			var param = {};
			param[ret.payload.dimension.pictureMode] = false;
			lunaAPI.setSystemSettings('picture', {
				pictureSettingModified: param
			}, function(m) {
				socket.emit('resetPictureDB' + msg.eventID, m.payload);
			});
		});
	});
}

function getSystemSettingDesc(socket, msg) {
	lunaAPI.getSystemSettingDesc(msg.category, msg.keys, function(m) {
		socket.emit('getSystemSettingDesc' + msg.eventID, m.payload);
	});
}

function getHdmiPcMode(socket, msg) {
	lunaAPI.getHdmiPcMode(function (m) {
		if (!m.payload.returnValue) {
			m.payload.settings = { hdmiPcMode: {} };
		}
		socket.emit("getHdmiPcMode" + msg.eventID, m.payload.settings.hdmiPcMode);
	});
}

function setInputSouce(socket, msg) {
	lunaAPI.setInputSouce(msg.appId, function (m) {
		socket.emit('setInputSouce' + msg.eventID, m.payload);
		sendReturn(socket, m.payload.returnValue, 'input');
	});
}

function getPowerCurrent(socket, msg) {
	lunaAPI.getPowerCurrent(function (m) {
		socket.emit('getPowerCurrent' + msg.eventID, m.payload);
	});
}

function getBluMaintain(socket, msg) {
	lunaAPI.getBluMaintain(function (m) {
		socket.emit('getBluMaintain' + msg.eventID, m.payload);
	});
}

function getPowerCurrentHistory(socket, msg) {
	var param = {};
	addHistoryRange(msg, param);
	lunaAPI.getPowerCurrentHistory(param, function (m) {
		socket.emit('getPowerCurrentHistory' + msg.eventID, m.payload);
	});
}

function getBluMaintainHistory(socket, msg) {
	var param = {};
	addHistoryRange(msg, param);
	lunaAPI.getBluMaintainHistory(param, function (m) {
		socket.emit('getBluMaintainHistory' + msg.eventID, m.payload);
	});
}

function doFactoryDefault(socket, msg) {
    luna.call('luna://com.webos.service.tv.systemproperty/doFactoryDefault', {}, function(m) {
        socket.emit('system' + msg.eventID, m.payload);
    });
}

function setNTPServerAddress (socket, msg) {
	var settings = {};
	function capitalizeFirstLetter(str) {
		return str.substring(0, 1).toUpperCase() + str.substring(1, str.length).toLowerCase();
	}

	settings['ntpServer' + capitalizeFirstLetter(msg.type)] = msg.address.slice(0, 512);

	lunaAPI.setSystemSettings('commercial', settings, function(m) {
		socket.emit('setNTPServerAddress' + msg.eventID, m.payload);
	});
}

function createCommandMap() {
	commands["setMuted"] = setMuted;
	commands["setVolume"] = setVolume;
	commands["getVolume"] = getVolume;

	commands["getInputList"] = getInputList;
	commands["setHdmiInput"] = setHdmiInput;
	commands["getHdmiInput"] = getHdmiInput;

	commands["setPortrait"] = setPortrait;
	commands["getPortrait"] = getPortrait;

	commands["capture"] = capture;
	commands["thumbnail"] = getThumbnail;
	commands["emptyThumbnail"] = emptyThumbnail;

	commands["sendAlert"] = sendAlert;
	commands["sendToast"] = sendToast;
	commands["sendScrollAlert"] = sendScrollAlert;

	commands["sendRemocon"] = sendRemocon;
	commands["sendMouseEvent"] = sendMouseEvent;
	commands["sendKeyEvent"] = sendKeyEvent;

	commands["deleteFile"] = deleteFile;

	commands["listUpdates"] = listUpdates;
	commands["getUpdateStatus"] = getUpdateStatus;
	commands["swupdate"] = swupdate;
	commands["fanMicomUpdate"] = fanMicomUpdate;

	commands["getCheckScreenOn"] = getCheckScreenOn;
	commands["setCheckScreenOn"] = setCheckScreenOn;
	commands["setCheckScreenPos"] = setCheckScreenPos;
	commands["getCheckScreenInfo"] = getCheckScreenInfo;
	commands["getCheckScreenColor"] = getCheckScreenColor;
	commands["calibrateCheckScreen"] = calibrateCheckScreen;
	commands["calibrateCheckScreenEYEKey"] = calibrateCheckScreenEYEKey;
	commands["getNormalCheckScreenStatus"] = getNormalCheckScreenStatus;

	commands["getNTPStatus"] = getNTPStatus;
	commands["setNTPStatus"] = setNTPStatus;

	commands["getDoorStatus"] = getDoorStatus;
	commands["changeDoorMuteOpt"] = changeDoorMuteOpt;
	commands["resetDoorState"] = resetDoorState;

	commands["mpPressPowerBtn"] = mpPressPowerBtn;
	commands["mpPressResetBtn"] = mpPressResetBtn;
	commands["mpCheckPowerLed"] = mpCheckPowerLed;

	commands["getTempHistory"] = getTempHistory;
	commands["getFanRpmHistory"] = getFanRpmHistory;
	commands["getBacklightHistory"] = getBacklightHistory;
	commands["getEyeQSensorHistory"] = getEyeQSensorHistory;
	commands["getHumidityHistory"] = getHumidityHistory;

	commands["getTemp"] = getTemp;

	commands["getSystemProperties"] = getSystemProperties;
	commands["setSystemProperties"] = setSystemProperties;
	commands["getSystemSettings"] = getSystemSettings;
	commands["setSystemSettings"] = setSystemSettings;
	commands["resetSystemSettings"] = resetSystemSettings;
	commands["getBasicInfo"] = getBasicInfo;
	commands["getWebOSInfo"] = getWebOSInfo;
	commands["getFanMicomInfo"] = getFanMicomInfo;

	commands["getDowntimeIncident"] = getDowntimeIncident;

	commands["getFanStatus"] = getFanStatus;
	commands["getFanRPM"] = getFanRPM;
	commands["getNumFans"] = getNumFans;
	commands["getFanDuty"] = getFanDuty;
	commands["setFanDuty"] = setFanDuty;
	commands["stopFanControl"] = stopFanControl;
	commands["restartFanControl"] = restartFanControl;
	commands["getFanControl"] = getFanControl;
	commands["setFanControl"] = setFanControl;
	commands["resetFanControl"] = resetFanControl;

	commands["isNoSignal"] = isNoSignal;
	commands["getVideoSize"] = getVideoSize;
	commands["getVideoStillStatus"] = getVideoStillStatus;
	commands["resetStalledImage"] = resetStalledImageSetting;

	commands['getEmergency'] = getEmergency;
	commands['setEmergency'] = setEmergency;

	commands["turnOffScreen"] = turnOffScreen;
	commands["turnOnScreen"] = turnOnScreen;
	commands["rebootScreen"] = rebootScreen;
	commands["getPowerState"] = getPowerState;

	commands["updatePictureMode"] = updatePictureMode;
	commands["getPictureMode"] = getPictureMode;

	commands["getNetworkStatus"] = getNetworkStatus;
	commands["getMacAddr"] = getMacAddr;
	commands["setNetworkStatus"] = setNetworkStatus;
	commands["setNetworkDNS"] = setNetworkDNS;
	commands["getSignageName"] = getSignageName;
	commands["setSignageName"] = setSignageName;

	commands["getPictureDBVal"] = getPictureDBVal;
	commands["setPictureDBVal"] = setPictureDBVal;

	commands["setCurrentTime"] = setCurrentTime;
	commands["getCurrentTime"] = getCurrentTime;
	commands["getLocaleInfo"] = getLocaleInfo;
	commands["getHistoryInterval"] = getHistoryInterval;
	commands["setHistoryInterval"] = setHistoryInterval;
	commands["clearHistory"] = clearHistory;

	commands["getPmMode"] = getPmMode;
	commands["setPmMode"] = setPmMode;

	commands["checkPasswd"] = checkPasswd;

	commands["setContinent"] = setContinent;
	commands["getCountryList"] = getCountryList;
	commands["setCountry"] = setCountry;
	commands["setCity"] = setCity;
	commands["getCityList"] = getCityList;
	commands["getlocaleContinent"] = getlocaleContinent;
	commands["getlocaleCountry"] = getlocaleCountry;
	commands["getTimeZone"] = getTimeZone;
	commands["setDstOnOff"] = setDstOnOff;
	commands["setDstStartTime"] = setDstStartTime;
	commands["setDstEndTime"] = setDstEndTime;
	commands["getDSTInfo"] = getDSTInfo;
	commands['readTempType'] = readTempType;
	commands['writeTempType'] = writeTempType;
	commands["getPanelErrorOut"] = getPanelErrorOut;
	commands["getBacklight"] = getBacklight;
	commands["setBacklight"] = setBacklight;
	commands["getForegroundAppInfo"] = getForegroundAppInfo;
	commands["getRunningAppInfo"] = getRunningAppInfo;
	commands["getRecentsAppInfo"] = getRecentsAppInfo;
	commands['getOSDLockMode'] = getOSDLockMode;

	commands['getMenuLanguage'] = getMenuLanguage;
	commands['setMenuLanguage'] = setMenuLanguage;
	commands['getSDMInfo'] = getSDMInfo;
	commands['getVideoInfo'] = getVideoInfo;
	commands["setInputSouce"] = setInputSouce;

	commands["testPattern"] = testPattern;
	commands['testsuiteForDIL'] = testsuiteForDIL;
	commands['testsuiteForEyeQ'] = testsuiteForEyeQ;

	commands['resetPictureDB'] = resetPictureDB;
	commands['getSystemSettingDesc'] = getSystemSettingDesc;

	commands['getHdmiPcMode'] = getHdmiPcMode;

	commands["getPowerCurrent"] = getPowerCurrent;
	commands["getBluMaintain"] = getBluMaintain;
	commands["getPowerCurrentHistory"] = getPowerCurrentHistory;
	commands["getBluMaintainHistory"] = getBluMaintainHistory;

	commands["setNTPServerAddress"] = setNTPServerAddress;
}

var captureType = 'DISPLAY';

function addSubscriptions() {
	if (web.env.supportXE) {
		startDoorSubscription();
	}

	var swupdate = luna.subscribe("luna://com.webos.service.update/getProgress", {
		subscribe: true
	});

	swupdate.on("response", function(msg) {
		if (msg.payload.progress !== undefined) {
			io.emit("swupdate", msg.payload.progress);
		}
	});

	var micom = luna.subscribe("luna://com.webos.service.tv.swupdate/getUpdateProgressEx", {
		type:"micom",
		subscribe: true
	});

	micom.on("response", function(msg) {
		if (msg.payload.progress) {
			io.emit("micom", msg.payload.progress);
		}
	});

	var capture = luna.subscribe("palm://com.palm.webappmanager/vtgStatusChanged", {
		subscribe: true
	});

	capture.on("response", function(msg) {
		captureType = msg.payload.status === 'activated' ? 'GRAPHIC' : 'DISPLAY';
	});

	if ( !web.env.supportLocale ){
		web.env.locale = 'en-US';
		web.env.localeTable = Locale;
		Locale.init(web.env);
		console.log('not support locale');
		return;
	}

	var language = luna.subscribe('luna://com.webos.settingsservice/getSystemSettings', {
		keys: ['localeInfo'],
		subscribe: true
	});

	language.on('response', function(ret) {
		localeInfo = ret.payload.settings.localeInfo;
		var code = localeInfo.locales.UI;
		web.env.locale = code;
		web.env.localeTable = Locale;
		Locale.callback = function() {
			// io.emit("locale", localeInfo.locales.UI);
		};
		Locale.init(web.env);
		LanguageManager.init(web.env, code, function () {
			io.emit("locale", code);
		});
	});

	if (web.env.supportLedSignage) {
		var picture = luna.subscribe('luna://com.webos.settingsservice/getSystemSettings', {
			category: 'picture',
			subscribe: true
		});

		var pictureFirst = true;
		picture.on('response', function(ret) {
			if (pictureFirst) {
				pictureFirst = false;
				return;
			}
			ledAPI.pictureChangeHandler(ret.payload.dimension, ret.payload.settings);
		});

		var colorTemp = luna.subscribe('luna://com.webos.settingsservice/getSystemSettings', {
			category: 'commercial',
			keys: ['ledColorTempDegree'],
			subscribe: true
		});
		
		var firstColorTemp = true;
		colorTemp.on('response', function(ret) {
			if (firstColorTemp) {
				firstColorTemp = false;
				return;
			}
			ledAPI.colorTempChangeHandler(ret.payload.settings.ledColorTempDegree);
		});
	}
}

var Locale = {
	langTable: {},
	etcTable: {},
	callback: function() {},

	getTable: function(localeCode, idx) {
		function getLocaleDir(code, index) {
			var ret = __dirname + '/resources/';
			for (var i = 0; i <= index; ++i) {
				ret += code[i] + '/';
			}

			ret += 'strings.json';

			return ret;
		}

		if (localeCode.length == idx) {
			var code = localeCode[0];
			if ( (code == 'zh' && localeCode[1] != 'Hans') 
				|| (code == 'pt' && localeCode.length > 1)) {
				code = localeCode.join('/');
			}
			Locale.getEtcTable(code, Locale.callback);
			return;
		}

		var dir = getLocaleDir(localeCode, idx);

		fs.readFile(dir, function(err, data) {
			if (!err) {
				Locale.insertToTable(JSON.parse(data));
			}
			Locale.getTable(localeCode, idx + 1);
		});
	},

	getEtcTable: function(locale, callback) {
		var localeArr = locale.split('/');

		fs.readFile(__dirname + '/env/languageEtc.json', function(err, data) {
			if (!err) {
				var code = '';
				for (var i = 0; i != localeArr.length; ++i) {
					if (code != 0) {
						code += '/';
					}
					code += localeArr[i];
					var ret = JSON.parse(data);
					var table = ret[code];

					for (var key in table) {
						Locale.etcTable[key] = table[key];
					}
				}
				callback();
			} else { 
				Locale.langTable = {};
				console.log('fail to get ETC lang table');	
				callback();
			}
		});
	},

	setLocale: function(locale) {
		var localeCode = locale.split('-');
		Locale.getTable(localeCode, 0);
	},

	insertToTable: function(table) {
		for (var key in table) {
			Locale.langTable[key] = table[key];
		}
	},

	init: function(env) {
		Locale.langTable = {};
		Locale.etcTable = {};
		Locale.setLocale(env.locale);
	}
}

function startDoorSubscription(mute) {
	if (mute) {
		param.muteOpt = Number(mute);
	}

	var doorSubscription = luna.subscribe("luna://com.webos.service.tv.outdoor/getDoorState", {
		subscribe: true
	});

	doorSubscription.on("response", function(msg) {
		io.emit("door", msg.payload);
	});
}

function setDefaultTimezone() {
	var timeZone = {
		offsetFromUTC: -600,
		City: "Adak",
		supportsDST: 1,
		CountryCode: "US",
		Description: "Hawaii-Aleutian Time",
		Country: "United States of America",
		ZoneID: "America/Adak",
		preferred: true
	};

	lunaAPI.setSystemSettings('time', {
		timeZone: timeZone
	}, function(m) {
		if (!m.payload.returnValue) {
			console.log(m.payload);
		}
	});

	luna.call('luna://com.palm.systemservice/setPreferences', {
		timeZone: timeZone
	}, function(ret) {
		if (!ret.payload.returnValue) {
			console.log(ret.payload);
		}
	});
}

function setOsdVisibility() {
	luna.call("luna://com.webos.settingsservice/setSystemSettingDesc", {
		category: "picture",
		key: "motionEyeCare",
		ui: {
			active: false,
			visible: false,
			widget: "None"
		}
	}, function(m) {
	});
}

var sharedsession;

function init(l, s, se) {
	luna = l;
	server = s;
	io = require('socket.io')(server);

	sharedsession = require("express-socket.io-session");
	io.use(sharedsession(se));

	createCommandMap();

	io.on('connection', function(socket) {
		if (!socket.handshake.session.login) {
			console.log("socket.io without login");
			socket.disconnect('unauthorized');
			return;
		}

		socket.on('api', function(msg) {
			var func = commands[msg.command];
			if (func) {
				func(socket, msg);
			}
		});

		socket.on('group', function(msg) {
			groupAPI.commandHandler(socket, msg);
		});

		socket.on('led', function(msg) {
			ledAPI.commandHandler(socket, msg);
		});

		socket.on('ledfilm', function(msg) {
			ledFilmAPI.commandHandler(socket, msg);
		});
	});
	exports.io = io;

	addSubscriptions();

	if (web.env.supportLedSignage) {
		setOsdVisibility();
	}
}

exports.init = init;

exports.luna = function() {
	return luna;
};

exports.sendReturn = sendReturn;

exports.getCaptureType = function() {
	return captureType;
};

exports.setDefaultTimezone = setDefaultTimezone;
