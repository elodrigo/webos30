var fs = require('fs');
var web = require('./web');
var api = require('./api');

var luna = undefined;
var remoCode = {};

function getSystemSettingsByDimension(category, dimension, keys, callback) {
	luna.call("luna://com.webos.settingsservice/getSystemSettings", {
		category: category,
		dimension: dimension,
		keys: keys
	}, callback);
}

function setSystemSettingsByDimension(category, dimension, settings, callback) {
	luna.call("luna://com.webos.settingsservice/setSystemSettings", {
		category: category,
		dimension: dimension,
		settings: settings
	}, callback);
}

function getSystemSettings(category, keys, callback) {
	luna.call("luna://com.webos.settingsservice/getSystemSettings", {
		category: category,
		keys: keys
	}, callback);
}

function setSystemSettings(category, settings, callback) {
	luna.call("luna://com.webos.settingsservice/setSystemSettings", {
		category: category,
		settings: settings
	}, callback);
}

function getSystemProperties(keys, callback) {
	luna.call("luna://com.webos.service.tv.systemproperty/getProperties", {
		keys: keys
	}, callback);
}

function setSystemProperties(properties, callback) {
	luna.call("luna://com.webos.service.tv.systemproperty/setProperties",
		properties, callback);
}

function getCurrentTimeLuna(callback) {
	luna.call("luna://com.palm.systemservice/time/getSystemTime",{}, callback);
}

function setCurrentTime(utc, callback) {
	luna.call("luna://com.palm.systemservice/time/setSystemTime",{ utc:utc }, callback);
}

function getCurrentTime(callback) {
	getCurrentTimeLuna(function(time){
		var currentdate = new Date();

		var proc = String(fs.readFileSync('/proc/uptime'));
		proc = proc.substr(0, proc.indexOf(' '));
		var uptime = Number(proc);
		var second = Math.floor(uptime) % 60;
		var minute = Math.floor(uptime / 60) % 60;
		var hours = Math.floor(uptime / 60 / 60) % 24;
		var days = Math.floor(uptime / 60 / 60 / 24);

		var strUpTime = "" + days + " Day " + hours + " H " + minute + " M " + second + " S";

		var localtime = time.payload.localtime;

		var year =  localtime.year;
		var month =  localtime.month;
		var day =  localtime.day;
		var hour = localtime.hour;
		var minute = localtime.minute;
		var second = localtime.second;

		var date = new Date(year,month-1,day,hour,minute,second);

		var ret = {
			current: date.toString(),
			uptime: strUpTime,
			year : year,
			month : month,
			day : day,
			hour : hour,
			minute : minute
		};

		callback(ret);
	});
}

function getTemperature(id, callback) {
	luna.call("luna://com.webos.service.tv.outdoor/getTemp", {
		id: id
	}, callback);
}

function getAllTemperature(callback) {
	var sensors = web.env.model.tempSensor;
	var completed = 0;
	var ret = {
		returnValue: true
	};

	sensors.forEach(function(sensor) {
		getTemperature(sensor.id, function(temp) {
			if (!temp.payload.returnValue) {
				ret.returnValue = false;
			}
			ret[sensor.id] = temp.payload.temperature;
			++completed;
			if (completed == sensors.length) {
				callback(ret);
			}
		});
	});
}

function screenShot(path, height, callback, format) {
	if (!format) {
		format = 'jpg';
	}

	var data = {
		path: path,
		method: api.getCaptureType(),
		width: Math.round((height * web.env.screen.width / web.env.screen.height)),
		height: height,
		format: format 
	};

	luna.call('luna://com.webos.service.tv.capture/executeOneShot', data, function(msg) {
		callback(msg);
	});
}

function getNetworkStatus(callback) {
	luna.call("luna://com.webos.service.connectionmanager/getstatus", {}, function(net) {
        if (!web.env.supportIPv6) {
            net.payload.wired.ipv6 = undefined;
            net.payload.wifi.ipv6 = undefined;
        }

		getMacAddr(function(mac) {
			if (mac.payload.returnValue) {
				net.payload.wired.macAddress = mac.payload.wiredInfo ? mac.payload.wiredInfo.macAddress : 'None';
				net.payload.wifi.macAddress = mac.payload.wifiInfo ? mac.payload.wifiInfo.macAddress : 'None';
			}
			callback(net);
		});
	});
}

function getMacAddr(callback) {
	luna.call("luna://com.webos.service.connectionmanager/getinfo", {}, callback);
}

function getDoorStatus(callback) {
	luna.call("luna://com.webos.service.tv.outdoor/getDoorState", {}, callback);
}

function getFanStatus(callback) {
	luna.call("luna://com.webos.service.tv.outdoor/getFanStatus", {}, callback);
}

function getNumFans(callback) {
	luna.call("luna://com.webos.service.tv.outdoor/getNumFans", {}, callback);
}

function getDowntimeIncident(callback) {
	luna.call("luna://com.webos.service.tv.outdoor/getDowntimeIncident", {}, callback);
}

function getCheckScreenOn(callback) {
	getSystemSettings('commercial', ['checkScreen'], callback);
}

function getCheckScreenInfo(callback) {
	luna.call("luna://com.webos.service.tv.outdoor/getCheckScreenInfo", {}, callback);
}

function calibrateCheckScreenEYEKey(callback) {
	luna.call("luna://com.webos.service.tv.outdoor/calibrateCheckScreenEYEKey", {}, callback);
}

function isNoSignal(callback) {
	luna.call("luna://com.webos.service.tv.signage/isNoSignal", {}, callback);
}

function getVideoSize(callback) {
	luna.call("luna://com.webos.service.tv.broadcast/getPipelineTemporary", {}, function(msg) {
		var id = msg.payload.broadcastId;
		luna.call("luna://com.webos.service.tv.display/getCurrentVideo", {
			context: id
		}, function(msg1) {
			if (msg1.payload.returnValue === false) {
				luna.call("luna://com.webos.service.tv.signage/getVideoSize", {}, function(msg2) {
					var source = msg2.payload.videoSize.source;
					callback({
						payload: {
							width: source.width,
							height: source.height,
							timingMode: 'PC',
							returnValue: true
						}
					});
				});
				return;
			}
			callback(msg1);
		});
	});
}

function getVideoStillStatus(callback) {
	luna.call("luna://com.webos.service.tv.outdoor/isStalledImage", {}, callback);
}

function getBasicInfo(callback) {
	var keys = ["firmwareVersion", "micomVersion", "serialNumber",
		"hardwareVersion", "bootLoaderVersion", "sdkVersion", "modelName"];
	getSystemProperties(keys, callback);
}

function getInputList(callback) {
	luna.call('luna://com.webos.service.eim/getAllInputStatus', {}, callback);
}

function getCurrentInput(callback) {
	luna.call("luna://com.webos.service.tv.externaldevice/getCurrentInput", {}, callback);
}

function setCurrentInput(id, callback) {
	sendRemocon(id, callback);
}

function sendRemocon(key, callback) {
	var code = remoCode[key];

	if (!code) {
		return;
	}

	if (code == "signage") {
		luna.call("luna://com.webos.service.tvpower/power/getPowerState", {}, function(ret) {
			if (ret.payload.state == 'Screen Off') {
				luna.call("luna://com.webos.service.tv.legacyinput/createKeyEvent", {
					type: "single",
					code: remoCode['ok']
				}, callback);
			} else {
				luna.call("luna://com.webos.service.tv.legacyinput/createKeyEvent", {
					type: "single",
					code: remoCode['exit']
				}, luna.call("luna://com.palm.applicationManager/launch", {
					id: "com.webos.app.installation"
				}, callback));
			}
		});
		return;
	}

	var sub = luna.call("luna://com.webos.service.tv.legacyinput/createKeyEvent", {
		type: "single",
		code: code
	}, callback);
}

function getVolume(callback) {
	luna.call("luna://com.webos.audio/getVolume", {}, callback);
}

function setVolume(volume, callback) {
	luna.call("luna://com.webos.audio/setVolume", {
		volume: volume
	}, callback);
}

function setMuted(muted, callback) {
	luna.call("luna://com.webos.audio/setMuted", {
		muted: muted
	}, callback);
}

function getWebOSInfo(callback) {
	luna.call("luna://com.palm.systemservice/osInfo/query", {
		parameters: ["core_os_release", "core_os_release_codename"]
	}, callback);
}

function getFanMicomInfo(callback) {
	luna.call("luna://com.webos.service.tv.outdoor/getFanMicomVersion", {}, callback);
}

function getTempHistory(param, callback) {
	luna.call("luna://com.webos.service.tv.outdoor/getTempHistory", param, callback);
}

function getFanRpmHistory(param, callback) {
	luna.call("luna://com.webos.service.tv.outdoor/getFanRpmHistory", param, callback);
}

function getBacklightHistory(param, callback) {
	luna.call("luna://com.webos.service.tv.outdoor/getBacklightHistory", param, callback);
}

function getContrastHistory(param, callback) {
	luna.call("luna://com.webos.service.tv.outdoor/getContrastHistory", param, callback);
}

function getEyeQSensorHistory(param, callback) {
	luna.call("luna://com.webos.service.tv.outdoor/getEyeQSensorHistory", param, callback);
}

function getHumidityHistory(param, callback) {
	luna.call("luna://com.webos.service.tv.outdoor/getHumidityHistory", param, callback);
}

function getPanelErrorOut(callback){
	luna.call("luna://com.webos.service.tv.outdoor/getPanelErrorOut", {subscribe:true}, callback);
}

function getBacklight(param, callback){
	luna.call("luna://com.webos.service.tv.outdoor/getBacklight", param, callback);
}

function setBacklight(param, callback){
	luna.call("luna://com.webos.service.tv.outdoor/setBacklight", param, callback);
}

function turnOffScreen(callback) {
	luna.call("luna://com.webos.service.tvpower/power/turnOffScreen", {}, callback);
}

function turnOnScreen(callback) {
	luna.call("luna://com.webos.service.tvpower/power/turnOnScreen", {}, callback);
}

function getForegroundAppInfo(callback){
	luna.call("luna://com.webos.applicationManager/getForegroundAppInfo", {}, callback);
}

function getRunningAppInfo(callback){
	luna.call("luna://com.webos.applicationManager/running", {}, callback);
}

function getRecentsAppInfo(callback){
	luna.call("luna://com.webos.surfacemanager/getRecentsAppList", {}, callback);
}

function getMediaList(path, offset, callback){
	luna.call("luna://com.webos.service.attachedstoragemanager/listFolderContents", {
		deviceId: "INTERNAL_STORAGE_SIGNAGE",
		path: path,
		offset: offset,
		limit: 100,
		requestType: "byItemType",
		itemType: ["all"]
	}, callback);
}

function getVideoInfo(videoPath, callback){
	luna.call("luna://com.webos.service.videothumbnailer/getVideoInfo", {
		videoPath: videoPath
	}, callback);
}

function getSDMInfo(callback){
	luna.call("luna://com.webos.service.tv.outdoor/getSDMInfo", {"subscribe":true}, callback);
}

function createRemoCodeMap() {
	remoCode = JSON.parse(fs.readFileSync(__dirname + '/env/remocode.json'));

	var model = remoCode.input[web.env.model.name];
	if (!model) {
		model = remoCode.input.default;
	}
	remoCode.RGB_1 = model.RGB_1;
	remoCode.HDMI_1 = model.HDMI_1;
	remoCode.HDMI_2 = model.HDMI_2;
	remoCode.HDMI_3 = model.HDMI_3;
	remoCode.HDMI_4 = model.HDMI_4;
}

function resetSystemSettings(category, keys, callback) {
	luna.call("luna://com.webos.settingsservice/resetSystemSettings", {
		category: category,
		keys: keys
	}, callback);
}

function resetSystemSettingsByDimension(category, dimension, callback) {
	luna.call("luna://com.webos.settingsservice/resetSystemSettings", {
		category: category,
		dimension: dimension
	}, callback);
}

function getSystemSettingDesc(category, keys, callback) {
	luna.call("luna://com.webos.settingsservice/getSystemSettingDesc", {
		category: category,
		keys: keys
	}, callback);
}

function getHdmiPcMode(callback) {
	luna.call("luna://com.lge.settingsservice/getSystemSettings", { category: "other", keys: ["hdmiPcMode"] }, callback);
}

function setInputSouce(appId, callback) {
	luna.call('luna://com.palm.applicationManager/launch', { id: appId }, callback);
}

function getPowerCurrent(callback) {
	luna.call('luna://com.webos.service.tv.outdoor/getPowerCurrent', {}, callback);
}

function getBluMaintain(callback) {
	luna.call('luna://com.webos.service.tv.outdoor/getBluMaintain', {}, callback);
}

function getPowerCurrentHistory(param, callback) {
	luna.call('luna://com.webos.service.tv.outdoor/getPowerCurrentHistory', param, callback);
}

function getBluMaintainHistory(param, callback) {
	luna.call('luna://com.webos.service.tv.outdoor/getBluMaintainHistory', param, callback);
}

function init(l) {
	luna = l;
}

exports.getSystemSettingsByDimension = getSystemSettingsByDimension;
exports.setSystemSettingsByDimension = setSystemSettingsByDimension;
exports.getSystemSettings = getSystemSettings;
exports.setSystemSettings = setSystemSettings;
exports.getSystemProperties = getSystemProperties;
exports.setSystemProperties = setSystemProperties;

exports.getCurrentTime = getCurrentTime;
exports.setCurrentTime = setCurrentTime;
exports.screenShot = screenShot;
exports.getTemperature = getTemperature;
exports.getAllTemperature = getAllTemperature;
exports.getNetworkStatus = getNetworkStatus;
exports.getMacAddr = getMacAddr;
exports.getDoorStatus = getDoorStatus;
exports.getFanStatus = getFanStatus;
exports.getNumFans = getNumFans;
exports.getDowntimeIncident = getDowntimeIncident;
exports.getCheckScreenOn = getCheckScreenOn;
exports.getCheckScreenInfo = getCheckScreenInfo;
exports.calibrateCheckScreenEYEKey = calibrateCheckScreenEYEKey;

exports.isNoSignal = isNoSignal;
exports.getVideoSize = getVideoSize;
exports.getVideoStillStatus = getVideoStillStatus;

exports.getBasicInfo = getBasicInfo;

exports.getInputList = getInputList;
exports.getCurrentInput = getCurrentInput;
exports.setCurrentInput = setCurrentInput;

exports.sendRemocon = sendRemocon;

exports.getVolume = getVolume;
exports.setVolume = setVolume;
exports.setMuted = setMuted;

exports.getWebOSInfo = getWebOSInfo;
exports.getFanMicomInfo = getFanMicomInfo;

exports.getTempHistory = getTempHistory;
exports.getFanRpmHistory = getFanRpmHistory;
exports.getBacklightHistory = getBacklightHistory;
exports.getContrastHistory = getContrastHistory;
exports.getEyeQSensorHistory = getEyeQSensorHistory;
exports.getHumidityHistory = getHumidityHistory;
exports.getPanelErrorOut = getPanelErrorOut;
exports.getBacklight = getBacklight;
exports.setBacklight = setBacklight;
exports.turnOnScreen = turnOnScreen;
exports.turnOffScreen = turnOffScreen;
exports.getForegroundAppInfo = getForegroundAppInfo;
exports.getRunningAppInfo = getRunningAppInfo;
exports.getRecentsAppInfo = getRecentsAppInfo;
exports.getSDMInfo = getSDMInfo;

exports.init = init;
exports.createRemoCodeMap = createRemoCodeMap;

exports.getMediaList = getMediaList;
exports.getVideoInfo = getVideoInfo;

exports.resetSystemSettings = resetSystemSettings;
exports.resetSystemSettingsByDimension = resetSystemSettingsByDimension;
exports.getSystemSettingDesc = getSystemSettingDesc;

exports.getHdmiPcMode = getHdmiPcMode;
exports.setInputSouce = setInputSouce;

exports.getPowerCurrent = getPowerCurrent;
exports.getBluMaintain = getBluMaintain;
exports.getPowerCurrentHistory = getPowerCurrentHistory;
exports.getBluMaintainHistory = getBluMaintainHistory;