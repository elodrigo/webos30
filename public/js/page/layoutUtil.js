function unitSelectHandler(id, curUnit) {
	if(env.supportLedFilm){
		var width = curUnit.w / 16;
		var height = curUnit.h / 16;
		var tableStr = '';
		var moduleNum = 1;

		for(var i=0; i<height; i++){
			tableStr += '<tr>';
			for(var j=0; j<width; j++){
				tableStr +='<td>'+Locale.getText("Modules")+moduleNum+'</td>';
				moduleNum++;
			}
			tableStr += '</tr>';
		}

		$('#moduleDetailTable').html(tableStr);

		getLedPowerState( (id-1), function(msg) {
			if(msg.powerState){
				$('#unitPower').html('OK');
				getLedSignalState( (id-1), function(msg){
					$('#unitSignal').html(msg.signalState ? 'OK' : 'NG');
				});

				getLedFilmTempHumidity( (id-1), function(msg){
					readTempType(function(tempType){
						isCelsius = tempType.isCelsius;
						if( isCelsius == 'true' ){
							$('#unitTemp').html(Math.floor(msg.temperature) + '°C');
						}else{
							$('#unitTemp').html(Math.floor(msg.temperature*1.8+32) + '°F');
						}
					});
				});
				$('#unitCalBtn').prop('disabled', false);
				$('#moduleCalBtn').prop('disabled', false);
			}else{
				$('#unitPower').html('NG');
				$('#unitSignal').html('-');
				$('#unitTemp').html('-');
				$('#unitCalBtn').prop('disabled', true);
				$('#moduleCalBtn').prop('disabled', true);
			}
		});
	}else{
		getLedPowerStatus(id * 2, function(msg) {
			$('#unitPower').html(msg.isPowerOk ? 'OK' : 'NG');

			$('#moduleCalBtn').prop('disabled', !msg.isPowerOk);

			if (!msg.isPowerOk) {
				$('#unitTemp').html('-');

				$('#unitSignal').html('-');

				for (var i = 1; i != 5; ++i) {
					$('#moduleTemp' + i).html('-');
				}

				for (var i = 0; i != 4; ++i) {
					$('#moduleCurrent' + (i + 1)).html('-');
				}

				return;
			}

			getLedTemperature(id * 2, function(msg) {
				$('#unitTemp').html(msg.temperature[0] + '°C');
				for (var i = 1; i != 5; ++i) {
					$('#moduleTemp' + i).html(msg.temperature[i] + '°C');
				}
			});

			getLedCurrent(id * 2, function(msg) {
				for (var i = 0; i != 4; ++i) {
					$('#moduleCurrent' + (i + 1)).html(msg.current[i] + 'mA');
				}
			});

			getLedSignalStatus(id * 2, function(msg) {
				$('#unitSignal').html(msg.isSignalOk ? 'OK' : 'NG');
			});
		});
	}

}

function drawUnitSelect() {

	var id = [];
	var val = [];
	mainCanvas.rectList.forEach(function(rect) {
		id.push(rect.id);
		val.push(rect.id);
	});

	$('#unitSelect').html('');
	drawDropDownBox('unitSelect', id, val, 'dropboxType3', function (selected) {
		curUnit = mainCanvas.rectList[selected - 1];
		refreshLed();
		unitSelectHandler(Number(selected), curUnit);
	}, 1);
}




function createCanvas(){}
function changeCanvas(){}


/**
 * Modal
 */
var outer = {
	width: 1920,
	height: 1080
};

function saveConfig(curConfig) {
	var unitList = [];
	curConfig.rectList.forEach(function(rect) {
		var unit = {
			id: rect.id,
			x: rect.x,
			y: rect.y,
			w: rect.w,
			h: rect.h,
		};

		if(unit.x < 0){
			unit.x = 0;
		}

		if(unit.y < 0){
			unit.y = 0;
		}

		if(env.supportLedFilm){
			unit.inputDir = rect.inputDir,
			unit.portLength = rect.portLength,
			unit.portUse = rect.portUse
			unit.trim = rect.trim;

			if(rect.portUse.length > 0){
				if( unit.inputDir < 2 ){
					unit.w = parseInt(unit.portLength) * 16;
					unit.h = ( unit.portUse.length > 0 ? unit.portUse.length : 2 ) * 16;
				}else{
					unit.h = parseInt(unit.portLength) * 16;
					unit.w = ( unit.portUse.length > 0 ? unit.portUse.length : 2 ) * 16;
				}
			}
		}

		unitList.push(unit);
	});

	var config = {
		from: curConfig.from,
		direction: curConfig.direction,
		resolution: {
			w: curConfig.resolX,
			h: curConfig.resolY
		},
		viewPort: curConfig.viewPort,
		unitList: unitList,
		physicalLayout : curConfig.physicalLayout
	};
	setLedConfig(config);
}

function applyConfig(curConfig) {
	saveConfig(curConfig);
	setSlaveAddr(curConfig.rectList.length, function() {
		curConfig.rectList.forEach(function(rect) {
			if(env.supportLedFilm){
				setFilmPosition(rect.id * 2, rect.x - rect.trim.left , rect.y);
				setInputDirection(rect.id * 2, rect.inputDir);

				for(var i=0; i<rect.h/16; i++){
					for(var j=0; j<rect.w/16; j++){
						setModuleCalibration(rect.id*2, i, j, 15);
						saveModuleCalibration(rect.id*2);
					}
				}

				setTrim(rect.id * 2, rect.trim.left, rect.trim.right);

				var portWidth = rect.portUse.length > 0 ? rect.portUse.length : 2 ;
				setWidthHeight(rect.id * 2, portWidth, rect.portLength);

				if(rect.portUse.length > 0){
					var displayPort = 0;
					rect.portUse.forEach(function(portNum){
						displayPort += Math.pow(2, portNum-1);
						setDisplayPortSel(rect.id * 2, displayPort);
					});
				}else{
					setDisplayPortSel(rect.id * 2, 15);
				}
				setPosition(rect.id * 2, rect.x - rect.trim.left, rect.y);
				saveConfigAllFpga();
			}else{
				setPosition(rect.id * 2, rect.x, rect.y);
			}
		});
	});
}

function clearSelection() {
	if(document.selection && document.selection.empty) {
		document.selection.empty();
	} else if(window.getSelection) {
		var sel = window.getSelection();
		sel.removeAllRanges();
	}
}

function restrictRect(inside, outside) {
	var restricted = false;
	if (inside.x < outside.x) {
		inside.x = outside.x;
		restricted = true;
	}
	if (inside.y < outside.y) {
		inside.y = outside.y;
		restricted = true;
	}
	if (inside.x + inside.w > outside.x + outside.w) {
		inside.x = outside.x + outside.w - inside.w;
		restricted = true;
	}
	if (inside.y + inside.h > outside.y + outside.h) {
		inside.y = outside.y + outside.h - inside.h;
		restricted = true;
	}

	return restricted;
}

function changeResolution(resol) {
	var config = LayoutEditorModal.config;
	var numX = config.viewPort.w / config.resolX;
	var numY = config.viewPort.h / config.resolY;
	
	var resolTxt = resol.split('x');
	config.resolX = Number(resolTxt[0]);
	config.resolY = Number(resolTxt[1]);

	var maxViewX = Math.floor(outer.width / config.resolX);
	var maxViewY = Math.floor(outer.height / config.resolY)

	if (numX > maxViewX) {
		numX = maxViewX
	}
	if (numY > maxViewY) {
		numY = maxViewY;
	}

	config.viewPort.w = numX * config.resolX;
	config.viewPort.h = numY * config.resolY;
	config.maxX = numX;
	config.maxY = numY;
			
	restrictRect(config.viewPort, {x: 0, y: 0, w: outer.width, h: outer.height});
	drawRowColSelect(config);
}

function drawLayoutSelect() {
	var config = LayoutEditorModal.config;
	drawDropDownBox('resolSelect', [
		'384x360', '256x240', '192x180', '384x180'
	], [
		'384 x 360', '256 x 240', '192 x 180', '384 x 180'
	], 'dropboxType3', function (selected) {
		changeResolution(selected);
		LayoutEditorModal.rearrange();
	}, '256x240');

	if(!env.supportLedFilm){
		drawDropDownBox('daisyFromSelect', [
			'0', '1', '2', '3'
		],
		[
			'Top Left', 'Top Right', 'Bottom Left', 'Bottom Right'
		], 'dropboxType2', function (selected) {
			config.from = Number(selected);
			LayoutEditorModal.rearrange();
		}, '0');
		drawDropDownBox('daisyDirSelect', [
			'0', '1'
		],
		[
			'Horizontal', 'Vertical'
		], 'dropboxType2', function (selected) {
			config.direction = Number(selected);
			LayoutEditorModal.rearrange();
		}, '0');
	}
}

function drawRowColSelect(config) {
	$('#frameRowSelect').html('');
	$('#frameColumnSelect').html('');

 	var row = Math.floor(outer.width / config.resolX);
	var col = Math.floor(outer.height / config.resolY);

	var rows = [];
	for (var i = 0; i < row; ++i) {
		rows.push(i + 1);
	}
	var cols = [];
	for (var i = 0; i < col; ++i) {
		cols.push(i + 1);
	}

	drawDropDownBox('frameRowSelect', rows, rows, 'dropboxType2', function (selected) {
		config.maxX = Number(selected);
		config.viewPort.w = config.resolX * config.maxX;
		restrictRect(config.viewPort, {x: 0, y: 0, w: outer.width, h: outer.height});
		applyConfig(config);
		refreshLed(config);
		compareLayout(config);
	}, config.viewPort.w / config.resolX);

	drawDropDownBox('frameColumnSelect', cols, cols, 'dropboxType2', function (selected) {
		config.maxY = Number(selected);
		config.viewPort.h = config.resolY * config.maxY
		restrictRect(config.viewPort, {x: 0, y: 0, w: outer.width, h: outer.height});
		applyConfig(config);
		refreshLed(config);
		compareLayout(config);
	}, Math.floor(config.viewPort.h / config.resolY) );
}