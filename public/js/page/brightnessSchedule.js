var CUR_BRIGHTNESS_STATUS = false;
var CUR_BRIGHTNESS_SCHEDULE = 0;
var MAX_BRIGHTNESS_SCHEDULE = 6;
var scheduleList;
var brightnessNewSchedule = {
	hour: 12,
	minute: 0,
	backlight: 70	// use 'backlight' instead of 'brightness' in this function
};

function initBrightnessSchedule() {
	getBrightnessSchedule();
	getBrightnessMode();
	setBrightnessMode();
	setBrightnessSchedule();
}

function showTableContents() {
	$("#brightnessScheduleTable tbody").empty();
	if (CUR_BRIGHTNESS_SCHEDULE > 0) {
		for (var i = 0; i < CUR_BRIGHTNESS_SCHEDULE; i++) {
			var hour = scheduleList[i].hour + ":" + (scheduleList[i].minute < 10 ? "0" : "") + scheduleList[i].minute + " " + (scheduleList[i].hour >= 12 ? 'PM' : 'AM');
			$('#brightnessScheduleTable tbody').append(
				$('<tr data-id="' + i + '">').append(
					$('<td class="scheduleTxt" width="80%" style="padding-left:10px;">').append(hour + ' ' + '(' + scheduleList[i].backlight + ')'),
					$('<td style="float:right;">').append('<button class="brightnessScheduleDel darkGrayBtn">X</button>')
				)
			)
		}
	} else {
		$('#brightnessScheduleTable').append(
			$('<tr>').append(
				$('<td width="100%" align="center" colspan="2">').append(Locale.getText('There is no list.'))
			)
		)
	}
}

function getBrightnessMode() {
	getSystemSettings('commercial', ['easyBrightnessMode'], function (ret) {
		if (ret.easyBrightnessMode === 'on') {
			CUR_BRIGHTNESS_STATUS = true;
			$('#brightnessScheduleEnable').attr('checked', true);
		} else {
			CUR_BRIGHTNESS_STATUS = false;
			$('#brightnessScheduleEnable').attr('checked', false);
		}
		setUpButtons();
	})
}

function getBrightnessSchedule() {
	getSystemSettings('commercial', ['easyBrightnessSchedule'], function (ret) {
		scheduleList = JSON.parse(ret.easyBrightnessSchedule);
		CUR_BRIGHTNESS_SCHEDULE = scheduleList.length;
		showTableContents();
	})
}

function setUpButtons() {
	if (CUR_BRIGHTNESS_STATUS) { // ON Brightness Scheduling
		$(".scheduleTxt").css("pointer-events", "auto");
		$('#brightnessScheduleAdd').removeAttr('disabled');
		$('#contrast').attr('disabled', true);
		$('#energySavingSelectDrop').attr('disabled', true);
		if (CUR_BRIGHTNESS_SCHEDULE > 0) {
			$('.brightnessScheduleDel').attr('disabled', false);
			$('#brightnessScheduleDelAll').removeAttr('disabled');
			if (CUR_BRIGHTNESS_SCHEDULE === MAX_BRIGHTNESS_SCHEDULE) {
				$('#brightnessScheduleAdd').attr('disabled', true);
			}
		} else {
			$('#brightnessScheduleDelAll').attr('disabled', true);
		}
	} else { // OFF Brightness Scheduling
		$('#brightnessScheduleAdd').attr('disabled', true);
		$('#brightnessScheduleDelAll').attr('disabled', true);
		$('.brightnessScheduleDel').attr('disabled', true);
		$(".scheduleTxt").css("pointer-events", "none");
		$('#contrast').removeAttr('disabled', true);
		$('#energySavingSelectDrop').attr('disabled', false);
	}
}

function setBrightnessMode() {
	$('#brightnessScheduleEnable').change(function () {
		var status = $("input:checkbox[id='brightnessScheduleEnable']").is(":checked") ? "on" : "off";
		setSystemSettings('commercial', {
			'easyBrightnessMode': status
		}, function () {
			if (status === 'on') {
				setSystemSettings('picture', {'energySaving': 'off'}, function() {
					setDropButtonText('energySavingSelect', Locale.getText("off"), true);
					getPictureDBVal(['contrast'], function(msg) {
						$('#contrast').val(Number(msg.contrast));
						$('#contrastVal').html(Number(msg.contrast));
						checkRangeUI('contrast', 0, 100);
					});
				});
			}

			CUR_BRIGHTNESS_STATUS = (status === "on" ? true : false);
			setUpButtons();
		})
	});
}

function setSchedule(schedule) {
	setSystemSettings('commercial', {
		'easyBrightnessSchedule': JSON.stringify(schedule)
	}, function (ret) {
		if(ret.returnValue === true){
			scheduleList = schedule;
			CUR_BRIGHTNESS_SCHEDULE = scheduleList.length;
			setUpButtons();
			getBrightnessSchedule();
		}
	})
}

function setBrightnessSchedule() {
	var baddScheduleModal = addBrightnessScheduleModal('brightnessScheduleAddModal', 'Schedule Time', 'Brightness', 'Cancel', 'Add');
	var bdeleteAllModal = addTwoButtonModal('brightnessScheduleDelAllModal', 'Do you want to delete all schedules?', 'Cancel', 'Delete All');
	var bdeleteModal = addTwoButtonModal('brightnessScheduleDelModal', 'Do you want to delete the schedule?', 'Cancel', 'Delete');
	$('body').append(baddScheduleModal);
	$('body').append(bdeleteModal);
	$('body').append(bdeleteAllModal);

	drawBrightnessHourSelect(0);
	drawBrightnessMinuteSelect(0);
	setUpBrightnessSchSlider('schBrightness', 0, 100);

	// 'Add' Schedule button
	$(document).on('click', '#brightnessScheduleAdd', function () {
		$('#brightnessScheduleAddModal').removeData('id'); // remove index(id) when newly adding
		$('#brightnessScheduleAddModal').modal({
			backdrop: 'static',
			keyboard: false,
			show: true
		});
		brightnessNewSchedule.hour = 0;
		brightnessNewSchedule.minute = 0;
		brightnessNewSchedule.backlight = 70;
		setDropButtonText('brHourSelect', brightnessNewSchedule.hour, true);
		setDropButtonText('brMinuteSelect', brightnessNewSchedule.minute, true);
		initBrightnessSchSliderValue('schBrightness', 0, 100, brightnessNewSchedule.backlight);
	});

	$(document).on('click', '.scheduleTxt', function () {
		var id = $(this).closest('tr').data('id');
		$('#brightnessScheduleAddModal').data('id', id).modal({
			backdrop: 'static',
			keyboard: false,
			show: true
		});
		brightnessNewSchedule.hour = scheduleList[id].hour;
		brightnessNewSchedule.minute = scheduleList[id].minute;
		brightnessNewSchedule.backlight = scheduleList[id].backlight;
		setDropButtonText('brHourSelect', scheduleList[id].hour, true);
		setDropButtonText('brMinuteSelect', scheduleList[id].minute, true);
		initBrightnessSchSliderValue('schBrightness', 0, 100, scheduleList[id].backlight);
	});

	$('#brightnessScheduleAddModalOK').click(function () {
		var editIdx = $('#brightnessScheduleAddModal').data('id');
		if(checkScheduleDuplicated(editIdx, brightnessNewSchedule)){
			var dupCheckModal = addTwoButtonModal('dupCheckModal', Locale.getText("Invalid schedule.") + " " + Locale.getText('Please check the schedule again.'), 'OK', '');
			$('body').append(dupCheckModal);
			$('#dupCheckModalOK').remove(); // Delete unused buttons
			$('#dupCheckModal').modal({
				backdrop: 'static',
				keyboard: false,
				show: true
			});
		} else {
			if(editIdx >= 0){
				scheduleList.splice(editIdx, 1);
				$('#brightnessScheduleAddModal').removeData('id'); // edit mode
			}
			scheduleList.push(brightnessNewSchedule);
		}
		setSchedule(scheduleList);
	});

	// 'Delete All' button
	$(document).on('click', '#brightnessScheduleDelAll', function () {
		$('#brightnessScheduleDelAllModal').modal({
			backdrop: 'static',
			keyboard: false,
			show: true
		});
	});

	$('#brightnessScheduleDelAllModalOK').click(function () {
		var schedule = [];
		setSchedule(schedule);
	});

	// 'Delete' button, table row
	$(document).on('click', '.brightnessScheduleDel', function () {
		var idx = $(this).closest('tr').data('id');
		$('#brightnessScheduleDelModal').data('id', idx).modal({
			backdrop: 'static',
			keyboard: false,
			show: true
		});
	});

	$('#brightnessScheduleDelModalOK').click(function () {
		var id = $('#brightnessScheduleDelModal').data('id');
		scheduleList.splice(id, 1);
		setSchedule(scheduleList);
		$('#brightnessScheduleDelModal').modal('hide');
	});

}

function addBrightnessScheduleModal(id, type1, type2, button1, button2) {
	var rtl = env.locale == 'ar-SA' ? 'dir=rtl' : '';
	var html =
	'<div class="modal fade" id="' + id + '" tabindex="-1" role="dialog">' +
		'<div class="modal-dialog modal-top30" role="document">' +
			'<div class="modal-content"'+ rtl + '>' +
				'<div class="modal-body">' +
					'<table id="' + id + 'Table">' +
						'<tr><th><span>' + Locale.getText(type1) + '</span></th></tr>' +
						'<tr class="selectTime">' +
							'<td>' +
								'<div id="brHourSelect" style="float:left;"></div>' +
								'<div style="float:left; font-weight:bold; font-size:14px; padding: 8px 0px 0px 10px;"> : </div>' +
								'<div id="brMinuteSelect" style="float:left; margin-left:15px;"></div>' +
							'</td>' +
						'</tr>' +
						'<tr> </tr>' +
						'<tr><th><span>' + Locale.getText(type2) + '</span></th></tr>' +
						'<tr>' +
							'<td class="sliderTd"><input class=custom id="schBrightness" type="range" min="0" max="100"></td>' +
							'<td id=schBrightnessVal style="padding-left:10px">&nbsp;</td>' +
						'</tr>' +
					'</table>' +
				'</div>' +
				'<div class="modal-footer">' +
					'<button type="button" class="btn whiteCardBtn" data-dismiss="modal" id="' + id + "Cancel" + '">' + Locale.getText(button1) + '</button>' +
					'<button type="button" class="btn whiteCardBtn" data-dismiss="modal" id="' + id + "OK" + '">' + Locale.getText(button2) + '</button>' +
				'</div>' +
			'</div>' +
		'</div>' +
	'</div>';
	return html;
}

function checkScheduleDuplicated (editIdx, newSchedule) {
	var found = -1;

	scheduleList.some(function(elem, idx) {
		if (editIdx !== idx && newSchedule.hour === elem.hour && newSchedule.minute === elem.minute) {
			found = idx;
			return true;
		}
	});

	if(found >= 0){
		return true;
	} else {
		return false;
	}
}

function drawBrightnessHourSelect(val){
	var arrHour = [];

	for(var i=0; i<=23; i++){
		arrHour.push(i);
	}

	$('#brHourSelect').empty();
	drawDropDownBox('brHourSelect', arrHour, arrHour, 'dropboxType2', function (selected) {
		brightnessNewSchedule.hour = parseInt(selected);
	});

	brightnessNewSchedule.hour = val;
	setDropButtonText('brHourSelect', val, true);
}

function drawBrightnessMinuteSelect(val){
	var arrMinute = [];

	for(var i=0; i<=59; i++){
		arrMinute.push(i);
	}

	$('#brMinuteSelect').empty();
	drawDropDownBox('brMinuteSelect', arrMinute, arrMinute, 'dropboxType2', function (selected) {
		brightnessNewSchedule.minute = parseInt(selected);
	});

	brightnessNewSchedule.minute = val;
	setDropButtonText('brMinuteSelect', val, true);
}

function initBrightnessSchSliderValue (name, start, end, value) {
	var initVal = value ? value : 70
	brightnessNewSchedule.backlight = initVal;

	$('#' + name).val(initVal);
    $('#' + name + 'Val').html(initVal);
    checkRangeUI(name, start, end);
}

function setUpBrightnessSchSlider(name, start, end, value) {
    initBrightnessSchSliderValue(name, start, end, value);

	var browserType = '';

	if (agent.indexOf("webkit") !== -1) {
		browserType = "webkit";
	} else if (agent.indexOf("firefox") !== -1) {
		browserType = "firefox";
	} else {
		browserType = "msie";
	}

	if (browserType == "msie") {
		$('#' + name).change(function() {
			brightnessNewSchedule.backlight = parseInt($(this).val());
			$('#' + name + 'Val').html($(this).val());
			checkRangeUI(name, start, end);
		});
	} else {
		$('#' + name).change(function() {
			brightnessNewSchedule.backlight = parseInt($(this).val());
		}).on('input', function() {
			$('#' + name + 'Val').html($(this).val());
			checkRangeUI(name, start, end);
		});
	}
}