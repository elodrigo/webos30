function getResolution(str) {
	return str.match(/\d+/g);
}

function unitBorderHandler() {
	$('.unit').removeClass('sel selRight selBottom');

	if (isEditMode) {
		return;
	}

	var id = Number($(this).attr('gridID'));

	$(this).addClass('sel');

	if (id % curConfig.size.h != 0) {
		$('#gridCol' + (id + 1)).addClass('selRight');
	}

	if ((id - 1) / curConfig.size.h < curConfig.size.v - 1) {
		$('#gridCol' + (id + curConfig.size.h)).addClass('selBottom');
	}
}

function getGridSize(resW, resH, gridCount, zoomIn) {
	var overGrid = false;
	var h = 1920 / resW;
	var v = 1080 / resH;
	var widthStr = $('#gridPanel').css('width');
	var heightStr = $('#gridPanel').css('height');
	var panelWidth = Number(widthStr.match(/\d+/)) - 80;
	var panelHeight = Number(heightStr.match(/\d+/)) - 60;
	var defaultSize = resW*3;

	if( ( defaultSize*gridCount.h > panelWidth ) || ( defaultSize*gridCount.v > panelHeight ) ){
		overGrid = true;
	}

	if(overGrid){
		var resizedWidth = panelWidth / gridCount.h;
		var resizedHeight = panelHeight / gridCount.v;
		var resizedSize = resizedWidth>resizedHeight ? resizedHeight:resizedWidth;

		if(zoomIn){
			return {
				height: defaultSize,
				width: defaultSize
			};
		}else{
			return {
				height: resizedSize,
				width: resizedSize
			};
		}
	}else{
		return {
			height: defaultSize,
			width: defaultSize
		};
	}
}

function changeGridConfig(h, v, resolution, zoomIn) {
	$('#unitGrid').empty();
	$('#gridVRule').empty();
	if (h > 0 && v > 0) {
		$('#gridVRule').append("<div class='col-xs-12 grid' style='width=100%'>0&nbsp;</div>");
	} else {
		$('#gridVRule').append("<div class='col-xs-12 grid' style='width=100%'>None</div>");
	}
	$('#gridHRule').empty();
	var gridWidth = 'col-xs-1';
	var noneDiv = 12 - h;

	var resArr = getResolution(resolution);
	var resW = Number(resArr[0]);
	var resH = Number(resArr[1]);

	var gridCount = {
		h:h,
		v:v
	}

	if( gridCount.h == 0 || gridCount.v == 0 ){
		return;
	}

	var gridSize = getGridSize(resW, resH, gridCount, zoomIn);

	for (var i = 0; i != v; ++i) {
		var id = 'gridRow' + i;
		var rowDiv = '<div width=100% class="col-xs-12 grid" id="' + id + '"></div>';
		$('#unitGrid').append(rowDiv);

		for (var j = 0; j != h; ++j) {
			var gridID = i * h + j + 1;
			var colID = 'gridCol' + gridID;
			var colDiv = '<div class="' + gridWidth + ' unit" id="' + colID + '" gridID=' + gridID + '></div>';
			$('#' + id).append(colDiv);
			$('#' + colID).css('width', gridSize.width + 'px');
			$('#' + colID).css('height', gridSize.height + 'px');

			if (i == v - 1) {
				$('#' + colID).addClass('bottom');
			}
		}

		$('#' + colID).addClass('rightmost');
	}

	var mergeCount = 1;
	while(true){
		if( gridSize.width*mergeCount >= 25 ){
			break;
		}
		mergeCount++;
	}

	var fontSize = mergeCount > 1 ? '8pt':'small';

	for (var i = 0; i != h; ++i) {
		if( Math.floor(i/mergeCount) >= Math.floor(h/mergeCount) -1 ){
			if( i == (h-1) ){
				var gridDiv = $('<div class="' + gridWidth + ' grid">' + ((i + 1) * resW) + '&nbsp;</div>');
				gridDiv.css('width', gridSize.width* (mergeCount + (h%mergeCount)) );
				gridDiv.css('border-width', '0px 0px 0px 1px');
				gridDiv.css('font-size', fontSize);
				$('#gridHRule').append(gridDiv);
			}else{
				continue;
			}
		}else{
			if( (i+1)%mergeCount ==0 ){
				var gridDiv = $('<div class="' + gridWidth + ' grid">' + ((i + 1) * resW) + '&nbsp;</div>');
				gridDiv.css('width', gridSize.width*mergeCount);
				gridDiv.css('border-width', '0px 0px 0px 1px');
				gridDiv.css('font-size', fontSize);
				$('#gridHRule').append(gridDiv);
			}
		}
	}

	if (gridDiv) {
		gridDiv.css('border-width', '0px 1px 0px 1px');
	}

	for (var i = 0; i != v; ++i) {
		var gridDiv = $('<div class="col-xs-12 grid"><span>' + ((i + 1) * resH) + '</span>&nbsp;</div>');
		gridDiv.css('border-width', '1px 0px 0px 0px');
		gridDiv.css('height', gridSize.height);
		gridDiv.css('line-height', (gridSize.height * 2 - 15) + 'px');
		gridDiv.css('font-size', fontSize);
		$('#gridVRule').append(gridDiv);
	}
	if (gridDiv) {
		gridDiv.css('border-width', '1px 0px 1px 0px');
	}

	if( ( $('#gridSpace').css('width') != undefined ) ){
		$('#gridSpace').css('width', (gridSize.width * h));
	}

	$('.unit').click(unitBorderHandler);
}

function setUnitID(config, auto) {

	function showUnitID(gridID, unitID) {
		unitID = auto ? unitID + 1 : config.onUnits[gridID];

		if (unitID == undefined) {
			unitID = -1;
		}
		if (config.regular == 0) {
			$('#unitIDBox' + gridID + ' span').html(unitID).show();
			$('#unitIDBox' + gridID).parent().attr('unitID', unitID);
			return true;
		}

		var selectVal = $('#switchSelect' + gridID).val();
		if (selectVal == undefined) {
			selectVal = 0;
		}

		var newID = selectVal >= 0 ? unitID : selectVal;
		$('#unitIDBox' + gridID).parent().attr('unitID', newID);
		$('#switchSelect' + gridID).val(newID);

		if (newID == -1) {
			return false;
		}

		$('#unitIDBox' + gridID + ' span').html(newID).toggle(!isEditMode);
		return true;
	}

	$('[id^=unitIDBox] span').html('');
	$('[id^=gridCol]').removeAttr('unitID');
	var startX = ([0, 2].indexOf(config.from) >= 0) ? 1 : config.size.h;
	var endX = ([0, 2].indexOf(config.from) >= 0) ? config.size.h + 1 : 0;
	var startY = ([0, 1].indexOf(config.from) >= 0) ? 1 : config.size.v;
	var endY = ([0, 1].indexOf(config.from) >= 0) ? config.size.v + 1 : 0;
	var diffX = ([0, 2].indexOf(config.from) >= 0) ? 1 : -1;
	var diffY = ([0, 1].indexOf(config.from) >= 0) ? 1 : -1;
	var unitID = 0;
	if (config.direction == 0) {
		for (var y = startY; y != endY; y += diffY) {
			for (var x = startX; x != endX; x += diffX) {
				var gridID = (y - 1) * config.size.h + x;
				if (showUnitID(gridID, unitID)) {
					++unitID;
				}
			}
			startX = diffX < 0 ? 1 : config.size.h;
			endX = diffX < 0 ? config.size.h + 1 : 0;
			diffX *= -1;
		}
	} else {
		for (var x = startX; x != endX; x += diffX) {
			for (var y = startY; y != endY; y += diffY) {
				var gridID = (y - 1) * config.size.h + x;
				if (showUnitID(gridID, unitID)) {
					++unitID;
				}
			}
			startY = diffY < 0 ? 1 : config.size.v;
			endY = diffY < 0 ? config.size.v + 1 : 0;
			diffY *= -1;
		}
	}
	return unitID;
}


