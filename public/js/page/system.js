function init() {
	getBasicInfo(function(msg) {
		$('#firmware').html(msg.firmwareVersion);
		$('#micom').html(msg.micomVersion);
		$('#serial').html(msg.serialNumber);
		$('#bootloader').html(msg.bootLoaderVersion);
		$('#model').html(msg.modelName);
	});

	getWebOSInfo(function(msg) {
		$('#webos').html(msg.core_os_release + " (" + msg.core_os_release_codename + ")");
	});

	if(env.supportMicomFanControl){
		getFanMicomInfo(function(msg){
			$('#fanmicom').html(msg.fanMicomVersion);
		});
	}
}