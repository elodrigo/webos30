var path = location.protocol + '//' + location.host + location.pathname;

function drawFilterSelect(curFile) {
	var dropDown = createDropDown('filter', '-', function(selected) {
		var newFilter = selected;
		var newPath = path + "?filter=" + newFilter + "&file=" + curFile;
		window.location.replace(newPath);
	});

	addDropDownOption(dropDown, " ", Locale.getText("All"));

	if (env.supportXE) {
		addDropDownOption(dropDown, "CHECKSCREEN", Locale.getText("CheckScreen"));
		addDropDownOption(dropDown, "DOOR", Locale.getText("Door"));
		addDropDownOption(dropDown, "MEMS", Locale.getText("Impact"));
		addDropDownOption(dropDown, "PANEL", Locale.getText("Panel"));
		addDropDownOption(dropDown, "SCREEN", Locale.getText("Screen"));
	}

	if (env.supportFan) {
		addDropDownOption(dropDown, "FAN", Locale.getText("Fan"));
	}

	if (env.supportPsuStatus) {
		addDropDownOption(dropDown, "PSU", Locale.getText("PSU"));
	}

	if (env.model.name === '49XE3D') {
		addDropDownOption(dropDown, "FILTER", Locale.getText("Filter"));
	}

	addDropDownOption(dropDown, "TEMP", Locale.getText("Temperature"));
	addDropDownOption(dropDown, "SIGNAL", Locale.getText("Signal"));
	addDropDownOption(dropDown, "TIME", Locale.getText("Time"));

	if (env.supportLedSignage) {
		addDropDownOption(dropDown, "LEDUNIT", Locale.getText('LED Unit'));
	}

	$('#filter').append(dropDown);
}

function initLog(curFile, filter) {
	$("#fileName").text($("#fileName").text().replace(".txt", ".csv"));
	drawFilterSelect(curFile);

	if (filter == '') {
		filter = ' ';
	}
	setDropButtonText('filter', filter);

	$('#delete').click(function() {
		if (!confirm("Are you sure to delete ALL log file?")) {
			return;
		}
		var newPath = path + "?delete=true&file=" + curFile;
		window.location.replace(newPath);
	});

	$('#pageTitle').html(Locale.getText('Log'));
	$('#log').append(convertCsvToTable(logJson.log));
	if (env.locale === 'ar-SA') {
		changeRtl();
	}

	if (!curFile) {
		$('#downloadLog').removeAttr('href');
	}
}

function changeRtl() {
    $("table").attr("dir", "rtl");
    $(".table>thead>tr>th").css("text-align", "right");
    $(".col-md-9").css("float", "right");
    $(".col-md-3").css("float", "right");
}
