// LED SI Server Setting for LCLG002
var defaultVal = {
    siServerIp: "0.0.0.0",
    serverIpPort: "0",
    secureConnection: "off",
    fqdnMode: "off",
    fqdnAddr: "http",
    appLaunchMode: "none",
    appType: "zip",
    autoSet: "on",
    proxyEnable: "off",
    proxySingleAddress: "0.0.0.0",
    proxySinglePassword: "",
    proxySinglePort: "0",
    proxySingleUsername: ""
};

var currVal = defaultVal;
var siAppName = 'com.lg.app.signage.ipk';
var updateFrom, updateTo;

var regExps = {
    portNumFormat: new RegExp('^([0-9]|[1-9][0-9]|[1-9][0-9][0-9]|[1-9][0-9][0-9][0-9]|[1-5][0-9][0-9][0-9][0-9]|6[0-4][0-9][0-9][0-9]|65[0-4][0-9][0-9]|655[0-2][0-9]|6553[0-5])$'),
    ipAddrFormat: new RegExp('^([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$'),
    urlFormat: new RegExp('^(https?)\://')
}

function siServerInit() {
    getSISetting();
    modalInit();
    btnInit();
}

function modalInit() {
    var resetModal = addTwoButtonModal('resetModal', Locale.getText('All setting data will be deleted.') + '</br>' + Locale.getText('Do you want to process?'), 'No', 'Yes', 'RESET');
    var upgradeModal = addTwoButtonModal('upgradeModal', '',  'Cancel', 'Confirm', 'Application Upgrade');
    var proxyModal = addProxyModal('proxyModal', Locale.getText('Proxy'), 'Cancel', 'OK');
    $('body').append(resetModal);
    $('body').append(upgradeModal);
    $('body').append(proxyModal);
}

function getSISetting() {
    var keys = ["siServerIp", "serverIpPort", "fqdnMode", "fqdnAddr", "appLaunchMode", "appType", "autoSet", "secureConnection", "proxyEnable"];
    getSystemSettings('commercial', keys, function (ret) {
        currVal = ret;
        $("#siServerIPAddr").val(ret.siServerIp);
        $("#pnum").val(ret.serverIpPort);
        ret.secureConnection === "on" ? $("#scEnable").prop('checked', true) : $("#scEnable").removeAttr('checked');
        $('input[name="appLaunchModeRadio"][value="' + ret.appLaunchMode + '"]').prop('checked', true)
        ret.fqdnMode === "on" ? ($("#fqdnEnable").prop('checked', true), $('#fqdnAddr').removeAttr("disabled")) : ($("#fqdnEnable").removeAttr('checked'), $('#fqdnAddr').prop("disabled", true));
        ret.fqdnMode === "on" ? ($('#siServerIPAddr').prop("disabled", true), $('#pnum').prop("disabled", true)) : ($('#siServerIPAddr').removeAttr("disabled"), $('#pnum').removeAttr("disabled"));
        $('#fqdnAddr').val(ret.fqdnAddr);
        $('input[name="appTypeRadio"][value="' + ret.appType + '"]').prop('checked', true);
        ret.autoSet === "on" ? $("#asEnable").prop('checked', true) : $("#asEnable").removeAttr('checked');
        ret.proxyEnable === "on" ? ($("#proxyEnable").prop('checked', true), $('#proxySetting').removeAttr("disabled")) : ($("#proxyEnable").removeAttr('checked'), $('#proxySetting').prop("disabled", true));
        subBtnInit(ret.appLaunchMode);
    });
}

function btnInit() {
    $(document).on('click', '#btnReset', function () {
        $('#resetModal').modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    });

    $(document).on('click', '#resetModalOK', function () {
        resetSystemSettings('commercial', Object.keys(defaultVal), function (ret) {
            if(ret.returnValue){
                location.reload();
            } else {
                console.log("Reset Error");
            }
        });
    });

    $(document).on('click', '#proxyModalOK', function () {
        var proxySetVal = {
            proxySingleAddress: $("#proxyIPAddr").val(),
            proxySinglePort: $('#proxyPortNum').val(),
            proxySingleUsername: $('#userName').val(),
            proxySinglePassword: $('#proxyPassword').val()
        }
        if(!regExps.ipAddrFormat.test(proxySetVal.proxySingleAddress)) proxySetVal.proxySingleAddress = currVal.proxySingleAddress;
        if(!regExps.portNumFormat.test(proxySetVal.proxySinglePort)) proxySetVal.proxySinglePort = currVal.proxySinglePort;
        if(proxySetVal.proxySingleUsername.length > 255 || proxySetVal.proxySinglePassword.length > 255){
            proxySetVal.proxySingleUsername = currVal.proxySingleUsername;
            proxySetVal.proxySinglePassword = currVal.proxySinglePassword;
        }
        setSystemSettings('commercial', proxySetVal);
    });

    $(document).on('click', '#proxySetting', function () {
        var keys = ["proxySingleAddress", "proxySinglePort", "proxySingleUsername", "proxySinglePassword"];
        getSystemSettings('commercial', keys, function (ret) {
            $("#proxyIPAddr").val(ret.proxySingleAddress);
            $("#proxyPortNum").val(ret.proxySinglePort);
            $("#userName").val(ret.proxySingleUsername);
            $("#proxyPassword").val(ret.proxySinglePassword);
            $('#proxyModal').modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            });
        });
    });

    $(document).on('click', '#localUpgradeUSB, #localUpgradeRemote, #usbUpgradeRemote', function () {
        updateFrom = $(this).attr('from');
        updateTo = $(this).attr('to');
        var trans = {
            "none": "None",
            "local": "Local",
            "remote": "Remote",
            "usb": "USB"
        }; // for translation
        var upgradeModalTxt = Locale.getText('Applications will be upgraded from {text}').replace('{text}', Locale.getText(trans[updateFrom]));
        $('#upgradeModalTxt').text(upgradeModalTxt);
        $('#upgradeModal').modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    });

    $(document).on('click', '#upgradeModalOK', function () {
        overlayControl20(true, Locale.getText("Installing…"));
        if (updateFrom === 'usb') {
            usbUpdate();
        } else if (currVal.appType === "zip") {
            if (currVal.appLaunchMode === "usb") {
                usbUpdate();
            } else {
                zipUpdate({
                    "from": updateFrom,
                    "to": updateTo
                });
            }
        } else if (updateFrom === 'remote') {
            remoteUpdate();
        }
    });

    $(document).on('change', '#siServerIPAddr', function () {
        var ip = $("#siServerIPAddr").val();
        if (!regExps.ipAddrFormat.test(ip)) {
            $("#siServerIPAddr").val(currVal.siServerIp);
            return;
        }
        currVal.siServerIp = ip;
        setSystemSettings('commercial', {
            siServerIp: ip
        });
    })

    $(document).on('change', '#pnum', function () {
        var port = $("#pnum").val();
        if (!regExps.portNumFormat.test(port)) {
            $("#pnum").val(currVal.serverIpPort);
            return;
        }
        currVal.serverIpPort = port;
        setSystemSettings('commercial', {
            serverIpPort: port
        });
    });

    $(document).on('change', 'input:radio[name=appLaunchModeRadio]', function () {
        var selected = $(this).val();
        var validation = ['none', 'local', 'remote', 'usb'];
        if (validation.indexOf(selected) < 0) {
            $('input[name="appLaunchModeRadio"][value="' + currVal.appLaunchMode + '"]').prop('checked', true);
            return;
        }
        if (selected === 'remote' || selected === 'usb') {
            $('input[name="appTypeRadio"][value="zip"]').prop('checked', true).trigger('change');
        }
        subBtnInit(selected);
        currVal.appLaunchMode = selected;
        setSystemSettings('commercial', {
            appLaunchMode: selected
        });
    });

    $(document).on('change', 'input:radio[name=appTypeRadio]', function () {
        var selected = $(this).val();
        var validation = ['zip', 'ipk'];
        if (validation.indexOf(selected) < 0) {
            $('input[name="appTypeRadio"][value="' + currVal.appType + '"]').prop('checked', true);
            return;
        }
        currVal.appType = selected;
        setSystemSettings('commercial', {
            appType: selected
        });
    });

    $(document).on('change', '#fqdnAddr', function () {
        var addr = $('#fqdnAddr').val();
        if(!regExps.urlFormat.test(addr)){
            $('#fqdnAddr').val(currVal.fqdnAddr);
            return;
        }
        currVal.fqdnAddr = addr;
        setSystemSettings('commercial', {
            fqdnAddr: addr
        });
    });

    $(document).on('change', '#scEnable, #asEnable, #fqdnEnable, #proxyEnable', function () {
        var keys = {
            scEnable : "secureConnection",
            asEnable : "autoSet",
            fqdnEnable : "fqdnMode",
            proxyEnable : "proxyEnable"
        };
        var settings = {};

        var btnid = $(this).attr('id');
        var enable = $("input:checkbox[id=" + btnid + "]").is(":checked") ? "on" : "off";
        if (btnid === "fqdnEnable") {
            enable === "on" ? ($('#fqdnAddr').removeAttr("disabled"), $('#siServerIPAddr').prop("disabled", true), $('#pnum').prop("disabled", true)) : ($('#fqdnAddr').prop("disabled", true), $('#siServerIPAddr').removeAttr("disabled"), $('#pnum').removeAttr("disabled"));
        }
        if (btnid === "proxyEnable") {
            enable === "on" ? $('#proxySetting').removeAttr("disabled") : $('#proxySetting').prop("disabled", true);
        }
        currVal[keys[btnid]] = enable;
        settings[keys[btnid]] = enable;
        setSystemSettings('commercial', settings);
    });
}

function usbUpdate() {
    listDevices(function (ret) {
        if (ret.returnValue) {
            var devices = ret.devices;
            for (var i = 0; i < devices.length; i++) {
                if (devices[i].deviceType === 'usb') {
                    var usb = devices[i];
                    break;
                }
            }
            if (usb === undefined || usb === null) {
                overlayControl20(false);
                alert(Locale.getText("Cannot connect to the USB"));
                return;
            }

            if (currVal.appType === "zip") {
                zipUpdate({
                    "from": updateFrom,
                    "to": updateTo
                });
            } else {
                copyApplication(usb, function (ret) {
                    if (ret.returnValue) {
                        ipkUpdate(ret.destinationPath);
                    } else {
                        overlayControl20(false);
                        alert(Locale.getText("Failed, try again."));
                    }
                });
            }
        } else {
            overlayControl20(false);
            alert(Locale.getText("Cannot connect to the USB"));
        }
    });
}

function remoteUpdate() {
    var sc = currVal.secureConnection === 'on' ? "https://" : "http://";
    var url = currVal.fqdnMode === "on" ? currVal.fqdnAddr : sc + currVal.siServerIp + ":" + currVal.serverIpPort + '/application/' + siAppName;
    downloadApplication({
        targetURL: url
    }, function () {});

    socket.on("downloadIPK", function (ret) {
        if (ret.completionStatusCode === 200) {
            ipkUpdate(ret.target);
        } else {
            overlayControl20(false);
            alert(Locale.getText("Download failed, try again."));
        }
    });
}

function zipUpdate(params) {
    upgradeApplication(params, function (ret) {
        overlayControl20(false);
        if (ret.returnValue === false) {
            alert(Locale.getText("Failed, try again."));
        } else {
            alert(Locale.getText("Upgrade Complete"));
        }
    });
}

function ipkUpdate(target) {
    installApplication({
        ipkUrl: target
    }, function () {});
    socket.on("installIPK", function (ret) {
        overlayControl20(false);
        if (ret.details.state === "installed") {
            alert(Locale.getText("Install Succeeded"));
        } else {
            alert(Locale.getText("Install Failed."));
        }
    });
}

function subBtnInit(launchMode) {
    switch (launchMode) {
        case "local":
            $('#localUpgradeUSB').removeAttr('disabled');
            $('#localUpgradeRemote').removeAttr('disabled');
            $('#usbUpgradeRemote').prop('disabled', true);
            $('input[name=appTypeRadio]').removeAttr('disabled');
            $('#fqdnEnable').removeAttr('disabled');
            break;
        case "remote":
            $('#localUpgradeUSB').prop('disabled', true);
            $('#localUpgradeRemote').prop('disabled', true);
            $('#usbUpgradeRemote').prop('disabled', true);
            $('input[name=appTypeRadio]').prop('disabled', true);
            $('#fqdnEnable').removeAttr('disabled');
            break;
        case "usb":
            $('#localUpgradeUSB').prop('disabled', true);
            $('#localUpgradeRemote').prop('disabled', true);
            $('#usbUpgradeRemote').removeAttr('disabled');
            $('input[name=appTypeRadio]').prop('disabled', true);
            $('#fqdnEnable').removeAttr('disabled');
            break;
        default:
            $('#siServerIPAddr').removeAttr('disabled');
            $('#pnum').removeAttr('disabled');
            $('#localUpgradeUSB').prop('disabled', true);
            $('#localUpgradeRemote').prop('disabled', true);
            $('#usbUpgradeRemote').prop('disabled', true);
            $('input[name=appTypeRadio]').prop('disabled', true);
            $('#fqdnEnable').prop('disabled', true);
            $("#fqdnEnable").removeAttr('checked');
            $('#fqdnAddr').prop("disabled", true);
            break;
    }
}

function addProxyModal(id, title, button1, button2) {
    var rtl = env.locale == 'ar-SA' ? 'dir=rtl' : '';
    var html =
    '<div class="modal fade" id="' + id + '" tabindex="-1" role="dialog">' +
		'<div class="modal-dialog modal-top30" role="document">' +
            '<div class="modal-content"'+ rtl + '>' +
                '<div class="modal-header"><h4 class="modal-title">' + title +'</h4></div>' +
                '<div class="modal-body">' +
                    '<table class="table-condensed" id="' + id + 'Table">' +
                        '<tr class="text_indent">' +
                            '<td class="first"><span class="proxyIpTxt">' + Locale.getText("IP Address") + '</span></td>' +
                            '<td class="second">' +
                                '<span><input type="text" id="proxyIPAddr" class="custom" maxlength="15" style="background-color:whitesmoke;" placeholder="0.0.0.0"/></span>' +
                            '</td>' +
                        '</tr>' +
                        '<tr class="text_indent">' +
                            '<td class="first"><span class="proxyPortTxt">' + Locale.getText("Port Number") + '</span> <span>(0~65535)</span></td>' +
                            '<td class="second"><span><input type="text" id="proxyPortNum" class="custom" maxlength="5" placeholder="0" onkeypress="onlyNumber();" style="background-color:whitesmoke;"></span></td>' +
                        '</tr>' +
                        '<tr class="text_indent">' +
                            '<td class="first"><span class="userNameTxt">' + Locale.getText("User Name") + '</span></td>' +
                            '<td class="second"><span><input type="text" id="userName" class="custom" style="background-color:whitesmoke;"></span></td>' +
                        '</tr>' +
                        '<tr class="text_indent">' +
                            '<td class="first"><span class="proxyPasswordTxt">' + Locale.getText("Password") + '</span></td>' +
                            '<td class="second"><span><input type="password" id="proxyPassword" class="custom" style="background-color:whitesmoke;"></span></td>' +
                        '</tr>' +
                    '</table>' +
                '</div>' +
                '<div class="modal-footer">' +
                    '<button type="button" class="btn whiteCardBtn" data-dismiss="modal">' + Locale.getText(button1) + '</button>' +
                    '<button type="button" class="btn whiteCardBtn" data-dismiss="modal" id="' + id + "OK" + '">' + Locale.getText(button2) + '</button>' +
                '</div>' +
            '</div>' +
        '</div>' +
    '</div>';
	return html;
}
