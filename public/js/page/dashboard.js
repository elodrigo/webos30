var currentTime;
var fullRefreshTimer;
var captureRefreshTimer;
var criticalTemp = 85;
var warningTemp = criticalTemp - 3;

function setCurTime(currentTime){
	var dateArr = currentTime.split(' ');

	var today = "";
	var monthYear = "";

	var weekDay = {
		Sun: Locale.getText("Sunday"),
		Mon: Locale.getText("Monday"),
		Tue: Locale.getText("Tuesday"),
		Wed: Locale.getText("Wednesday"),
		Thu: Locale.getText("Thursday"),
		Fri: Locale.getText("Friday"),
		Sat: Locale.getText("Saturday")
	};
	var month = {
		Jan: Locale.getText("January"),
		Feb: Locale.getText("February"),
		Mar: Locale.getText("March"),
		Apr: Locale.getText("April"),
		May: Locale.getText("May"),
		Jun: Locale.getText("June"),
		Jul: Locale.getText("July"),
		Aug: Locale.getText("August"),
		Sep: Locale.getText("September"),
		Oct: Locale.getText("October"),
		Nov: Locale.getText("November"),
		Dec: Locale.getText("December")
	};

	today = weekDay[dateArr[0]] + ', ' + dateArr[2];
	monthYear = month[dateArr[1]] + ' ' + dateArr[3];

	$("#day").html(today);
	$("#monthYear").html(monthYear);
	$("#curTime").html(dateArr[4]);

	if(env.supportLedFilm && (env.locale == "ko-KR") ){
		today = dateArr[3]+'년 '+Locale.getText(month[dateArr[1]])+' '+dateArr[2]+'일 ('+Locale.getText(weekDay[dateArr[0]])+') '+dateArr[4];
		$("#day").html(today);
		$("#monthYear").html('');
		$("#curTime").html('');
	}
}

function secToTime(sec) {
	if (sec == 0) {
		return "-"
	}
	var hour = Math.floor(sec / 60 / 60);
	var min = Math.floor(sec / 60) % 60;
	sec = sec % 60;

	return "" + hour + "H " + min + "M " + sec + "S";
}

function getMobileTimeFormat(time) {
	return time.replace(/-/g, '.').substring(2, time.length - 3);
}

function addIncident(inc) {
	currentTime = currentTime.replace(" ", "T");
	var startTime = inc.startTime.replace(" ", "T");

	var isNG = inc.state == Locale.getText('NG');
	var duration = inc.duration;

	if (duration == 0 && inc.endTime.length > 0 && inc.startTime.length > 0) {
		duration = (new Date(currentTime)).getTime() - (new Date(startTime)).getTime();
		duration = Math.round(duration / 1000);
	}

	if (duration < 0 || duration == undefined ) {
		duration = 0;
	}

	if (!isMobile) {
		if(env.supportLedSignage20){
			$('#incidentTable > tbody:last').prepend(
				'<tr>'
				//+ '<td>' + inc.category + '</td>'
				+ '<td>' + inc.category + "(" + inc.sensor + ')</td>'
				+ '<td>' + inc.event + '</td>'
				+ '<td>' + (isNG ? "<font color=red>" : "") + inc.state + '</td>'
				+ '<td>' + secToTime(duration) + '</td>'
				+ '<td>' + inc.startTime + '</td>'
				+ '<td>' + inc.endTime + '</td></tr>');
		}else{
			$('#incidentTable > tbody:last').prepend(
				'<tr>'
				//+ '<td>' + inc.category + '</td>'
				+ '<td>' + inc.category + "(" + inc.sensor + ')</td>'
				+ '<td>' + inc.event + '</td>'
				+ '<td>' + (isNG ? "<font color=red>" : "") + inc.state + '</td>'
				+ '<td>' + inc.startTime + '</td>'
				+ '<td>' + inc.endTime + '</td>'
				+ '<td>' + secToTime(duration) + '</td></tr>');
		}
	} else {
		$('#incidentTable > tbody:last').prepend(
			'<tr>'
			+ '<td style="font-size:10px">' + inc.category + '</td>'
			+ '<td style="font-size:10px">' + inc.sensor
			+ '<br>' + inc.event + '</td>'
			+ '<td style="font-size:10px">' + getMobileTimeFormat(inc.startTime)
			+ '<br>' + getMobileTimeFormat(inc.endTime) + '</td>'
			+ '<td style="font-size:10px">' + (isNG ? "<font color=red>" : "") + inc.state
			+ '<br>' + secToTime(duration) + '</td></tr>');
	}
}

function setBackground(card, isOK) {
	if (isOK) {
		card.removeClass('red');
		card.addClass('green');
		return;
	}

	card.removeClass('green');
	card.addClass('red');
}

function getSensorData() {
	getSignalInfo();

	var tempOK = true;
	var tempCount = 0;
	env.curTempSensor.forEach(function(sensor) {
		getTemp(sensor.id, function(msg) {
			++tempCount;
			if (!msg || !msg.returnValue) {
				$('#' + sensor.id + 'Temp').html('-');
				tempOK = false;
			} else {
				readTempType(function(tempType){
					if ( !tempType.isCelsius ) {
						$('#' + sensor.id + 'Temp').html( Math.floor( (msg.temperature)*1.8 + 32) + '°F' );
					}else{
						$('#' + sensor.id + 'Temp').html(msg.temperature + '°C');
					}
				});
			}

			if (msg.temperature >= warningTemp) {
				tempOK = false;
			}

			if (tempCount == env.curTempSensor.length) {
				setBackground($('#tempCard'), tempOK);
			}
		});
	});

	getCheckScreenOn(function(m1) {
		if (m1 == 'off') {
			$('#checkScreen').html('Off');
			setBackground($('#screenCard'), true);
			return;
		}
		if (env.checkscreen.type === 'outdoor') {
			getCheckScreenColor(function(msg) {
				$('#checkScreen').html(msg.colorValid ? Locale.getText("OK") : Locale.getText("NG"));
				setBackground($('#screenCard'), msg.colorValid);
			});
		} else {
			getNormalCheckScreenStatus(function(msg) {
				var isNG = msg.screen == 'ng';
				$('#checkScreen').html(isNG ? Locale.getText("NG") : Locale.getText("OK"));
				setBackground($('#screenCard'), !isNG);
			});
		}
	});

	if (env.supportFan) {
		getFanStatus(function(msg) {
			var isOK = true;
			env.fan.forEach(function(fan) {
				for (var i = 0; msg[fan.id] && i != msg[fan.id].length; ++i) {
					if (!msg[fan.id][i]) {
						isOK = false;
						break;
					}
				}
			});
	
			$('#fan').html(isOK ? Locale.getText("OK") : Locale.getText("NG"));
			setBackground($('#fanCard'), isOK);
		});
	}

	if (env.supportXE){
		getPanelErrorOut(function(msg){
			var isNG = !msg.isPanelBLUOk;
			$('#BLU').html(isNG ? Locale.getText("NG") : Locale.getText("OK"));
			setBackground($('#BLUCard'), !isNG);
		});
	}

	if (env.supportLedSignage) {
		checkUnitStatusHandler();
	}
}

function getData() {
	getBasicInfo(function(msg) {
		$('#firmware').html(msg.firmwareVersion);
	});

	getNetworkStatus(function(msg) {
		var connection = msg.wired.state === 'connected' ? msg.wired : msg.wifi;

		$('#ip').html(connection.state === 'connected' ? connection.ipAddress : Locale.getText("Not Connected"));
		$('#wan').html(msg.isInternetConnectionAvailable ? Locale.getText("Internet available") : Locale.getText("Internet Not available"));

		setBackground($('#networkCard'), connection.state === 'connected');
	});

	getSensorData();

	if (env.supportPsuStatus) {
		getPsuStatus();
	}

	getDowntimeIncident(function(msg) {
		if (!msg.returnValue) {
			return;
		}
		$('#incidentTable > tbody').empty();
		var incs = msg.incident;

		incs.forEach(function(inc) {
			if (inc.state != 'NG') {
				addIncident(inc);
			}
		});
		incs.forEach(function(inc) {
			if (inc.state == 'NG') {
				addIncident(inc);
			}
		});
	});

	if (env.supportBrighness) {
		getBrightness();
	}
	if (env.supportAcCurrent) {
		getAcCurrent();
	}
}

function showDoorStatus(doors) {
	if (!doors) {
		return;
	}

	var opened = false;

	for (var i = 0; i != doors.length; ++i) {
		if (doors[i]) {
			opened = true;
			break;
		}
	}

	$('#door').html(opened ? Locale.getText("Opened") : Locale.getText("Closed"));
	setBackground($('#doorCard'), !opened);
}

function getAllData() {
	getCurrentTime(function(msg) {
		currentTime = msg.current;
		setCurTime(msg.current);
		$('#currentTime').html(msg.current);
		$('#upTime').html(msg.uptime);
		getData();
	});
	$('#scr').click();
}

function getCookie(key) {
	var replace = new RegExp("(?:(?:^|.*;\\s*)" + key + "\\s*\\=\\s*([^;]*).*$)|^.*$");
	return document.cookie.replace(replace, "$1");
}

function getExpireDate() {
	var curDay = new Date();
	curDay = new Date(curDay.getTime() + 7 * 24 * 60 * 60 * 1000);
	var ret = ';expires=' + curDay.toGMTString() + ';';

	return ret;
}

function init() {
	if (env.model.criticalTemp) {
		criticalTemp = env.model.criticalTemp;
		warningTemp = criticalTemp -3
	}

	getIsMobile();
	var sensorCount = $('.temperature').length;
	$('.temperature').css('width', 100/sensorCount + '%');
	$('.sensorname').css('width', 100/sensorCount + '%');

	if (env.supportXE) {
		socket.on('door', function(msg) {
			showDoorStatus(msg.status);
		});

		getDoorStatus(function(msg) {
			showDoorStatus(msg.status);
		});
	}

	getAllData();

	$('#refresh').click(function() {
		getSensorData();
	});

	$("#scr").click(function(e) {
		showRotationWarning();
		getCapture($("#scr").height(), function(m) {
			$("#scr").attr('src',  m);
		});

		getPowerState(function(msg) {
			if (msg.state === 'Screen Off') {
				$('#scr').hide();
			} else {
				$('#scr').show();
			}
		});
	});

	$('#blackScr').click(function() {
		$('#scr').click();
	});

	$('#dataRefresh').click(function() {
		var checked = $(this).prop('checked');
		if (checked) {
			fullRefreshTimer = setInterval(function() {
				getAllData();
			}, 5 * 60 * 1000);
			document.cookie = 'dataRefresh=1' + getExpireDate();
		} else {
			if (fullRefreshTimer) {
				clearTimeout(fullRefreshTimer);
			}
			document.cookie = 'dataRefresh=';
		}
	});

	$('#captureRefresh').click(function() {
		var checked = $(this).prop('checked');

		if (checked) {
			captureRefreshTimer = setInterval(function() {
				$('#scr').click();
			}, 10 * 1000);
			document.cookie = 'captureRefresh=1' + getExpireDate();
		} else {
			if (captureRefreshTimer) {
				clearTimeout(captureRefreshTimer);
			}
			document.cookie = 'captureRefresh=';
		}
	});

	if (getCookie('dataRefresh')) {
		$('#dataRefresh').click();
		$('#dataRefresh').prop('checked', true);
	}

	if (getCookie('captureRefresh')) {
		$('#captureRefresh').click();
		$('#captureRefresh').prop('checked', true);
	}

	$("#pageTitle").html(Locale.getText("Dashboard"));

	$('#scr').click();

	if (isMobile) {
		adjustCardTitle();
	}
}

// LED Signage
var unitStatus = {
	ok: 0,
	ng: 0,
	off: 0
};

function showCardMessage() {
	var isOk = unitStatus.ng == 0 && unitStatus.off == 0;
	if (unitStatus.total == unitStatus.off) {
		$('#ledUnit').html(Locale.getText('Off'));
	} else {
		$('#ledUnit').html(isOk ? 'OK' : Locale.getText('NG'));
	}
	setBackground($('#ledCard'), isOk);
}

function checkUnitOk(unitID, gridID) {
	if(env.supportLedFilm){
		getLedPowerState( (unitID-1), function(msg) {
			if (!msg.powerState) {
				unitStatus.off++;
				showCardMessage();
				return;
			}
			getLedOverallStatus(unitID * 2, function(msg) {
				unitStatus[msg.isOverallOk ? 'ok' : 'ng']++;
				showCardMessage();
			});
		});
	}else{
		getLedPowerStatus(unitID * 2, function(msg) {
			if (!msg.isPowerOk) {
				unitStatus.off++;
				showCardMessage();
				return;
			}
			getLedOverallStatus(unitID * 2, function(msg) {
				unitStatus[msg.isOverallOk ? 'ok' : 'ng']++;
				showCardMessage();
			});
		});
	}
}

function checkUnitStatusHandler() {
	getUnitConfig(function(msg) {
		// connect to SDM later.
		curConfig = msg;
		var h = curConfig.size.h;
		var v = curConfig.size.v;

		if (!curConfig.onUnits.numOn || curConfig.onUnits.numOn == 0) {
			$('#ledUnit').html("None");
			return;
		}

		unitStatus.total = curConfig.onUnits.numOn;
		unitStatus.ok = 0;
		unitStatus.ng = 0;
		unitStatus.off = 0;

		for (var i = 0; i != h * v; ++i) {
			if (curConfig.regular == 0) {
				checkUnitOk(i + 1, i + 1);
			} else if (isOn(i + 1, curConfig)) {
				var id = curConfig.onUnits[i + 1];
				checkUnitOk(id, i + 1);
			}
		}
	});
}

function adjustCardTitle() {
	$(".card .card-header .card-title").each(function() {
		if ($(this).height() > $(this).parent().height()) {
			$(this).css('padding-top', '0')
		}
	});
}

function setInputName(currInput) {
	$('#stall').html('');
	getInputList(function (allInput) {
		allInput.forEach(function (input) {
			if (currInput == input.id) {
				$("#signalCard .tail").show();
				$("#stall").html("(" + input.name + ")");
			}
		});
	});
}

function getSignalInfo() {
	function setSignalStatusAttr() {
		if (isMobile) {
			$('#signal-status').attr('class', 'status mobile');
		} else {
			$('#signal-status').attr('class', 'status');
		}
	}

	function checkExternalInput(foregroundAppInfo, inputList) {
		var isExternalInput = false;
		for (var i = 0; i < inputList.length; i++) {
			if (foregroundAppInfo.appId === inputList[i].appId) {
				isExternalInput = true;
				break;
			}
		}
		console.log("checkExternalInput() isExternalInput: " + isExternalInput);
		return isExternalInput;
	}

	function getIsPcMode(videoInfo, hdmiPcMode, currInput) {
		var isPcMode = false;
		if (videoInfo.timingMode === "PC") {
			isPcMode = true;
		} else if (videoInfo.timingMode === "TV") {
			isPcMode = false;
		} else {
			if ((hdmiPcMode.hdmi1 !== undefined && hdmiPcMode.hdmi1 && currInput === "HDMI_1")
				|| (hdmiPcMode.hdmi2 !== undefined && hdmiPcMode.hdmi2 && currInput === "HDMI_2")
				|| (hdmiPcMode.hdmi3 !== undefined && hdmiPcMode.hdmi3 && currInput === "HDMI_3")
				|| (hdmiPcMode.hdmi4 !== undefined && hdmiPcMode.hdmi4 && currInput === "HDMI_4")) {
				isPcMode = true;
			}
			else {
				isPcMode = false;
			}
		}
		return isPcMode;
	}

	function getScanType(videoInfo) {
		var scanType = "";
		if (videoInfo.scanType === "VIDEO_PROGRESSIVE") {
			scanType = "p";
		} else if (videoInfo.scanType === "VIDEO_INTERLACED") {
			scanType = "i";
		}
		return scanType;
	}

	getForegroundAppInfo(function (foregroundAppInfo) {
		console.log('foregroundAppInfo:', foregroundAppInfo);
		getInputList(function (inputList) {
			console.log('inputList:', inputList);
			if (!checkExternalInput(foregroundAppInfo, inputList)) {
				$('#signal').html(capitalizeFirstLetter(Locale.getText("NOT AVAILABLE")));
				setBackground($('#signalCard'), true);
				$("#signalCard .tail").hide();
				return;
			}

			isNoSignal(function (msg) {
				setSignalStatusAttr();

				getHdmiInput(function (currInput) {
					var isNoSignal = !msg || msg.noSignal;
					if (isNoSignal) {
						$('#signal').html(Locale.getText("No Signal"));
						setBackground($('#signalCard'), false);
						setInputName(currInput);
						return;
					}

					getVideoSize(function (videoInfo) {
						if (!videoInfo || !videoInfo.returnValue || (videoInfo.width === 0 && videoInfo.height === 0)) {
							$('#signal').html(Locale.getText("NOT AVAILABLE"));
							setBackground($('#signalCard'), true);
							$("#signalCard .tail").hide();
							return;
						}

						getHdmiPcMode(function (hdmiPcMode) {
							var isPcMode = getIsPcMode(videoInfo, hdmiPcMode, currInput);
							var scanType = getScanType(videoInfo);

							var generalResolution = videoInfo.height + scanType;
							var pcModeResolution = videoInfo.width + " X " + videoInfo.height;
							var text = isPcMode ? pcModeResolution : generalResolution;

							$('#signal').html(text);
							setBackground($('#signalCard'), true);
							setInputName(currInput);

							getVideoStillStatus(function (msg) {
								if (msg.state == "stalled") {
									$('#signal').html(Locale.getText("Stalled Image"));
									$('#stall').html("over " + msg["period(minutes)"] + " min.");
									setBackground($('#signalCard'), false);
								}
							});
						});
					});
				});
			});
		});
	});
}

function getBrightness() {
	getBluMaintain(function (res) {
		if (res.returnValue) {
			$('#brightness').text(res.bluMaintain + ' cd/m2');
			setBackground($('#brighnessCard'), true);
		} else {
			$('#brightness').text(Locale.getText('NG'));
			setBackground($('#brighnessCard'), false);
		}
	})
}

function getAcCurrent() {
	getPowerCurrent(function (res) {
		if (res.returnValue) {
			$('#acCurrent').text(res.powerCurrent + ' A');
			setBackground($('#acCurrentCard'), true);
		} else {
			$('#acCurrent').text(Locale.getText('NG'));
			setBackground($('#acCurrentCard'), false);
		}
	})
}

function getPsuStatus() {
	getSDMInfo(function (msg) {
		var PSUStatus = msg.PSUStatus;
		var checkResult = true;

		if (PSUStatus) {
			for (var i = 0; i < PSUStatus.length; i++) {
				for (var variable in PSUStatus[i]) {
					if (PSUStatus[i].hasOwnProperty(variable)) {
						if (!PSUStatus[i][variable]) {
							checkResult = false;
							break;
						}
					}
				}
				if (!checkResult) {
					break;
				}
			}

			$('#PSU').html(checkResult ? 'OK' : 'NG');
			setBackground($('#PSUCard'), checkResult);
		} else {
			setBackground($('#PSUCard'), false);
		}
	});
}
