var movie = ["avi", "mp4", "wmv", 'divx', 'asf', 'm4v', 'mov',
			'3gp', '3g2', 'mkv', 'ts', 'trp', 'tp', 'mts',
			'mpg', 'mpeg', 'dat', 'vob'];
//var music = ["mp3", "wav"];
var image = ["png", "bmp", "jpg", 'jpeg', 'jpe'];

function addFileFilter() {
	var filter = '';

	var addFilter = function(ext) {
		if (filter != '') {
			filter += ',';
		}
		filter += '.' + ext;
	}

	movie.forEach(addFilter);
	image.forEach(addFilter);

	$('#upload').attr('accept', filter);
}

function drawFilterSelect(curDir, filter) {
	var dropDown = createDropDown('filter', '-', function(selected) {
		window.location.replace(window.location.pathname + "?dir=" + curDir + "&filter=" + selected);
	});
	addDropDownOption(dropDown, " ", Locale.getText("All"));
	addDropDownOption(dropDown, "video", Locale.getText("Video"));
	addDropDownOption(dropDown, "image", Locale.getText("Image"));

	$('#filter').append(dropDown);

	setDropButtonText('filter', filter);
}

function addRenameModal(id, title, content, renameCallback, overwriteCallback) {
	var html =
		'<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="' + id + '">'
			+ '<div class="modal-dialog modal-sm">'
				+ '<div class="modal-content">'
					+ '<div class="modal-header">'
						+ '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
						+ '<h4 class="modal-title" id="gridSystemModalLabel">' + title + '</h4>'
					+ '</div>'
					+ '<div class="modal-body">'
						+ '<span id="' + id + 'Content">' + content + '</span>'
						+ '<br>'
						+ '<button class="btn btn-primary" data-dismiss="modal" id="' + id + 'ModalRename">Rename</button>&nbsp;&nbsp;'
						+ '<button class="btn" data-dismiss="modal" id="' + id + 'ModalOverwrite">Overwrite</button>'
					+ '</div>'
				+ '</div>'
			+ '</div>'
		+ '</div>';

	var modal = $(html);
	$('body').append(modal);

	$('#' + id + 'ModalRename').click(renameCallback);
	$('#' + id + 'ModalOverwrite').click(overwriteCallback);
}

function rename(filename) {
	var split = filename.lastIndexOf('.');
	var name = filename.substring(0, split);
	var ext = split >= 0 ? filename.substr(split + 1) : '';

	var regex = /\(\d+\)$/;
	if (regex.test(name)) {
		name = name.substring(0, name.lastIndexOf('(')).trim();
	}

	var i = 2;
	do {
		var newname = name + ' (' + i + ').' + ext;
		++i;
	} while (allFiles.indexOf(newname.replace(/&/gi, "&amp;")) >= 0);

	return newname;
}

function init(curDir, filter) {
	if (filter == '') {
		filter = ' ';
	}

	$('span.dir').click(function() {
		var dir = $(this).attr('name');
		window.location.replace(window.location.pathname + "?dir=" + curDir + '/' + dir + '&filter=' + filter);
	});

	$("span.img").each(function(index) {
		var id = $(this).attr('id');
		var media = $(this).attr('name');
		var format = media.substr(media.length - 3, 3).toLowerCase();
		getThumbnail(id, curDir + '/' + media);
	});

	$("div.icon").each(function(index) {
		var media = $(this).attr('name');
		var format = media.substring(media.lastIndexOf('.') + 1).toLowerCase();

		if(movie.indexOf(format) != -1){
			$(this).attr('class', 'icon_record');
		}else if(image.indexOf(format) != -1){
			$(this).attr('class', 'icon_photo');
		/*
		}else if(music.indexOf(format) != -1){
			$(this).attr('class', 'icon_music');
		*/
		} else {
			$(this).attr('class', 'icon_not_supported');
		}
	});

	$(document).on('change', '.btn-file :file', function() {
		$('#fileName').html($(this).val());
	});

	function uploadFile(file, newname) {
		var formData = new FormData();

		formData.append('dir', curDir);
		formData.append('newname', newname);
		formData.append('media', file);

		if (!file) {
			showWarning('Please choose a file to upload', 5 * 1000);
			return;
		}

		var fileName = file.name;
		var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();

		if (movie.indexOf(ext) < 0 && image.indexOf(ext) < 0) {
			showWarning(Locale.getText('Unsupported File type. Only Image/Video files are allowed.'), 5 * 1000);
			return;
		}

		if (file.size > maxSize) {
			showWarning(Locale.getText('Flash memory is not enough.'), 5 * 1000);
			return;
		}

		overlayControl(true, Locale.getText("Uploading") + "<br>" + newname);
		$("#progress").show();
		$("input").prop("disabled", true);
		$("button").prop("disabled", true);
		
		var xhr = new XMLHttpRequest();
		
		xhr.open('post', '/upload', true);
		//xhr.setRequestHeader("Content-type","multipart/form-data");
		
		xhr.upload.onprogress = function(e) {
			if (e.lengthComputable) {
				var per = Math.round((e.loaded / e.total) * 100)
				$('#uploadProgress').html(per + "%");
				$('#uploadProgress').css('width', per + '%');
				$('#overlay_progress').html(per + "%");
			}
		};

		xhr.upload.onload = function(e) {
			setTimeout(function() {
				overlayControl(false);
				window.location.reload(true);
			}, 2000);
		}
		
		xhr.onerror = function(e) {
			overlayControl(false);
			alert('An error occurred while submitting the form.');
			console.log('An error occurred while submitting the form. Maybe your file is too big');
		};

		xhr.onload = function() {
			console.log(this.statusText);
		};

		xhr.send(formData);
	}

	$('input[type="submit"]').on('click', function(evt) {
		evt.preventDefault();
		var file = document.getElementById('upload').files[0];
		//var newname = file.name.replace(/ /g, '_');
		var newname = file.name;

		if (allFiles.indexOf(newname.replace(/&/gi, "&amp;")) >= 0) {
			var modalID = 'rename' + (new Date().valueOf());
			var tempName = rename(newname);
			addRenameModal(modalID, "Rename or Overwrite?",
				newname + ' already exists.<br>Rename to ' + tempName + '<br>or Overwrite?',
			function() {
				newname = tempName;
				setTimeout(function() {
					uploadFile(file, newname);
				}, 500);
				$('#' + modalID).close();
				$('#' + modalID).remove();
			}, function() {
				setTimeout(function() {
					uploadFile(file, newname);
				}, 500);
				$('#' + modalID).close();
				$('#' + modalID).remove();
			});

			$('#' + modalID).modal();
			return;
		}

		uploadFile(file, newname);

	});

	$('#upload').change(function(e) {
		if (e.target.files[0].name) {
			$('#submit').attr('disabled', false);
		}
	});

	var fileName = undefined;
	addConfirmModal('deleteModal', Locale.getText('Delete'), '', function() {
		deleteFile("signage", fileName, function() {
			window.location.reload(true);
		});
	});

	$("[action='del']").click(function() {
		fileName = curDir + $(this).attr("name");
		//$('#deleteModalContent').html("Do you want to delete this Media file?<br>" + fileName.replace('/',''));
		$('#deleteModalContent').html(Locale.getText('Delete?') + "<br>" + fileName.replace('/',''));
		$('#deleteModal').modal();
	});

	drawFilterSelect(curDir, filter);

	addFileFilter();

	$("#progress").hide();

	$('#pageTitle').html(Locale.getText('Media Library'));
}

socket.on("thumbnail", function(msg) {
	var newSrc = "/tmp/tnm/" + encodeURIComponent(msg.fileName + "_.jpg");
	var imgClass = 'img-responsive';
	if (msg.result && msg.result.returnValue === false) {
		imgClass = 'img-responsive default-thumb-iamge';
		// refer to photo&video
		switch(msg.result.errorCode) {
			case 'COMMON_ERROR(-3)':
			case 'COMMON_ERROR(-51)':
				newSrc = msg.mediaType === 'image' ? '/images/photo_invalid.png' : '/images/video_invalid.png';
				break;

			case 'COMMON_ERROR(-55)': //video only
				newSrc = '/images/video_invalid.png;'
				break;

			case 'COMMON_ERROR(-61)':
			case 'COMMON_ERROR(-62)':
			case 'COMMON_ERROR(-63)': //video only
				//#IF_COMMERCIAL add fhd option
				newSrc = '/images/HEVC_H264thumb.png';
				break;

			default:
				newSrc = msg.mediaType === 'image' ? '/images/photo_default.png' : '/images/video_default.png';
				break;
		}
	}

	var html =
		'<div class="media-image-box">' +
			'<img src="' + newSrc +'" title="' + msg.fileName + '" class="' + imgClass + '">'
		'</div>'

	$("#" + msg.id).html(html);
});
