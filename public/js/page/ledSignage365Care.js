var installPopUpTimer;
var updatePopupTimer;
var signature ='';
var downloadUrl ='';
var appVersion='00.00.00';
var accountName='UnKnown';
var accountNo='000000'

function input_pin_for() {
    var input_pin = '';
    for(var i = 0; i < 6; ++i) {
        input_pin += '<input class="input_circle_pin" type="text">';
    }
    return input_pin;
}

function addAppTimeZoneSettingError() {
    var html
        = '<div class="modal-content" name=appTimeZoneError>'
            + '<div class="modal-header">'
                + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
            + '</div>'
            + '<div class="modal-body">'
                + '<table style="border:0px;">'
                    + '<tr><th style="border-top: none;"><font color="#4D4D4D">' + Locale.getText('There is a difference between your signage and the {apptitle} server in time and date.').replace('{apptitle}', 'LG ConnectedCare') + '<br/>' + Locale.getText('Do you want to set the time or date automatically?') + '</font></th></tr>'
                +'</table>'
            + '</div>'
            + '<div class="modal-footer">'
                + '<button class="cardBtn" data-dismiss="modal">' + Locale.getText('No') + '</button>'
                + '<button class="cardBtn" name=autoTimeSet>' + Locale.getText('Yes') + '</button>'
            + '</div>'
        +'</div>'
    return html;
}

function addAppUpdateFailedModal() {
    var html
        = '<div class="modal-content" id=appUpdateFailed>'
            + '<div class="modal-header">'
                + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                    + '<h4 class="modal-title" id="gridSystemModalLabel">' + Locale.getText('Update Failed') + '</h4>'
            + '</div>'
            + '<div class="modal-body">'
                + '<table style="border:0px;">'
                    + '<tr><th style="border-top: none;"><font color="#4D4D4D">' + Locale.getText('The update of the {apptitle} has failed.').replace('{apptitle}', Locale.getText('LG ConnectedCare app')) + '<br/>' + Locale.getText('Please check the installation file and try again.') + '</font></th></tr>'
                +'</table>'
            + '</div>'
            + '<div class="modal-footer">'
                + '<button class="cardBtn" data-dismiss="modal">' + Locale.getText('OK') + '</button>'
            + '</div>'
        +'</div>'
    return html;
}

function addAppInstallFailedModal() {
    var html
        = '<div class="modal-content" id=appInstallFailed>'
            + '<div class="modal-header">'
                + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                    + '<h4 class="modal-title" id="gridSystemModalLabel">' + Locale.getText('Installation Failed') + '</h4>'
            + '</div>'
            + '<div class="modal-body">'
                + '<table style="border:0px;">'
                    + '<tr><th style="border-top: none;"><font color="#4D4D4D">' + Locale.getText('The installation of the {apptitle} has failed.').replace('{apptitle}', Locale.getText('LG ConnectedCare app')) + '<br/>' + Locale.getText('Please check the installation file and try again.') + '</font></th></tr>'
                +'</table>'
            + '</div>'
            + '<div class="modal-footer">'
                + '<button class="cardBtn" data-dismiss="modal">' + Locale.getText('OK') + '</button>'
            + '</div>'
        +'</div>'
    return html;
}

function addAppUpdateSucceedModal() {
    var html
        = '<div class="modal-content" id=appUpdateSucceeded>'
            + '<div class="modal-header">'
                + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                    + '<h4 class="modal-title" id="gridSystemModalLabel">' + Locale.getText('Update Succeeded') + '</h4>'
            + '</div>'
            + '<div class="modal-body">'
                + '<table style="border:0px;">'
                    + '<tr><th style="border-top: none;"><font color="#4D4D4D">' + Locale.getText('The {apptitle} has been successfully updated.').replace('{apptitle}', Locale.getText('LG ConnectedCare app')) + '<br/>' + Locale.getText('This pop up will be closed automatically in 5 sec.') + '</font></th></tr>'
                +'</table>'
            + '</div>'
            + '<div class="modal-footer">'
                + '<button class="cardBtn" id=updateSucceed>' + Locale.getText('OK') + '</button>'
            + '</div>'
        +'</div>'
    return html;
}

function addAppInstallSucceedModal() {
    var html
        = '<div class="modal-content" id=appInstallSucceeded>'
            + '<div class="modal-header">'
                + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                    + '<h4 class="modal-title" id="gridSystemModalLabel">' + Locale.getText('Install Succeeded') + '</h4>'
            + '</div>'
            + '<div class="modal-body">'
                + '<table style="border:0px;">'
                    + '<tr><th style="border-top: none;"><font color="#4D4D4D">' + Locale.getText('The {apptitle} has been successfully installed.').replace('{apptitle}', Locale.getText('LG ConnectedCare app')) + '<br/>' + Locale.getText('This pop up will be closed automatically in 5 sec.') + '</font></th></tr>'
                +'</table>'
            + '</div>'
            + '<div class="modal-footer">'
                + '<button class="cardBtn" id=installSucceed>' + Locale.getText('OK') + '</button>'
            + '</div>'
        +'</div>'
    return html;
}

function addAppInstallModal() {
    var html
        = '<div class="modal-content" id=appInstall>'
            + '<div class="modal-header">'
                + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                    + '<h4 class="modal-title" id="gridSystemModalLabel">' + Locale.getText('Installing…') + '</h4>'
            + '</div>'
            + '<div class="modal-body">'
                + '<table style="border:0px;">'
                    + '<tr><th style="border-top: none;"><font color="#4D4D4D">' + Locale.getText('The installation of the {apptitle} is in progress.').replace('{apptitle}', Locale.getText('LG ConnectedCare app')) + '<br/>' + Locale.getText("Please don't turn off the device and wait until the installation is completed.") + '</font></th></tr>'
                +'</table>'
            + '</div>'
            + '<div class="modal-footer" id="appInstallOverlay">'
            + '</div>'
        +'</div>'
    return html;
}


function addAccountCheckModal() {
    var html
        = '<div class="modal-content" id=accountCheck>'
            + '<div class="modal-header">'
                + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                    + '<h4 class="modal-title" id="gridSystemModalLabel">' + Locale.getText('Confirm Account Number') + '</h4>'
            + '</div>'
            + '<div class="modal-body">'
                + '<table style="border:0px;">'
                    + '<tr><th style="border-top: none;"><font color="#4D4D4D">' + Locale.getText('Confirm the account number you entered.') + '</font></th></tr>'
                    + '<tr><td id="accountPasswd"></td></tr>'
                    + '<tr><td id="accountName"></td></tr>'
                +'</table>'
            + '</div>'
            + '<div class="modal-footer">'
                + '<button class="cardBtn" data-dismiss="modal">' + Locale.getText('Cancel') + '</button>'
                + '<button class="cardBtn" id=acccountNameChkBack>' + Locale.getText('Back') + '</button>'
                + '<button class="cardBtn" id=acccountConfirm>' + Locale.getText('Confirm') + '</button>'
            + '</div>'
        +'</div>'
    return html;
}

function addUpdateModal() {
    var title = Locale.getText("Updating…");
    var html
        = '<div class="modal fade" tabindex="-1" role="dialog" id="updateModal">'
            + '<div class="modal-dialog" style="height: 60%; width: 70%;">'
                + '<div class="modal-content" id=appUpdate>'
                    + '<div class="modal-header">'
                        + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                        + '<h4 class="modal-title" id="gridSystemModalLabel">' + title + '</h4>'
                    + '</div>'
                    + '<div class="modal-body">'
                        + '<table class="table" style="">'
                            + '<tr><th style="border-top: none;"><font color="#4D4D4D">' + Locale.getText('The update of the {apptitle} is in progress.').replace('{apptitle}', Locale.getText('LG ConnectedCare app')) + '<br/>' + Locale.getText("Please don't turn off the device and wait until the installation is completed.") +'</font></th></tr>'
                        + '</table>'
                    + '</div>'
                    + '<div class="modal-footer" id=appUpdateOverlay>'
                    + '</div>'
                + '</div>'
                    + addAppUpdateFailedModal()
                    + addAppUpdateSucceedModal()
                    + addAppTimeZoneSettingError()
                    + addNetworkError()
            + '</div>'
        + '</div>';

    var modal = $(html);
    $('body').append(modal);
}

function addResetModal() {
    var deleteMsg = Locale.getText('{apptitle} will be deleted.').replace('{apptitle}', Locale.getText('LG ConnectedCare App'));

    var html
        = '<div class="modal fade" tabindex="-1" role="dialog" id="resetModal">'
            + '<div class="modal-dialog" style="height: 60%; width: 70%;">'
                + '<div class="modal-content" id=appReset>'
                    + '<div class="modal-header" style="border-bottom: none;">'
                        + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                    + '</div>'
                    + '<div class="modal-body">'
                        + '<table class="table" style="color: #4D4D4D;">'
                            + '<tr><th style="border-top: none;">' + deleteMsg + '<br/>' + Locale.getText('Do you want to proceed?') + '</th></tr>'
                        + '</table>'
                    + '</div>'
                    + '<div class="modal-footer">'
                        + '<button class="cardBtn" data-dismiss="modal">' + Locale.getText('Cancel') + '</button>'
                        + '<button class="cardBtn" id=appResetChk>' + Locale.getText('OK') + '</button>'
                    + '</div>'
                + '</div>'
            + '</div>'
        + '</div>';

    var modal = $(html);
    $('body').append(modal);
}

function addInstallModal() {
    var title = Locale.getText("Enter Account Number");
    var html
        = '<div class="modal fade" tabindex="-1" role="dialog" id="installModal">'
            + '<div class="modal-dialog" style="height: 60%; width: 70%;">'
                + '<div class="modal-content" id=accountInput>'
                    + '<div class="modal-header">'
                        + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                        + '<h4 class="modal-title" id="gridSystemModalLabel">' + title + '</h4>'
                    + '</div>'
                    + '<div class="modal-body">'
                        + '<table class="table" style="color: white;">'
                            + '<tr><th style="border-top: none;">'
                                + '<div id="txtCategoryPin">'
                                    + Locale.getText('Enter the account number to activate {apptitle}.').replace('{apptitle}', 'LG ConnectedCare')
                                + '</div></th></tr>'
                            + '<tr><td style="border-top: none;"><div id="currentPin" class="box_input_circle_pin">'
                                + input_pin_for()
                            + '</div></td></tr>'
                        +'</table>'
                    + '</div>'
                    + '<div class="modal-footer">'
                        + '<button class="cardBtn" data-dismiss="modal">' + Locale.getText('Cancel') + '</button>'
                        + '<button class="cardBtn" id=acccountNameChk>' + Locale.getText('OK') + '</button>'
                    + '</div>'
                + '</div>'
                    + addAccountCheckModal()
                    + addAppInstallModal()
                    + addAppInstallSucceedModal()
                    + addAppInstallFailedModal()
                    + addNetworkError()
                    + addAppTimeZoneSettingError()
            + '</div>'
        + '</div>';

    var modal = $(html);
    $('body').append(modal);
}

function addNetworkModal() {
    var title = Locale.getText("Require Network Settings");
    var html
        = '<div class="modal fade" tabindex="-1" role="dialog" id="networkModal">'
            + '<div class="modal-dialog" style="height: 60%; width: 70%;">'
                + '<div class="modal-content" id=networkSettings>'
                    + '<div class="modal-header">'
                        + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                        + '<h4 class="modal-title" id="gridSystemModalLabel">' + title + '</h4>'
                    + '</div>'
                    + '<div class="modal-body">'
                        + '<table class="table" style="">'
                            + '<tr><th style="border-top: none;">' + Locale.getText('To use {apptitle}, you need to connect the device to the network. Do you want to set the network connection?').replace('{apptitle}', 'LG ConnectedCare') + '</th></tr>'
                        + '</table>'
                    + '</div>'
                    + '<div class="modal-footer">'
                        + '<button class="cardBtn" data-dismiss="modal">' + Locale.getText('No') + '</button>'
                        + '<button class="cardBtn" name="networkSettingsChk">' + Locale.getText('Network Settings') + '</button>'
                    + '</div>'
                + '</div>'
            + '</div>'
        + '</div>';

    var modal = $(html);
    $('body').append(modal);
}

function addNetworkError() {
    var html =
        '<div class="modal-content" name=networkError>'
            + '<div class="modal-header">'
                + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                + '<h4 class="modal-title" id="gridSystemModalLabel">' + Locale.getText("Require Network Settings") + '</h4>'
            + '</div>'
            + '<div class="modal-body">'
                + '<table class="table" style="">'
                    + '<tr><th style="border-top: none;">' + Locale.getText('To use {apptitle}, you need to connect the device to the network. Do you want to set the network connection?').replace('{apptitle}', 'LG ConnectedCare') + '</th></tr>'
                + '</table>'
            + '</div>'
        + '<div class="modal-footer">'
                + '<button class="cardBtn" data-dismiss="modal">' + Locale.getText('No') + '</button>'
                + '<button class="cardBtn" name="networkSettingsChk">' + Locale.getText('Network Settings') + '</button>'
            + '</div>'
        + '</div>'
    return html;
}


function appInstall() {
    overlayControlSC(true, Locale.getText('Loading…'), '#appInstallOverlay');

    get365CareServiceMode( function(mode) {
        appDownloadURL( mode.commer365CareServiceMode, function(ret) {
            if (ret === "ERROR") {
                $('#appInstall').hide();
                $('[name="networkError"]').show();
            } else {
                signature = ret.signature;
                installIpkDownload(ret.downloadUrl, function(ret) {
                });
            }
        });
    });

    socket.on('installIpk', function(ret) {
        if (ret.completionStatusCode == 200) {
            care365Disable( function() {
                setSystemSettings('commercial', {
                    care365Enable: "off"
                }, function() {
                    install365CareApp(signature, ret.destFile,  function(ret) {
                    });
                });
            });
        } else {
            overlayControlSC(false);
            $('#appInstall').hide();
            $('#appInstallFailed').show();
        }
    });

    socket.on('careInstall', function(ret) {
        overlayControlSC(false);
        $('#appInstall').hide();

        if (ret.details.state === "installed") {
            $('#appInstallSucceeded').show();
            installPopupTimer = setTimeout(function() {
                $('#installModal').modal('hide');
                overlayControl20(true, Locale.getText("Loading…"));
                care365Enable( function() {
                   setSystemSettings('commercial', {
                        care365Enable: "on"
                    }, function() {
                        getServerStatusCare365Subscribe( function (ret) {
                        });
                        location.reload();
                    });
                });
            }, 5000);
        } else {
            $('#appInstallFailed').show();
        }
    });
}

function appUpdate() {
    overlayControlSC(true, Locale.getText('Loading…'),'#appUpdateOverlay');

    installIpkDownload(downloadUrl, function(ret) {
    });

    socket.on('installIpk', function(ret) {
        if (ret.completionStatusCode == 200) {
            install365CareApp(signature, ret.destFile,  function(ret) {
            });
        } else {
            overlayControlSC(false);
            $('#appUpdate').hide();
            $('#appUpdateFailed').show();
        }
    });

    socket.on('careInstall', function(ret) {
        overlayControlSC(false);
        $('#appUpdate').hide();

        if (ret.details.state === "installed") {
            $('#appUpdateSucceeded').show();
            updatePopupTimer = setTimeout(function() {
                $('#updateModal').modal('hide');
                overlayControl20(true, Locale.getText("Loading…"));
                care365Enable( function() {
                    setSystemSettings('commercial', {
                         care365Enable: "on"
                     }, function() {
                         getServerStatusCare365Subscribe( function (ret) {
                         });
                         location.reload();
                     });
                 });
            }, 5000);
        } else {
            $('#appUpdateFailed').show();
        }
    });
}

function appReset() {
    care365Disable( function() {
        remove365CareApp( function() {
            setSystemSettings('commercial', {
                care365Enable: "off",
                signage365CareAccountNumber: "",
                signage365CareAccountName: ""
            }, function() {
                setTimeout( function() {
                    $('#resetModal').modal('hide');
                    location.reload();
                }, 2500);
            });
        });
    });
}

function refreshPinPassWord() {
    $(".input_circle_pin").each(function () {
        $(this).val("");
        onBlurInputPin($(this));
    });
}

function accountChk(currentPin) {
    var accountName = '(Unknown)';

    if (currentPin.length != 6) {
        $("#txtCategoryPin").css("color", "#ff0000");
        refreshPinPassWord();
    } else {
        searchAccountName(currentPin, function(ret) {
            $('#accountInput').hide();
            if (ret === "ERROR") {
                $('[name="networkError"]').show();
            } else if (ret === "TIME") {
                getUseNetworkTime( function(ret) {
                    if (!ret.useNetworkTime) {
                        $('[name="appTimeZoneError"]').show();
                    } else {
                        $('[name="networkError"]').show();
                    }
                });
            } else {
                if (ret.resultCode == 0) {
                    accountName = ret.companyInfo.name;
                    $('#acccountConfirm').attr('disabled', false);

                    setSystemSettings('commercial', {
                        signage365CareAccountName: accountName,
                        signage365CareAccountNumber: currentPin
                    }, function(ret){} );
                } else {
                    $('#acccountConfirm').attr('disabled', true);
                }
                $('#accountName').html("<h2><font color='#4D4D4D'>" + accountName + "</font></h2>");
                $('#accountCheck').show();
            }
        });
    }
}

function initResetModal() {
    $('#scReset').click(function() {
        $('#appReset').show();
        $('#resetModal').modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    $('#appResetChk').click(function() {
        appReset();
    });
}

function networkModalOn() {
    $('#networkSettings').show();
    $('#networkModal').modal({
        backdrop: 'static',
        keyboard: false
    });
}

function initNetworkModal() {
    $('#scUpdate').click(function() {
        networkModalOn();
    });

     $('#scUpdateCheck').click(function() {
        networkModalOn();
    });

    $('#scInstall').click(function() {
        networkModalOn();
    });
}

function initUpdateModal() {
    $('#scUpdate').click(function() {
        $('#appUpdate').show();
        $('[name="appTimeZoneError"]').hide();
        $('#appUpdateSucceeded').hide();
        $('#appUpdateFailed').hide();
        $('[name="networkError"]').hide();
        $('#updateModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        appUpdate();
    });

    $('#updateSucceed').click(function() {
        $('#updateModal').modal('hide');
        overlayControl20(true, Locale.getText("Loading…"));
        clearTimeout(updatePopupTimer);
        care365Enable( function() {
            setSystemSettings('commercial', {
                care365Enable: "on"
            }, function() {
                getServerStatusCare365Subscribe( function (ret) {
                });
                location.reload();
            });
        });
    });

    $('#scUpdateCheck').click(function() {
        overlayControl20(true, Locale.getText("Checking for update..."));

        get365CareServiceMode( function(mode) {
            appDownloadURL( mode.commer365CareServiceMode, function(ret) {
                if (ret === "ERROR" || ret === "TIME") {
                    $('#appUpdate').hide();
                    $('[name="appTimeZoneError"]').hide();
                    $('#appUpdateSucceeded').hide();
                    $('#appUpdateFailed').hide();
                    $('[name="networkError"]').hide();
                    $('#updateModal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }

                if (ret === "ERROR") {
                    $('[name="networkError"]').show()
                } else if (ret === "TIME") {
                    getUseNetworkTime( function(ret) {
                        if (!ret.useNetworkTime) {
                            $('[name="appTimeZoneError"]').show();
                        } else {
                            $('[name="networkError"]').show();
                        }
                    });
                } else {
                    $('#updateModal').modal('hide');
                    var newVersion = ret.downloadUrl.split('/');
                    if (newVersion[5] != appVersion) {
                        $('#appVersion').html(Locale.getText('Ver') + '.' + appVersion + ' (' + Locale.getText('Ver') + '.' + newVersion[5] + ')');
                        $('#scUpdateCheck').hide();
                        $('#scUpdate').show();
                        signature = ret.signature;
                        downloadUrl = ret.downloadUrl;
                    }
                }
                overlayControl20('false');
            });
        });
    });

    $("button[name='autoTimeSet']").click(function() {
        autoTimeSetting();
    });
}

function autoTimeSetting() {
    setAutomaticallyTime( function(ret) {
        $('#installModal').modal('hide');
        $('#updateModal').modal('hide');
    });
}

function initInstallModal() {
    $('#scInstall').click(function() {
        $('#accountInput').show();
        $('#accountCheck').hide();
        $('#appInstall').hide();
        $('#appInstallSucceeded').hide();
        $('#appInstallFailed').hide();
        $('[name="appTimeZoneError"]').hide();
        $('[name="networkError"]').hide();
        $('#installModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $("#txtCategoryPin").css("color", "#4D4D4D");
        refreshPinPassWord();
        updateInputPin();
    });

    $('#acccountNameChk').click(function() {
        var currentPin = '';
        $("#currentPin .input_circle_pin").each(function () {
            currentPin += $(this).val();
        });
        $("#accountPasswd").html("<h2><font color='#4D4D4D'>" + currentPin + "</font></h2>");
        accountChk(currentPin);
    });

    $('#installSucceed').click(function() {
        $('#installModal').hide();
        overlayControl20(true, Locale.getText("Loading…"));
        clearTimeout(installPopupTimer);
        care365Enable( function() {
            setSystemSettings('commercial', {
                care365Enable: "on"
            }, function() {
                getServerStatusCare365Subscribe( function (ret) {
                });
                location.reload();
            });
        });
    });

    $('#acccountNameChkBack').click(function() {
        $('#accountInput').show();
        $('#accountCheck').hide();
        $("#txtCategoryPin").css("color", "#4D4D4D");
        refreshPinPassWord();
    });

    $('#acccountConfirm').click(function() {
        $('#appInstall').show();
        $('#accountCheck').hide();
        appInstall();
    });

    $("button[name='autoTimeSet']").click(function() {
        autoTimeSetting();
    });
}

function getSystemSetting365Care() {
    getSystemSettings('commercial', ['signage365CareAccountNumber', 'signage365CareAccountName','care365Enable'], function(ret) {
        accountName = ret.signage365CareAccountName;
        accountNo = ret.signage365CareAccountNumber;
        $('#appAccountNo').text(accountNo);
        $('#appAccount').text(accountName);

        var care365Enable = ret.care365Enable == "on" ? true : false;
        $('#appEnable').prop('checked', care365Enable);

        isFirstBoot( function(ret) {
            if (ret && care365Enable) {
                getServerStatusCare365Subscribe( function (ret) {
                });
            }
        });

        if (!$('#appEnable').prop('checked')) {
            $('#serverStatus').html(Locale.getText("Not Connected"));
        } else {
            getServerStatusCare365( function(ret) {
                if (ret.serverStatus == "Waiting") {
                    $('#serverStatus').html(Locale.getText("Waiting for Approval"));
                } else {
                    $('#serverStatus').html(Locale.getText(ret.serverStatus));
                }
            });
        }
    });
}

function change365Enable() {
    $('#appEnable').change( function() {
        overlayControl20(true, Locale.getText('Loading…'));
        if($('#appEnable').prop('checked')) {
            care365Enable( function() {
                setSystemSettings('commercial', {
                    care365Enable: "on"
                }, function() {
                    getServerStatusCare365Subscribe( function (ret) {
                    });
                    overlayControl20(false);
                });
            });
        } else {
            care365Disable( function() {
                setSystemSettings('commercial', {
                    care365Enable: "off"
                }, function() {
                    $('#serverStatus').html(Locale.getText("Not Connected"));
                    overlayControl20(false);
                });
            });
        }
    });
}

function getContents365Care() {
    getListApps( function(ret) {
        for (app in ret.apps) {
            if (ret.apps[app].id == "com.webos.app.commercial.care365") {
                appVersion = ret.apps[app].version;
                $('#appVersion').html(Locale.getText('Ver') + '.' + appVersion);
                $('#365CareNotInstall').hide();
                $('#365CareInstalled').show();
                break;
            }
        }
    });

    socket.on('serverStatus', function(ret) {
        if (ret.serverStatus == "Waiting") {
            $('#serverStatus').html(Locale.getText("Waiting for Approval"));
        } else {
            $('#serverStatus').html(Locale.getText(ret.serverStatus));
        }
    });
}

function changeRtl365Care() {
    if (env.locale === 'ar-SA') {
        $("h3").css("text-align", "left");
        $("h3").attr("dir", "rtl");
        $("h4").css("text-align", "left");
        $("h4").attr("dir", "rtl");
        $("table tr").attr("dir", "rtl");
        $("table tr").css("text-align", "left");
        $('#currentPin').attr("dir", "ltr");
        $("#installPrepare").append("- ");
        $("#account365").append("- ");
        $("#version365").append("- ");
        $("#serverStatus365").append("- ");
        $("#appInstallOverlay").attr("dir", "rtl");
        $("#appInstallOverlay").css("text-align", "left");
        $("#appUpdateOverlay").attr("dir", "rtl");
        $("#appUpdateOverlay").css("text-align", "left");
    } else {
        $("#installPrepare").prepend("- ");
        $("#account365").prepend("- ");
        $("#version365").prepend("- ");
        $("#serverStatus365").prepend("- ");
    }
}

function getModalonNetwork() {
    getNetworkStatus(function (ret) {
        if (ret.wired.onInternet == "yes") {
            addInstallModal();
            initInstallModal();
            addUpdateModal();
            initUpdateModal();
            adjustInputPin();
            initInputPin();
        } else {
            addNetworkModal();
            initNetworkModal();
            networkModalOn();
        }
        $("button[name='networkSettingsChk']").click(function() {
            location.href = "/ledSignage/ledControl#network";
        });

        addResetModal();
        initResetModal();
        changeRtl365Care();
    });
}