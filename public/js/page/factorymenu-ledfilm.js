function getReceivedMACsForTestPattern (slave) {
	getReceivedMac(slave, function(res) {
		if(res.returnValue){
			var tmpMac = res.result.receiverMAC;
			var receiverCount = 0;
			var macList = [];
			while(true){
				if( tmpMac['index'+ receiverCount ] != undefined ){
					if( (tmpMac['index'+receiverCount][0] != '00') || (tmpMac['index'+receiverCount][1] != '00') ){
						macList.push(tmpMac['index'+ receiverCount].toString().replace(',', ' '));
					}
					receiverCount++;
				}else{
					break;
				}
			}

			for (var i = 0; i < macList.length; i++) {
				$('#factoryUnit').append($('<option>', {value: i, text : macList[i]}));
			}

			$('#factoryUnit, #factoryTestPatternOn').prop('disabled', macList.length <= 0);
		}else{
			console.log('getReceivedMac false');
		}
	});
}

function setLayoutAutomatically () {
	overlayControl20(true, Locale.getText("Loading..."), 'factoryAppContainer');

	$.ajax({
		type: "post",
		url: "/factorymenu/ledfilm/setLedLayout",
		success: function (data) {
			switch(data.status) {
				case 'fail':
					alert(data.message);
					setTimeout(function () {
						overlayControl20(false);
						window.location.reload();
					}, 2000);
					break;
				case 'OK':
					overlayControl20(true, Locale.getText("Done"));
					setTimeout(function () {
						window.location.reload();
					}, 3000);
					break;
				default:
					break;
			}
		}
	});
}

function initLedfilm () {
	$('#factorySender').data('pre', $('#factorySender').val());
	getReceivedMACsForTestPattern($('#factorySender').val());

	$('#factorySender').change(function (ev) {
		var prevSlave = $('#factorySender').data('pre');
		setTestPattern(prevSlave, '0', 0, 0, 0, 'FF', 'FF');

		$('#factoryUnit').empty();
		$('#factoryTestPatternOn').prop('checked', false);
		$('#factorySender').data('pre', $(this).val());

		getReceivedMACsForTestPattern($(this).val());
	});

	$('#layoutAutoBtn').click(function () {
		setLayoutAutomatically();
	});

	$('#factoryTestPatternOn').change(function (ev) {
		var slave = parseInt($('#factorySender').val());

		if ($(this).prop('checked')) {
			var mac = $('#factoryUnit').text().split(' ');
			setTestPattern(slave, '1', 255, 255, 255, mac[0], mac[1]);
		} else {
			var slave = parseInt($('#factorySender').val());
			setTestPattern(slave, '0', 0, 0, 0, 'FF', 'FF');
		}
	});

	$('.uploadLayout').change(function(e) {
		try {
			if (e.target.files[0].name) {
				var file = e.target.files[0];
				var newname = 'ledLayout.json';

				uploadFile(file, newname);
				$('#layoutAutoBtn').prop('disabled', false);
			}
		} catch (e) {
			$('#layoutAutoBtn').prop('disabled', true);
		}
	});
}

function uploadFile(file, newname) {
	var formData = new FormData();

	formData.append('newname', newname);
	formData.append('ledlayout', file);

	if (!file) {
		showWarning(Locale.getText('Please choose a file to upload'), 5 * 1000);
		return;
	}

	overlayControl20(true, Locale.getText("Uploading") + "<br>" + newname, 'factoryAppContainer');

	var xhr = new XMLHttpRequest();

	xhr.open('post', '/upload', true);

	xhr.onerror = function(e) {
		overlayControl20(false);
		alert('An error occurred while submitting the form.');
	};

	xhr.onload = function() {
		overlayControl20(false);
		console.log(this.statusText);
	};

	xhr.send(formData);
}