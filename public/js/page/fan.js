function toRpmString(rpm) {
	if (rpm < 0) {
		return "<font color=red>NG";
	}
	return rpm;
}

function boolToHtml(bool) {
	if (!bool) {
		return '<font color=red>NG</font>';
	}
	return "OK";
}

function refreshSensor() {
	$('.alert').remove();
	fans.forEach(function(fan) {
		$('#' + fan.id + 'RPM > tbody').empty();
	});

	getFanStatus(function(status) {
		getFanRPM(function(rpm) {
            $(".table-fan .fan_name").each(function () {
                var text = $(this).text();
                if (text === "Normal Fan" || text === "PSU Fan") {
                    text = text.replace(" Fan", "");
                    $(this).text(Locale.getText(text) + " " + Locale.getText("Fan"));
                } else {
                    $(this).text(Locale.getText(text));
                }
            });

			fans.forEach(function(fan) {
				if (!rpm[fan.id] || rpm[fan.id].length == 0) {
					$('#' + fan.id + 'RPM').hide();
				}

				if (!rpm[fan.id]) {
					return;
				}

				for (var i = 0; i != rpm[fan.id].length; ++i) {
					var row = '<tr><td>' + (i + 1) + '</td>';
					if (status[fan.id][i]) {
						row += '<td>' + rpm[fan.id][i] + '</td>';
					} else {
						row += '<td>' + Locale.getText( boolToHtml(status[fan.id][i]) ) + '</td>';
					}
					row += '</td></tr>';

					$('#' + fan.id + 'RPM > tbody:last').append(row);
				}
			});
		});
	});

	tempSensors.forEach(function(temp) {
		getTemp(temp.id, function(msg) {
			if (!msg || !msg.returnValue) {
				$('#' + temp.id + 'Temp').html('-');
			} else {
				readTempType(function(tempType){
					if ( !tempType.isCelsius ) {
						$('#' + temp.id + 'Temp').html( Math.floor( (msg.temperature)*1.8 + 32) + '°F' );
					}else{
						$('#' + temp.id + 'Temp').html(Number(msg.temperature) + '°C' );
					}
				});
			}
		});
	});
}
