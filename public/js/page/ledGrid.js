function getResolution(str) {
	return str.match(/\d+/g);
}

function getGridSize(resW, resH) {
	var h = 1920 / resW;
	var v = 1080 / resH;
	var widthStr = $('#unitGrid').css('width');
	var heightStr = $('#gridPanel').css('height');
	var panelHeight = Number(heightStr.match(/\d+/)) - 30;
	var width = Math.floor((Number(widthStr.match(/\d+/))) / h);
	var height = Math.floor(width * resH / resW);

	/*
	if (height * v > panelHeight) {
		height = Math.floor(panelHeight / v);
		var newWidth = Math.floor(height * resW / resH);
		width = newWidth < width ? newWidth : width;
	}
	*/

	return {
		height: height,
		width: width
	};
}

function unitBorderHandler() {
	$('.unit').removeClass('sel selRight selBottom');

	if (isEditMode) {
		return;
	}

	var id = Number($(this).attr('gridID'));

	$(this).addClass('sel');

	if (id % curConfig.size.h != 0) {
		$('#gridCol' + (id + 1)).addClass('selRight');
	}

	if ((id - 1) / curConfig.size.h < curConfig.size.v - 1) {
		$('#gridCol' + (id + curConfig.size.h)).addClass('selBottom');
	}
}

function getOutputWindowDelta(curConfig) {
	var deltaX = 0;
	var deltaY = 0;
	if (curConfig.resolution === '256 x 240') {
		var outWinVal = curConfig.outputWindow;
		if (outWinVal == 1 || outWinVal == 3) {
			deltaX = 128;
		}
		if (outWinVal == 2 || outWinVal == 3) {
			deltaY = 120;
		}
	}

	return [deltaX, deltaY];
}

function changeGridConfig(h, v, resolution, outWinDelta) {
	$('#unitGrid').empty();
	$('#gridVRule').empty();
	if (h > 0 && v > 0) {
		$('#gridVRule').append("<div class='col-xs-12 grid' style='width=100%; font-size: 5pt; height:18px;'>" + outWinDelta[0] + ',' + outWinDelta[1] + "</div>");
	} else {
		$('#gridVRule').append("<div class='col-xs-12 grid' style='width=100%'>None</div>");
	}
	$('#gridHRule').empty();

	var gridWidth = 'col-xs-1';
	var noneDiv = 12 - h;

	var resArr = getResolution(resolution);
	var resW = Number(resArr[0]);
	var resH = Number(resArr[1]);

	var gridSize = getGridSize(resW, resH);

	for (var i = 0; i != v; ++i) {
		var id = 'gridRow' + i;
		var rowDiv = '<div width=100% class="col-xs-12 grid" id="' + id + '"></div>';
		$('#unitGrid').append(rowDiv);

		for (var j = 0; j != h; ++j) {
			var gridID = i * h + j + 1;
			var colID = 'gridCol' + gridID;
			var colDiv = '<div class="' + gridWidth + ' unit" id="' + colID + '" gridID=' + gridID + '></div>';
			$('#' + id).append(colDiv);
			$('#' + colID).css('width', gridSize.width + 'px');
			$('#' + colID).css('height', gridSize.height + 'px');

			if (i == v - 1) {
				$('#' + colID).addClass('bottom');
			}
		}

		$('#' + colID).addClass('rightmost');
	}

	for (var i = 0; i != h; ++i) {
		var gridDiv = $('<div class="' + gridWidth + ' grid">' + ((i + 1) * resW + outWinDelta[0]) + '&nbsp;</div>');
		gridDiv.css('width', gridSize.width);
		gridDiv.css('border-width', '0px 0px 0px 1px');
		$('#gridHRule').append(gridDiv);
	}
	if (gridDiv) {
		gridDiv.css('border-width', '0px 1px 0px 1px');
	}

	for (var i = 0; i != v; ++i) {
		var gridDiv = $('<div class="col-xs-12 grid"><span>' + ((i + 1) * resH + outWinDelta[1]) + '</span>&nbsp;</div>');
		gridDiv.css('border-width', '1px 0px 0px 0px');
		gridDiv.css('height', gridSize.height);
		gridDiv.css('line-height', (gridSize.height * 2 - 15) + 'px');
		$('#gridVRule').append(gridDiv);
	}
	if (gridDiv) {
		gridDiv.css('border-width', '1px 0px 1px 0px');
	}

	$('.unit').click(unitBorderHandler);
}

function setUnitID(config, auto) {
	function showUnitID(gridID, unitID) {
		unitID = auto ? unitID + 1 : config.onUnits[gridID];

		if (unitID == undefined) {
			unitID = -1;
		}
		if (config.regular == 0) {
			$('#unitIDBox' + gridID + ' span').html(unitID).show();
			$('#unitIDBox' + gridID).parent().attr('unitID', unitID);
			return true;
		}

		var selectVal = $('#switchSelect' + gridID).val();

		if (selectVal == undefined) {
			selectVal = 0;
		}

		var newID = selectVal >= 0 ? unitID : selectVal;
		$('#unitIDBox' + gridID).parent().attr('unitID', newID);
		$('#switchSelect' + gridID).val(newID);

		if (newID == -1) {
			return false;
		}

		$('#unitIDBox' + gridID + ' span').html(newID).toggle(!isEditMode);

		return true;
	}

	$('[id^=unitIDBox] span').html('');
	$('[id^=gridCol]').removeAttr('unitID');
	var startX = ([0, 2].indexOf(config.from) >= 0) ? 1 : config.size.h;
	var endX = ([0, 2].indexOf(config.from) >= 0) ? config.size.h + 1 : 0;
	var startY = ([0, 1].indexOf(config.from) >= 0) ? 1 : config.size.v;
	var endY = ([0, 1].indexOf(config.from) >= 0) ? config.size.v + 1 : 0;
	var diffX = ([0, 2].indexOf(config.from) >= 0) ? 1 : -1;
	var diffY = ([0, 1].indexOf(config.from) >= 0) ? 1 : -1;

	var unitID = 0;
	if (config.direction == 0) {
		for (var y = startY; y != endY; y += diffY) {
			for (var x = startX; x != endX; x += diffX) {
				var gridID = (y - 1) * config.size.h + x;
				if (showUnitID(gridID, unitID)) {
					++unitID;
				}
			}
			startX = diffX < 0 ? 1 : config.size.h;
			endX = diffX < 0 ? config.size.h + 1 : 0;
			diffX *= -1;
		}
	} else {
		for (var x = startX; x != endX; x += diffX) {
			for (var y = startY; y != endY; y += diffY) {
				var gridID = (y - 1) * config.size.h + x;
				if (showUnitID(gridID, unitID)) {
					++unitID;
				}
			}
			startY = diffY < 0 ? 1 : config.size.v;
			endY = diffY < 0 ? config.size.v + 1 : 0;
			diffY *= -1;
		}
	}

	return unitID;
}


