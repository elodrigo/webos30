var inited = false;

function getDoorStatusString(opened) {
	var statusString = Locale.getText(opened ? 'Opened' : 'Closed');
	if (opened) {
		return '<font color="red">' + statusString + '</font>';
	}
	return statusString;
}

function doorStatusCallback(msg) {
	$('.alert-warning').hide();

	$('#lockout').html(msg.muteUntilReset ? Locale.getText( "Yes" ) : Locale.getText( "No" ) );
	setDropButtonText('muteOption', msg.muteOpt);

	if (!inited) {
		inited = true;
		var size = msg.status.length;
		for (var i = 0; i != size; ++i) {
			$('#doorTable > tbody:last').append('<tr><td class=first>' + Locale.getText('Door Sensor') + (i + 1) + '</td>'
				+ '<td class=second id="door' + (i + 1) + '">'
				+ getDoorStatusString(msg.status[i]) + '</td></tr>');
		}
	} else {
		var size = msg.status.length;
		for (var i = 0; i != size; ++i) {
			$('#door' + (i + 1)).html( Locale.getText(getDoorStatusString(msg.status[i])));
			if (msg.status[i]) {
				showWarning(Locale.getText("Door xxx is opened").replace("xxx", (i + 1)));
			}
		}
	}
}

function drawDoorSelect() {
	var dropDown = createDropDown('muteOption', '-', function(selected) {
		changeDoorMuteOpt(selected);
	});
	addDropDownOption(dropDown, "never", Locale.getText('On When Open'));
	addDropDownOption(dropDown, "whenOpen", Locale.getText('Off When Open'));
	addDropDownOption(dropDown, "untilReset", Locale.getText('Off Until Reset'));

	$('#muteOption').append(dropDown);

	getDoorStatus(doorStatusCallback);
}

function initDoor(env) {
	socket.on('door', doorStatusCallback);
	
	drawDoorSelect();

	$('#reset').click(function () {
		resetDoorState();
	});

	$('#pageTitle').html(Locale.getText('Door Monitor'));
}

