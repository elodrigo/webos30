var tempSensors = [];
var fans = [];
var color = [
	"#4572a7", "#aa4643", "#89a54e", "#71588f", "#4198af", "#db843d",
	"#93a9cf", "#d19392", "#b9cd96", "#a99bbd", "#88c8da", "#f3b07a",
	"#0d62f5", "#ec1f1c", "#a1ec20", "#6e1ce2", "#1eb6e0", "#f3740c"
];

var totalHistorySize = 0;

var chartOption = {
	pointDot : true,
	pointDotRadius : 1,
	pointHitDetectionRadius : 2,
	datasetStrokeWidth : 2,

	scaleLabel: " <%=value%>",

	tooltipTemplate: "<%if (label){%><%=label%> <%}%><%=value%>",
	tooltipCaretSize: 0,
	responsive: true,
	bezierCurve : false,
	maintainAspectRatio: true,
	animation: false
};

var isCelsius = true;

function toTimeStr(date) {
	var timeStr = date.toTimeString();
	timeStr = timeStr.substr(0, timeStr.indexOf(" "));
	timeStr = timeStr.substr(0, timeStr.lastIndexOf(":"));
	return timeStr;
}

function createLabels(length, interval) {
	var labels = [];
	var curTime = currentTime.split(' ').splice(0, 5).join(' ');
	var curDate = new Date(curTime);
	var tempDate = new Date();
	interval = interval * 1000;
	for (var i = 0; i != length; ++i) {
		tempDate.setTime(curDate.getTime() - interval * (defaultHistorySize * (chartPage - 1) + (length - i - 1)));
		var timeStr = toTimeStr(tempDate);
		labels.push(timeStr);
	}
	return labels;
}

function toggletemperature(){
		if(!isCelsius){
			$("#img_celsius").css("visibility", "hidden");
			$("#img_fahrenheit").css("visibility", "visible");
		}else{
			$("#img_fahrenheit").css("visibility", "hidden");
			$("#img_celsius").css("visibility", "visible");
		}
		writeTempType(!isCelsius);
		window.location.reload();
}

function init() {

	readTempType(function(tempType){

		isCelsius = tempType.isCelsius;
		if(isCelsius){
			$('#fahrButton').removeClass('sel');
			$('#celButton').addClass('sel');
		}else{
			$('#celButton').removeClass('sel');
			$('#fahrButton').addClass('sel');
		}

	});


	function unitButtonHandler() {
		if(!isCelsius){
			$('#fahrButton').addClass('sel');
			$('#celButton').removeClass('sel');
		}else{
			$('#celButton').addClass('sel');
			$('#fahrButton').removeClass('sel');
		}
		writeTempType(isCelsius);
		window.location.reload();
	}
	$('#celButton').click(function(){
		isCelsius = true;
		unitButtonHandler();
	});

	$('#fahrButton').click(function(){
		isCelsius = false;
		unitButtonHandler();
	});
}

function drawTempChart(history, interval, isFromDashboard, contextId) {
	if (!history || !history[0]) {
		showWarning(Locale.getText("Temperature history is not yet collected"));
		return;
	}

	var repHistory = undefined;
	for (var i = 0; i != history.length; ++i) {
		if (!history[i]) {
			continue;
		}
		if (history[i].length == 0) {
			alert("Data is not collected now. wait for maximum " + interval + " second(s)");
			return;
		}
		refHistory = history[i];
		break;
	}

	for (var i = 0; i != history.length; ++i) {
		if (history[i]) {
			history[i].reverse();
		}
	}

	var min = 987;
	var max = -987;

	var errorSensors = '';
	var sensorCount = env.support49XE ? tempSensors.length + 1 : tempSensors.length;

	for (var i = 0; i != sensorCount; ++i) {
		var error = false;
		for (var j = 0; history[i] && j != history[i].length; ++j) {

			if(!isCelsius){
				history[i][j] = Math.floor( (history[i][j]*1.8) + 32);
			}

			if (history[i][j] <= -40) {
				history[i][j] = -40;
				error = true;
			}
			min = history[i][j] < min ? history[i][j] : min;
			max = history[i][j] > max ? history[i][j] : max;
		}
		if (error) {
			if(tempSensors[i]!=undefined)
				errorSensors += Locale.getText(tempSensors[i].name) + ' ';
		}
	}

	if (min == -40) {
		var msg = Locale.getText('Check Temperature sensors. -40 means errors when reading Temperature sensors') + '\u202A (' + errorSensors + ')\u202C';
		/*
		if (isCelsius) {
			msg = msg.replace('-40', '-40°C');
		} else {
			msg = msg.replace('-40', '-40°F');
		}
		*/
		showWarning(msg);
	}

	var labels = createLabels(refHistory.length, interval);
	var data = {
		labels: labels,
		datasets: []
	};

	var colorIndex = 0;
	for (var i = 0; i != tempSensors.length; ++i) {
		if (!history[i]) {
			continue;
		}

		data.datasets.push({
			label: Locale.getText(tempSensors[i].name),
			fillColor: "rgba(0,0,0,0)",
            strokeColor: color[colorIndex],
            pointColor: color[colorIndex],
            data: history[i]
		});
		++colorIndex;

		if( (i == tempSensors.length - 1) && (env.model.name == '49XE3D') ){
			data.datasets.push({
				label: Locale.getText('ΔT') + '(' + Locale.getText('External') + ' - ' + Locale.getText('Bottom') + ')',
				fillColor: "rgba(0,0,0,0)",
	            strokeColor: color[colorIndex],
	            pointColor: color[colorIndex],
	            data: history[4]
			});
		}
	}

	if(!isCelsius){
		if (min >= 32) {
			min = 32;
		} else if (min > -40) {
			min = Math.floor(min / 5) * 5 - 5;
		}
	}else{
		if (min >= 5) {
			min = 0;
		} else if (min > -40) {
			min = Math.floor(min / 5) * 5 - 5;
		}
	}

	var stepWidth = 10;
	steps = (max - min) / stepWidth + 1;

	chartOption.scaleOverride = true;
	chartOption.scaleStepWidth = stepWidth;
	chartOption.scaleStartValue = min;
	chartOption.scaleSteps = steps;
	chartOption.maintainAspectRatio = !isFromDashboard;

	var id = contextId ? contextId : 'tempChart'
    var ctx = document.getElementById(id).getContext("2d");
    var tempChart = new Chart(ctx).Line(data, chartOption);
	tempChart.update();
	var legend = tempChart.generateLegend();
	$('#tempLegend').append(legend);

	addRtlDir();
}

function clearChart(id){
	var cnvs = document.getElementById(id);
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, cnvs.width, cnvs.height);
    ctx.beginPath();
}

function drawPager() {
	var maxPage = Math.ceil(totalHistorySize / defaultHistorySize);

	function addNav(page, label) {
		if (page > maxPage) {
			page = maxPage;
		}
		if (page < 1) {
			page = 1;
		}
		var html = "<li><a href='charts?page=" + page + "'><span aria-hidden=true>" + label + "</span></a></li>";
		$('#pager').append(html);
	}

	var prevSet = chartPage + pageSet;
	var prevOne = chartPage + 1;

	addNav(prevSet, Locale.getText("Prev.") + pageSet);
	if (pageSet > 5) {
		addNav(prevOne, "&laquo;");
	}

	var startSet = (Math.ceil(chartPage / pageSet) - 1) * pageSet + 1;
	var endSet = startSet + pageSet - 1;
	if (endSet > maxPage) {
		endSet = maxPage;
	}

	for (var i = endSet; i >= startSet; --i) {
		html = '<li ';
		if (i == chartPage) {
			html += 'class=active';
		}
		html += '><a href="charts?page=' + i + '">' + i;

		html += '</a></li>';

		$('#pager').append(html);
	}

	var nextSet = chartPage - pageSet;
	var nextOne = chartPage - 1;
	if (pageSet > 5) {
		addNav(nextOne, "&raquo;");
	}
	addNav(nextSet, Locale.getText("Next") + pageSet);
}

function drawFanRpmChart(data, numFans) {
	var firstFan;

	fans.forEach(function(fan) {
		if (numFans[fan.id] != 0) {
			firstFan = data[fan.id][0];
		}
	});

	if (!firstFan.returnValue) {
		showWarning(Locale.getText("Fan RPM history is not yet collected"));
		return;
	}
	var labels = createLabels(firstFan.history.length, firstFan.interval);
	var chartData = {};
	chartData.labels = labels;
	chartData.datasets = [];

	var colorIndex = 0;
	fans.forEach(function(fan) {
		if (!data[fan.id] || data[fan.id] == 0) {
			return;
		}
		for (var i = 0; i != numFans[fan.id]; ++i) {
			var dataset = {
				label: Locale.getText( fan.name.replace(/ /gi, "") ) + (data[fan.id][i].num+1),
				fillColor: "rgba(0, 0, 0, 0)",
				strokeColor: color[colorIndex],
				pointColor: color[colorIndex],
				data: data[fan.id][i].history.reverse()
			};
			++colorIndex;
			chartData.datasets.push(dataset);
		}
	});

	var stepWidth = 1000 * 3;
	var maxRPM = 16000;

	chartOption.scaleOverride = true;
	chartOption.scaleStepWidth = stepWidth;
	chartOption.scaleStartValue = 0;
	chartOption.scaleSteps = maxRPM / stepWidth;
    var ctx = document.getElementById("fanChart").getContext("2d");
    var fanChart = new Chart(ctx).Line(chartData, chartOption);
	fanChart.update();

	var legend = fanChart.generateLegend();
	$('#fanLegend').append(legend);
}

function drawBacklightChart(backlightData, eyeqData) {
	if (!backlightData || !backlightData.returnValue) {
		showWarning(Locale.getText("History is not yet collected"));
		return;
	}
	var labels = createLabels(backlightData.history.length, backlightData.interval);
	var chartData = {};
	chartData.labels = labels;
	chartData.datasets = [];

	var backlightSet = {
		label: env.supportLedSignage ? Locale.getText("Contrast") : Locale.getText("Backlight")+"(%)",
		fillColor: "rgba(0, 0, 0, 0)",
		strokeColor: color[0],
		pointColor: color[0],
		data: backlightData.history.reverse()
	};
	chartData.datasets.push(backlightSet);

	var eyeqSet = {
		label: Locale.getText("EyeQ sensor"),
		fillColor: "rgba(0, 0, 0, 0)",
		strokeColor: color[1],
		pointColor: color[1],
		data: eyeqData.history.reverse()
	};
	chartData.datasets.push(eyeqSet);

	var max = -999;
	backlightData.history.forEach(function(val) {
		if (val > max) {
			max = val;
		}
	});
	eyeqData.history.forEach(function(val) {
		if (val > max) {
			max = val;
		}
	});

	chartOption.scaleOverride = false;
	chartOption.scaleBeginAtZero = true;

    var ctx = document.getElementById("backlightChart").getContext("2d");
    var backlightChart = new Chart(ctx).Line(chartData, chartOption);
	backlightChart.update();

	var legend = backlightChart.generateLegend();
	$('#backlightLegend').append(legend);

	addRtlDir();
}

function startTemperatureChart(tempSensors) {
	var index = 0;
	var tempHistory = [];
	function tempHistoryCallback(msg) {
		tempHistory.push(msg.history);
		if (index == tempSensors.length - 1) {
			totalHistorySize = msg.queueSize;

			if( env.model.name == '49XE3D' ){
				var deltaT = [];
				for(var i=0; i<tempHistory[0].length; i++){
					if( (tempHistory[3][i] <= -40) || (tempHistory[0][i]<= -40) ){
						deltaT.push( -40 );
					}else{
						deltaT.push( (tempHistory[3][i]-tempHistory[0][i]).toFixed(1) );
					}
				}
				tempHistory.push(deltaT);
			}
			drawPager();
			drawTempChart(tempHistory, msg.interval);
			index = 0;
			return;
		}
		++index;
		getTempHistory(tempSensors[index].id, start, end, tempHistoryCallback);
	}

	getTempHistory(tempSensors[index].id, start, end, tempHistoryCallback);

}

function startFanChart() {
	var fanHistory = {};
	var inserted = 0;
	getNumFans(function(numFans) {
		var totalFans = 0;
		fans.forEach(function(fan) {
			if (numFans[fan.id]) {
				totalFans += numFans[fan.id];
			}
		});

		fans.forEach(function(fan) {
			if (!numFans[fan.id]) {
				return;
			}
			fanHistory[fan.id] = [];
			for (var i = 0; i != numFans[fan.id]; ++i) {
				getFanRpmHistory(fan.id, i, start, end, function(msg) {
					if (msg.returnValue) {
						fanHistory[fan.id].push(msg);
					}
					++inserted
					if (inserted == totalFans) {
						drawFanRpmChart(fanHistory, numFans);
					}
				});
			}
		});
	});
}

function startBacklightChart() {
	getBacklightHistory(start, end, function(backMsg) {
		getEyeQSensorHistory(start, end, function(eyeqMsg) {
			drawBacklightChart(backMsg, eyeqMsg);
		});
	});
}

function checkHistoryData(data) {
	var result = true;
	if (!data || !data.returnValue) {
		showWarning(Locale.getText("History is not yet collected"));
		result = false;
	}

	return result;
}

function drawBrightnessChart(res) {
	var labels = createLabels(res.history.length, res.interval);
	var data = {
		labels: labels,
		datasets: []
	};

	for (var i = 0; i < res.history.length; i++) {
		if (res.history[i] < 0) {
			res.history[i] = 0;
		}
	}

	data.datasets.push({
		label: Locale.getText('Brightness'),
		fillColor: "rgba(0,0,0,0)",
		strokeColor: color[0],
		pointColor: color[0],
		data: res.history.reverse()
	});

	chartOption.scaleOverride = true;
	chartOption.scaleStepWidth = isMobile ? 1000 : 500;
	chartOption.scaleStartValue = 0;
	chartOption.scaleSteps = isMobile ? 5 : 10;

	var ctx = document.getElementById('brightnessChart').getContext("2d");
	var chart = new Chart(ctx).Line(data, chartOption);
	chart.update();
	var legend = chart.generateLegend();
	$('#brightnessLegend').append(legend);
	addRtlDir();
}

function startBrightnessChart() {
	getBluMaintainHistory(start, end, function (res) {
		if (!checkHistoryData(res)) {
			return;
		}
		drawBrightnessChart(res);
	})
}

function drawAcCurrentChart(res) {
	var labels = createLabels(res.history.length, res.interval);
	var data = {
		labels: labels,
		datasets: []
	};

	for (var i = 0; i < res.history.length; i++) {
		if (res.history[i] < 0) {
			res.history[i] = 0;
		}
	}

	data.datasets.push({
		label: Locale.getText('AC Current'),
		fillColor: "rgba(0,0,0,0)",
		strokeColor: color[0],
		pointColor: color[0],
		data: res.history.reverse()
	});

	chartOption.scaleOverride = true;
	chartOption.scaleStepWidth = isMobile ? 6 : 3;
	chartOption.scaleStartValue = 0;
	chartOption.scaleSteps = isMobile ? 5 : 10;

	var ctx = document.getElementById('AcCurrentChart').getContext("2d");
	var chart = new Chart(ctx).Line(data, chartOption);
	chart.update();
	var legend = chart.generateLegend();
	$('#AcCurrentLegend').append(legend);
	addRtlDir();
}

function startAcCurrentChart() {
	getPowerCurrentHistory(start, end, function (res) {
		if (!checkHistoryData(res)) {
			return;
		}
		drawAcCurrentChart(res);
	})
}
