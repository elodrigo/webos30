function mediaInit(curDir) {
    addConfirmModal('deleteModal', Locale.getText('Delete?'), '', function() {
        selectedFile.forEach(function(fileName){
            deleteFile("signage", fileName, function() {
            });
        });
        setTimeout(function(){
            window.location.reload(true);
        }, 500);
    });

    $('.addMedia, .uploadMedia').change(function(e) {
		if (e.target.files[0].name) {
			var file = e.target.files[0];
			var newname = file.name;
			if (mediaFiles.indexOf(newname.replace(/&/gi, "&amp;")) >= 0) {
				var modalID = 'rename' + (new Date().valueOf());
				var tempName = rename(newname);
				addRenameModal(modalID,
					Locale.getText('Rename or Overwrite?'),
					Locale.getText('{{filename}} already exists.').replace("{{filename}}", newname) + '<br>' +
					Locale.getText('Rename to {{filename (2)}} or Overwrite?').replace("{{filename (2)}}", tempName) + '<br>',
					Locale.getText('Rename'),
					Locale.getText('Overwrite'),
				function() {
					newname = tempName;
					setTimeout(function() {
						uploadFile(file, newname);
					}, 500);
					$('#' + modalID).remove();
				}, function() {
					setTimeout(function() {
						uploadFile(file, newname);
					}, 500);
					$('#' + modalID).remove();
				});
				$('#' + modalID).modal();
				return;
			}
			uploadFile(file, newname);

		}
	});

	$("img.img").each(function(index) {
		var id = $(this).attr('id');
		var media = $(this).attr('name');
		var format = media.substr(media.length - 3, 3).toLowerCase();
		getThumbnail(id, media);

		getVideoInfo(media, function(msg){
			// console.log(msg);
			if(msg.returnValue){
				$('#'+id+'_res').text(msg.originalWidth + 'X' + msg.originalHeight);
			}else{
				$('#'+id+'_res').text('Unknown');
			}
		});
	});

	$('#toolBarDelete').click(function() {
		var fileName = '<ul>'
		selectedFile.forEach(function(file){
			// console.log('do');
			fileName += '<li>'+ file;
		});
		fileName += '</ul>';

		$('#deleteModalTitle').text(Locale.getText("Delete?"));
		$('#deleteModalContent').html("<br>" + fileName.replace('/',''));
		$('#deleteModalModalOK').text(Locale.getText("OK"));
		$('#deleteModalModalCancel').text(Locale.getText("Cancel"));
		$('#deleteModal').modal();
	});

	$('.media-checkBox').click(function(){
		if($(this).prop("checked")){
			selectedFile.push($(this).attr("id"));
		}else{
			selectedFile.splice(selectedFile.indexOf($(this).attr("id")), 1);
		}

		if( selectedFile.length > 0 ){
			$('#normalToolBar').hide();
			$('#multiToolBar').show();
			$('#selectedCount').text(selectedFile.length+' item selected');
		}else{
			$('#normalToolBar').show();
			$('#multiToolBar').hide();
			$('#selectedCount').text('');
		}
	});

	$('#toolBarExport').click(function() {
		var link = document.createElement('a');
		link.setAttribute('download','');
		link.style.display = 'none';
		document.body.appendChild(link);

		var index = 0;
		selectedFile.forEach(function(fileName, index, array){
			setTimeout(function(){
				link.setAttribute('href', '/download?name='+encodeURIComponent(curDir + '/' + fileName));
				link.click();
				if(index === array.length - 1){
					setTimeout(function(){
						document.body.removeChild(link);
					}, (index++)*1000);
				}
			}, (index++)*1000)
		});
	});

	$( "button[id^='down']" ).click(function(){
		var fileName = $(this).attr('name');
		var link = document.createElement('a');
		link.setAttribute('download','');
		link.style.display = 'none';
		document.body.appendChild(link);
		link.setAttribute('href', '/download?name='+encodeURIComponent(curDir + '/' + fileName));
		link.click();
	});
}

function addRenameModal(id, title, content, button1, button2, renameCallback, overwriteCallback) {
	var html =
		'<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="' + id + '">'
			+ '<div class="modal-dialog">'
				+ '<div class="modal-content">'
					+ '<div class="modal-header">'
						+ '<button id="' + id + 'Close" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
						+ '<h4 class="modal-title" id="gridSystemModalLabel">' + title + '</h4>'
					+ '</div>'
					+ '<div class="modal-body">'
						+ '<span id="' + id + 'Content">' + content + '</span>'
						+ '<br>'
						+ '<button class="btn btn-primary" data-dismiss="modal" id="' + id + 'ModalRename">' + button1 + '</button>&nbsp;&nbsp;'
						+ '<button class="btn" data-dismiss="modal" id="' + id + 'ModalOverwrite">' + button2 + '</button>'
					+ '</div>'
				+ '</div>'
			+ '</div>'
		+ '</div>';

	var modal = $(html);
	$('body').append(modal);

	$('#' + id + 'ModalRename').click(renameCallback);
	$('#' + id + 'ModalOverwrite').click(overwriteCallback);
}

function rename(filename) {
	var split = filename.lastIndexOf('.');
	var name = filename.substring(0, split);
	var ext = split >= 0 ? filename.substr(split + 1) : '';

	var regex = /\(\d+\)$/;
	if (regex.test(name)) {
		name = name.substring(0, name.lastIndexOf('(')).trim();
	}

	var i = 2;
	do {
		var newname = name + ' (' + i + ').' + ext;
		++i;
	} while (mediaFiles.indexOf(newname.replace(/&/gi, "&amp;")) >= 0);

	return newname;
}

function uploadFile(file, newname) {
	var formData = new FormData();

	formData.append('dir', curDir);
	formData.append('newname', newname);
	formData.append('media', file);

	if (!file) {
		showWarning(Locale.getText('Please choose a file to upload'), 5 * 1000);
		return;
	}

	var fileName = file.name;
	var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();

	if (movie.indexOf(ext) < 0 && image.indexOf(ext) < 0) {
		showWarning(Locale.getText('Unsupported File type. Only Image/Video files are allowed.'), 5 * 1000);
		return;
	}

	if (file.size > maxSize) {
		showWarning(Locale.getText('Flash memory is not enough.'), 5 * 1000);
		return;
	}

	overlayControl20(true, Locale.getText("Uploading") + "<br>" + newname);
	$("#progress").show();
	$("input").prop("disabled", true);
	$("button").prop("disabled", true);

	var xhr = new XMLHttpRequest();

	xhr.open('post', '/upload', true);
	//xhr.setRequestHeader("Content-type","multipart/form-data");

	xhr.upload.onprogress = function(e) {
		if (e.lengthComputable) {
			var per = Math.round((e.loaded / e.total) * 100)
			$('#uploadProgress').html(per + "%");
			$('#uploadProgress').css('width', per + '%');
			$('#overlay_progress').html(per + "%");
		}
	};

	xhr.upload.onload = function(e) {
		setTimeout(function() {
			overlayControl20(false);
			window.location.reload(true);
		}, 2000);
	}

	xhr.onerror = function(e) {
		overlayControl20(false);
		alert('An error occurred while submitting the form.');
		console.log('An error occurred while submitting the form. Maybe your file is too big');
	};

	xhr.onload = function() {
		console.log(this.statusText);
	};

	xhr.send(formData);
}