var isPortrait = false;
var isContentRotated = false;
var isMobile = false;

function overlayControlSC(on, msg, parentDiv) {
    if (parentDiv === undefined) {
        parentDiv = 'nav.navbar-overlay';
    }
    if (on === true) {
        var over = '<div id="overlay" class="update_overlay_sc"> '
            + '<div class="update_in_progress_sc">'
            + '<center>'
            + '<img src="../images/loading.gif">'
            + '<span id=overlay_progress></span><br>'
            + '<span>' + msg + '</span>'
            + '</center>'
            + '</div>'
            + '</div>';
        $(over).prependTo(parentDiv);
    } else {
        $('#overlay').remove();
    }
}

function overlayControl(on, msg) {
	if (on === true) {
		var over = '<div id="overlay" class="update_overlay"> '
					+ '<div class="update_in_progress">'
					+ '<center>'
					+ '<img src="../images/loading.gif"><br>'
					+ '<span id=overlay_progress></span><br>'
					+ '<span>'+ msg + '</span>'
					+ '</center>'
					+ '</div>'
					+ '</div>';
		$(over).prependTo('nav.navbar-overlay');
	} else {
		$('#overlay').remove();
	}
}

function overlayControl20(on, msg, from) {
	if (on === true) {
		var over = '<div id="overlay" class="update_overlay"> '
					+ '<div class="update_in_progress">'
					+ '<center>'
					+ '<img src="../images/loading.gif"><br>'
					+ '<span id=overlay_progress></span><br>'
					+ '<span id="overlay_msg">'+ msg + '</span>'
					+ '</center>'
					+ '</div>'
					+ '</div>';
		if(!from)
			$(over).prependTo('.main-body');
		else
			$(over).prependTo('#'+from);
	} else {
		$('#overlay').remove();
	}
}



function overlaySenderUpdate(on, from) {
	var maxSenderCnt = env.numOfExtInputs === 1 ? 1 : 4;
	var senderBtns = '';

	for (var i=1; i<=maxSenderCnt; i++) {
		senderBtns += '<td><button class="overlayBtn" name="'+ i + '">Sender' + i + '</button></td>'
	}

	if (on === true) {
		var over = '<div id="overlay" class="update_overlay"> '
					+ '<div class="update_in_progress">'
					+ '<center>'
					+ '<table id="overlayBtnTable">'
					+ '<tr>'
					+ senderBtns
					+ '<td><span id=overlay_progress></span><br>'
					+ '</tr>'
					+ '<tr>'
					+ '<td colspan="4">'
					+ '<center>'
					+ '<button class="overlayBtn" id="closeOverlay">close</button>'
					+ '</center>'
					+ '</td>'
					+ '</tr>'
					+ '</table>'
					+ '<div id="updateImage" style="display:none">'
					+ '<img src="../images/loading.gif"><br>'
					+ '<span id="overlay_msg">'+ Locale.getText("LED FPGA Updating ......") + '</span>'
					+ '</div>'
					+ '</center>'
					+ '</div>'
					+ '</div>';
		if(!from)
			$(over).prependTo('.main-body');
		else
			$(over).prependTo('#'+from);
	} else {
		$('#overlay').remove();
	}

	$('#closeOverlay').click(function(){
		$('#overlay').remove();
	});
}




function overlayLEDType(on, title, message) {
	if (on === true) {
		var over = '<div id="overlay" class="ledType_overlay"> '
					+ '<div class="update_in_progress">'
					+ '<center>'
					+ '<div style="width:500px; height:200px; background-color:white; color:black;">'
					+ '<span style="font-size:20pt; font-weight:bold;">'+title+'</span><br/>'
					+ '<span style="font-size:10pt;">'+message+'</span>'
					+ '<table style="margin-top:50px;"><tr>'
					+ '<td><button id="typeMono" name="mono" class="cardBtn ledType"><span class="monoTxt">MONO</span></button></td>'
					+ '<td><button id="typeColor" name="color" class="cardBtn ledType"><span class="colorTxt">COLOR</span></button></td>'
					+ '</tr></table>'
					+ '</div>'
					+ '</center>'
					+ '</div>'
					+ '</div>';
		$(over).prependTo('.main-body');
	} else {
		$('#overlay').remove();
	}
}

function showRotationWarning() {
	$("#screenshotStatus").html('');
	getPortrait(function(msg) {
		isPortrait = msg.osdPortraitMode != "off";
		isContentRotated = msg.contentRotation != 'off';

		var text = '';
		if (isPortrait) {
			text += Locale.getText("OSD Portrait Mode is ON") + " (" + msg.osdPortraitMode + "°)<br>";
		}
		if (isContentRotated) {
			text += Locale.getText("Content is rotated") + " &#x202D;(&#x202C;" + msg.contentRotation + "°, " + Locale.getText(msg.rotationAspectRatio + " aspect ratio") + ")";
		}

		if (isPortrait || isContentRotated) {
			$("#screenshotStatus").html(text);
		}
	});
}

getIsMobile();
var caretClass = 'caret' + (isMobile ? ' mobile' : '');

function setDropButtonText(id, val, noCaret) {
	var li = $('#' + id).find('div ul li [data-value="' + val + '"]');
	var text = li.text();
	var button = $('#' + id).find('div button');
	if (noCaret !== true) {
		text += '&nbsp;&nbsp;<span class="' + caretClass + '"></span>';
	}
	button.html(text);
	if (env.locale == 'ar-SA' && noCaret !== true) {
		button.find('.caret').css('left', '4%').css('right', '65%');
	}
}

function disableDropButton(id, disabled) {
	var li = $('#' + id).find('div button').attr('disabled', disabled);
}

function createDropDown(id, tag, callback, noCaret) {
	var selectClass = 'dropdown-select';
	if (isMobile) {
		selectClass += ' mobile';
	}

	var div = $('<div class="dropdown"/>');
	var button = $('<button class="btn btn-default dropdown-toggle ' + selectClass + '" type="button" id="' + id
		+ 'Drop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">'
		+ tag + (noCaret !== true ? '<span class="' + caretClass + '"></span>' : '') + '</button>');
	div.append(button);

	var menu = $('<ul class="dropdown-menu scrollable-menu ' + selectClass + '" aria-labelledby="' + id + '"/>');
	menu.on('click', 'li a', function() {
		if (!$(this).attr('data-value') || $(this).parent().hasClass('disabled')) {
			return;
		}
		setDropButtonText(id, $(this).attr('data-value'), noCaret);
		callback($(this).attr("data-value"));
		$(this).blur();
	});

	div.append(menu);

	return div;
}

function addDropDownOption(menu, value, text, disabled) {
	var m = menu.children('ul');
	var item = '';
	if (!disabled) {
		item = $('<li><a href="javascript:;" data-value="' + value + '">' + text + '</a></li>');
	} else {
		item = $('<li class=disabled><a href="javascript:;" data-value="' + value + '">' + text + '</a></li>');
	}

	m.append(item);
}

function drawDropDownBox(id, keys, texts, type, callback, defaultValue)  {
	var dropDown = createDropDown(id, '-', callback, true);

	for (var i = 0; i != keys.length; ++i) {
		addDropDownOption(dropDown, keys[i], Locale.getText(texts[i]));
	}

	$('#' + id).append(dropDown);
	$('#' + id + 'Drop').addClass(type);

	if (defaultValue) {
		setDropButtonText(id, defaultValue, true);
	}

}

function getIsMobile(){
	isMobile = document.location.pathname.indexOf('mobile') > 0;
}

function addTwoButtonModal(id, msg, button1, button2, title) {
	var rtl = env.locale == 'ar-SA' ? 'dir=rtl' : '';
	if (title === undefined) {
		title = "";
	}
	var html =
	'<div class="modal fade" id="' + id + '" tabindex="-1" role="dialog">' +
		'<div class="modal-dialog modal-top30" role="document">' +
			'<div class="modal-content"'+ rtl + '>' +
				'<div class="modal-header">' +
					'<h4 class="modal-title" id="' + id + 'Title">' + Locale.getText(title) + '</h4>' +
				'</div>' +
				'<div class="modal-body">' +
					'<span id="' + id + "Txt" + '" style="padding-top:10px">'+ Locale.getText(msg) + '</span>' +
				'</div>' +
			'<div class="modal-footer">' +
				'<button type="button" class="btn whiteCardBtn" data-dismiss="modal">' + Locale.getText(button1) + '</button>' +
				'<button type="button" class="btn whiteCardBtn" data-dismiss="modal" id="' + id + "OK" + '">' + Locale.getText(button2) + '</button>' +
			'</div>' +
		'</div>' +
		'</div>' +
	'</div>';
	return html;
}

function addPitchSelectModal(id, title, initialValue, callback) {
	var p14Checked = (initialValue == 14 ? 'checked="checked"' : '');
	var p24Checked = (initialValue == 24 ? 'checked="checked"' : '');

	var rtl = env.locale == 'ar-SA' ? 'dir=rtl' : '';
	var html = '<div class="modal fade" tabindex="-1" role="dialog" id="' + id + '">'
		+ '<div class="modal-dialog modal-top30">'
			+ '<div class="modal-content"'+ rtl + '>'
				+ '<div class="modal-header">'
					+ '<h4 class="modal-title id="'+ id +'">'+ title +'</h4>'
				+ '</div>'
				+ '<div class="modal-body">'
					+ '<label><input type="radio" name="'+ id + '"' + 'id="' + id + 'Val' + '"value="14" style="word-wrap:break-word;" disabled ' + p14Checked + '" >' + ' 14mm' + '</label><br>'
					+ '<label><input type="radio" name="'+ id + '"' + 'id="' + id + 'Val' + '"value="24" style="word-wrap:break-word;" disabled ' + p24Checked + '" >' + ' 24mm' + '</label><br>'
				+ '</div>'
				+ '<div class="modal-footer">'
					+ '<button type="button" class="btn whiteCardBtn" data-dismiss="modal" id="' + id + 'ModalOK" disabled>'+(env.supportLedSignage ? Locale.getText('OK') : 'OK')+'</button>'
				+ '</div>'
			+ '</div>'
		+ '</div>'
	+ '</div>';

	var modal = $(html);
	$('body').append(modal);
	$('#' + id + 'ModalOK').click(callback);
}

function addConfirmModal(id, title, content, callback, callbackCancel) {
	var html 
		= '<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="' + id + '">'
			+ '<div class="modal-dialog modal-sm">'
				+ '<div class="modal-content">'
					+ '<div class="modal-header">'
						+ '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
						+ '<h4 class="modal-title" id="' + id + 'Title">' + title + '</h4>'
					+ '</div>'
					+ '<div class="modal-body">'
						+ '<span id="' + id + 'Content" style="word-wrap:break-word;">' + content + '</span>'
						+ '<br>'
						+ '<button class="btn whiteCardBtn" data-dismiss="modal" id="' + id + 'ModalOK">' + (env.supportLedSignage ? Locale.getText('OK') : 'OK') + '</button>&nbsp;&nbsp;'
						+ '<button class="btn whiteCardBtn" data-dismiss="modal" id="' + id + 'ModalCancel">' + (env.supportLedSignage ? Locale.getText('Cancel') : 'Cancel') + '</button>'
					+ '</div>'
				+ '</div>'
			+ '</div>'
		+ '</div>';

	var modal = $(html);
	$('body').append(modal);

	$('#' + id + 'ModalOK').click(callback);
	$('#' + id + 'ModalCancel').click(callbackCancel);
}

function addFilmTypeModal(id, title, content, callback, callbackCancel) {
	var html
		= '<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="' + id + '">'
			+ '<div class="modal-dialog modal-sm">'
				+ '<div class="modal-content">'
					+ '<div class="modal-header">'
						+ '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
						+ '<h4 class="modal-title" id="' + id + 'Title">' + title + '</h4>'
					+ '</div>'
					+ '<div class="modal-body">'
						+ '<span id="' + id + 'Content">' + content + '</span>'
						+ '<br>'
						+ '<button class="btn whiteCardBtn" data-dismiss="modal" id="' + id + 'ModalCancel">' + (env.supportLedSignage ? Locale.getText('Cancel') : 'Cancel') + '</button>&nbsp;&nbsp;'
						+ '<button class="btn whiteCardBtn" data-dismiss="modal" id="' + id + 'ModalOK">' + (env.supportLedSignage ? Locale.getText('Continue') : 'Continue') + '</button>'
					+ '</div>'
				+ '</div>'
			+ '</div>'
		+ '</div>';

	var modal = $(html);
	$('body').append(modal);

	$('#' + id + 'ModalOK').click(callback);
	$('#' + id + 'ModalCancel').click(callbackCancel);
}

function addDetailModal(id, title, htmlTxt, callback, callbackCancel){
	var layout
		= '<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="' + id + '">'
			+ '<div class="modal-dialog modal-sm" style="margin:0px; top:90px; left:260px;">'
				+ '<div class="modal-content" style="width:1100px; height:680px; background-color:#cdcdcd;">'
					+ '<div class="modal-header detailModalHeader" style="border-bottom: 1px solid #b7b7b7">'
						+ '<button type="button" class="modalCloseBtn" data-dismiss="modal" aria-label="Close" style="float:left;"><span aria-hidden="true"></span></button>'
						+ '<span class="modal-title" id="' + id + 'Title">' + title + '</span>'
						+ '<br><span class="modal-subTitle" id="subTitle"></span>'
						+ '<button class="darkGrayBtn" id="modalActionBtn" style="margin-top: 10px; padding-top:0px; font-size:13pt; display:none; position:absolute; top:10px; right:10px;"><span id="modalactionName"></span></button>'
						+ '<div id="modalWarningMessage" style="float:right; margin-top10px; margin-right:10px; color:red;"></div>'
					+ '</div>'
					+ '<div class="modal-body">'
					+ htmlTxt
					+ '</div>'
				+ '</div>'
			+ '</div>'
		+ '</div>';
	var modal = $(layout);
	$('body').append(modal);
}

function addCalibModal(id, title, htmlTxt, pos, callback, callbackCancel){
	var layout
		= '<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="' + id + '">'
			+ '<div class="modal-dialog modal-sm" style="margin:0px; top:'+pos.top+'px; left:'+pos.left+'px;">'
				+ '<div class="modal-content" style="width:'+pos.w+'px; height:'+pos.h+'px; background-color:#686868;">'
					+ '<div class="modal-header" style="border-bottom: 1px solid #b7b7b7; color:#eeeeee;">'
						+ '<span class="modal-title" id="' + id + 'Title" style="padding-left:0px;">' + title + '</span>'
					+ '</div>'
					+ '<div class="modal-body">'
					+ htmlTxt
					+ '</div>'
				+ '</div>'
			+ '</div>'
		+ '</div>';
	var modal = $(layout);
	$('body').append(modal);
}

function queryString () {
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");

    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));

        }
    }
    return query_string;
}

function convertCsvToTable(data) {
    var allRows = data.split(/\r?\n|\r/);
    var table = "<table class='table'>";
    for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
        if (allRows[singleRow]) {
            if (singleRow === 0) {
                table += "<thead>";
                table += "<tr>";
            } else {
                table += "<tr>";
            }

            var rowCells = allRows[singleRow].split(',');
            for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
                if (singleRow === 0) {
                    table += "<th>";
                    table += rowCells[rowCell].replace(/^ */g, "");
                    table += "</th>";
                } else {
                    table += "<td>";
                    table += rowCells[rowCell].replace(/^ */g, "");
                    table += "</td>";
                }
            }
            if (singleRow === 0) {
                table += "</tr>";
                table += "</thead>";
                table += "<tbody>";
            } else {
                table += "</tr>";
            }
        }
    }
    table += "</tbody>";
    table += "</table>";
    return table;
}

function capitalizeFirstLetter(str) {
    var str = str.substring(0, 1).toUpperCase() + str.substring(1, str.length).toLowerCase();
    return str;
}

function onlyNumber() {
    if ((event.keyCode < 48) || (event.keyCode > 57))
        event.returnValue = false;
}
