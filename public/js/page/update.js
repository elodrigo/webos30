var currentVer = "";
var updatePercent = 0;
var updateStarted = false;

function init(env) {
	var updateMsg = Locale.getText('Please Keep this Signage on.') + '<br>'
	+ Locale.getText('The POWER remote controller key will not work during the update.') + '<br>'
	+ Locale.getText('Do not reboot in Device page.');
	var fileName = undefined;
	addConfirmModal('deleteModal', Locale.getText('Delete') + '?', '', function() {
		deleteFile("update", fileName, function() {
			window.location.reload(true);
		});
	});

	socket.on('swupdate', function(msg) {
		updateStarted = true;

		if (updatePercent > 0 && msg == 0) {
			overlayControl(false);
			showWarning('Fail to update. Check "' + fileName + '" file please.');
			return;
		}
		updatePercent = msg;

		$('#updateProgress').html(msg + "%");
		$('#overlay_progress').html(msg + "%");
		$('#updateProgress').css('width', msg + '%');
		if (msg == 100) {
			$('.progress-bar').removeClass("active progress-bar-striped");

			var micomUpdate = $('#micomUpdate').prop('checked');
			if( !micomUpdate ){
				overlayControl(false);
				overlayControl(true, "Update completed. Set is rebooting now.<br>Please login again.");
			}

			var micomProgress = -1;
			getMicomUpdateProgress(function(msg) {
				if (micomProgress == -1) {
					$('#update-prog').hide();
					$('#micom-prog').show();
				}
				micomProgress = msg;
				$('#micomProgress').html(msg + "%");
				$('#micomProgress').css('width', msg + '%');
				$('#overlay_progress').html('Micom Update : ' + msg + "%");
				if (msg == 100) {
					overlayControl(false);
					overlayControl(true, "Update completed. Set is rebooting now.<br>Please login again.");
					setTimeout(function() {
						window.location.replace("/login.html");
					}, 2 * 60 * 1000);
				}
			});

			setTimeout(function() {
				if (micomProgress != -1) {
					return;
				}
				window.location.replace("/login.html");
			}, 2 * 60 * 1000);
		}
	});

	addConfirmModal('updateModal', Locale.getText('Update?'), '', function() {
		$('#update-prog').show();
		$('.progress-bar').addClass("active progress-bar-striped");

		overlayControl(true, Locale.getText('S/W Updating......') + updateMsg);

		var micomUpdate = $('#micomUpdate').prop('checked');

		updatePercent = 0;
		updateStarted = false;

		swupdate(fileName, micomUpdate);

		setTimeout(function() {
			getUpdateStatus(function(msg) {
				if (msg.status != 'in progress' && !updateStarted) {
					overlayControl(false);
					showWarning(Locale.getText('Cannot start S/W update. Check xxx file please.').replace('xxx', fileName));
				}
			});
		}, 10 * 1000);
	});

	if (env.supportMicomFanControl) {
		addConfirmModal('micomUpdateModal', Locale.getText('Fan Micom Update?'), '', function() {
			overlayControl(true, Locale.getText("Fan Micom S/W Updating") + " ......<br>" + updateMsg);
			fanMicomUpdate(fileName, function(msg) {
				if (msg.returnValue) {
					showWarning(Locale.getText('Update fan micom with xxx done').replace('xxx', fileName), 10 * 1000);
				} else {
					showWarning(Locale.getText('Failed to update fan micom with xxx. Check the file please').replace('xxx', fileName), 10 * 1000);
				}
				overlayControl(false);
			});
		});

		env.uploadedFiles.forEach(function(file) {
			var ext = file.substr(file.length - 3, 3).toLowerCase();
			var button = $(".update[name='" + file + "']");
			if (ext === 'txt') {
				button.prop('disabled', false);
				button.removeClass('update');
				button.addClass('fanMicomUpdate');
			}
		});
	
		$('.fanMicomUpdate').click(function() {
			fileName = $(this).attr('name');
			$('#micomUpdateModalContent').html(Locale.getText("Update fan micom with this S/W Update file?") + "<br>" + fileName);
			$('#micomUpdateModal').modal();
		});
	}

	if (env.supportLedSignage) {
		addConfirmModal('fpgaUpdateModal', Locale.getText('Update') + ' LED FPGA?', '', function() {
			overlayControl(true, Locale.getText("LED FPGA Updating ......"));
			ledFpgaUpdate(fileName, function(msg) {
				if (msg.returnValue) {
					$('#update-prog').show();
					var percent = 0;
					$('#updateProgress').html(percent + "%");
					$('#overlay_progress').html(percent + "%");
					$('#updateProgress').css('width', percent + '%');

					setTimeout(function() {
						var fpgaProgressTimer = setInterval(function() {
							getFpgaUpdatePercent(function(ret) {
								if(env.supportLedFilm){
									getFPGAUpdateErrorStatus(function(retErrorStatus){
										console.log('getFPGAUpdateErrorStatus::'+ret.returnValue);
										ret.returnValue = retErrorStatus.returnValue;
									});
								}

								if (ret.returnValue == false) {
									clearInterval(fpgaProgressTimer);
									overlayControl(false);
									$('#update-prog').hide();
									if (percent == 0) {
										showWarning(Locale.getText('Failed to update LED FPGA with xxx. Check the file please').replace('xxx', fileName), 10 * 1000);
										return;
									}
									showWarning(Locale.getText('Update LED FPGA with xxx done').replace("xxx", fileName), 10 * 1000);
								}
								percent = ret.Percent;
								$('#updateProgress').html(percent + "%");
								$('#overlay_progress').html(percent + "%");
								$('#updateProgress').css('width', percent + '%');
							});
						}, 2 * 1000);
					}, 11 * 1000);
				} else {
					showWarning("Failed to update LED FPGA with " + fileName + ". Check the file please", 10 * 1000);
					overlayControl(false);
				}
			});
		});

		env.uploadedFiles.forEach(function(file) {
			var ext = file.substring(file.lastIndexOf('.') + 1).toLowerCase();
			var button = $(".update[name='" + file + "']");
			if (ext === 'hexout') {
				button.prop('disabled', false);
				button.removeClass('update');
				button.addClass('ledFpgaUpdate');
			}
		});
	
		$('.ledFpgaUpdate').click(function() {
			fileName = $(this).attr('name');
			$('#fpgaUpdateModalContent').html(Locale.getText("Update") + " LED FPGA?<br>" + fileName);
			$('#fpgaUpdateModal').modal();
			/*
			getFpgaCheckSum('NULL', function(ret1) {
				var curVer = 'N/A';
				if (ret1.returnValue) {	
					curVer = ret1.CheckSum;
				}
				getFpgaCheckSum(fileName, function(ret2) {
					var newVer = 'N/A';
					if (ret2.returnValue) {	
						newVer = ret2.CheckSum;
					}
					$('#fpgaUpdateModalContent').html(Locale.getText("Update") + " LED FPGA?<br>" + fileName
														+ '<br>Current Checksum : ' + curVer + '<br>New Checksum : ' + newVer);
					$('#fpgaUpdateModal').modal();
				});
			});
			*/
		});
	}

	getBasicInfo(function(msg) {
		currentVer = msg.firmwareVersion;
	});

	listUpdates(function(msg) {
		if (!msg || !msg.returnValue) {
			return;
		}

		msg.fileinfo.forEach(function(file) {
			var button = $(".update[name='" + file.name + "']");
			var newVersion = file.version.toString(16);
			newVersion = '0'+ newVersion.substring(0,1) + '.' + newVersion.substring(1,3) + '.' + newVersion.substring(3,5);
			button.prop('id', newVersion);
			button.prop('disabled', false);
			$('.file-info[name="' + file.name + '"]').append('(' + newVersion + ')');
		});
	});

	$(document).on('change', '.btn-file :file', function() {
		$('#fileName').html($(this).val());
	});

	$(".update").click(function(){
		fileName = $(this).attr('name');
		var newVer = $(this).attr('id');
		if (newVer === undefined) {
			newVer = '-';
		}
		$('#updateModalContent').html(Locale.getText("Do you want to install this S/W Update file?") + "<br>" + currentVer + "->" + newVer);
		$('#updateModal').modal();
	});
	
	$(".delete").click(function(){
		fileName = $(this).attr('name');

		var warnText = Locale.getText("Do you want to delete this S/W Update file?") + "<br><br>";
		if ($(this).hasClass('fpga')) {
			warnText = Locale.getText("Do you want to delete this LED FPGA Update file?") + "<br><br>";
		}
		for(var i=0; i<(fileName.length/37); i++){
			warnText += fileName.substr(i*37,37)+"<br>";
		}

		$('#deleteModalContent').html(warnText);
		$('#deleteModal').modal();
	});

	function uploadFile(file) {
		var formData = new FormData();
		formData.append('update', file);
		formData.append('target', 'update');
		
		$('#upload-prog').show();
		overlayControl(true, Locale.getText("Uploading") + '<br>' + file.name);
		document.onkeydown = function(e) {
			return false;
		}

		$('input').prop('disabled', true);
		$('button').prop('disabled', true);

		var xhr = new XMLHttpRequest();
		
		xhr.open('post', '/upload', true);
		//xhr.setRequestHeader("Content-type","multipart/form-data");
		
		xhr.upload.onprogress = function(e) {
			if (e.lengthComputable) {
				var per = Math.round((e.loaded / e.total) * 100)
				$('#uploadProgress').html(per + "%");
				$('#uploadProgress').css('width', per + '%');
				$('#overlay_progress').html(per + "%");
			}
		};

		xhr.upload.onload = function(e) {
			setTimeout(function() {
				overlayControl(false);
				window.location.reload(true);
			}, 2000);
		}
		
		xhr.onerror = function(e) {
			overlayControl(false);
			showWarning('An error occurred while submitting the form.', 5 * 1000);
		};

		xhr.onload = function() {
			console.log(this.statusText);
		};

		xhr.send(formData);
	}

	$('input[type="submit"]').on('click', function(evt) {
		evt.preventDefault();
		var file = document.getElementById('upload').files[0];

		if (!file) {
			showWarning('Please choose a file to upload', 5 * 1000);
			return;
		}

		var fileName = file.name;
		var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();

		if (supportExt.indexOf(ext) < 0) {
			showWarning(Locale.getText('Unsupported File type.'), 5 * 1000);
			return;
		}

		if (file.size > env.maxSize) {
			showWarning(Locale.getText('Flash memory is not enough.'), 5 * 1000);
			return;
		}

		if (env.uploadedFiles.indexOf(fileName) >= 0) {
			addConfirmModal('overwrite', Locale.getText('Overwrite?'), fileName, function() {
				setTimeout(function() {
					uploadFile(file);
				}, 500);
				$('#overwrite').close();
				$('#overwrite').remove();
			});
			$('#overwrite').modal();
			return;
		}

		uploadFile(file);
	});

	$('#upload').change(function(e) {
		if (e.target.files[0].name) {
			$('#submit').attr('disabled', false);
		}
	});

	$('#upload-prog').hide();
	$('#update-prog').hide();
	$('#micom-prog').hide();

}
