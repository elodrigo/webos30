var ampm = '';
var continent = '';
var country = '';
var city = '';
var setAuto = false;
var var_dstStartMonth = '';
var var_dstStartWeek = '';
var var_dstStartWeekday = '';
var var_dstStartHour = '';
var var_dstEndMonth = '';
var var_dstEndWeek = '';
var var_dstEndWeekday = '';
var var_dstEndHour = '';
var timeZoneInited = false;
var isCityState = false;
var agent = navigator.userAgent.toLowerCase();


function checkDisable() {
	var useNTP = $("#useNTP").prop('checked');
	setAuto = useNTP;
	if(useNTP){
		$('#daylight-saving-time').css('display','none');
		$('#time-zone').css('display','table');
		$('#time-date').css('display','none');
	}else{
		$('#daylight-saving-time').css('display','table');
		$('#time-zone').css('display','none');
		$('#time-date').css('display','table');
		initCurrentTime();
	}
}

function drawAmpm() {
	var dropDown = createDropDown('ampm', '-', function(selected) {
		ampm = selected;
	});
	addDropDownOption(dropDown, "am", Locale.getText("AM"));
	addDropDownOption(dropDown, "pm", Locale.getText("PM"));

	$('#ampm').html(dropDown);
	setDropButtonText('ampm', ampm );

	addRtlDir();

	if(agent.indexOf("firefox") != -1){
		$('#ampm').css('margin-right', 60);
		if(window.location.pathname.indexOf("mobile") != -1){
			$('#ampm').css('margin-right', 0);
		}
	}
}


function initTimeZone(){

	getlocaleContinent(function(ret){
		continent = ret.localeContinent;
		drawContinent(continent);
	});

	getlocaleCountry(function(ret){
		country = ret.localeCountry;
		drawCountry(continent, country);

		getTimeZone(function(timeZone){
			drawCity(country, timeZone.City );
		});
	});
}

function drawContinent(defaultVal) {
	var dropDown = createDropDown('continent', '-', function(selected) {
		continent = selected;
		drawCountry(selected);

		$('#city').html('');
	});
	addDropDownOption(dropDown, "Africa", Locale.getText("Africa"));
	addDropDownOption(dropDown, "Asia", Locale.getText("Asia"));
	addDropDownOption(dropDown, "CIS", Locale.getText("CIS"));
	addDropDownOption(dropDown, "Europe", Locale.getText("Europe"));
	addDropDownOption(dropDown, "MiddleEast", Locale.getText("Middle East"));
	addDropDownOption(dropDown, "NorthAmerica", Locale.getText("North America"));
	addDropDownOption(dropDown, "Oceania", Locale.getText("Oceania"));
	addDropDownOption(dropDown, "Pacific", Locale.getText("Pacific"));
	addDropDownOption(dropDown, "SouthAmerica", Locale.getText("South America"));

	$('#continent').html(dropDown);

	setDropButtonText('continent', defaultVal);
	addRtlDir();
}

function drawCountry(continent, defaultVal) {
	var dropDown = createDropDown('country', '-', function(selected) {
		country = selected;
		drawCity(selected);
	});
	
	getCountryList(continent, function(msg){
		msg.forEach(function(item) {
			addDropDownOption(dropDown, item.shortName, Locale.getText(item.fullName));
		});
		setDropButtonText('country', defaultVal);
		country = defaultVal;
		addRtlDir();
	});

	$('#country').html(dropDown);
}

function drawCity(country, defaultVal) {
	var dropDown = createDropDown('city', '-', function(selected) {
		city = selected;
	});
	getCityList(country, function(msg){
		msg.forEach(function(item) {
			if( item.City != ''){
				addDropDownOption(dropDown, item.City, Locale.getText(item.City));
				isCityState = false;
			}else{
				addDropDownOption(dropDown, item.Country, Locale.getText(item.Country));
				isCityState = true;
				setDropButtonText('city', item.Country);
				city = item.Country;
			}
		});
		if(!isCityState){
			setDropButtonText('city', defaultVal);
			city = defaultVal;
		}
		addRtlDir();
	});

	$('#city').html(dropDown);
}




function drawDaylightSaving(){

	//start, End Month
	var dropDown = createDropDown('dstStartMonth', '-', function(selected) {
		var_dstStartMonth = selected;
	});

	for (var i = 1; i < 13; i++) {
	    addDropDownOption(dropDown, i, i);
	}

	$('#dstStartMonth').html(dropDown);

	var dropDown = createDropDown('dstEndMonth', '-', function(selected) {
		var_dstEndMonth = selected;
	});

	for (var i = 1; i < 13; i++) {
	    addDropDownOption(dropDown, i, i);
	}

	$('#dstEndMonth').html(dropDown);


	//start, End Week
	var dropDown = createDropDown('dstStartWeek', '-', function(selected) {
		var_dstStartWeek = selected;
	});

	addDropDownOption(dropDown, "1", Locale.getText("1st"));
	addDropDownOption(dropDown, "2", Locale.getText("2nd"));
	addDropDownOption(dropDown, "3", Locale.getText("3rd"));
	addDropDownOption(dropDown, "4", Locale.getText("4th"));
	addDropDownOption(dropDown, "5", Locale.getText("Last"));

	$('#dstStartWeek').html(dropDown);

	dropDown = createDropDown('dstEndWeek', '-', function(selected) {
		var_dstEndWeek = selected;
	});

	addDropDownOption(dropDown, "1", Locale.getText("1st"));
	addDropDownOption(dropDown, "2", Locale.getText("2nd"));
	addDropDownOption(dropDown, "3", Locale.getText("3rd"));
	addDropDownOption(dropDown, "4", Locale.getText("4th"));
	addDropDownOption(dropDown, "5", Locale.getText("Last"));

	$('#dstEndWeek').html(dropDown);


	//start, End Weekday
	dropDown = createDropDown('dstStartWeekday', '-', function(selected) {
		var_dstStartWeekday = selected;
	});
	
	addDropDownOption(dropDown, "0", Locale.getText("Sun"));
	addDropDownOption(dropDown, "1", Locale.getText("Mon"));
	addDropDownOption(dropDown, "2", Locale.getText("Tue"));
	addDropDownOption(dropDown, "3", Locale.getText("Wed"));
	addDropDownOption(dropDown, "4", Locale.getText("Thu"));
	addDropDownOption(dropDown, "5", Locale.getText("Fri"));
	addDropDownOption(dropDown, "6", Locale.getText("Sat"));

	$('#dstStartWeekday').html(dropDown);

	dropDown = createDropDown('dstEndWeekday', '-', function(selected) {
		var_dstEndWeekday = selected;
	});
	
	addDropDownOption(dropDown, "0", Locale.getText("Sun"));
	addDropDownOption(dropDown, "1", Locale.getText("Mon"));
	addDropDownOption(dropDown, "2", Locale.getText("Tue"));
	addDropDownOption(dropDown, "3", Locale.getText("Wed"));
	addDropDownOption(dropDown, "4", Locale.getText("Thu"));
	addDropDownOption(dropDown, "5", Locale.getText("Fri"));
	addDropDownOption(dropDown, "6", Locale.getText("Sat"));

	$('#dstEndWeekday').html(dropDown);


	//start, End Hour
	dropDown = createDropDown('dstStartHour', '-', function(selected) {
		var_dstStartHour = selected;
	});

	for (var i = 0; i < 24; i++) {
	    addDropDownOption(dropDown, i, i);
	}

	$('#dstStartHour').html(dropDown);

	dropDown = createDropDown('dstEndHour', '-', function(selected) {
		var_dstEndHour = selected;
	});

	for (var i = 0; i < 24; i++) {
	    addDropDownOption(dropDown, i, i);
	}

	$('#dstEndHour').html(dropDown);
}

function initDST(){
	drawDaylightSaving();
	getDSTInfo(function(dstInfo){

		var_dstStartMonth = dstInfo.dstStartMonth;
		var_dstStartWeek = dstInfo.dstStartWeek;
		var_dstStartWeekday = dstInfo.dstStartDayOfWeek;
		var_dstStartHour = dstInfo.dstStartHour;

		var_dstEndMonth = dstInfo.dstEndMonth;
		var_dstEndWeek = dstInfo.dstEndWeek;
		var_dstEndWeekday = dstInfo.dstEndDayOfWeek;
		var_dstEndHour = dstInfo.dstEndHour;

		$('#useDST').attr('checked', dstInfo.dstMode == "on" ? true : false );
		$('#useDST').change();

		setDropButtonText('dstStartMonth', dstInfo.dstStartMonth);
		setDropButtonText('dstStartWeek', dstInfo.dstStartWeek);
		setDropButtonText('dstStartWeekday', dstInfo.dstStartDayOfWeek);
		setDropButtonText('dstStartHour', dstInfo.dstStartHour);

		setDropButtonText('dstEndMonth', dstInfo.dstEndMonth);
		setDropButtonText('dstEndWeek', dstInfo.dstEndWeek);
		setDropButtonText('dstEndWeekday', dstInfo.dstEndDayOfWeek);
		setDropButtonText('dstEndHour', dstInfo.dstEndHour);

	});
}

function checkDateValid(){

	var number1to12Regex = /(^[0]{1}[1-9]{1}$|^[1-9]{1}$|^[1]{1}[0-2]{1}$)/;
	var number0to23Regex = /(^[0-9]{1}$|^[1]{1}[0-9]{1}$|^[2]{1}[0-3]{1}$)/;
	var number1to31Regex = /(^[0]{1}[1-9]{1}$|^[1-9]{1}$|^[1-2]{1}[0-9]{1}$|^[3]{1}[0-1]{1}$)/;
	var number1to59Regex = /(^[1-9]{1}$|^[0-5]{1}[0-9]{1}$)/;
	var number2015to2037Regex = /(^[2]{1}[0]{1}[1]{1}[5-9]{1}$|^[2]{1}[0]{1}[2]{1}[0-9]{1}$|^[2]{1}[0]{1}[3]{1}[0-7]{1}$)/;
	var number2016to2037Regex = /(^[2]{1}[0]{1}[1]{1}[6-9]{1}$|^[2]{1}[0]{1}[2]{1}[0-9]{1}$|^[2]{1}[0]{1}[3]{1}[0-7]{1}$)/;
	var maxDaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

	if( !number2016to2037Regex.test( $('#year').val() ) ){
		alert("invalid year value");
		return false;
	}

	if( $('#year').val()%4==0 && $('#year').val()%100!=0 || $('#year').val()%400==0 )
        maxDaysInMonth[1] = 29;            

	if( !number1to12Regex.test( $('#month').val() ) ){
		alert("invalid month value");
		return false;
	}

	if( ( !number1to31Regex.test($('#day').val()) ) || ( $('#day').val()<=0 || $('#day').val()>maxDaysInMonth[$('#month').val()-1] ) ){
		alert("invalid day value");
		return false;
	}

	if( !number1to12Regex.test( $('#hour').val() ) ){
		alert("invalid hour value");
		return false;
	}

	if( !number1to59Regex.test( $('#minute').val() ) ){
		alert("invalid minute value");
		return false;
	}

	return true;

}

function addZero(n, digits) {
  var zero = '';
  n = n.toString();

  if (n.length < digits) {
    for (var i = 0; i < digits - n.length; i++)
      zero += '0';
  }
  return zero + n;
}

function initCurrentTime(){
	getCurrentTime(function(msg) {
		$('#currentTime').html(msg.current);
		$('#year').val(msg.year);
		$('#month').val(msg.month);
		$('#day').val(msg.day);

		ampm = msg.hour < 12 ? 'am' : 'pm';
		setDropButtonText('ampm', ampm );

		$('#hour').val( (msg.hour%12) == 0 ? 12 : (msg.hour%12) );
		msg.minute.toString().length == 1 ? $('#minute').val('0'+msg.minute) : $('#minute').val(msg.minute);
	});

}

var isFirstDstLoad = true;

function initTime() {
	getNTPStatus(function(ret) {
		if (ret.networkTimeSource) {
			$("#useSNTP").attr('checked', ret.networkTimeSource.useSNTP);
		}

		$("#useNTP").attr('checked', ret.useNetworkTime);
		checkDisable();		
		if(ret.useNetworkTime){
			if(!timeZoneInited){
				initTimeZone();
				timeZoneInited = true;
			}
		}
	});

	$("#useNTP").change(function() {
		overlayControl(true, Locale.getText('Loading'));
		checkDisable();
		var useNTP = $("#useNTP").prop('checked');
		var param = {useNTP: useNTP}
		$("#useNTP").prop('disabled', true);
		setNTPStatus(param, function(ret){
			$("#useNTP").prop('disabled', false);
			overlayControl(false);
			if(!timeZoneInited){
				initTimeZone();
				timeZoneInited = true;
			}
		});
	});

	$("#useDST").change(function(){
		$('.dstTable').css('display', $("#useDST").prop('checked') ? 'table' : 'none');
		setDstOnOff( $("#useDST").prop('checked') ? 'on' : 'off' );
		if (!isFirstDstLoad) {
			setTimeout(function() {
				initCurrentTime();
			}, 100);
		}
		isFirstDstLoad = false;
	});

	addConfirmModal('applyTimeModal', Locale.getText('Apply?'), '', function() {

		var currentHour = ampm == "am" ?  $('#hour').val() : parseInt( $('#hour').val() ) + 12;
		if( $('#hour').val()==12 )
			currentHour -= 12;

		var timeInfo = {
			hour:addZero(currentHour,2),
			minute:addZero($('#minute').val(),2),
			year:$('#year').val(),
			month:addZero($('#month').val(),2),
			day:addZero($('#day').val(),2)
		};
		setCurrentTime(timeInfo);

		if( $("#useDST").prop('checked') ){
			var Month = var_dstStartMonth;
			var Week = var_dstStartWeek;
			var Weekday = var_dstStartWeekday;
			var Hour = var_dstStartHour;
			setDstStartTime(Month, Week, Weekday, Hour);

			Month = var_dstEndMonth;
			Week = var_dstEndWeek;
			Weekday = var_dstEndWeekday;
			Hour = var_dstEndHour;
			setDstEndTime(Month, Week, Weekday, Hour);
		}
	}, function(){
		initCurrentTime();
		if( $("#useDST").prop('checked') ){
			initDST();
		}
	});
	$('#applyTimeModalContent').html(Locale.getText("Changing Time may cause you to be logged out.") + "<br>");

	$('#time-apply').click(function(){
		if( !checkDateValid() ){
			initCurrentTime();
			return;
		}
		$('#applyTimeModal').modal();
	});


	addConfirmModal('applyTimeZoneModal', Locale.getText('Apply?'), '', function() {
		setContinent(continent, function(msg){

		});

		setCountry(country,function(msg){

		});

		getCityList(country, function(msg){
			var timeZone = undefined;

			msg.forEach(function(item) {
				if(!isCityState){
					if( item.City == city ){
						timeZone = item;
					}
				}else{
					if( item.Country == city ){
						timeZone = item;
					}
				}
			});

			setCity(timeZone, function(msg){
			});
		});
	}, function(){
		initTimeZone();
	});

	$('#applyTimeZoneModalContent').html(Locale.getText("Changing Time may cause you to be logged out.") + "<br>");

	$('#time-zone-apply').click(function() {
		if( country == undefined ){
			alert(Locale.getText('Please select country'));
			return;
		}else if( city == undefined ){
			alert(Locale.getText('Please select city'));
			return;
		}
		$('#applyTimeZoneModal').modal();
	});

	initCurrentTime();

	$('#pageTitle').html(Locale.getText('Time'));

	drawAmpm();
	initDST();
}
