var isDashboard = false;
var inited = false;
var curUnit = undefined;
var cpatureFileName = '';

function compareZ(a, b) {
	return a.z - b.z;
}

function rgba(r, g, b, a) {
	return 'rgba(' + r + ',' + g + ',' + b + ',' + a + ')';
}

function drawRect(config, rect) {
	var ctx = config.ctx;
	var curRect = jQuery.extend(true, {}, rect);
	var border = 2;
	ctx.strokeStyle = '#eeeeee'

	if (!rect.status) {
		if(rect.pattern != undefined){
			if(rect.pattern){
				ctx.fillStyle = rgba(0xfd, 0x31, 0x1c, 0.7);
			}else{
				ctx.fillStyle = rgba(0, 0, 0, 0.4);
			}
		}else{
			ctx.fillStyle = (rect == config.curUnit) ? rgba(0xfd, 0x31, 0x1c, 0.4) : rgba(0, 0, 0, 0.4);
		}
	} else {
		if (rect.status == 'ok') {
			ctx.fillStyle = rgba(0, 0, 0, 0.4);
		} else if (rect.status == 'ng') {
			ctx.fillStyle = rgba(0xfd, 0x31, 0x1c, 0.7);
		} else if (rect.status == 'off') {
			ctx.fillStyle = rgba(0, 0, 0, 0.7);
		}

		//draw border
		if (!isDashboard && curUnit !== undefined && rect == curUnit) {
			border = 10;
			ctx.strokeStyle = rgba(0xcf, 0x06, 0x52, 1);
		}
	}

	ctx.lineWidth = border;
	ctx.strokeRect(rect.x + border / 2,
					rect.y + border / 2,
					rect.w - border - (Number(rect.trim.left) + Number(rect.trim.right)),
					rect.h - border);
	curRect.x += border;
	curRect.y += border;
	curRect.w -= border * 2;
	curRect.h -= border * 2;
	ctx.fillRect(curRect.x, curRect.y, curRect.w - (Number(rect.trim.left) + Number(rect.trim.right)), curRect.h);
	
	if (!rect.id) {
		return;
	}
	
	ctx.fillStyle = 'white';
	ctx.textAlign = 'left';
	ctx.textBaseline = 'top';

	if(env.supportLedFilm){
		if(rect.id >= 10){
			ctx.font = "25px 'Arial'";
			ctx.fillText(rect.id, rect.x + 2, rect.y + 2)
		}else{
			ctx.font = "25px 'Arial'";
			ctx.fillText(rect.id, rect.x + 8, rect.y + 2)
		}
	}else{
		ctx.font = "50px 'Arial'";
		ctx.fillText(rect.id, rect.x + 10, rect.y + 10)
	}
	
	if(!env.supportLedFilm){
		ctx.font = "30px 'Arial'";
		ctx.fillStyle = 'white';
		ctx.textAlign = 'right';
		ctx.textBaseline = 'top';
		ctx.fillText('(' + rect.x + ',' + rect.y + ')', rect.x + rect.w - 10, rect.y + 15);
	}
}

function draw(config) {
	if (!inited) {
		return;
	}

	config.ctx.clearRect(0, 0, outer.width, outer.height);
	config.ctx.drawImage(config.img, 0, 0, outer.width, outer.height);

	config.ctx.strokeStyle = 'black';
	config.ctx.lineWidth = 1;
	config.ctx.strokeRect(0, 0, outer.width, outer.height);
	
	if (config != mainCanvas) {
		config.ctx.strokeStyle = '#cf0652';
		config.ctx.lineWidth = 10;
		config.ctx.strokeRect(config.viewPort.x, config.viewPort.y,
							config.viewPort.w, config.viewPort.h);
	}
	
	config.ctx.strokeStyle = 'black';
	config.ctx.lineWidth = 1;

	var sorted = config.rectList;
	sorted.forEach(function(rect) {
		drawRect(config, rect);
	});
}

function setupInputList() {
	getInputList(function(msg) {
		for (var i = 0; i != msg.length; ++i) {
			var radio = $('<tr> <td><input type=radio name=input value="' + msg[i].id + '" id="' + msg[i].id + '">' + msg[i].name + '</td> </tr>');
			radio.appendTo('#inputTable');
		}
		$('[name=input]').click(function() {
			setHdmiInput($(this).val());
		});
	
		getHdmiInput(function(msg) {
			$("input:radio[name=input][value='" + msg + "']").attr("checked", true);
		});
	});
}

function refreshLed(config) {
	if (!config) {
		config = mainCanvas;
	}
	var height = env.supportLedFilm ? 1080 : Math.floor(1080 * config.scaleScreen);
	getCapture(height, function(m) {
		if(m.indexOf('noimage')<0){
			cpatureFileName = m;
		}else{
			m = cpatureFileName;
			setTimeout(function(){
				refreshLed(config);
			}, 1000);
			return;
		}

		inited = true;
		config.img.onload = function() {
			draw(config);
		}
		config.img.src = m;
	});
}

function drawFromConfig(ledConfig, config) {
	config.rectList = [];
	ledConfig.unitList.forEach(function(unit) {
		var rect = {
			x: unit.x,
			y: unit.y,
			w: unit.w,
			h: unit.h,
			id: unit.id,
			inputDir: unit.inputDir,
			portLength: unit.portLength,
			portUse: unit.portUse,
			trim : unit.trim,
			z: 0
		}
		config.rectList.push(rect);
	});

	config.viewPort = ledConfig.viewPort;
	config.from = ledConfig.from;
	config.direction = ledConfig.direction;

	var scaleW =  config.viewPort.w / outer.width;
	var scaleH =  config.viewPort.h / outer.height;

	if(env.supportLedFilm){
		config.unitScale = scaleW > scaleH ? scaleW : scaleH;
	}else{
		config.unitScale = 1;
	}

	refreshLed(config);
}

function getMousePos(evt) {
	var outer = evt.target.getBoundingClientRect();
	if(env.supportLedFilm){
		return {
	        x: (evt.clientX - outer.left),
	        y: (evt.clientY - outer.top)
	    };
	}else{
		return {
	        x: (evt.clientX - outer.left) / mainCanvas.scaleScreen,
	        y: (evt.clientY - outer.top) / mainCanvas.scaleScreen
	    };
	}
}

function isInsideRect(pos, rect) {
	var x = rect.x;
	var y = rect.y;
	var w = rect.w;
	var h = rect.h;
	return x <= pos.x && y <= pos.y && (x + w) >= pos.x && (y + h) >= pos.y;
}

function getSelectedRect(config, pos) {
	return config.rectList.filter(function(rect) {
		return isInsideRect(pos, rect, config.unitScale);
	});
}

var refreshTimer;
var interval = 1;
function initLed(sec, config) {
	interval = sec;
}

$('#stop').prop('disabled', true);
$('#stop').click(function() {
	$('#stop').prop('disabled', true);
	$('#play').prop('disabled', false);
	clearInterval(refreshTimer);
	refreshTimer = undefined;
});
$('#play').click(function() {
	$('#stop').prop('disabled', false);
	$('#play').prop('disabled', true);
	if (!refreshTimer) {
		refreshTimer = setInterval(refreshLed, interval * 1000);
	}
	refreshLed(mainCanvas);
});

function loadBC_CC(id) {
	for (var i = 0; i != 4; ++i) {
		(function() {
			var bank = i;
			getBC_CC(id * 2, bank, function(msg) {
				$('#moduleBC' + (bank + 1)).val(msg.bc)
				$('#ccR' + (bank + 1)).val(msg.r)
				$('#ccG' + (bank + 1)).val(msg.g)
				$('#ccB' + (bank + 1)).val(msg.b)
			});
		})();
	}
}

function loadGamut(id) {
	getGamut(id * 2, function(msg) {
		for (var i = 0; i != 3; ++i) {
			for (var j = 0; j != 3; ++j) {
				var gamut = Number(msg[i][j]).toFixed(2);
				$('#gamut' + i + j).val(gamut);
			}
		}
	});
}

function getOverallStatusHandler() {
	var counter = 0;
	mainCanvas.rectList.forEach(function(rect, idx) {
		getLedPowerStatus(rect.id * 2, function(ret1) {
			if (!ret1.isPowerOk) {
				rect.status = 'off';
				if (++counter == mainCanvas.rectList.length) {
					draw(mainCanvas);
				}
				return;
			}
			getLedOverallStatus(rect.id * 2, function(ret2) {
				rect.status = ret2.isOverallOk ? 'ok' : 'ng';
				if (++counter == mainCanvas.rectList.length) {
					draw(mainCanvas);
				}
			});
		});
	});
}
