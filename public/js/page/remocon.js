var interval = undefined;

var height = 0;
function reloadCapture() {
	var h = Math.ceil($('#scr-wrap').height());

	if (height == 0) {
		height = isMobile? 214 : 254;
	} else if (Math.abs(h - height) > 10) {
		height = h;
	}

	getCapture(height, function(m) {
		$("#scr").attr('src',  m);
	});
}

function refresh() {
	var count = 0;
	if (interval) {
		clearInterval(interval)
	}
	$('#play').prop('disabled', true);
	interval = setInterval(function() {
		if (count % 10 == 0) {
			showRotationWarning();
		}
		reloadCapture();
		++count;
		if (count >= 20) {
			$('#play').prop('disabled', false);
			clearInterval(interval);
			count = 0;
		}
	}, 1000);
}


function showRemote(index){
	var buttonID = "p_"+index;
	var direction = ["up", "right", "down", "left", "ok"];

	$("#"+buttonID).css("visibility", "visible");

	setTimeout(function(){
	  $("#"+buttonID).css("visibility", "hidden");
	}, 100);

	sendRemocon(direction[index-1]);
	refresh();
}


$(document).ready(function(){
	$("#screenshotStatus").css("background-color","transparent");
	$("button.play").click(function() {
		if (interval) {
			clearInterval(interval);
		}
		refresh();
	});

	$("button.remo").click(function() {
		var command = $(this).val();
		getOSDLockMode(function(msg){
			if(msg.settings.enableOsdVisibility == "off"){
				if( (command=="menu") || (command=="home") || (command=="inputKey") ){
					return;
				}else{
					sendRemocon(command);
					refresh();
				}
			}else{
				sendRemocon(command);
				refresh();
			}
		});
	});

	var captureSize = {
		"1": {
			class: "col-mo-4",
			height: 150
		},
		"2": {
			class: "col-mo-6",
			height: 254
		},
		"3": {
			class: "col-mo-12",
			height: 450
		}
	};
	$("button.size").click(function() {
		var size = $(this).val();
		$("#scr-wrap").height(captureSize[size].height);

		var pardiv = $('#scr-wrap').parent();
		pardiv.removeClass('col-mo-4 col-mo-6 col-mo-12');
		pardiv.addClass(captureSize[size].class);

		$('#scr').click();
	});

	$("#scr").click(function(e) {
		refresh();
		if (false && env.supportLedSignage && e.pageX) {
			var offset = $(this).offset();
			var ratio = 1080.0 / $("#scr").height();
			var height = $("#scr").height();
			var width = 16.0 / 9 * $("#scr").height();
			
			var x = (e.pageX - offset.left) / width;
			var y = (e.pageY - offset.top) / height;
			sendMouseEvent(x, y);
		}
	});
	
	var width = document.body.clientWidth;
	if (width <= 378) {
		$("#small").click();
	} else {
		$("#middle").click();
	}
	$("button.play").click();

	var keyCodeMap = {}
	keyCodeMap["8"] = "backspace";
	keyCodeMap["13"] = "enter";
	keyCodeMap["32"] = "space";
	keyCodeMap["190"] = ".";
	keyCodeMap["37"] = "left"
	keyCodeMap["38"] = "up"
	keyCodeMap["39"] = "right"
	keyCodeMap["40"] = "down"

	function keydownHandler(e) {
		return;
		var keyCode;

		if (e.target.id && (e.target.id == 'message' || e.target.id == 'scrollMsg')) {
			return;
		}

		if (e.keyCode == 8 || e.keyCode == 32 || e.keyCode == 13
				|| (e.keyCode >= 37 && e.keyCode <= 40)) {
			e.preventDefault();
		}

		if (e.keyCode == 27) {
			sendRemocon("exit");
			refresh();
			return;
		}

		if (e.keyCode >= 65 && e.keyCode <=90) {
			keyCode = String.fromCharCode(e.keyCode);
		} else if (e.keyCode >=48 && e.keyCode <= 57) {
			keyCode = String.fromCharCode(e.keyCode);
		} else {
			keyCode = keyCodeMap[String(e.keyCode)]
		}

		if (!keyCode || keyCode.length == 0) {
			return;
		}
		sendKeyEvent(keyCode);
		refresh();
	}

	if (document.addEventListener) {
		document.addEventListener("keydown",keydownHandler,false);
	} else if (document.attachEvent) {
		document.attachEvent("onkeydown", keydownHandler);
	} else {
		document.onkeydown = keydownHandler;
	}

	showRotationWarning();

	$("img, area, map").focus(function(e) {
		e.preventDefault();
	});

	if ( (navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (navigator.userAgent.toLowerCase().indexOf("msie") != -1) ){
		$('.4way-imagemap').attr('onfocus', "blur()");
	}

	getOSDLockMode(function(msg){
		if(msg.settings.enableOsdVisibility == "off"){
			$("button[value*=menu]").css("visibility","hidden");
			$("button[value*=home]").css("visibility","hidden");
			$("button[value*=inputKey]").css("visibility","hidden");
		}
	});

	$('#play').click(function() {
		refresh();
	});

	reloadCapture();
	refresh();
});

