/**
 * Copyright (c) 2019 LG Electronics, Inc.
 */
function init() {
    $("#change").click(function(e) {
        e.preventDefault();
        handleChangePasswd();
    });
}

function handleChangePasswd() {
    var current = $("#current").val();
    var passwd = $("#passwd").val();
    var check = $("#check").val();

    $.ajax({
        type: "post",
        url: "/changePasswd",
        data: {
            current: current,
            passwd: passwd,
            check: check
        },
        success: function (data) {
            onChangePasswdResponse(data);
        }
    });
}

function onChangePasswdResponse(data) {
    switch (data) {
        case 'fail_incorrect':
            showWarning(Locale.getText('Password is incorrect. Please Try Again'), 5000);
            break;
        case 'fail_same':
            showWarning(Locale.getText('You\'ve entered the same password as the current one. Please enter a different password.'), 5000);
            break;
        case 'fail_short':
            showWarning(Locale.getText('Invalid new password. The password must be at least 8 characters.'), 5000);
            break;
        case 'fail_inavlid_format':
            showWarning(Locale.getText('Invalid new password. You must use a mixture of characters, numbers and special characters.'), 5000);
            break;
        case 'fail_not_match':
            showWarning(Locale.getText('Validation error. The new password and the confirm password do not match.'), 5000);
            break;
        case 'success':
            alert(Locale.getText('Your Password has been changed.'));
            location.href = '/dashboard';
            break;
        default:
    }
}

(function () {
    init();
})();
