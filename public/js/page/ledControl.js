var pictureMode = undefined;
var currentTime = undefined;
var selectTime = {};
var wired = undefined;
var ipType = 4;
var defaultControlItem = 'picture';

function convertTemp(temp) {
	if (!isCelsius) {
		return (Number(temp) * 1.8 + 32).toFixed(1);
	} 
	return temp
}

function drawControlItemSelect() {
	var ControlItem = [Locale.getText("Picture"), Locale.getText("Virtual Controller"), Locale.getText("Network"), Locale.getText("Time")];
	var ControlItemID = ['picture', 'virtual', 'network', 'time'];

	if (env.supportLedFilm) {
		if (env.numOfExtInputs > 1) {
			ControlItem.unshift(Locale.getText("Input"));
			ControlItemID.unshift('input')
		}
	} else {
		ControlItem.push(Locale.getText('Video Scaler'));
		ControlItemID.push('scaler');
	}

	drawDropDownBox('controlItemSelect', ControlItemID, ControlItem, 'dropboxType3', function (selected) {
		$('.panel').hide();
		$('#' + selected + 'Panel').show();
		
		switch (selected) {
			case 'picture':
				getPictureModeVal();
				pictureCallback();
				break;
		}
	}, defaultControlItem);
}

function setupInputList() {
	getInputList(function(msg) {
		for (var i = 0; i != msg.length; ++i) {
			var radio = $('<li class=list-group-item><label class="radio-inline"><input type="radio" class="custom" value="' + msg[i].id + '" name="hdmi" id="' + msg[i].id + '"><label for="'+msg[i].id+'">' + msg[i].name + '</label></label></li>');
			radio.appendTo('#inputList');
		}
		$('[name=hdmi]').click(function() {
			setHdmiInput($(this).val());
		});
	
		getHdmiInput(function(msg) {
			$("input:radio[name=hdmi][value='" + msg + "']").attr("checked", true);
		});
	});
}

/**********************
 * Picture Mode
 */

function drawPictureModeSelect() {
	drawDropDownBox('pictureModeSelect', [
		'vivid', 'normal', 'eco', 'cinema', 'sports', 'game', 'expert1'
	],
	[
		Locale.getText("Vivid"), Locale.getText("Standard"), 'APS', Locale.getText("Cinema"), Locale.getText("Sports"), Locale.getText("Game"), Locale.getText("Expert")
	], 'dropboxType2', function (selected) {
		setPictureMode(selected);
		getPictureModeVal();
		pictureCallback();
	});
}

function drawEnergySavingSelect() {
	drawDropDownBox('energySavingSelect', [
		'auto', 'off', 'min', 'med', 'max', 'screen_off'
	],
	[
		Locale.getText("Auto"), Locale.getText("Off"), Locale.getText("Minimum"), Locale.getText("Medium"), Locale.getText("Maximum"), Locale.getText("Screen Off")
	], 'dropboxType2', function (selected) {
		setSystemSettings('picture', {energySavingModified: true, energySaving: selected}, function(ret){
			getPictureDBVal(['contrast'], function(msg) {
				$('#contrast').val(Number(msg.contrast));
				$('#contrastVal').html(Number(msg.contrast));
				$('#contrast').attr('disabled', (selected != 'off'));
				checkRangeUI('contrast', 0, 100);
			});
		});
	});
}

function gammaSelected(pictureMode, selected) {
	var setting = {};
	setting['gamma'] = selected;
	setting.pictureSettingModified = {};
	setting.pictureSettingModified[pictureMode] = true;
	setPictureDBVal(setting, name);
	refreshPictureDropDownVal();
	checkChanged();
}

function drawGammaSelect() {
	getPictureMode(function(msg) {
		if (msg.pictureMode === 'vivid') {
			drawDropDownBox('gammaSelect', [], [], 'dropboxType2', function() {});
			disableDropButton('gammaSelect', true);
		} else if (msg.pictureMode === 'expert1'
		|| msg.pictureMode === 'cinema') {
			drawDropDownBox('gammaSelect', [
				'low', 'medium', 'high1', 'high2'
			],
			[
				'1.9', '2.2', '2.4', 'BT.1886'
			], 'dropboxType2', function (selected) {
				gammaSelected(msg.pictureMode, selected);
			});
		} else {
			drawDropDownBox('gammaSelect', [
				'low', 'medium', 'high1', 'high2'
			],
			[
				'Low', 'Medium', 'High1', 'High2'
			], 'dropboxType2', function (selected) {
				gammaSelected(msg.pictureMode, selected);
			});
		}
	});
}

function drawColorTempSelect() {
	getPictureMode(function(msg) {
		if (msg.pictureMode !== 'expert1') {
			drawDropDownBox('colorTempSelect', [], [], 'dropboxType2', function() {});
			disableDropButton('colorTempSelect', true);
		} else {
			drawDropDownBox('colorTempSelect', [
				'3200K', '4000K', '5200K', '6500K', '9300K'
			],
			[
				'3200K', '4000K', '5200K', '6500K', '9300K'
			], 'dropboxType2', function (selected) {
				var setting = {};
				setting.pictureSettingModified = {};
				setting.pictureSettingModified[msg.pictureMode] = true;
				setPictureDBVal(setting, name);
				setSystemSettings('commercial', {
					ledColorTempDegree: selected
				});
				refreshPictureDropDownVal();
				checkChanged();
			});
		}
	});
}

function drawBlackLevelSelect() {
	getSystemSettingDesc('picture', ['blackLevel'], function(msg) {
		var active = msg.results[0].ui.active;
		var visible = msg.results[0].ui.visible;

		var option = [];
		var optionName = [];
		msg.results[0].values.arrayExt.forEach(function(mode){
			if(mode.active && mode.visible){
				var name = mode.value[0].toUpperCase() + mode.value.slice(1, mode.value.length);
				option.push(mode.value);
				optionName.push(name);
			}
		});

		if (active && visible) {
			drawDropDownBox('blackLevelSelect', option, optionName, 'dropboxType2', function (selected) {
				getPictureMode(function(msg) {
					getSystemSettings('dimensionInfo', ['colorSystem'], function(msg2) {
						var setting = {};
						var selectedObject = {};
						selectedObject[msg2.colorSystem] = selected;
						setting['blackLevel'] = selectedObject;
						setting.pictureSettingModified = {};
						setting.pictureSettingModified[msg.pictureMode] = true;
						setPictureDBVal(setting, name);
						refreshPictureDropDownVal();
						checkChanged();
					});
				});
			});
		} else if (!active && visible) {
			drawDropDownBox('blackLevelSelect', [], [], 'dropboxType2', function () {});
			disableDropButton('blackLevelSelect', true);
		} else {
			// WHEN INVISIBLE
			console.log('LOG: Invisible');
		}
	});
}

function setUpPictureSlider(name, start, end) {
	checkRangeUI(name, start, end);

	var browserType = '';

	if ( agent.indexOf("webkit") != -1 ) {
		browserType = "webkit";
	} else if ( agent.indexOf("firefox") != -1 ) {
		browserType = "firefox";
	} else{
		browserType = "msie";
	}

	if( browserType == "msie" ){
		$('#' + name).mouseup(function() {
			var val = $(this).val();
			var setting = {};
			setting[name] = Number(val);
			getPictureMode(function(msg) {
				setting.pictureSettingModified = {};
				setting.pictureSettingModified[msg.pictureMode] = true;
				setPictureDBVal(setting, name);
				pictureCallback();
			});
		}).on('change', function() {
			checkRangeUI(name, start, end);

			if (name === 'tint') {
				var tintValue = $("#" + name).val();

				if (tintValue != 0) {
					tintValue = (tintValue < 0 ? 'R' : 'G') + Math.abs(tintValue);
				}

				$("#" + name + "Val").text(tintValue);
			} else {
				$("#" + name + "Val").text($("#" + name).val());
			}
		});
	}else{
		$('#' + name).change(function() {
			var val = $(this).val();
			var setting = {};
			setting[name] = Number(val);
			getPictureMode(function(msg) {
				setting.pictureSettingModified = {};
				setting.pictureSettingModified[msg.pictureMode] = true;
				setPictureDBVal(setting, name);
				pictureCallback();
			});
		}).on('input', function() {
			checkRangeUI(name, start, end);

			if (name === 'tint') {
				var tintValue = $("#" + name).val();

				if (tintValue != 0) {
					tintValue = (tintValue < 0 ? 'R' : 'G') + Math.abs(tintValue);
				}

				$("#" + name + "Val").text(tintValue);
			} else {
				$("#" + name + "Val").text($("#" + name).val());
			}
		});
	}
}

function refreshPictureDropDownVal() {
	var colorTemp;

	getPictureDBVal(['gamma', 'blackLevel'], function (msg) {
		var gamma = msg.gamma;
		setDropButtonText('gammaSelect', gamma, true);

		getSystemSettings('dimensionInfo', ['colorSystem'], function(msg2) {
			setDropButtonText('blackLevelSelect', msg.blackLevel[msg2.colorSystem], true);
		});
	});

	getSystemSettings("commercial", ["ledColorTempDegree"], function(ret) {
		colorTemp = '6500K'
		if (ret !== null) {
			colorTemp = ret.ledColorTempDegree;
		}
		setDropButtonText('colorTempSelect', colorTemp, true);
	});
}

function checkUiDisableAndDraw() {
	$('#controlItemSelect').html('');
	drawControlItemSelect();
	$('#pictureModeSelect').html('');
	drawPictureModeSelect();
	$('#energySavingSelect').html('');
	drawEnergySavingSelect();

	getPictureMode(function(msg) {
		var modified = msg.pictureSettingModified;
		var mode = msg.pictureMode;
		setDropButtonText('pictureModeSelect', mode, true);
		$('#pictureUser').html(mode !== 'expert1' && modified[mode] ? '(' + Locale.getText('User') + ')' : '');
	});

	$('#gammaSelect').html('');
	drawGammaSelect();
	$('#colorTempSelect').html('');
	drawColorTempSelect();
	$('#blackLevelSelect').html('');
	drawBlackLevelSelect();
}

function getPictureModeVal() {
	getPictureMode(function(msg) {
		var modified = msg.pictureSettingModified;
		var mode = msg.pictureMode;
		setDropButtonText('pictureModeSelect', mode, true);
		$('#pictureUser').html(mode !== 'expert1' && modified[mode] ? '(' + Locale.getText('User') + ')' : '');
		checkUiDisableAndDraw();
		refreshPictureDropDownVal();
	});
}

function getEnergySavingVal() {
	getSystemSettings('commercial', ['easyBrightnessMode'], function (ret) {
		getPictureDBVal(['energySaving'], function(msg) {
			var option = msg.energySaving;
			setDropButtonText('energySavingSelect', option, true);
			$('#contrast').attr('disabled', (ret.easyBrightnessMode === 'on' || option != 'off'));
		});
	});
}

function checkChanged() {
	getPictureMode(function(msg) {
		var modified = msg.pictureSettingModified;
		var mode = msg.pictureMode;
		$('#pictureUser').html(mode !== 'expert1' && modified[mode] ? '(' + Locale.getText('User') + ')' : '');
		$('#pictureResetBtn').prop('disabled', !modified[mode]);
	});
}

function checkRangUISharpness() {
	getPictureMode(function(msg) {
		if (msg.pictureMode === 'expert1') {
			$('#sharpnessTitle').hide();
			$('#sharpnessUi').hide();
			$('#hSharpnessTitle').show();
			$('#hSharpnessUi').show();
			$('#vSharpnessTitle').show();
			$('#vSharpnessUi').show();
			checkRangeUI('hSharpness', 0, 50);
			checkRangeUI('vSharpness', 0, 50);
		} else {
			$('#sharpnessTitle').show();
			$('#sharpnessUi').show();
			$('#hSharpnessTitle').hide();
			$('#hSharpnessUi').hide();
			$('#vSharpnessTitle').hide();
			$('#vSharpnessUi').hide();
			checkRangeUI('sharpness', 0, 50);
		}
	});
}

function pictureCallback() {
	refreshLed();
	getEnergySavingVal();
	getBrightnessMode();
	getPictureDBVal(['brightness', 'contrast', 'hSharpness', 'vSharpness', 'sharpness', 'color', 'tint'], function (msg) {
		for (var key in msg) {
			if (msg.hasOwnProperty(key)) {
				if(key=='tint'){
					if(msg[key] == 0){
						$('#' + key + 'Val').html(msg[key]);
					}else{
						var tintVal = ''
						msg[key] < 0 ? tintVal += 'R' : tintVal += 'G'
						$('#' + key + 'Val').html(tintVal + Math.abs(msg[key]) );
					}
					$('#' + key).val(msg[key]);
				}else{
					$('#' + key).val(msg[key]);
					$('#' + key + 'Val').html(msg[key]);
				}
			}
		}
		checkRangeUI('brightness', 0, 100);
		checkRangeUI('contrast', 0, 100);
		checkRangUISharpness();
		checkRangeUI('color', 0, 100);
		checkRangeUI('tint', -50, 50);
		refreshPictureDropDownVal();
		checkChanged();
	});
}

function pictureResetButton() {
	var pictureResetModal = addTwoButtonModal('pictureResetModal', 'Are you sure to change Settings?', 'Cancel', 'OK');
	$('body').append(pictureResetModal);
	$('#pictureResetModalOK').click(function () {
		resetPictureDB(function (msg) {
			if (msg.returnValue) {
				pictureCallback();
				getPictureMode(function (msg) {
					if (msg.pictureMode === 'expert1') {
						setSystemSettings('commercial', {
							ledColorTempDegree: '6500K'
						});
						refreshPictureDropDownVal();
						checkChanged();
					}
				});
			}
		});
	});

	$("#pictureResetBtn").click(function() {
		$('#pictureResetModal').modal();
	});
}

function setUpPicturePanel() {
	//drawControlItemSelect();
	//drawPictureModeSelect();
	//drawEnergySavingSelect();

	setUpPictureSlider('brightness', 0, 100);
	setUpPictureSlider('contrast', 0, 100);
	setUpPictureSlider('hSharpness', 0, 50);
	setUpPictureSlider('vSharpness', 0, 50);
	setUpPictureSlider('sharpness', 0, 50);
	setUpPictureSlider('color', 0, 100);
	setUpPictureSlider('tint', -50, 50);

	getPictureModeVal();
	getEnergySavingVal();
	pictureCallback();
	pictureResetButton();
}

/**********************
 * network
 */
function checkIPAddress(ip) {
	var numbers = ip.split(".");

	if (numbers.length != 4) {
		return false;
	}

	for (var i = 0; i != 4; ++i) {
		if( (numbers[i].length>1) && (numbers[i][0] == 0) ){
			return false;
		}

		if (!$.isNumeric(numbers[i])) {
			return false;
		}
		var n = parseInt(numbers[i]);

		if (n < 0 || n > 255) {
			return false;
		}
	}

	return true;
}

function checkAddressValid(userData){
	var checkResult = {};
	checkResult.valid = true;
	checkResult.message = '';

	if(userData.dhcp){
		return checkResult;
	}

	if(!checkIPAddress(userData.ip)){
		checkResult.valid = false;
		checkResult.message += '<br>IP address';
	}

	if(!checkIPAddress(userData.netMask)){
		checkResult.valid = false;
		checkResult.message += '<br>Subnet Mask';
	}

	if(!checkIPAddress(userData.gateway)){
		checkResult.valid = false;
		checkResult.message += '<br>Gateway';
	}

	if(!checkIPAddress(userData.dns)){
		checkResult.valid = false;
		checkResult.message += '<br>DNS Server';
	}

	return checkResult;
}


function drawIpTypeSelect(wired){
	var ipType = ['IPv4'];
	var ipTypeVal = [4];

	if( wired.ipv6 != undefined ){
		ipType.push('IPv6');
		ipTypeVal.push(6);
	}

	drawDropDownBox('ipTypeSelect', ipTypeVal, ipType, 'dropboxType2', function (selected) {
		ipType = selected;
		if(selected==4){
			enableTextFiled(true);
			getNetworkStatus(function(ret) {
				var wired = ret.wired;
				$('#dhcp').prop('checked', wired.method=="dhcp");
				$('#dhcp').change();
				$('#mac').val(wired.macAddress);
				$('#ip').val(wired.ipAddress);
				$('.subnetMaskTxt').html( Locale.getText("Subnet Mask") );
				$('#netMask').val(wired.netmask);
				$('#gateway').val(wired.gateway);
				$('#dns').val(wired.dns1);
			});
		}else{
			enableTextFiled(false);
			getNetworkStatus(function(ret) {
				var wired = ret.wired;
				var ipv6 = wired.ipv6;
				if( ipv6 == undefined ){
					return;
				}

				$('#mac').val(wired.macAddress);
				$('#ip').val(ipv6.ipAddress);
				$('.subnetMaskTxt').html(Locale.getText('Subnet prefix length') );
				$('#netMask').val(ipv6.prefixLength);
				$('#gateway').val(ipv6.gateway);
				$('#dns').val(getDnsInfo(wired));

				$('#ip').attr('disabled', true);
				$('#netMask').attr('disabled', true);
				$('#gateway').attr('disabled', true);
				$('#dns').attr('disabled', true);
			});
		}
	},4);
}


function enableTextFiled(on){
	if(on){
		$('#setAutoTR').show();
	}else{
		$('#setAutoTR').hide();
	}

	$('#ip').attr('disabled', !on);
	$('#netMask').attr('disabled', !on);
	$('#gateway').attr('disabled', !on);
	$('#dns').attr('disabled', !on);
	$('#networkSave').attr('disabled', !on);
}


function getDnsInfo(data) {
    if (!data) {
        return "";
    }
    var dns = "";
    var repeat = true;
    var i = 1;
    while (repeat) {
        var key = "dns" + i;
        if (data[key]) {
            if (data[key].indexOf(":") >= 0) {
                dns = data[key];
                repeat = false;
            }
        } else {
            repeat = false;
        }
        i++;
    }
    return dns;
}



var originDns = '';

function setUpNetworkPanel() {
	getNetworkStatus(function(ret) {
		wired = ret.wired;
		originDns = wired.dns1;
		drawIpTypeSelect(wired);

		$('#dhcp').prop('checked', wired.method=="dhcp");
		$('#dhcp').change();
		$('#mac').val(wired.macAddress);
		$('#ip').val(wired.ipAddress);
		$('#netMask').val(wired.netmask);
		$('#gateway').val(wired.gateway);
		$('#dns').val(wired.dns1);
	});

	$('#dhcp').change(function(){
		var checked = $(this).prop('checked');
		
		$('#ip').prop('disabled', checked);
		$('#netMask').prop('disabled', checked);
		$('#gateway').prop('disabled', checked);
		$('#dns').prop('disabled', checked);

		if(!checked){
			$('#mac').val(wired.macAddress);
			$('#ip').val(wired.ipAddress);
			$('#netMask').val(wired.netmask);
			$('#gateway').val(wired.gateway);
			$('#dns').val(wired.dns1);
		}else{
			$('#ip').val('');
			$('#netMask').val('');
			$('#gateway').val('');
			$('#dns').val('');
		}
	});

	$('#networkSave').click(function(){
		var userData = {};
		userData.dhcp = $('#dhcp').prop('checked');
		userData.ip = $('#ip').val();
		userData.netMask = $('#netMask').val();
		userData.gateway = $('#gateway').val();
		userData.dns = $('#dns').val();

		checkResult = checkAddressValid(userData);
		if(checkResult.valid){
			setNetworkStatus(userData.dhcp, userData.ip, userData.netMask, userData.gateway, undefined, function(retNetwork){
				if(retNetwork.returnValue){
					setNetworkDNS(userData.dhcp ? originDns : userData.dns, undefined, function(retDNS){
						if(retDNS.returnValue){
							showWarning("<strong style='color:green;'>" + Locale.getText("Success") + "</strong>" + checkResult.message, 5 * 1000);
						}
					});
				}
			});
		}else{
			$('#warningMessage').empty();
			showWarning("<strong>" + Locale.getText("Invalid value") + "</strong>" + checkResult.message, 5 * 1000);
			$('#ip').val(wired.ipAddress);
			$('#netMask').val(wired.netmask);
			$('#gateway').val(wired.gateway);
			$('#dns').val(wired.dns1);
		}
	});
}

/**********************
 * time
 */

 function drawYearSelect(){
	var yearMonth = [];

	for(var i=2016; i<=2037; i++){
		yearMonth.push(i);
	}

	$('#yearSelect').empty();
	drawDropDownBox('yearSelect', yearMonth, yearMonth, 'dropboxType2', function (selected) {
		selectTime.year = selected;
		drawDaySelect();
	});

	setDropButtonText('yearSelect', currentTime.year, true);
}

function drawMonthSelect(){
	var arrMonth = [];

	for(var i=1; i<=12; i++){
		arrMonth.push(i);
	}

	$('#monthSelect').empty();
	drawDropDownBox('monthSelect', arrMonth, arrMonth, 'dropboxType2', function (selected) {
		selectTime.month = selected;
		drawDaySelect(selected);
	});

	setDropButtonText('monthSelect', currentTime.month, true);
}

function drawDaySelect(month){

	var dayCount = [31,28,31,30,31,30,31,31,30,31,30,31];
	var arrDay = [];
	var year = selectTime.year;
	var currMonth = month == undefined ? currentTime.month : month;

	if( ((year % 4 == 0) && (year % 100 != 0)) || year % 400 == 0){
		dayCount[1] = 29;
	}

	for(var i=1; i<=dayCount[currMonth-1]; i++){
		arrDay.push(i);
	}

	$('#daySelect').empty();
	drawDropDownBox('daySelect', arrDay, arrDay, 'dropboxType2', function (selected) {
		console.log(selected);
		selectTime.day = selected;
	});

	if( currentTime.day > dayCount[currentTime.month-1] ){
		currentTime.day = dayCount[currentTime.month-1];
		setDropButtonText('daySelect', dayCount[currentTime.month-1], true);
	}else{
		setDropButtonText('daySelect', currentTime.day, true);
	}
}

function drawHourSelect(){
	var arrHour = [];

	for(var i=0; i<=23; i++){
		arrHour.push(i);
	}

	$('#hourSelect').empty();
	drawDropDownBox('hourSelect', arrHour, arrHour, 'dropboxType2', function (selected) {
		selectTime.hour = selected;
	});

	setDropButtonText('hourSelect', currentTime.hour, true);
}

function drawMinuteSelect(){
	var arrMinute = [];

	for(var i=0; i<=59; i++){
		arrMinute.push(i);
	}

	$('#minuteSelect').empty();
	drawDropDownBox('minuteSelect', arrMinute, arrMinute, 'dropboxType2', function (selected) {
		selectTime.minute = selected;
	});

	setDropButtonText('minuteSelect', currentTime.minute, true);
}

function setUpTimePanel() {
	getCurrentTime(function(msg){
		currentTime = msg;
		selectTime = currentTime;
		drawYearSelect();
		drawMonthSelect();
		drawDaySelect();
		drawHourSelect();
		drawMinuteSelect();

		getNTPStatus(function(ret) {
			var auto = ret.useNetworkTime;
			$('#setAuto').attr('checked', auto);
			$('#yearSelectDrop').attr('disabled', auto);
			$('#monthSelectDrop').attr('disabled', auto);
			$('#daySelectDrop').attr('disabled', auto);
			$('#hourSelectDrop').attr('disabled', auto);
			$('#minuteSelectDrop').attr('disabled', auto);
			$('#saveTimeBtn').attr('disabled', auto);
			$('#savePCTimeBtn').attr('disabled', auto);
			$('#ntpManualSettingBtn').attr('disabled', !auto);
		});

		initNtpServerSetting();
	});

	$('#setAuto').click(function(){
		var auto = $(this).prop('checked');
		overlayControl20(true, Locale.getText('Loading'));
		var param = {useNTP: auto}
		setNTPStatus(param, function(ret){
			getCurrentTime(function(msg){
				currentTime = msg;
				selectTime = currentTime;
				drawYearSelect();
				drawMonthSelect();
				drawDaySelect();
				drawHourSelect();
				drawMinuteSelect();
				$('#yearSelectDrop').attr('disabled', auto);
				$('#monthSelectDrop').attr('disabled', auto);
				$('#daySelectDrop').attr('disabled', auto);
				$('#hourSelectDrop').attr('disabled', auto);
				$('#minuteSelectDrop').attr('disabled', auto);
				$('#saveTimeBtn').attr('disabled', auto);
				$('#savePCTimeBtn').attr('disabled', auto);
				$('#ntpManualSettingBtn').attr('disabled', !auto);
			});
			overlayControl20(false);
		});
	});

	$('#saveTimeBtn').click(function(){
		setCurrentTime(selectTime);
	});

	$('#savePCTimeBtn').click(function(){
		var currDate = new Date();
		selectTime.year = currDate.getFullYear();
		selectTime.month = currDate.getMonth()+1;
		selectTime.day = currDate.getDate();
		selectTime.hour = currDate.getHours();
		selectTime.minute = currDate.getMinutes();

		setDropButtonText('yearSelect', currDate.getFullYear(), true);
		setDropButtonText('monthSelect', currDate.getMonth()+1, true);
		setDropButtonText('daySelect', currDate.getDate(), true);
		setDropButtonText('hourSelect', currDate.getHours(), true);
		setDropButtonText('minuteSelect', currDate.getMinutes(), true);

		setNTPStatus({useNTP: false}, function(ret){
			setCurrentTime(selectTime);
		});
	});
}

/**********************
 * Video Scaler
 */

function drawVideoScalerSelect() {
	drawDropDownBox('videoScalerSelect', [
		'on', 'off'
	],
	[
		'On', 'Off'
	], 'dropboxType2', function (selected) {
		console.log(selected);
	});
}

function drawCanvasSelect() {
	drawDropDownBox('canvasSelect', [
		'create'
	],
	[
		'Create New'
	], 'dropboxType2', function (selected) {
		console.log(selected);
		if( selected == 'create')
			createCanvas();
		else
			changeCanvas();
	});
}

function setUpScalerPanel() {
	drawVideoScalerSelect();
	drawCanvasSelect();
}
