//for Dashboard
var refreshInterval = undefined;

//for TimeZone
var continentNameList = ["Africa", "Asia", "CIS", "Europe", "Middle East", "North America", "Oceania", "Pacific", "South America"];
var continentList = ["Africa", "Asia", "CIS", "Europe", "MiddleEast", "NorthAmerica", "Oceania", "Pacific", "SouthAmerica"];
var currContinent = '';
var currCountry = '';
var currCity = '';
var isCityState = false;

//for FirstUse
var originalVal = {};
var selectVal = {};

//for Log
var currLogPage = 0;
var logRowCount = 0;
var sepLogString = undefined;

function drawAutoRefreshSelect() {
	$('#autoRefreshSelect').empty();

	var addChar = '';

	if (env.locale === 'ar-SA') {
		addChar = "\u202A";
	}

	drawDropDownBox('autoRefreshSelect', [
		'0', '30', '300'
	], [
		addChar + Locale.getText("None"), addChar + Locale.getText("30sec"), addChar + Locale.getText("5Min")
	], 'dropboxType3', function (selected) {
		var interval = parseInt(selected);
		if(interval == 0){
			clearInterval(refreshInterval);
		}else{
			clearInterval(refreshInterval);
			refreshInterval = setInterval(function(){
				initInformation();
			}, interval * 1000);
		}
	}, '0');
}

function getLog() {
	getCurrentTime(function(msg) {
		sepLogString = log.split('\n');
		sepLogString.splice(sepLogString.length-1, 1);
		sepLogString = sepLogString.reverse();

		var tableTxt = '';
		var max = 20;

		if( sepLogString.length < 20 ){
			max = sepLogString.length;
		}

		for(var i = 0; i < max; i++){
			tableTxt += '<tr>';
			tableTxt += '<td>' + sepLogString[i].replace(/Please, Set a time/g, "Please Set a time").split(',')[4] + '</td>'
			tableTxt += '<td>' + sepLogString[i].split(',')[0] + '</td>'
			tableTxt += '<td>' + sepLogString[i].split(',')[1] + '</td>'
			tableTxt += '<td>' + sepLogString[i].split(',')[3] + '</td>'
			tableTxt += '</tr>';
		}
		$('#incidentTable > tbody').empty();
		$('#incidentTable > tbody:last').prepend(tableTxt);
	});
}

function showTempGraph(id) {
	if (chartPage < 1) {
		chartPage = 1;	
	}

	var pageSet = 10;

	var start = (chartPage - 1) * defaultHistorySize;
	var end = start + defaultHistorySize;

	$('#tempChartWrapper').empty();
	$('#tempChartWrapper').append('<canvas id="tempChart"></canvas>');

	function startTemperatureChart(tempSensors) {
		var index = 0;
		var tempHistory = [];
		function tempHistoryCallback(msg) {

			readTempType(function(tempType){
				var isCelsius = tempType.isCelsius;

				if(isCelsius == 'true'){
					tempHistory.push(msg.history);
				}else{
					for(var i=0; i<msg.history.length;i++){
						msg.history[i] = msg.history[i]* 1.8 + 32
					}
					tempHistory.push(msg.history);
				}

				if (index == tempSensors.length - 1) {
					totalHistorySize = msg.queueSize;
					drawTempChart(tempHistory, msg.interval, true, id);
					index = 0;
					return;
				}
				++index;
				getTempHistory(tempSensors[index].id, start, end, tempHistoryCallback);
			});
		}
		getTempHistory(tempSensors[index].id, start, end, tempHistoryCallback);
	}
	startTemperatureChart(tempSensors);
}

// LED Status
var unitStatus = {
	ok: 0,
	ng: 0,
	off: 0
};

function showLedStatusMessage() {
	$('#normalUnit').html(unitStatus.ok);
	$('#NGUnit').html(unitStatus.ng);
	$('#offUnit').html(unitStatus.off);
}

function checkUnitOk(unitID) {
	if(env.supportLedFilm){
		getLedPowerState( (unitID-1), function(msg) {
			if (!msg.powerState) {
				unitStatus.off++;
				showLedStatusMessage();
				return;
			}
			getLedOverallStatus(unitID * 2, function(msg) {
				unitStatus[msg.isOverallOk ? 'ok' : 'ng']++;
				showLedStatusMessage();
			});
		});
	}else{
		getLedPowerStatus(unitID * 2, function(msg) {
			if (!msg.isPowerOk) {
				unitStatus.off++;
				showLedStatusMessage();
				return;
			}
			getLedOverallStatus(unitID * 2, function(msg) {
				unitStatus[msg.isOverallOk ? 'ok' : 'ng']++;
				showLedStatusMessage();
			});
		});
	}
}

function checkUnitStatusHandler() {
	unitStatus.ok = 0;
	unitStatus.ng = 0;
	unitStatus.off = 0;
	if(env.supportLedFilm){
		if( env.ledFilmType == 'color' ){
			getSystemSettings('commercial', ['ledLayout'], function(ret){
				console.log("ledLayout Check : " + ret.ledLayout);
				if(ret.ledLayout.pixelPitch == undefined){ // for select pitch modal(14mm/24mm)
					getP14Status(function (msg) {
						var cpuRunStatus = msg.result && msg.result.cpuRunStatus ? msg.result.cpuRunStatus : false;
						var initialPitch = 0;

						if (cpuRunStatus) {
							initialPitch = msg.result && msg.result.p14Status === true ? 14 : 24;
						}

						addPitchSelectModal('selectPitchModal', Locale.getText('Select LED Film Pitch'), initialPitch, function(){
							// Luna Call for setting LED pitch(14mm, 24mm), when clicked OK button
							var ledLayout = $("input[id=selectPitchModalVal]:checked").val();
							console.log("Set LED Layout : " + ledLayout);
							setSystemSettings('commercial', {'ledLayout' : {'pixelPitch' : ledLayout}});
						});

						$('#selectPitchModal').modal({
							backdrop: 'static',
							keyboard: false,
							show: true
						});
						$('#selectPitchModalModalOK').attr('disabled', !cpuRunStatus);
					});
				}
				if(ret.ledLayout.senderList != undefined){
					var senderList = ret.ledLayout.senderList;
					for(var i=0; i<senderList.length; i++){
						for(var j=0; j<senderList[i].unitList.length; j++){
							var slave = parseInt(senderList[i].senderId) * 2
							var unit = parseInt(senderList[i].unitList[j].unitId) - 1;

							console.log('sender::'+slave+', unit::'+unit);
							getReceiverStatus( slave, unit, function(status){
								if(status.returnValue){
									if(!status.result.power){
										unitStatus.off++;
									}else if(!status.result.signal){
										unitStatus.ng++;
									}else{
										unitStatus.ok++;
									}
								}
								showLedStatusMessage();
							});
						}
					}
				}
			});
		}else{
			getLedConfig(function(curConfig){
				for (var i = 0; i != curConfig.unitList.length; ++i) {
					checkUnitOk(i + 1);
				}
			});
		}
	} else {
		getLedConfig(function(curConfig) {
			for (var i = 0; i != curConfig.unitList.length; ++i) {
				checkUnitOk(i + 1);
			}
		});
	}
}
