function toggleMute(val) {
	var muted = val ? "on" : "off";
	$("#muted").bootstrapToggle(muted);
}

var init = false;
var pictureMode = undefined;

function addListener() {
	init = true;
	$("#muted").change(function() {
		setMuted($("#muted").prop('checked'));
	});

	$("#volumn-level").change(function() {
		$('#muted').prop('checked', false);
		$("#volumn-val").html($(this).val());
		setMuted(false);
		setVolume($(this).val());
	}).on('input', function() {
		$("#volumn-val").html($(this).val());
		checkRangeUI('volumn-level', 0, 100);
	});
}

function checkEnableBacklight(energySaving, easyBrightnessMode) {
	if (env.supportLedSignage) {
		return energySaving == 'off';
	}

	var isEnergySavingMode = !(energySaving == "off" || energySaving == "min" || energySaving == "med");
	var isEasyBrightnessMode = easyBrightnessMode === 'on';

	return !isEnergySavingMode && !isEasyBrightnessMode;
}

function backlightCallback() {
	energySavMsg = {
		auto: 'Auto',
		min: 'Minimum',
		max: 'Maximum',
		med: 'Medium'
	};

	$.when((function () {
		var deferred = $.Deferred();
		getSystemSettings('commercial', ['easyBrightnessMode'], function (msg) {
			deferred.resolve(msg);
		});
		return deferred.promise();
	})(), (function () {
		var deferred = $.Deferred();
		getPictureDBVal(['backlight', 'contrast', 'energySaving', 'pictureMode'], function (msg) {
			deferred.resolve(msg);
		});
		return deferred.promise();
	})()).done(function (msg1, msg2) {
		var easyBrightnessMode = msg1.easyBrightnessMode;
		var energySavingVal = msg2.energySaving;
		var backlightVal = msg2.backlight;
		var contrastVal = msg2.contrast;
		var pictureModeVal = msg2.pictureMode;

		var enableBacklightVal = false;

		if ($("#btnScreen").prop("checked")) {
			if (checkEnableBacklight(energySavingVal, easyBrightnessMode)) {
				enableBacklightVal = true;
			}
		}

		$('#backlight').val(backlightVal);
		$('#contrast').val(contrastVal);

		checkRangeUI('backlight', 0, 100);
		checkRangeUI('contrast', 0, 100);

		if (!enableBacklightVal) {
			disableBacklight(energySavingVal, easyBrightnessMode);
		} else {
			enableBacklight();
		}

		pictureMode = pictureModeVal;
	})
}

function disableBacklight(energySavingVal, easyBrightnessMode){
	var isEnergySavingMode = !(energySavingVal == "off" || energySavingVal == "min" || energySavingVal == "med");
	var isEasyBrightnessMode = easyBrightnessMode === 'on';

	$('#backlight').prop('disabled', true);
	$('#contrast').prop('disabled', true);
	if (!isEnergySavingMode && isEasyBrightnessMode) {
		$('#backlightVal').html($('#backlight').val() + '%');
		$('#contrastVal').html($('#contrast').val());
	} else {
		$('#backlightVal').html(Locale.getText("Energy Saving"));
		$('#contrastVal').html(Locale.getText("Energy Saving"));
	}
}

function enableBacklight(){
	$('#backlight').prop('disabled', false);
	$('#backlightVal').html($('#backlight').val() + '%');
	$('#contrast').prop('disabled', false);
	$('#contrastVal').html($('#contrast').val());
}

function setHdmi(msg) {
	if (!msg) {
		return;
	}

	$("input:radio[name=hdmi]:input[value='" + msg + "']").val(msg).attr("checked", true);
}

function drawPmModeSelect() {
	var dropDown = createDropDown('pmModeSelect', '-', function(selected) {
		setPmMode(selected);
	});
	addDropDownOption(dropDown, "powerOff", Locale.getText("Power Off(Default)") );
	addDropDownOption(dropDown, "sustainAspectRatio", Locale.getText("Sustain Aspect Ratio"), true);
	addDropDownOption(dropDown, "screenOff", Locale.getText("Screen Off"), true);
	addDropDownOption(dropDown, "screenOffAlways", Locale.getText("Screen Off Always") );
	if (env.supportXE) {
		//addDropDownOption(dropDown, "screenOffBacklight", "Screen Off &amp; Backlight(Outdoor Mode)");
		addDropDownOption(dropDown, "screenOffBacklight", Locale.getText("Screen Off & Backlight")+'('+ Locale.getText("Outdoor Mode") +')');
	}

	$('#pmModeSelect').append(dropDown);

	getPmMode(function(msg) {
		setDropButtonText('pmModeSelect', msg.pmMode);
	});
}

function initDevice() {
	$("#btnScreen").click(function() {
		if (env.supportLedSignage){
			if( $("#btnScreen").is(":checked") ){
				setCtrlDCDC(0,1);
				backlightCallback();
			}else{
				setCtrlDCDC(0,0);
				disableBacklight();
			}
		}else{
			if( $("#btnScreen").is(":checked") ){
				turnOnScreen();
				backlightCallback();
			}else{
				turnOffScreen();
				disableBacklight();
			}
		}

    });

    $('#backlight').change(function() {
		var setting = {
			backlight: Number($(this).val())
		}
		setting.pictureSettingModified = {};
		setting.pictureSettingModified[pictureMode] = true;
		setPictureDBVal(setting, "backlight");
		$('#backlightVal').html($(this).val()+"%");
	})
	.on('input', function() {
		$('#backlightVal').html($(this).val()+"%");

		checkRangeUI('backlight', 0, 100);
	});

	$('#contrast').change(function() {
		var val = $(this).val();
		$('#contrastVal').html(val);
		var setting = {
			contrast: Number($(this).val())
		}
		setting.pictureSettingModified = {};
		setting.pictureSettingModified[pictureMode] = true;
	setPictureDBVal(setting, 'backlight');
	}).on('input', function() {
		checkRangeUI('contrast', 0, 100);
	});

    $("#btnReboot").click(function() {
		$('#rebootModal').modal();
	});

	getVolume(function(ret) {
		$("#volumn-level").val(ret.volume);

		checkRangeUI('volumn-level', 0, 100);

		$("#volumn-val").html(ret.volume);
		$('#muted').prop('checked', ret.muted).change();

		if (!init) {
			addListener();
		}
	});

	function refreshPowerState(callback) {
		if (env.supportLedSignage) {
			getCtrlDCDC(0, function(msg) {
				$("#btnScreen").prop("checked", (msg.returnValue!==false) && (msg.result.DCDC !== 0) );
			});
		}else {
			getPowerState(function(msg) {
				$("#btnScreen").prop("checked", msg.state !== 'Screen Off');
				if ($("#btnScreen").prop("checked") == false) {
					disableBacklight();
				}

				if (callback) {
					callback();
				}
			});
			return
		}

		if ( $("#btnScreen").prop("checked") == false ) {
			disableBacklight();
		}

		if (callback) {
			callback();
		}
	}

	getInputList(function(msg) {
		var rtl = env.locale == 'ar-SA' ? 'dir=rtl' : '';
		for (var i = 0; i != msg.length; ++i) {
			var radio = $('<li class=list-group-item><label class="radio-inline" ' + rtl + '><input type="radio" class="custom" value="' + msg[i].id + '" name="hdmi" id="' + msg[i].id + '" ' + rtl + '><label for="' + msg[i].id + '">' + msg[i].name + '</label></label></li>');
			radio.appendTo('#inputList');
		}
		$('[name=hdmi]').click(function () {
			for (var i = 0; i != msg.length; ++i) {
				if ($(this).val() === msg[i].id) {
					setInputSouce(msg[i].appId, function (msg) {
						refreshPowerState();
					});
				}
			}
		});

		getHdmiInput(setHdmi);
	});

	refreshPowerState(backlightCallback);

	$('#pageTitle').html(Locale.getText('Device Control'));

	drawPmModeSelect();

	addConfirmModal('rebootModal', Locale.getText('Reboot'), Locale.getText('Are you sure to reboot?'), function() {
		overlayControl(true, Locale.getText('Reboot') + '......');
		rebootScreen();

		setTimeout(function(){
		   window.location.replace("/login.html");
		}, 60 * 1000);
	});
}

