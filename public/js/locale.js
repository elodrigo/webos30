function commonCallback() {
	$('.LanguageSettingTxt').text(Locale.getText("Menu Language"));
	$('.ChangePasswdTxt').text(Locale.getText("Change Password"));
	$('.LogoutTxt').text(Locale.getText("Logout"));
	$('.DashboardTxt').text(Locale.getText("Dashboard"));
	$('.DeviceControlTxt').text(Locale.getText("Device Control"));
	$('.VirtualControllerTxt').text(Locale.getText("Virtual Controller"));
	$('.SystemSettingsTxt').text(Locale.getText("System Settings"));
	$('.PictureTxt').text(Locale.getText("Picture"));
	$('.NetworkTxt').text(Locale.getText("Network"));
	$('.TimeTxt').text(Locale.getText("Time"));
	$('.CheckScreenTxt').text(Locale.getText("CheckScreen"));
	$('.DoorMonitorTxt').text(Locale.getText("Door Monitor"));
	$('.SwUpdateTxt').text(Locale.getText("S/W Update"));
	$('.InformationTxt').text(Locale.getText("Information"));
	$('.ChartTxt').text(Locale.getText("Charts"));
	$('.FanStatusTxt').text(Locale.getText("Fan Status"));
	$('.LogTxt').text(Locale.getText("Log"));
	$('.SystemInfoTxt').text(Locale.getText("System Information"));
	$('.MediaLibTxt').text(Locale.getText("Media Library"));
	$('.LedSettingsTxt').text(Locale.getText("LED Settings"));
	$('.LedConfigTxt').text(Locale.getText("Screen Configuration"));
	$('.LedControlTxt').text(Locale.getText("System Control"));
	$('.LedUpdateTxt').text('FPGA ' + Locale.getText("Update"));
	$('.LgControlMgr').text(Locale.getText("LG Control Manager"));
}

function addRtlDir() {
	if (env.locale != 'ar-SA') {
		return;
	}
	$('table').attr('dir', 'rtl');
	$('h4').attr('dir', 'rtl');
	$('.list-group-item label').attr('dir', 'rtl');
	$('#inputList').attr('dir', 'rtl').css('padding', '0 0 0 0');
	$('input').attr('dir', 'rtl');
	$('#incidentTable').attr('dir', 'ltr');
	$('.table-update-file').attr('dir', 'ltr');
	$('.dropdown-toggle').attr('dir', 'rtl').css('text-align', 'right');
	$('.dropdown-select').attr('dir', 'rtl').css('text-align', 'right');
	$('.dropdown-menu').attr('dir', 'rtl').css('text-align', 'right');
	$('#tempButton').attr('style', "position:absolute; left:20px; top:-5px");
	$('.navbar-nav').attr('dir', 'rtl').css('padding-right', '0px');
	$('.dropdown').addClass('arabic');
	if (!isMobile) {
		$('.navbar-right').css('position', 'absolute').css('left', '25%');
	}
	$('.caret.mobile').css('left', '').css('right', '90%');
	$('.breadcrumb').css('position', 'absolute').css('right', '10%');
	$('.list-group-item').css('text-align', 'right');
	$('.badge').css('float', 'left');
	$('.card-title').css('float', 'right');
	$('.card-title').css('padding-left', '');
	$('.card-title').css('padding-right', '15px');
	$('.card-title').attr('dir', 'rtl');
	$('#linkarrow').css('padding-right', '50px');
	$('#category').attr('dir', 'rtl');
	$('.date').css('float', 'right').css('padding-right', '25px');
	$('.cur-time').css('float', 'right').css('padding-right', '25px');
	$('.run-time').css('float', 'left').css('padding-left', '25px');
	$('.top-refresh').css('float', 'left');
	$('#pagingTable').attr('dir', 'ltr');
}

var Locale = {
	langTable: {},
	etcTable: {},
	callback: function() {},

	getText: function(text) {
		if (Locale.etcTable && Locale.etcTable[text]) {
			return Locale.etcTable[text];
		}
		if (Locale.langTable && Locale.langTable[text]) {
			return Locale.langTable[text];
		}
		return text;
	},

	init: function(env) {
		Locale.langTable = env.localeTable.langTable;
		Locale.etcTable = env.localeTable.etcTable;

		commonCallback();
		document.title = Locale.getText("LG Control Manager");
		Locale.callback();

		addRtlDir();

		socket.on('locale', function() {
			location.reload()
		});
	}
}
