function isOn(unitID, config) {
	return unitID in config.onUnits;
}

function callLedAPI(command, data, event, callback) {
	if (event && callback) {
		var id = Math.round(Math.random() * 1000000);
		socket.on(event + id, function(msg) {
			callback(msg);
			socket.removeAllListeners(event + id);
		});
		data.eventID = id;
	}
	data.command = command;
	socket.emit("led", data);
}

function getUnitConfig(callback) {
	callLedAPI('getUnitConfig', {}, 'config', callback);
}

function setUnitConfig(config) {
	callLedAPI('setUnitConfig', config, 'config');
}

function getUnitResolutionList(callback) {
	callLedAPI('getUnitResolutionList', {}, 'config', callback);
}

function showPattern(slave, on, r, g, b) {
	callLedAPI('showPattern', {
		slave: slave,
		on: on,
		r: r,
		g: g,
		b: b
	}, 'pattern');
}

function setSlaveAddr(numUnits, callback) {
	callLedAPI('setSlaveAddr', {
		numUnits: numUnits
	}, 'config', callback);
}

function setPosition(slave, x, y) {
	callLedAPI('setPosition', {
		slave: slave,
		x: x,
		y: y
	}, 'config');
}

function getGamma(slave, callback) {
	callLedAPI('getGamma', {
		slave: slave
	}, 'unit', callback);
}

function setGamma(slave, gamma) {
	callLedAPI('setGamma', {
		slave: slave,
		gamma: Number(gamma)
	}, 'unit');
}

function getGamut(slave, callback) {
	callLedAPI('getGamut', {
		slave: slave
	}, 'unit', callback);
}

function setGamut(slave, array) {
	callLedAPI('setGamut', {
		slave: slave,
		array: array
	}, 'unit');
}

function setBC_CC(slave, bank, bc, r, g, b) {
	callLedAPI('setBC_CC', {
		slave: slave,
		bank: bank,
		bc: bc,
		r: r,
		g: g,
		b: b
	}, 'module');
}

function getBC_CC(slave, bank, callback) {
	callLedAPI('getBC_CC', {
		slave: slave,
		bank: bank
	}, 'module', callback);
}

function setCtrlDCDC(slave, dcdc) {
	callLedAPI('setCtrlDCDC', {
		slave: slave,
		dcdc: dcdc,
	}, 'unit');
}

function getCtrlDCDC(slave, callback) {
	callLedAPI('getCtrlDCDC', {
		slave: slave,
	}, 'unit', callback);
}


function getLedTemperature(slave, callback) {
	callLedAPI('getTemperature', {
		slave: slave,
	}, 'unit', callback);
}

function getLedCurrent(slave, callback) {
	callLedAPI('getCurrent', {
		slave: slave,
	}, 'unit', callback);
}


function getLedSignalStatus(slave, callback) {
	callLedAPI('getSignalStatus', {
		slave: slave,
	}, 'unit', callback);
}

function getLedPowerStatus(slave, callback) {
	callLedAPI('getPowerStatus', {
		slave: slave,
	}, 'unit', callback);
}

function getLedLOD(slave, callback) {
	callLedAPI('getLOD', {
		slave: slave,
	}, 'unit', callback);
}

function getLedOverallStatus(slave, callback) {
	callLedAPI('getOverallStatus', {
		slave: slave,
	}, 'unit', callback);
}

function saveConfig(slave) {
	callLedAPI('saveConfig', {
		slave: slave
	}, 'config');
}

function loadConfig(slave) {
	callLedAPI('loadConfig', {
		slave: slave
	}, 'config');
}

function saveCalibrationData(slave, bank, callback) {
	callLedAPI('saveCalibrationData', {
		slave: slave,
		bank: bank
	}, 'module', callback);
}

function loadCalibrationData(slave, from, callback) {
	callLedAPI('loadCalibrationData', {
		slave: slave,
		from: from
	}, 'module', callback);
}

function isOn(unitID, config) {
	return unitID in config.onUnits;
}

function ledFpgaUpdate(fileName, callback) {
	callLedAPI("ledFpgaUpdate", {
		fileName: fileName
	}, 'ledFpga', callback);
}

function getFpgaUpdatePercent(callback) {
	callLedAPI("getFpgaUpdatePercent", {
	}, 'ledFpga', callback);
}

function getFpgaCheckSum(name, callback) {
	callLedAPI("getFpgaCheckSum", {
		name: name
	}, 'ledFpga', callback);
}

function startLiveLOD(start, callback) {
	callLedAPI('liveLOD', { start: start }, 'liveLOD', callback);
}

function liveLOD(callback) {
	callLedAPI('liveLOD', {}, 'liveLOD', callback);
}

function setPMControl(onOff) {
	callLedAPI('setPMControl', {
		onOff: onOff
	}, 'factory');
}

function setI2C(slave, sub, val) {
	callLedAPI('setI2C', {
		slave: slave,
		subAddr: sub,
		byte: val
	});
}

function getI2C(slave, sub, callback) {
	callLedAPI('getI2C', {
		slave: slave,
		subAddr: sub,
	}, 'ledFpga', callback);
}

function setPixelBrightness(slave, bank, v, h, coeff) {
	callLedAPI('setPixelBrightness', {
		slave: slave,
		bank: bank,
		v: v,
		h: h,
		coeff: coeff
	});
}

function setEdgeBrightness(slave, bank, side, coeff, rgbw) {
	if (rgbw == undefined) {
		rgbw = 3;
	}
	callLedAPI('setEdgeBrightness', {
		slave: slave,
		bank: bank,
		side: side,
		coeff: coeff,
		rgbw: rgbw
	});
}

function setWhiteBalance(temp) {
	callLedAPI('setWhiteBalance', {
		temp: temp
	});
}

function getLedConfig(callback) {
	callLedAPI('getLedConfig', {}, 'config', callback);
}

function setLedConfig(config) {
	callLedAPI('setLedConfig', {
		config: config
	});
}

function getLgseInterfereVal(slave, module, type, rgb, callback) {
	callLedAPI('getLgseInterfereVal', {
		slave: slave,
		module: module,
		type: type,
		rgb: rgb
	}, 'getLgseInterfereVal', callback);
}

function setLgseInterfereVal(slave, module, type, rgb, setVal, callback) {
	callLedAPI('setLgseInterfereVal', {
		slave: slave,
		module: module,
		type: type,
		rgb: rgb,
		setVal: setVal
	}, 'setLgseInterfereVal', callback);
}

function getConfigs(callback) {
	callLedAPI("getConfigs", {},
		"configs", callback
	);
}

function setConfigs(configVal) {
	callLedAPI("setConfigs", {
		configs: {
			"commercial.disableOsd": configVal
		}
	});
}

function setMouseVisibility(configVal) {
	callLedAPI("setMouseVisibility", {
		configs: {
			"enableMouseVisible": configVal
		}
	}, "configs", function (ret) {
		console.log(ret);
	});
}

function getSharedIpList(callback) {
	callLedAPI('getSharedIpList', {}, 'config', callback);
}

function setGroupFile(type, groupStr) {
	callLedAPI('setGroupFile', {
		type: type,
		str: groupStr
	});
}

function getGroupFile(type, callback) {
	callLedAPI('getGroupFile', {
		type: type
	}, 'config', callback);
}

function setAverageEdgeGain(slave, offset, callback) {
	callLedAPI('setAverageEdgeGain', {
		slave: slave,
		offset: offset
	}, 'setAverageEdgeGain', callback);
}

function setPixelCalibration(slave, bank, gain, callback) {
	callLedAPI('setPixelCalibration', {
		slave: slave,
		bank: bank,
		gain: gain
	}, 'setPixelCalibration', callback);
}

// color R:0, G:1, B:2, W:3
function setRandomPixelGain(slave, bank, defaultGain, from, to, color, range, callback) {
	callLedAPI('setRandomPixelGain', {
        slave: slave,
        bank: bank,
        defaultGain: defaultGain,
        from: from,
        to: to,
        color: color,
        range: range
	}, 'setRandomPixelGain', callback);
}

function searchAccountName(accountNumber, callback) {
	callLedAPI('searchAccountName', {
	   accountNumber: accountNumber,
   }, "signage365Care", callback);
}

function care365Enable(callback) {
   callLedAPI('care365Enable', {}, "signage365Care", callback)
}

function care365Disable(callback) {
   callLedAPI('care365Disable', {}, "signage365Care", callback);
}

function  appDownloadURL(serviceMode, callback) {
    callLedAPI('appDownloadURL', {
        serviceMode: serviceMode
    }, "signage365Care", callback);
}

function getServerStatusCare365(callback) {
   callLedAPI('getServerStatusCare365', {}, "signage365Care", callback);
}

function getServerStatusCare365Subscribe(callback) {
    callLedAPI('getServerStatusCare365Subscribe', {}, "signage365Care", callback);
}

function remove365CareApp(callback) {
   callLedAPI('remove365CareApp', {}, "signage365Care", callback);
}

function getListApps(callback) {
   callLedAPI("getListApps", {}, 'getListAppsInfo', callback);
}

function installIpkDownload(downloadUrl, callback) {
   callLedAPI('installIpkDownload', {
	   downloadUrl: downloadUrl
   }, 'signage365Care', callback);
}

function install365CareApp(signature, destFile, callback) {
   callLedAPI('install365CareApp', {
	   signature: signature,
	   destFile: destFile
   }, 'signage365Care', callback);
}

function getUseNetworkTime(callback) {
    callLedAPI("getUseNetworkTime", {}, "signage365Care", callback);
}

function setAutomaticallyTime(callback) {
    callLedAPI("setAutomaticallyTime", {}, "signage365Care", callback);
}

function get365CareServiceMode(callback) {
    callLedAPI("get365CareServiceMode", {}, "signage365Care", callback);
}

function isFirstBoot(callback) {
    callLedAPI("isFirstBoot", {}, "signage365Care", callback);
}
