document.addEventListener("DOMContentLoaded", function () {
  $("head").append(
    '<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w4/popup_network_ping.css">'
  );

  let state = getState("networkState");

  var myContainer = document.getElementById("ping-ing-popup");
  var myFocusTrap = focusTrap.createFocusTrap("#ping-ing-popup", {
    onActivate: function () {
      myContainer.classList.add("is-active");
    },
    onDeactivate: function () {
      myContainer.classList.remove("is-active");
    },
    escapeDeactivates: false,
  });

  $("#btn-ping-stop").on("click", function () {
    myFocusTrap.deactivate();
    _closePopup("ping-ing-popup");
    state.set("pingCancelled", true);
    $("#btn-ping-test").focus();    

    // showMyModal("ping-complete-popup");
  });

  $("#ping-ing-popup").on("cssClassChanged", function (e) {
    // e.stopPropagation();
    myFocusTrap.activate();
  });

  $(document).on("pingTestStart", onModalLoad);

  function onModalLoad() {
    startPing(state.get("ipAddressInput"));
  }

  function startPing(hostname) {
    let cnt = 0;
    let success = 0;
    let totalTime = 0;
    let min = Number.MAX_VALUE;
    let max = 0;
    let isIPv6 = checkIP(hostname, true);

    $("#pingResult").empty();
    function processPing(result) {
      ++cnt;
      if (result.bytes == undefined) {
        result.bytes = "000";
      }
      // $('#pingTitle').text(getLanguageText(languageTable, 'Pinging [X.X.X.X] with YYY bytes of data').replace('X.X.X.X', hostname).replace('YYY', result.bytes));
      if (result.returnValue) {
        ++success;
        totalTime += result.time;
        min = Math.min(min, result.time);
        max = Math.max(max, result.time);
        $("#pingResult").append(
          "<li" +
            (isRtlDir() ? " dir=rtl" : "") +
            ">" +
            getLanguageText(
              languageTable,
              "Reply from [X.X.X.X] : bytes = YY, time = ZZms, TTL = WW"
            )
              .replace("X.X.X.X", result.requestAddress)
              .replace("YY", result.bytes)
              .replace("ZZ", result.time)
              .replace("WW", result.ttl) +
            "</li>"
        );
      } else {
        var orig = result.errorText;
        var trans = getLanguageText(languageTable, orig);
        if (orig == trans) {
          trans = getLanguageText(languageTable, orig + ".");
        }
        $("#pingResult").append(
          "<li" + (isRtlDir() ? " dir=rtl" : "") + ">" + trans + "</li>"
        );
      }
      if (cnt != 10) {
        setTimeout(function () {
          if (!state.get("pingCancelled")) {
            detailPing(hostname, isIPv6, processPing);
          }
        }, 1000);
      } else {
        setTimeout(pingCompleted, 1000);
      }
    }

    function pingCompleted() {
        if (state.get("pingCancelled")) {
            return;
        }
      _closePopup("ping-ing-popup");
      showMyModal("ping-complete-popup");
      myFocusTrap.deactivate();

      if (success == 0) {
        $("#pingTimeBody").text(
          getLanguageText(
            languageTable,
            "Minimum = XXms, Maximum = YYms, Average = ZZms"
          )
            .replace("XX", "0")
            .replace("YY", "0")
            .replace("ZZ", "0.000")
        );
      } else {
        $("#pingTimeBody").text(
          getLanguageText(
            languageTable,
            "Minimum = XXms, Maximum = YYms, Average = ZZms"
          )
            .replace("XX", min)
            .replace("YY", max)
            .replace("ZZ", Math.round((totalTime / success) * 1000) / 1000)
        );
      }
      $("#pingStatTitle").text(
        getLanguageText(languageTable, "Ping statistics for [X.X.X.X]").replace(
          "X.X.X.X",
          hostname
        )
      );
      $("#pingStatBody").text(
        getLanguageText(
          languageTable,
          "Packets : Sent = XX. Received = YY. Lost = ZZ (WW% loss)"
        )
          .replace("XX", 10)
          .replace("YY", success)
          .replace("ZZ", 10 - success)
          .replace("WW", (10 - success) * 10)
      );
    }

    detailPing(hostname, isIPv6, processPing);
  }
});
