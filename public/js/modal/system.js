var systemDetailModal = {
	init: function() {
		var modal = this;
		$.get('/detail/system.html', function(data, success, type){
			addDetailModal('systemDetailModal', Locale.getText("System Information"), data);
			$('#goFirstUseBtn').click(function(){
				firstUseModal.modal();
				$('#systemDetailModal').modal('hide');
			});

			$('#goUpdateBtn').click(function(){
				updateModal.modal();
				$('#systemDetailModal').modal('hide');
			});
		});
	},

	modal: function() {
		systemDetailModal.getInformation();
		$('#systemDetailModal').modal();
	},

	getInformation: function() {
		getSignageName(function(ret) {
			$("#modalSignageName").html(ret);
		});

		getBasicInfo(function(msg) {
			$('#modalModelName').html(msg.modelName);
			$('#modalSerialNumber').html(msg.serialNumber);

			$('#modalFirmwareVersion').html(msg.firmwareVersion);
			$('#modalMicomVersion').html(msg.micomVersion);
			$('#modalBootLoaderVersion').html(msg.bootLoaderVersion);
		});

		getWebOSInfo(function(msg) {
			$('#modalWebosVersion').html(msg.core_os_release + " (" + msg.core_os_release_codename + ")");
		});

		if (env.supportLedFilm) {
			$('#fpgaVer').show();

			if(env.ledFilmType == 'color' ){
				$('#FPGAVersion').html('');
				for(var i=0; i<4; i++){
					getFPGAVersionText(i);
				}
			}else{
				getFpgaVersion(1, function(msg){
					$('#FPGAVersion').html(msg.result.strArray[0]);
				});
			}
		}
	}
};

function getFPGAVersionText(index){
	getFpgaVersionColor((index+1)*2, function(msg){
		console.log(msg.result);

		if (msg.result) {
			var result = Locale.getText("Sender") + (index+1) + '('+ msg.result.version1st + 'x' + msg.result.version2nd.toString(16) +') ';
			$('#FPGAVersion').append(result);
		}
	});
}