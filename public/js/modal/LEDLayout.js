var LedLayoutEditorModal = {
	layout: {
		selected: undefined,
		startPos: undefined,
		snapX: 16,
		snapY: 8,
		zIndex: 0,
		prevClickTime: 0,
		isDoubleClick: false,
	},

	config: {
		dragging: false,
		curUnit: undefined,
		rectList: [],
		scaleScreen: 702 / 1920,
		resolX: 384,
		resolY: 360,
		viewPort: {
			x: 0,
			y: 0,
			w: 5 * this.resolX,
			h: 3 * this.resolY
		},
		maxX: 0,
		maxY: 0,
		from: 0,
		direction: 0,
		canvas: undefined,
		ctx: undefined,
		img: new Image()
	},

	mouseDown: function(e) {
		var layout = LayoutEditorModal.layout;
		var config = LayoutEditorModal.config;

		clearSelection();

		var pos = getMousePos(e);
		var cur = new Date().getTime();
		var diff= cur - layout.prevClickTime;

		layout.prevClickTime = cur;
		layout.selected = undefined;

		if (diff < 300) {
			if (isInsideRect(pos, config.viewPort, config.unitScale)) {
				layout.selected = config.viewPort;
				layout.startPos = pos;
				config.dragging = true;			
				return;
			}
		}
	
		var rects = getSelectedRect(LayoutEditorModal.config, pos)
	
		if (rects.length > 0) {
			rects.sort(compareZ);
			layout.selected = rects[rects.length - 1];
			config.curUnit = layout.selected;
			layout.zIndex++;
			layout.selected.z = layout.zIndex;
			layout.startPos = pos;
			config.dragging = true;
		}
	},

	mouseUp: function(e) {
		var layout = LayoutEditorModal.layout;
		var config = LayoutEditorModal.config;

		refreshLed(config);

		if (!config.dragging || !layout.selected) {
			return
		}

		$('#frameX').val(config.viewPort.x);
		$('#frameY').val(config.viewPort.y);

		config.dragging = false;

		applyConfig(config);
	},

	mouseMove: function(e) {
		var layout = LayoutEditorModal.layout;
		var config = LayoutEditorModal.config;

		if (!config.dragging || !layout.selected) {
			return;
		}
	
		clearSelection();
		var pos = getMousePos(e);
		var prevX = layout.selected.x;
		var prevY = layout.selected.y;
		var diffX = Math.floor((pos.x - layout.startPos.x) / layout.snapX) * layout.snapX;
		var diffY = Math.floor((pos.y - layout.startPos.y) / layout.snapY) * layout.snapY;

		if (diffX != 0) {
			layout.selected.x += diffX;
			layout.startPos.x += diffX;
		}

		if (diffY != 0){
			layout.selected.y += diffY;
			layout.startPos.y += diffY;
		}

		if (diffX == 0 && diffY == 0) {
			return;
		}
	
		restrictRect(layout.selected, config.viewPort);
		restrictRect(config.viewPort, {x: 0, y: 0, w: outer.width, h: outer.height});

		if (layout.selected == config.viewPort) {
			diffX = config.viewPort.x - prevX;
			diffY = config.viewPort.y - prevY;
			config.rectList.forEach(function(rect) {
				rect.x += diffX;
				rect.y += diffY;
			});
		}

		draw(config);	
	},

	getNewPosition: function(newID) {
		var config = LayoutEditorModal.config;

		function backwardFirst(loc, size, resol) {
			return loc + Math.floor(size / resol) * resol- resol;
		}

		var from = config.from;
		var direction = config.direction;

		var startX = ([0, 2].indexOf(from) >= 0) ? config.viewPort.x : backwardFirst(config.viewPort.x, config.viewPort.w, config.resolX);
		var startY = ([0, 1].indexOf(from) >= 0) ? config.viewPort.y : backwardFirst(config.viewPort.y, config.viewPort.h, config.resolY);
	
		var diffX = ([0, 2].indexOf(from) >= 0) ? 1 : -1;
		var diffY = ([0, 1].indexOf(from) >= 0) ? 1 : -1;
	
	
		if (direction == 0) {		
			var dir = config.resolX * diffX * Math.pow(-1, Math.floor(newID / config.maxX));		
			startX = dir > 0 ? config.viewPort.x : backwardFirst(config.viewPort.x, config.viewPort.w, config.resolX);
			x = startX + (newID % config.maxX) * dir;
			y = startY + config.resolY * diffY * Math.floor(newID / config.maxX);		
		} else {
			var dir = config.resolY * diffY * Math.pow(-1, Math.floor(newID / config.maxY));	
			startY = dir > 0 ? config.viewPort.y : backwardFirst(config.viewPort.y, config.viewPort.h,config.resolY);
			x = startX + config.resolX * diffX * Math.floor(newID / config.maxY);
			y = startY + (newID % config.maxY) * dir;
		}

		return {x: x, y: y};
	},

	rearrange: function() {
		var modal = LayoutEditorModal;
		var config = modal.config;

		var numUnits = config.rectList.length;

		for (var i = 0; i != numUnits; ++i) {
			$('#removeUnitBtn').click();
		}

		for (var i = 0; i != numUnits; ++i) {
			$('#addUnitBtn').click();
		}
	},

	addEventHandlers: function() {
		$('#addUnitBtn').click(function() {
			var modal = LayoutEditorModal;
			var numUnits = modal.config.rectList.length;
			if (numUnits >= modal.config.maxX * modal.config.maxY) {
				return;
			}
			var pos = modal.getNewPosition(numUnits);
			var newRect = {
				x: pos.x,
				y: pos.y,
				z: modal.zIndex,
				w: modal.config.resolX,
				h: modal.config.resolY,
				id: modal.config.rectList.length + 1
			};

			modal.config.rectList.push(newRect);
			applyConfig(modal.config);
		});

		$('#removeUnitBtn').click(function() {
			var config = LayoutEditorModal.config;

			if (config.rectList.length <= 0) {
				return;
			}

			var index = config.rectList.indexOf(config.curUnit);
			if (index < 0) {
				index = config.rectList.length - 1;
				config.curUnit = config.rectList[index];
			}

			config.rectList.splice(index, 1);
			config.rectList.forEach(function(rect) {
				if (rect.id > config.curUnit.id) {
					rect.id--;
				}
			});
			config.curUnit = config.rectList.length == 0 ? undefined : config.rectList[config.rectList.length - 1];
			applyConfig(config);
		});

		$('#rearrBtn').click(function() {
			LayoutEditorModal.rearrange();
		});
	},

	init: function() {
		$.get('/detail/ledLayoutEditor.html', function(data, success, type){
			var modal = LayoutEditorModal;
			addDetailModal('layoutEditorModal', 'Layout Editor', data);
			modal.config.canvas = document.getElementById("layoutEditorCanvas");
			modal.config.ctx = modal.config.canvas.getContext("2d");

			modal.config.canvas.width= 1920 * modal.config.scaleScreen;
			modal.config.canvas.height = 1080 * modal.config.scaleScreen;
			modal.config.ctx.scale(modal.config.scaleScreen, modal.config.scaleScreen);

			drawLayoutSelect();
			modal.addEventHandlers();
	
			$('#layoutEditorModal').on('hidden.bs.modal', function () {
				modal.config.canvas.removeEventListener('mousedown', modal.mouseDown );
				//document.removeEventListener("keydown", keydown, false);
				document.removeEventListener("mouseup", modal.mouseUp);
				document.removeEventListener("mousemove", modal.mouseMove);
				getOverallLedStatus();
			})
		});
	},

	modal: function() {
		var modal = LayoutEditorModal;
		var config = modal.config;
		$('#layoutEditorModal').modal();
		clearInterval(overallStatusTimer);
		overallStatusTimer = undefined;
		getLedConfig(function(ret) {
			if(!env.supportLedFilm){
				setDropButtonText('daisyFromSelect', '' + ret.from, true);
				setDropButtonText('daisyDirSelect', '' + ret.direction, true);
			}

			initLed(5, config);
			drawFromConfig(ret, config);

			config.resolX = ret.resolution.w;
			config.resolY = ret.resolution.h;
			config.maxX = ret.viewPort.w / ret.resolution.w;
			config.maxY = ret.viewPort.h / ret.resolution.h;

			drawRowColSelect(config);
			setDropButtonText('resolSelect', config.resolX + 'x' + config.resolY, true);
			$('#frameX').val(config.viewPort.x);
			$('#frameY').val(config.viewPort.y);
		});
		modal.config.canvas.addEventListener('mousedown', modal.mouseDown, false);
		document.addEventListener("mouseup", modal.mouseUp, false);
		document.addEventListener("mousemove", modal.mouseMove, false);
	}
};