var unitCalirationModal = {
	prevVal : [],
	currUnitID : 0,
	init: function() {
		$.get('/detail/unitCalibration.html', function(data, success, type){
			var modalPosition = {
				top : 250,
				left: 420,
				w: 450,
				h: 240
			};
			addCalibModal('unitCalirationModal', 'Unit Calibration', data, modalPosition);
		});
	},

	modal: function() {
		var modal = this;
		modal.drawCalibUnitSelect(curUnit);
		$('#unitCalirationModal').modal();

		checkRangeUI('unitCalibRange', 0, 5);

		modal.currUnitID = curUnit.id;
		modal.getGlobalBC(modal.currUnitID);

		$('#unitCalibResetBtn').click(function(){
			$('#unitCalibRange').val(4);
			checkRangeUI('unitCalibRange', 0, 5);
			$('#unitCalibRangeVal').html('4');
			setGlobalBC(curUnit.id*2, 4);
		});

		$('#unitCalibRange').change(function() {
			var val = $(this).val();
			$('#unitCalibRangeVal').html(val);
			if( $('#applyAllUnit').prop('checked') ){
				mainCanvas.rectList.forEach(function(rect) {
					setGlobalBC(rect.id*2, val);
				});
			}else{
				setGlobalBC(curUnit.id*2, val);
			}
		}).on('input', function() {
			checkRangeUI('unitCalibRange', 0, 5);
		});

		$('#unitCalibCancel').unbind('click').click(function(){
			modal.prevVal.forEach(function(prevVal, index){
				setGlobalBC( ((index+1)*2), prevVal);
			});
			$('#unitCalirationModal').modal('hide');
		});
	},

	drawCalibUnitSelect: function(curUnit){
		var modal = this;
		var id = [];
		var val = [];
		modal.prevVal = [];
		mainCanvas.rectList.forEach(function(rect) {
			id.push(rect.id);
			val.push('Unit'+rect.id);

			getGlobalBC(rect.id*2, function(ret){
				modal.prevVal.push(ret ? ret.bc : -1);
			});
		});

		$('#calibUnitSelect').empty();

		drawDropDownBox('calibUnitSelect', id, val, 'dropboxType3', function (selected) {
			modal.getGlobalBC(selected);
		}, curUnit.id);
	},

	getGlobalBC: function(unitID){
		getGlobalBC(unitID*2,function(ret){
			if(ret){
				bcVal = ret.bc;
				$('#unitCalibRange').val(ret.bc);
				$('#unitCalibRangeVal').html(ret.bc);
				checkRangeUI('unitCalibRange', 0, 5);
			}
		});
	}
};


var moduleCalibOrigin = [];
var moduleCalirationModal = {
	init: function() {
		/*$.get('/detail/moduleCalibration.html', function(data, success, type){
			var modalPosition = {
				top : 250,
				left: 320,
				w: 1000,
				h: 500
			};
			addCalibModal('moduleCalirationModal', 'Module Calibration', data, modalPosition);
		});*/
	},

	modal: function() {
		console.log('mono?')
		var modal = this;
		var moduleCount = (curUnit.w/16) * (curUnit.h/16);
		var calibTable = '';

		for(var i=0; i<curUnit.h/16; i++){
			calibTable += '<tr><th class="calibTH">Module</th>';
			for(var j=0; j<curUnit.w/16; j++){
				calibTable += '<th class="calibTH">'+(i*(curUnit.w/16)+j+1)+'</th>';
			}
			calibTable += '</tr><tr><td style="border-right:1px solid black; text-align:center;">Brightness</td>';

			for(var j=0; j<curUnit.w/16; j++){
				calibTable += '<td><table style="width:90%; margin-left:10px;"><tr>';
				calibTable += '<td><div style="width:245px;"><input class="custom" id="moduleCalibRange'+(i*(curUnit.w/16)+j+1)+'" type=range min=0 max=15 name="'+i+','+j+'" style="margin-top: 0px;"></div><td>';
				calibTable += '<td><div id="moduleCalibRange'+(i*(curUnit.w/16)+j+1)+'Val" style="margin-left:10px; width:54px;">15</div></td></tr></table></td>';
			}
			calibTable += '</tr>';
		}
		$('#moduleCalibTable').html(calibTable);

		for(var i=0; i<moduleCount; i++){
			checkRangeUI('moduleCalibRange'+(i+1), 0, 15);
		}

		moduleCalibOrigin = [];

		var moduleIndex = 0;
		for(var i=0; i<curUnit.h/16; i++){
			for(var j=0; j<curUnit.w/16; j++){
				getModuleCalibration(curUnit.id*2, i, j, function(msg){
					if(msg){
						moduleIndex++;
						$('#moduleCalibRange'+moduleIndex).val(msg.value);
						checkRangeUI('moduleCalibRange'+moduleIndex, 0, 15);
						$('#moduleCalibRange'+moduleIndex+'Val').html(msg.value);
						moduleCalibOrigin.push(msg.value);
					}
				});
			}
		}

		$('[id^=moduleCalibRange]').change(function(){

			var moduleLoc = $(this).attr('name').split(',');

			$('#'+$(this).attr('id')+'Val').html($(this).val());
			setModuleCalibration(curUnit.id*2, moduleLoc[0], moduleLoc[1], $(this).val());
		}).on('input', function() {
			checkRangeUI($(this).attr('id'), 0, 15);
		});

		$('#moduleCalibOK').click(function(){
			saveModuleCalibration(curUnit.id*2);
			$('#moduleCalirationModal').modal('hide');
		});

		$('#moduleCalibReset').click(function(){
			var moduleIndex = 0;
			for(var i=0; i<curUnit.h/16; i++){
				for(var j=0; j<curUnit.w/16; j++){
					moduleIndex++;
					setModuleCalibration(curUnit.id*2, i, j, 15);
					$('#moduleCalibRange'+moduleIndex).val(15);
					checkRangeUI('moduleCalibRange'+moduleIndex, 0, 15);
					$('#moduleCalibRange'+moduleIndex+'Val').html(15);
				}
			}
		});

		$('#moduleCalibCancel').click(function(){
			for(var i=0; i<curUnit.h/16; i++){
				for(var j=0; j<curUnit.w/16; j++){
					setModuleCalibration(curUnit.id*2, i, j, moduleCalibOrigin[i+j]);
					saveModuleCalibration(curUnit.id*2);
				}
			}
		});

		$('#moduleCalirationModal').modal();
	}
}
