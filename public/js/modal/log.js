var index = 0;
var order = false; // 0 descending / 1 ascending
var sortOrder = [false,false,false,false];


function sortFunction(a, b) {
	var v1 = a.split(',')[index];
	var v2 = b.split(',')[index];

    if (v1 === v2) {
        return 0;
    }
    else {
		if(order)
		return (v1 < v2) ? -1 : 1;
		else
		return (v1 > v2) ? -1 : 1;
    }
}



function sortArray(ord, idx){ // lower

	index = idx;
	order = ord;
	sepLogString.sort(sortFunction);

}



function printLog(rowPerPage){
	var logTable = '<table class="table table-striped" style="width:100%;margin-top:-10px;">';


	for(var i=currLogPage*rowPerPage; i<(currLogPage+1)*rowPerPage; i++){
		if( i == logRowCount )
		break;
		
		if( sepLogString[i].length == 0 )
		break
		
		logTable += '<tr>'
		logTable += '<tr>';
		
		logTable += '<td style="width:25%;">' + sepLogString[i].replace(/Please, Set a time/g, "Please Set a time").split(',')[4].trim() + '</td>'
		logTable += '<td style="width:25%;">' + sepLogString[i].split(',')[0].trim() + '</td>'
		logTable += '<td style="width:25%;">' + sepLogString[i].split(',')[1].trim() + '</td>'
		logTable += '<td style="width:25%;">' + sepLogString[i].split(',')[3].trim() + '</td>'

		logTable += '</tr>';
		
		logTable += '</tr>'
	}

	logTable += '</table>'

	$('#logpre').empty();
	$('#logpre').append(logTable);
}

function pagingLog(){
	var rowPerPage = Number( $('#logPerPageDrop').text() );
	var currPageText = (currLogPage*rowPerPage + 1)+'-'+(currLogPage*rowPerPage + rowPerPage);
	var lastPage = parseInt(logRowCount/rowPerPage);
	var pageTxt = Locale.getText('XX of YY');

	$('#logDetailModal #prevPageBtn').prop('disabled', false);
	$('#logDetailModal #nextPageBtn').prop('disabled', false);
	if(currLogPage <= 0){
		$('#logDetailModal #prevPageBtn').prop('disabled', true);
	}
	if(currLogPage >= lastPage){
		$('#logDetailModal #nextPageBtn').prop('disabled', true);
		currPageText = (currLogPage*rowPerPage + 1)+'-'+logRowCount;
	}

	pageTxt = pageTxt.replace('XX', currPageText);
	pageTxt = pageTxt.replace('YY', logRowCount);
	$('#currentLogPage').text(pageTxt);

	printLog(rowPerPage);
}

function filteringLog(filter){
	if(filter == 'ALL'){
		sepLogString = log.split('\n');
		sepLogString.splice(sepLogString.length-1, 1);
		sepLogString = sepLogString.reverse();
		logRowCount = sepLogString.length;
	}else{
		var tmpLog = [];
		log.split('\n').forEach(function(item){
			if(item.indexOf(filter) > 0)
				tmpLog.push(item);
		});

		sepLogString = tmpLog;
		sepLogString = sepLogString.reverse();
		logRowCount = sepLogString.length;
	}

	pagingLog();
}

function downloadFile(fileName, urlData, logForDown) {
	if (navigator.msSaveBlob){
		var blob = new Blob([logForDown], { type: 'text/csv;charset=utf-8;' });
		navigator.msSaveBlob(blob, fileName);
	}else{
		var aLink = document.createElement('a');
	    aLink.download = fileName;
	    aLink.href = urlData;
	    document.body.appendChild(aLink);
	    var event = new MouseEvent('click');
	    aLink.dispatchEvent(event);
	}
}

var logDetailModal = {
	init: function() {
		var modal = this;
		$.get('/detail/log.html', function(data, success, type){
			addDetailModal('logDetailModal', Locale.getText("Log Report"), data);
			modal.drawLogFilter();
			modal.drawLogPerPage();
			sepLogString = log.split('\n');
			sepLogString.splice(sepLogString.length-1, 1);
			logRowCount = sepLogString.length;
		});
	},

	modal: function() {
		$('#logDetailModal').modal();
		$('#logDetailModal #modalActionBtn').show();
		$('#logDetailModal #modalActionBtn').unbind().click(function(){
			var logForDown = 'Date,	Sensor,	Event,  State' + '\n';

			sepLogString.forEach(function(data){
				data = data.replace(/^ */g, "").replace(/\n[ ]+/g, "\n").replace(/\t/g, " ").replace(/ {2,}/g, " ");
				data = data.split(',')[4]+','+data.split(',')[0]+','+data.split(',')[1]+','+data.split(',')[3];
				logForDown += data.replace(/Please, Set a time/g, "Please Set a time") + '\n'
			});
			
			logForDown.replace(/Please, Set a time/g, "Please Set a time");
			downloadFile('log.csv', 'data:text/csv;charset=UTF-8,' + encodeURIComponent(logForDown), logForDown);
		});
		$('#logDetailModal #modalactionName').text(Locale.getText("Export"));
		$('#logDetailModal #prevPageBtn').unbind().click(function(){
			currLogPage--;
			pagingLog();
		});

		$('#logDetailModal #nextPageBtn').unbind().click(function(){
			currLogPage++;
			pagingLog();
		});
		currLogPage = 0;
		pagingLog();
	},

	drawLogFilter: function(){
		$('#logFilter').empty();
		drawDropDownBox('logFilter', ['ALL', 'TIME', 'SIGNAL', 'LEDUNIT', 'TEMPERATURE'], ['ALL', 'TIME', 'SIGNAL', 'LEDUNIT', 'TEMPERATURE'], 'dropboxType3', function (selected) {
			currLogPage = 0;
			filteringLog(selected);
		}, 'ALL' );
	},

	drawLogPerPage: function(){
		$('#logPerPage').empty();
		drawDropDownBox('logPerPage', ['10', '30', '50'], ['10', '30', '50'], 'dropboxType3', function (selected) {
			currLogPage = 0;
			pagingLog();
		}, '30' );
	}
};