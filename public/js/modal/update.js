function uploadFile(file) {
	var formData = new FormData();
	formData.append('update', file);
	formData.append('target', 'update');

	$('#upload-prog').show();

	overlayControl20(true, Locale.getText("Uploading") + "<br>" + file.name, 'updateModal');
	document.onkeydown = function(e) {
		return false;
	}

	$('input').prop('disabled', true);
	$('button').prop('disabled', true);

	var xhr = new XMLHttpRequest();

	xhr.open('post', '/upload', true);

	xhr.upload.onprogress = function(e) {
		if (e.lengthComputable) {
			var per = Math.round((e.loaded / e.total) * 100)
			$('#uploadProgress').html(per + "%");
			$('#uploadProgress').css('width', per + '%');
			$('#overlay_progress').html(per + "%");
		}
	};

	xhr.upload.onload = function(e) {
		setTimeout(function() {
			overlayControl20(false);
			$('#updateModal').modal('hide');
			window.location.href = "ledDashboard#updateModal";
			window.location.reload();
		}, 2000);
	}

	xhr.onerror = function(e) {
		overlayControl20(false);
		showWarning(Locale.getText("An error occurred while submitting the form."), 5 * 1000);
	};

	xhr.onload = function() {
		overlayControl20(false);
	};

	xhr.send(formData);
}

var updateStarted = false;
var updatePercent = 0;
var updateModal = {
	init: function() {
		var modal = this;
		$.get('/detail/update.html', function(data, success, type){
			addDetailModal('updateModal', Locale.getText('Update'), data);

			$(document).on('change', '#upload', function() {
				var fileName = $("#upload").val();

				if( fileName.indexOf('epk') > 0 ){
					$('#fileName').html($("#upload").val());
				}else{
					showWarning(Locale.getText('Unsupported File type.'), 5 * 1000);
					$("#upload").val("");
				}
			});

			$(document).on('change', '#FPGAupload', function() {
				var fileName = $("#FPGAupload").val();

				if( env.ledFilmType == 'mono' ){
					if( fileName.indexOf('hexout') > 0  ){
						$('#FPGAfileName').html($("#FPGAupload").val());
					}else{
						showWarning(Locale.getText('Unsupported File type.'), 5 * 1000);
						$("#FPGAupload").val("");
					}
				}else{
					if( fileName.indexOf('img') > 0  ){
						$('#FPGAfileName').html($("#FPGAupload").val());
					}else{
						showWarning(Locale.getText('Unsupported File type.'), 5 * 1000);
						$("#FPGAupload").val("");
					}
				}
			});

			$('input[type="submit"]').on('click', function(evt) {
				evt.preventDefault();

				var file = document.getElementById('upload').files[0];

				if( $(this).attr('name') == 'FPGA' ){
					file = document.getElementById('FPGAupload').files[0];
				}

				if (!file) {
					showWarning(Locale.getText('Please choose a file to upload'), 5 * 1000);
					return;
				}

				var fileName = file.name;
				var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
				var supportExt = ['epk', 'hexout', 'img'];

				if (supportExt.indexOf(ext) < 0) {
					showWarning(Locale.getText('Unsupported File type.'), 5 * 1000);
					return;
				}

				if (file.size > env.maxSize) {
					showWarning(Locale.getText('Flash memory is not enough.'), 5 * 1000);
					return;
				}

				if (env.updateFiles.indexOf(fileName) >= 0) {
					addConfirmModal('overwrite', Locale.getText('Overwrite?'), fileName, function() {
						setTimeout(function() {
							uploadFile(file);
						}, 500);
						$('#overwrite').close();
						$('#overwrite').remove();
					});
					$('#overwrite').modal();
					return;
				}

				uploadFile(file);
			});

			env.updateFiles.forEach(function(updateFile){
				var fileHTML = '<tr class="file-list">'
				+ '<td><p class="file-info">'+updateFile+'</p></td>'
				+ '<td style="width:10%"><button class="whiteCardBtn update" name ="'+updateFile+'" >'+Locale.getText('Update')+'</button></td>'
				+ '<td style="width:10%"><button class="whiteCardBtn delete" name ="'+updateFile+'" >'+Locale.getText('Delete')+'</button></td></tr>';
				if(updateFile.indexOf('epk') > 0){
					$('#firmAnounce').hide();
					$('#updateFileTable').append(fileHTML);
				}else{
					$('#FPGAAnounce').hide();
					$('#FPGAFileTable').append(fileHTML);
				}
			});

			$('.update').click(function(){
				var fileName = $(this).attr('name');
				if(fileName.indexOf('.epk') > 0){

					overlayControl20(true, Locale.getText("S/W Updating......"), 'updateModal');
					swupdate(fileName, false);
					setTimeout(function() {
						getUpdateStatus(function(msg) {

							if (msg.status != 'in progress' && !updateStarted) {
								overlayControl(false);
								showWarning(Locale.getText('Cannot start S/W update. Check xxx file please.').replace('xxx', fileName));
							}
						});
					}, 10 * 1000);

				}else if(fileName.indexOf('.epk') > 0){

					overlayControl20(true, Locale.getText("LED FPGA Updating ......"), 'updateModal');
					ledFpgaUpdate(fileName, function(msg) {
						setTimeout(function() {
							var fpgaProgressTimer = setInterval(function() {
								getFpgaUpdatePercent(function(ret) {
									getFPGAUpdateErrorStatus(function(retErrorStatus){

										if( !(ret.returnValue && retErrorStatus.returnValue) ){
											clearInterval(fpgaProgressTimer);
											overlayControl20(false);
										}

										percent = ret.Percent;

										if( percent == undefined ){
											overlayControl20(false);
										}else{
											$('#overlay_progress').html(percent + "%");
										}
									});
								});
							}, 2 * 1000);
						}, 11 * 1000);
					});
				}else{
					overlaySenderUpdate(true, 'updateModal');
					$('.overlayBtn').click(function(){
						if( $(this).prop('id').length == 0 ){
							var slave = Number( $(this).attr('name') ) * 2;
							updateSender(slave, fileName);
							$('#overlayBtnTable').hide();
							$('#updateImage').show();
						}
					});
				}
			});

			$('.delete').click(function(){
				var fileName = $(this).attr('name');
				deleteFile("update", fileName, function() {
					$('#updateModal').modal('hide');
					window.location.href = "ledDashboard#updateModal";
					window.location.reload();
				});
			});

			socket.on('swupdate', function(msg) {

				updateStarted = true;
				if (updatePercent > 0 && msg == 0) {
					overlayControl20(false);
					showWarning(Locale.getText("Fail to update. Check xxx file please.").replace('xxx', fileName));
					return;
				}

				updatePercent = msg;
				$('#overlay_progress').html(msg + "%");
				if (msg == 100) {
					overlayControl20(false);
					overlayControl20(true, Locale.getText("Update completed. Set is rebooting now."), true);
				}

			});

			if( window.location.href.indexOf('#') > -1 ){
				updateModal.modal();
			}
		});
	},

	modal: function() {
		$('#updateModal').modal();
	}
};

function FPGADone(sender){
	getFpgaVersionColor(sender, function(){
	});
}

function updateSender(slave, fileName){
	moveUpdateFile(fileName);

	getNetworkStatus(function(msg) {
		var connection = msg.wired;
		var ip = connection.ipAddress.split('.');
		var tftp = '';

		setIP( slave, ip[0], ip[1], ip[2], (50+slave) , ip,  function(ret){
			console.log('setIP::');
			console.log(ret);
		});

		execTFTP(function(excuteMsg){
			console.log(excuteMsg);
			tftp = excuteMsg+1;
		});

		setFpgaControl(slave, 1 , 1, function(triggerMsg){
			console.log(triggerMsg);
		});

		var interval = setInterval(function(){
			getUpgradeStatus(slave, function(upgradeRet){
				console.log('update Status'+upgradeRet.result.value);
				if(upgradeRet.result.value == 10){
					clearInterval(interval);

					var overlayInterval = setInterval(function(){
						getFpgaVersionColor(slave, function(ver){
							console.log('version :: '+ver.result.version2nd);
							if(ver.result.version2nd > 0){
								$('#overlay_msg').html(Locale.getText("Success"));
								setTimeout(function(){
									overlaySenderUpdate(false);
								}, 5000);
								clearInterval(overlayInterval);
								killTFTP(tftp);
								$('.overlayBtn').unbind('click');
							}
						});
					}, 1000);

				}else if(upgradeRet.result.value == 128){
					$('#overlay_msg').html(Locale.getText("Fail"));
					setTimeout(function(){
						overlaySenderUpdate(false);
					}, 5000);
					clearInterval(interval);
					killTFTP(tftp);
					$('.overlayBtn').unbind('click');
				}
			});
		}, 1000);
	});
}