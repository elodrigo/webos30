function compareFirstUse(){
	var modified = JSON.stringify(originalVal)!=JSON.stringify(selectVal);
	$('#firstUseModal #modalActionBtn').prop('disabled', !modified);
}

function saveFirstUse(){
	setSystemSettings('commercial', {'ledLayout' : {'pixelPitch' : selectVal.pixelPitch}});
	setSignageName(selectVal.signageName);
	setHdmiInput(selectVal.input);
	setContinent(selectVal.continent, function(msg){
		setCountry(selectVal.country,function(msg){
			getCityList(selectVal.country, function(msg){
				var timeZone = undefined;

				msg.forEach(function(item) {
					if(!isCityState){
						if( item.City == selectVal.city ){
							timeZone = item;
						}
					}else{
						if( item.Country == selectVal.city ){
							timeZone = item;
						}
					}
				});
				setCity(timeZone, function(msg){
				});
				saveFirstUseFile(selectVal);
				if(selectVal.language != originalVal.language){
					setMenuLanguage(selectVal.language);
				}else{
					$('#firstUseModal').modal('hide');
					location.reload();
				}
				originalVal = JSON.parse(JSON.stringify(selectVal));
			});
		});
	});

	if( originalVal.ledFilmType != selectVal.ledFilmType ){
		overlayControl20(true, Locale.getText('Reboot') + '......');
		setSystemSettings('commercial', {'ledFilmType' : selectVal.ledFilmType});

		rebootScreen();

		setTimeout(function(){
		   window.location.replace("/login.html");
		}, 60 * 1000);
	}
}

var firstUseModal = {

	init: function() {
		var modal = this;
		$.get('/detail/firstUse.html', function(data, success, type){
			addDetailModal('firstUseModal', Locale.getText('First Use'), data);
		});
	},

	modal: function() {
		firstUseModal.getFirstUseInfo(firstUseModal);
		$('#firstUseModal').modal();
		$('#firstUseModal #modalActionBtn').show();
		firstUseDone(function(done){
			$('#firstUseModal #modalActionBtn').prop('disabled', done);
		});
		$('#firstUseModal #modalActionBtn').unbind().click(function(){
			saveFirstUse();
		});

		$('#firstUseModal #modalactionName').text(Locale.getText('Save'));
	},

	getFirstUseInfo: function(modal){
		getSignageName(function(ret) {
			$("#signageName").val(ret);
			originalVal.signageName = ret;
			$("#signageName").keyup(function(){
				selectVal.signageName = $(this).val();
				compareFirstUse();
			});
		});

		getMenuLanguage(function(currLanguage){
			var langParse = currLanguage.split('-');
			var langCode = langParse[0];
			if (langCode == 'pt') {
				langCode = currLanguage;
			}
			langCode += (langParse.length == 2) ? '' : '-' + langParse[1];

			originalVal.language = langCode;
			modal.drawLanguageSelect();
		});

		getHdmiInput(function(currentInput){
			originalVal.input = currentInput;
			modal.drawInputSelect();
		});

		getlocaleContinent(function(ret){
			originalVal.continent = ret.localeContinent;
			getlocaleCountry(function(ret){
				originalVal.country = ret.localeCountry;
				getTimeZone(function(timeZone){
					originalVal.timeZone = timeZone;
					selectVal = JSON.parse(JSON.stringify(originalVal));
					modal.drawContinentSelect(modal);
				});
			});
		});

		getSystemSettings('commercial', ['ledFilmType'], function(ret){
			originalVal.ledFilmType = ret.ledFilmType;
			modal.drawLedTypeSelect(modal);
		});

		getSystemSettings('commercial', ['ledLayout'], function (ret) {
			originalVal.pixelPitch = ret.ledLayout.pixelPitch;
			$('#ledPitchSelect').empty();
			var pitchList = [14, 24];
			var pitchNameList = ["14mm", "24mm"];
			drawDropDownBox('ledPitchSelect', pitchList, pitchNameList, 'dropboxType2', function (selected) {
				selectVal.pixelPitch = selected;
				if (originalVal.pixelPitch !== Number(selected)) {
					var pitchChangeModal = addTwoButtonModal('pitchChangeModal', 'If the pitch is changed, all of the configuration settings are initialized.', 'OK', '');
					$('body').append(pitchChangeModal);
					$('#pitchChangeModalOK').remove(); // Delete unused buttons
					$('#pitchChangeModal').modal({
						backdrop: 'static',
						keyboard: false,
						show: true
					});
				}
				compareFirstUse();
			}, originalVal.pixelPitch);
			$('#ledPitchSelectDrop').attr('disabled', true);
		});
	},

	drawLanguageSelect: function(){
		$.getJSON('/resources/languageTable.json', function(table) {
			var languageList = [];
			var codeList = [];
			table.languages.forEach(function(lang) {
				if( (lang.languageCode == 'ar') && (env.addLanguage != 'ar') ){
					return;
				}
				languageList.push(lang.name);
				codeList.push(lang.languageCode);
			});

			$('#languageSelect').empty();

			drawDropDownBox('languageSelect', codeList, languageList, 'dropboxType2', function (selected) {
				selectVal.language = selected;
				compareFirstUse();
			}, originalVal.language);

		})
	},

	drawInputSelect: function(){
		getInputList(function(msg){
			var inputNameList = [];
			var inputIDList = [];
			msg.forEach(function(input){
				inputNameList.push(input.name);
				inputIDList.push(input.id);
			});

			$('#inputSelect').empty();

			drawDropDownBox('inputSelect', inputIDList, inputNameList, 'dropboxType2', function (selected) {
				selectVal.input = selected;
				compareFirstUse();
			}, originalVal.input);

		});
	},

	drawLedTypeSelect: function(){
		var filmTypeList = [1, 2];
		var filmNameList = [Locale.getText('MONO').toLowerCase(), Locale.getText('color')];
		$('#ledTypeSelect').empty();
		drawDropDownBox('ledTypeSelect', filmTypeList, filmNameList, 'dropboxType2', function (selected) {
			var originType = originalVal.ledFilmType  == 'color' ? 2 : 1;
			if( originType != selected){
				addFilmTypeModal('ledTypeModal', Locale.getText('warning'), 'The browser should be restarted to reflect the selected transparent LED type. Continue?', function() {
					selectVal.ledFilmType = (selected == 2 ? 'color' : 'mono');
					compareFirstUse();
				}, function(){
					setDropButtonText('ledTypeSelect', originType, true);
					selectVal.ledFilmType = (originType == 2 ? 'color' : 'mono');
					compareFirstUse();
				});
				$('#ledTypeModal').modal();
			}else{
				selectVal.ledFilmType = (selected == 2 ? 'color' : 'mono');
				compareFirstUse();
			}
		}, (originalVal.ledFilmType == 'color' ? 2 : 1) );
	},

	drawContinentSelect: function(modal){
		modal.drawCountrySelect(modal);
		$('#continentSelect').empty();
		drawDropDownBox('continentSelect', continentList, continentNameList, 'dropboxType2', function (selected) {
			selectVal.continent = selected;
			$('#firstUseModal #modalActionBtn').prop('disabled', true);
			modal.drawCountrySelect(modal, selected);
		}, originalVal.continent);
	},

	drawCountrySelect: function(modal, continent){
		if(continent){
			$('#citySelect').empty();
		}else{
			modal.drawCitySelect();
		}

		getCountryList(continent ? continent : originalVal.continent, function(msg){
			var shortNameList = [];
			var fullNameList = [];
			msg.forEach(function(ret) {
				shortNameList.push(ret.shortName);
				fullNameList.push(ret.fullName);
			});

			$('#countrySelect').empty();
			drawDropDownBox('countrySelect', shortNameList, fullNameList, 'dropboxType2', function (selected) {
				selectVal.country = selected;
				modal.drawCitySelect(selected);
			}, continent ? undefined : originalVal.country);
		});
	},

	drawCitySelect: function(country){
		getCityList(country ? country : originalVal.country, function(msg){
			var cityList = [];

			msg.forEach(function(item) {
				if( item.City == ''){
					cityList.push(item.Country);
					isCityState = true;
				}else{
					cityList.push(item.City);
				}
			});

			$('#citySelect').empty();
			drawDropDownBox('citySelect', cityList, cityList, 'dropboxType2', function (selected) {
				selectVal.city = selected;
				compareFirstUse();
			}, isCityState ? cityList[0] : originalVal.timeZone.City );
		});
	}
};