document.addEventListener("DOMContentLoaded", function () {
  var myContainer = document.getElementById("ping-address-popup");
  var myFocusTrap = focusTrap.createFocusTrap("#ping-address-popup", {
    onActivate: function () {
      myContainer.classList.add("is-active");
    },
    onDeactivate: function () {
      myContainer.classList.remove("is-active");
    },
    escapeDeactivates: false,
  });

  //   initialize selectors
  var label = $("#ip-address-label");
  const addressInputBox = "#modal-ip-address";
  const beginTestButton = "#btn-test-start";
  const IPCheckBox = "#IP";
  const URLCheckBox = "#URL";
  const startPingTestEvent = new Event("pingTestStart");

  //   initialize state
  initializeNewState("networkState", {
    ipAddressInput: "",
    pingCancelled: false,
  });
  let state = getState("networkState");

  //   set initial values
  $(IPCheckBox).prop("checked", true);

  function enableBeginTestButton() {
    !!state.get("ipAddressInput")
      ? $(beginTestButton).removeClass("disabled")
      : $(beginTestButton).addClass("disabled");
  }

  function resetInputBox() {
    $(addressInputBox).val("");
    state.set("ipAddressInput", "");
    enableBeginTestButton();
  }

  $(addressInputBox).on("input", function (e) {
    //   set value to state and check to enable begin test button
    let inputValue = e.target.value;
    state.set("ipAddressInput", inputValue);
    enableBeginTestButton();
  });

  $(IPCheckBox).on("change", function () {
    var isChecked = $(this).prop("checked");
    if (isChecked) {
      label.text(getLanguageText(languageTable, "IP Address"));
      resetInputBox();
    }
  });

  $(URLCheckBox).on("change", function () {
    var isChecked = $(this).prop("checked");
    if (isChecked) {
      label.text("URL");
      resetInputBox();
    }
  });

  $("#ip-address-auto").on("click", function () {
    var isChecked = $(this).prop("checked");
    if (isChecked) {
      myFocusTrap.deactivate();
      _closePopup("ping-address-popup");
      showMyModal("auto-modal");
    }
  });

  $(beginTestButton).on("click", startPing);

  $("#btn-test-cancel").on("click", function () {
    myFocusTrap.deactivate();
    _closePopup("ping-address-popup");
    $("#btn-ping-test").focus();    
  });

  $("#ping-address-popup").on("cssClassChanged", function (e) {
    // e.stopPropagation();
    myFocusTrap.activate();
  });

  function startPing() {
    //   reset cancel value
    state.set("pingCancelled", false);

    const hostname = state.get("ipAddressInput");
    const isIPv6 = checkIP(hostname, true);
    const isIPv4 = checkIP(hostname, false);
    const type = label.text() === "URL" ? "url" : "ip";
    if (env.supportCinemaModel) {
      if (
        (type == "ip" && !isIPv6 && !isIPv4) ||
        (type == "url" && (isIPv4 || isIPv6)) ||
        hostname.length == 0
      ) {
        alert(
          getLanguageText(
            languageTable,
            "Ping test failed. Invalid format of network address field."
          )
        );
        resetInputBox();
        return;
      }
    } else {
      if (type == "ip") {
        if (!isIPv6 && !isIPv4) {
          alert(
            getLanguageText(languageTable, "Error") +
              "\n" +
              getLanguageText(languageTable, "IP Address")
          );
          resetInputBox();
          return;
        }
      }
      if (type == "url") {
        if (isIPv4 || isIPv6) {
          alert(
            getLanguageText(languageTable, "Error") +
              "\n" +
              getLanguageText(languageTable, "URL")
          );
          resetInputBox();
          return;
        }
      }
      if (hostname.length == 0) {
        alert(
          getLanguageText(languageTable, "Error") +
            "\n" +
            getLanguageText(languageTable, "URL")
        );
        resetInputBox();
        return;
      }
    }

    $("#pingAdressTitle").text(
      getLanguageText(languageTable, "Ping statistics for [X.X.X.X]").replace(
        "[X.X.X.X]",
        hostname
      )
    );

    myFocusTrap.deactivate();
    _closePopup("ping-address-popup");
    showMyModal("ping-ing-popup");
    document.dispatchEvent(startPingTestEvent);
  }
});

function _testStartClick() {
  _closePopup("ping-address-popup");
  togglePopup("btn-test-start", "ping-ing-popup");
}
