//for Chart
var chartRefresh = 5;
var currChartPage = 0;
var currHistoryCount = 0;
var totalChartPage = 0;
var historyCountPerPage = [];
var useHistoryCount = 0;
var chartOffset = 0;
var currtime = undefined;
var currHour = 0;
var currMinute = 0;
var initOffset = 0;
var currOffset = 0;
var originAM = false;
var isAM = true;
var isCelsius = true;

function getOffsetTime(){

	getCurrentTime(function(msg){
		currtime = msg;
		currtime.hour = currtime.hour - ( currChartPage*12 );
		if( currtime.hour < 0 ){
			currtime.hour += 24;
			currtime.day = currtime.day-1;
		}

		if( currtime.hour > 11 ){
			isAM = false;
			currtime.hour = currtime.hour%12;
		}else{
			isAM = true;
		}

		currentTime = msg.current;
		currHour = msg.hour;
		currMinute = msg.minute;
		initOffset = currMinute%chartRefresh;
		currOffset = initOffset;

		var offSetDate = new Date(currtime.current.split(' ').splice(0, 5).join(' '));
		currentTime = new Date(offSetDate.getTime() - (currOffset*60*1000 + currChartPage*(60*1000*60*12) ) ).toString();
		setHistoryOffset();
	});
}

function setHistoryOffset(){
	getTempHistory('main', 0, 10000, function(tempHistory){

		var totalHistoryCount = tempHistory.history.length;


		var offsetedHistory = tempHistory.history.splice(currOffset, 10000);


		var filteredHistory = [];
		var resultHistory = [];
		currMinute -= currOffset;

		for(var i=0; i<offsetedHistory.length; i++){
			if( i%chartRefresh == 0 ){

				var tmpVal = {};
				tmpVal.value = offsetedHistory[i];

				if(i>0){
					currMinute = currMinute - chartRefresh;
				}

				if(currMinute < 0){
					currMinute += 60;

					if(--currHour < 0){
						currHour += 12;
						tmpVal.am = !isAM;
					}
				}

				tmpVal.minute = currMinute;
				tmpVal.hour = currHour;
				tmpVal.am = isAM;

				filteredHistory.push(tmpVal);

				if( currMinute==0 ){
					if( currHour==0 ){
						resultHistory.push(filteredHistory);
						filteredHistory = [];
					}
				}
			}
		}

		if(filteredHistory.length > 0)
			resultHistory.push(filteredHistory);

		var selectedHistory = [];
		for(var i=0; i<resultHistory[currChartPage].length; i++){
			if( isCelsius == 'true' ){
				selectedHistory.push(resultHistory[currChartPage][i].value);
			}else{
				selectedHistory.push( (resultHistory[currChartPage][i].value) * 1.8 + 32 );
			}
		}

		$('#canvasWrapper').empty();
		$('#canvasWrapper').append('<canvas id="modalTempChart">');

		var tmpTime = currtime.current.split(' ').splice(0, 5);
		tmpTime[4] = resultHistory[currChartPage][0].hour + ':' + resultHistory[currChartPage][0].minute + ':00';
		tmpTime[2] = currentTime.split(' ')[2];
		var offSetDate = new Date(tmpTime.join(' '));

		var year = offSetDate.getFullYear();
		var month = offSetDate.getMonth() + 1;
		var day = offSetDate.getDate();

		if(month<10) month = '0'+month;
		if(day<10) day = '0'+day;

		var currentDate = year+'-'+month+'-'+day;

		drawTempChart([selectedHistory], tempHistory.interval*chartRefresh, true, 'modalTempChart');
		drawChartPaging(resultHistory[currChartPage][0].am, currentDate, currChartPage==(resultHistory.length-1))
	});
}



function showModalTempChart(){
	readTempType(function(tempType){
		isCelsius = tempType.isCelsius;
		getOffsetTime();
	});
}

function drawChartPaging(isAM, currentDate, lastPage){
	$('#chartDate').text(currentDate);

	if(originAM){
		if( currChartPage%2 == 0 ){
			$('.goFirstUseTxt').text(Locale.getText("Prev DAY"));
			$('#tempDetailModal #prevText').text(Locale.getText("Prev DAY"));
			$('#tempDetailModal #nextText').text(Locale.getText("PM"));
		}else{
			$('#tempDetailModal #prevText').text(Locale.getText("AM"));
			$('#tempDetailModal #nextText').text(Locale.getText("Next Day"));
		}
	}else{
		if( currChartPage%2 == 0 ){
			$('#tempDetailModal #prevText').text(Locale.getText("AM"));
			$('#tempDetailModal #nextText').text(Locale.getText("Next Day"));
		}else{
			$('#tempDetailModal #prevText').text(Locale.getText("Prev DAY"));
			$('#tempDetailModal #nextText').text(Locale.getText("PM"));
		}
	}

	$('#tempDetailModal #prevChartBtn, #nextChartBtn').prop('disabled', false);

	if( lastPage ){
		$('#tempDetailModal #prevChartBtn').prop('disabled', true);
	}

	if( currChartPage == 0 ){
		$('#tempDetailModal #nextChartBtn').prop('disabled', true);
	}
}

var autoInterval = undefined;
var TempDetailModal = {
	init: function() {
		var modal = this;
		$.get('/detail/charts.html', function(data, success, type){
			addDetailModal('tempDetailModal', Locale.getText("Temperature"), data);
			modal.drawAutoRefreshSelect();
			modal.drawTempUnitSelect();

			$('#tempDetailModal #prevChartBtn').click(function(){
				currChartPage++;
				setTimeout(function() {
					showModalTempChart();
				}, 200);
			});

			$('#tempDetailModal #nextChartBtn').click(function(){
				currChartPage--;
				setTimeout(function() {
					showModalTempChart();
				}, 200);
			});

			$('#chartRefresh').click(function(){
				setTimeout(function() {
					showModalTempChart();
				}, 200);
			});

			getCurrentTime(function(msg){
				originAM = (msg.hour < 12);
			});
		});
	},

	modal: function() {
		$('#tempDetailModal').modal();
		setTimeout(function() {
			showModalTempChart();
		}, 200);
	},

	drawAutoRefreshSelect: function() {

		autoInterval = setInterval(function() {
			showModalTempChart();
		}, 300*1000 );

		var addChar = '';
		if (env.locale === 'ar-SA') {
			var addChar = "\u202A";
		}

		drawDropDownBox('modalRefreshSelect', [
			'300', '600'
		], [
			addChar + Locale.getText('5Min'), addChar + Locale.getText('10Min')
		], 'dropboxType3', function (selected) {
			clearInterval(autoInterval);
			autoInterval = setInterval(function() {
				showModalTempChart();
			}, selected*1000 );
		}, '300');
	},

	drawTempUnitSelect: function() {

		readTempType(function(tempType){
			var isCelsius = tempType.isCelsius;
			drawDropDownBox('tempUnitSelect', [
				true, false
			], [
				'°C', '°F'
			], 'dropboxType3', function (selected) {
				writeTempType(selected);
				showModalTempChart();
			}, isCelsius);
		});
	}

};