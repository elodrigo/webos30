document.addEventListener("DOMContentLoaded", function () {
  $("head").append(
    '<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w4/popup_network_ping.css">'
  );

  var myContainer = document.getElementById("ping-complete-popup");
  var myFocusTrap = focusTrap.createFocusTrap("#ping-complete-popup", {
    onActivate: function () {
      myContainer.classList.add("is-active");
    },
    onDeactivate: function () {
      myContainer.classList.remove("is-active");
    },
    escapeDeactivates: false,
  });

  $("#btn-ping-complete").on("click", function () {
    myFocusTrap.deactivate();
    _closePopup("ping-complete-popup");
    $("#btn-ping-test").focus();
  });

  $("#ping-complete-popup").on("cssClassChanged", function (e) {
    // e.stopPropagation();
    myFocusTrap.activate();
  });
});
