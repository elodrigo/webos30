var filmGainOrigin = [];

var unitCalirationModal = {
	prevVal : [],
	currUnitID : 0,
	init: function() {
		$.get('/detail/colorUnitCalibration.html', function(data, success, type){
			var modalPosition = {
				top : 250,
				left: 420,
				w: 450,
				h: 300
			};
			addCalibModal('unitCalirationModal', 'Unit Calibration', data, modalPosition);

			//$('#unitCalirationModal .modal-content').css('height', 300);
		});
	},

	modal: function() {
		var modal = this;
		var senderIndex = currControlUnit.split(':')[0];
		var unitIndex = currControlUnit.split(':')[1];
		var filmCnt = 0;

		$('#unitCalirationModal').modal();

		$('#unitCalibR').val(255);
		$('#unitCalibG').val(255);
		$('#unitCalibB').val(255);
		$('#unitCalibRVal').html(255);
		$('#unitCalibGVal').html(255);
		$('#unitCalibBVal').html(255);
		checkRangeUI('unitCalibR', 0, 255);
		checkRangeUI('unitCalibG', 0, 255);
		checkRangeUI('unitCalibB', 0, 255);

		getSystemSettings('commercial', ['ledLayout'], function(ret){
			var unit = ret.ledLayout.senderList[senderIndex-1].unitList[unitIndex-1];
			var rows = unit.rows;
			var ports = unit.portSelection == 0 ? 2 : 1;
			filmCnt = rows * ports;

			var filmIdx = 0;

			filmGainOrigin = [];
			for(var i=0; i<filmCnt; i++){
				getFilmGain( senderIndex*2, (unitIndex-1), i, function(ret){
					filmGainOrigin.push(ret.result);
					filmIdx++;
				});
			}
		});

		$('[id^=unitCalib]').change(function() {
			var val = $(this).val();
			var id = $(this).attr('id');
			$('#'+id+'Val').html(val);

			var rValue = $('#unitCalibR').val();
			var gValue = $('#unitCalibG').val();
			var bValue = $('#unitCalibB').val();

			for(var i=0; i<filmCnt; i++){
				setFilmGain(senderIndex*2, unitIndex-1, i, rValue, gValue, bValue);
			}
			
		}).on('input', function() {
			var id = $(this).attr('id');
			checkRangeUI(id, 0, 255);
		});

		$('#unitCalibResetBtn').click(function(){
			$('#unitCalibR').val(255);
			$('#unitCalibG').val(255);
			$('#unitCalibB').val(255);

			checkRangeUI('unitCalibR', 0, 255);
			checkRangeUI('unitCalibG', 0, 255);
			checkRangeUI('unitCalibB', 0, 255);

			$('#unitCalibRVal').html(255);
			$('#unitCalibGVal').html(255);
			$('#unitCalibBVal').html(255);
		});

		$('#unitCalibOK').unbind('click').click(function(){
			//$('#unitCalirationModal').modal('hide');
		});

		$('#unitCalibCancel').unbind('click').click(function(){
			rollbackGain();
			$('#unitCalirationModal').modal('hide');
		});
	},

	drawCalibUnitSelect: function(curUnit){
		var modal = this;
		var id = [];
		var val = [];
		modal.prevVal = [];
		mainCanvas.rectList.forEach(function(rect) {
			id.push(rect.id);
			val.push('Unit'+rect.id);

			getGlobalBC(rect.id*2, function(ret){
				modal.prevVal.push(ret ? ret.bc : -1);
			});
		});

		$('#calibUnitSelect').empty();

		drawDropDownBox('calibUnitSelect', id, val, 'dropboxType3', function (selected) {
			modal.getGlobalBC(selected);
		}, curUnit.id);
	}
};


var moduleCalibOrigin = [];
var moduleCalirationModal = {
	init: function() {
		$.get('/detail/colorModuleCalibration.html', function(data, success, type){
			var modalPosition = {
				top : 250,
				left: 320,
				w: 800,
				h: 600
			};
			addCalibModal('moduleCalirationModal', 'Module Calibration', data, modalPosition);
		});
	},

	modal: function() {
		var modal = this;

		getSystemSettings('commercial', ['ledLayout'], function(ret){
			var senderIndex = currControlUnit.split(':')[0];
			var unitIndex = currControlUnit.split(':')[1];
			var sender = ret.ledLayout.senderList[findSender(senderIndex)];
			var unitIdx = findUnit(sender, unitIndex);
			var unit = sender.unitList[unitIdx];
			var rows = parseInt(unit.rows) + (unit.mini > 0 ? 1 : 0);
			var ports = unit.portSelection == 0 ? 2 : 1;
			var tableTxt = '<table>';

			for(var i = 0; i<rows; i++){
				tableTxt += '<tr>';
				for(var j=0; j<ports; j++){
					if( (i == 0) && (unit.mini > 0) ){
						tableTxt += '<th class="colorCalibSub"><span class="moduleTxt">'+Locale.getText('Modules')+'</span>' + ( (i*2)+(j+1) ) + '(<span class="miniTxt">'+Locale.getText('Mini Module')+'</span>)</th>';
					}else{
						tableTxt += '<th class="colorCalibSub"><span class="moduleTxt">'+Locale.getText('Modules')+'</span>' + ( (i*2)+(j+1) ) + '</th>';
					}
				}

				tableTxt += '</tr><tr>';

				for(var j=0; j<ports; j++){
					tableTxt += '<td><table class="colorCalibTable">';

					tableTxt += '<tr>'
					tableTxt += '<td class="calibTD1">R</td>'
					tableTxt += '<td class="calibTD2">'
					tableTxt += '<input class=custom id="colorCalibR'+( (i*2)+(j+1) )+'" type=range min=0 max=255 name="'+( (i*2)+(j+1) )+'"style="margin-top: 0px; width:230px;">'
					tableTxt += '</td>'
					tableTxt += '<td class="calibTD2">'
					tableTxt += '<div class="colorCalibVal" id="colorCalibR'+( (i*2)+(j+1) )+'Val"></div>'
					tableTxt += '</td>'
					tableTxt += '</tr>'

					tableTxt += '<tr>'
					tableTxt += '<td class="calibTD1">G</td>'
					tableTxt += '<td class="calibTD2">'
					tableTxt += '<input class=custom id="colorCalibG'+( (i*2)+(j+1) )+'" type=range min=0 max=255 name="'+( (i*2)+(j+1) )+'" style="margin-top: 0px; width:230px;">'
					tableTxt += '</td>'
					tableTxt += '<td class="calibTD3">'
					tableTxt += '<div class="colorCalibVal" id="colorCalibG'+( (i*2)+(j+1) )+'Val"></div>'
					tableTxt += '</td>'
					tableTxt += '</tr>'

					tableTxt += '<tr>'
					tableTxt += '<td class="calibTD1">B</td>'
					tableTxt += '<td class="calibTD2">'
					tableTxt += '<input class=custom id="colorCalibB'+( (i*2)+(j+1) )+'" type=range min=0 max=255 name="'+( (i*2)+(j+1) )+'"style="margin-top: 0px; width:230px;">'
					tableTxt += '</td>'
					tableTxt += '<td class="calibTD3">'
					tableTxt += '<div class="colorCalibVal" id="colorCalibB'+( (i*2)+(j+1) )+'Val">255</div>'
					tableTxt += '</td>'
					tableTxt += '</tr>'

					tableTxt += '</table></td>';
				}

				tableTxt += '</tr>';
			}
			tableTxt += '</table>';

			$('#moduleCalibTable').html(tableTxt);

			$('[id^=colorCalib]').change(function() {
				var val = $(this).val();
				var id = $(this).attr('id') + 'Val';
				var filmIdx = $(this).attr('name')-1;

				var rValue = $('#colorCalibR'+(filmIdx+1)).val();
				var gValue = $('#colorCalibG'+(filmIdx+1)).val();
				var bValue = $('#colorCalibB'+(filmIdx+1)).val();

				$('#'+id).html(val);

				setFilmGain(senderIndex*2, unitIndex-1, filmIdx, rValue, gValue, bValue);
			}).on('input', function() {
				var val = $(this).val();
				var id = $(this).attr('id');
				checkRangeUI(id, 0, 255);

				$('#'+id).html(val);
			});

			var filmIdx = 0;
			filmGainOrigin = [];
			for(var i=0; i<ports*rows; i++){
				getFilmGain( senderIndex*2, (unitIndex-1), i, function(ret){
					console.log('filmIdx::'+filmIdx);
					console.log(ret.result);

					filmGainOrigin.push(ret.result);

					$('#colorCalibR'+(filmIdx+1)).val(ret.result.R);
					$('#colorCalibG'+(filmIdx+1)).val(ret.result.G);
					$('#colorCalibB'+(filmIdx+1)).val(ret.result.B);

					checkRangeUI('colorCalibR'+(filmIdx+1), 0, 255);
					checkRangeUI('colorCalibG'+(filmIdx+1), 0, 255);
					checkRangeUI('colorCalibB'+(filmIdx+1), 0, 255);

					$('#colorCalibR'+(filmIdx+1)+'Val').html(ret.result.R);
					$('#colorCalibG'+(filmIdx+1)+'Val').html(ret.result.G);
					$('#colorCalibB'+(filmIdx+1)+'Val').html(ret.result.B);
					
					filmIdx++;
				});
			}
		});

		$('#moduleCalibOK').click(function(){
			var senderIndex = currControlUnit.split(':')[0];
			console.log('senderIndex::'+ senderIndex*2 );
			saveFilmGain( senderIndex*2, function(msg){
				console.log(msg);
			});
			$('#moduleCalirationModal').modal('hide');
		});

		$('#moduleCalibCancel').click(function(){
			rollbackGain();
			currPath = window.location.href.split('#');
			if(currPath.length > 1){
				window.location.href = currPath[0];
			}
		});

		$('#moduleCalibReset').click(function(){

			getSystemSettings('commercial', ['ledLayout'], function(ret){
				var senderIndex = currControlUnit.split(':')[0];
				var unitIndex = currControlUnit.split(':')[1];
				var sender = ret.ledLayout.senderList[findSender(senderIndex)];
				var unitIdx = findUnit(sender, unitIndex );
				var unit = sender.unitList[unitIdx];
				var rows = unit.rows + (unit.mini>0 ? 1:0);
				var ports = unit.portSelection == 0 ? 2 : 1;
				var filmIdx = 0;

				setFilmGain(senderIndex*2, (unitIndex-1), 255, 255, 255, 255);

				for(var i=0; i<ports*rows; i++){
					$('#colorCalibR'+(filmIdx+1)).val(255);
					$('#colorCalibG'+(filmIdx+1)).val(255);
					$('#colorCalibB'+(filmIdx+1)).val(255);

					checkRangeUI('colorCalibR'+(filmIdx+1), 0, 255);
					checkRangeUI('colorCalibG'+(filmIdx+1), 0, 255);
					checkRangeUI('colorCalibB'+(filmIdx+1), 0, 255);

					$('#colorCalibR'+(filmIdx+1)+'Val').html(255);
					$('#colorCalibG'+(filmIdx+1)+'Val').html(255);
					$('#colorCalibB'+(filmIdx+1)+'Val').html(255);

					filmIdx++;
				}
			});
		});

		$('#moduleCalirationModal').modal();
	}
}

function rollbackGain(){
	var senderIndex = currControlUnit.split(':')[0];
	loadFilmGain(senderIndex*2, function(msg){
		console.log(msg);
	});
}