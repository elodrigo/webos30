var tempConfig = {};
var zoomStart = 0;
var originalConfig = '';
var tmpSelectedUnit = undefined;
var startPosition = {};
var maxUnitCnt = 17;

function disableLayoutPanel(disabled){
	$('#autoDetectBtn, #addUnitBtn, #removeUnitBtn, #testBtn').prop('disabled', disabled);
	$('#frameRowSelectDrop, #frameColumnSelectDrop, #frameX, #frameY').prop('disabled', disabled);
}

function drawPitchSelect(config){

	$('#pitchSelect').empty();

	drawDropDownBox('pitchSelect', [30], [30], 'dropboxType2', function (selected) {
	}, 30);

	if(config.physicalLayout){
		$('#physicalX').val(config.physicalLayout.x);
		$('#physicalY').val(config.physicalLayout.y);
	}

	$('#pitchSelect button').attr('disabled', true);
}

function drwaPortSelect(config){
	var rowCount = 4;
	var colCount = 6;
	var rows = [2,3,4];
	var cols = [2,3,4,5,6];
	var rowDefault = config.portLength;
	var colDefault = config.portUse.length;
	var isPortrait = config.inputDir > 1;

	if(!isPortrait){
		rowCount = 6;
		colCount = 4;
		rows = [2,3,4,5,6];
		cols = [2,3,4];
		rowDefault = config.portUse.length;
		colDefault = config.portUse.length;
	}

	$('#unitRowSelect, #unitColumnSelect, #inputDirSelect, #portRowSelect').empty();

	drawDropDownBox('unitRowSelect', rows, rows, 'dropboxType2', function (selected) {
		if(isPortrait){
			var tempArr = [];
			for(var i=1; i<=selected; i++){
				tempArr.push(i);
			}
			config.portUse = tempArr;
		}else{
			config.portLength = selected;
		}
		showPortUse(config);
	}, rowDefault);

	drawDropDownBox('unitColumnSelect', cols, cols, 'dropboxType2', function (selected) {
		if(isPortrait){
			config.portLength = selected;
		}else{
			var tempArr = [];
			for(var i=1; i<=selected; i++){
				tempArr.push(i);
			}
			config.portUse = tempArr;
		}
		showPortUse(config);
	}, colDefault);

	drawDropDownBox('inputDirSelect', [3,0,2,1], ['Top', 'Right', 'Bottom', 'Left'], 'dropboxType2', function (selected) {
		config.inputDir = selected;
		showPortUse(config);
	}, config.inputDir);

	drawDropDownBox('portRowSelect', [1,2,3,4,5,6], [1,2,3,4,5,6], 'dropboxType2', function (selected) {
		config.portLength = selected;
		showPortUse(config);
	}, config.portLength);

	$('#rearrBtn').attr('disabled', true);
}

function showPortUse(config){
	var isPortrait = config.inputDir > 1;
	var rowDefault = config.portUse.length;
	var colDefault = config.portLength;

	if(config.inputDir == '0'){
		$('#portInfoPanel').css('height', 535);
		$('#portView').css('transform', 'rotate(270deg)');
		$('#portView').css('margin-top', '40px');
		$('#portView').css('margin-bottom', '50px');
	}else if(config.inputDir == '1'){
		$('#portInfoPanel').css('height', 535);
		$('#portView').css('transform', 'rotate(90deg)');
		$('#portView').css('margin-top', '40px');
		$('#portView').css('margin-bottom', '50px');
	}else{
		$('#portInfoPanel').css('height', 495);
		$('#portView').css('transform', 'rotate(0deg)');
		$('#portView').css('margin-top', '0px');
		$('#portView').css('margin-bottom', '0px');
	}

	setDropButtonText('unitRowSelect', rowDefault, true);
	setDropButtonText('unitColumnSelect', colDefault, true);
	setDropButtonText('portRowSelect', config.portLength , true);
	setDropButtonText('inputDirSelect', config.inputDir , true);

	$('[id^=UseRow]').css('height', 0);

	if( config.inputDir%2 == 0 ){
		$('#inputUp').show();
		$('#inputDown').hide();
	}else{
		$('#inputDown').show();
		$('#inputUp').hide();
	}

	if(config.inputDir == 1){
		$('#inputDown').hide();
		$('#inputUp').show();
	}

	$('[id^=Column]').prop('checked', false);
	config.portUse.forEach(function(index){
		$('#Column'+index).prop('checked', true);

		if( config.inputDir%2 == 0 ){
			$('#UseRow'+index).css('top', '');
			$('#UseRow'+index).css('bottom', 0);
		}else{
			$('#UseRow'+index).css('bottom', '');
			$('#UseRow'+index).css('top', 0);
		}

		if(config.inputDir == 1){
			$('#UseRow'+index).css('top', '');
			$('#UseRow'+index).css('bottom', 0);
		}

		$('#UseRow'+index).css('height', 24.5*config.portLength);
		$('#UseRow'+index).css('left', 42*(index-1));
	});
}

function compareLayout(selectConfig){
	var modified = originalConfig!=JSON.stringify(selectConfig);
	$('#layoutEditorModal #modalActionBtn').prop('disabled', !modified);
}

function addAutoDetect(index, modal){
	getInputDirection(index*2, function(direction){
		getWidthHeight(index*2, function(widthHeight){
			getDisplayPortSel(index*2, function(portUse){
				getTrim(index*2, function(trim){
					getPosition(index*2, function(pos){
						var newRect = {
							x: pos.x,
							y: pos.y,
							z: modal.zIndex,
							w: widthHeight.width*16,
							h: widthHeight.height*16,
							inputDir: direction.inputDirection,
							portUse: [1,2],
							portLength: widthHeight.height,
							id: index,
							trim : {
								left : trim.width,
								right : trim.height
							}
						};
						modal.config.rectList.push(newRect);
						refreshLed(modal.config);
						compareLayout(modal.config);
						id: index
					});
				});
			});
		});
	});
}




function drawEdgeTable(dir, count){

	var tableText = '';

	for(var i=0; i<16; i++){
		tableText += '<tr style="">'
		for(var j=0; j<16; j++){
			if( dir=='Left' ){
				if(j < Number(count) ){
					tableText += '<td class="cutted" style="height:6.25%; width:6.25%; border: 1px solid black;"></td>'
				}else{
					tableText += '<td class="notcutted" style="height:6.25%; width:6.25%; border: 1px solid black;"></td>'
				}
			}else{
				if(j+ Number(count) >= 16){
					tableText += '<td class="cutted" style="height:6.25%; width:6.25%; border: 1px solid black;"></td>'
				}else{
					tableText += '<td class="notcutted" style="height:6.25%; width:6.25%; border: 1px solid black;"></td>'
				}
			}
		}
		tableText += '</tr>'
	}

	$('#edgeTable').empty();
	$('#edgeTable').append(tableText);
}

function hideToolTip(){
	$('#posToolTip').css('display', 'none');
}

var FilmLayoutEditorModal = {
	layout: {
		selected: undefined,
		startPos: undefined,
		snapX: 2,
		snapY: 8,
		zIndex: 0,
		prevClickTime: 0,
		isDoubleClick: false,
	},

	config: {
		dragging: false,
		curUnit: undefined,
		rectList: [],
		scaleScreen: 702 / 1920,
		editorScale : 1,
		resolX: 384,
		resolY: 360,
		viewPort: {
			x: 0,
			y: 0,
			w: 5 * this.resolX,
			h: 3 * this.resolY
		},
		physicalLayout: {
			x: 0,
			y: 0
		},
		maxX: 0,
		maxY: 0,
		from: 0,
		direction: 0,
		inputDir: 2,
		portUse: [1,2],
		portLength: 2,
		trim: {
			left:0,
			right:0
		},
		canvas: undefined,
		ctx: undefined,
		img: new Image()
	},

	patternConfig:{},
	patternLayout:{},


	mouseDown: function(e) {
		var layout = LayoutEditorModal.layout;
		var config = LayoutEditorModal.config;

		clearSelection();

		var pos = getMousePos(e);
		var cur = new Date().getTime();
		var diff= cur - layout.prevClickTime;

		pos.x = pos.x / (config.scaleScreen * config.editorScale);
		pos.y = pos.y / (config.scaleScreen * config.editorScale);

		layout.prevClickTime = cur;
		layout.selected = undefined;

		if (diff < 300) {
			if (isInsideRect(pos, config.viewPort)) {
				layout.selected = config.viewPort;
				layout.startPos = pos;
				config.dragging = true;
				config.curUnit = undefined;
				$('#posToolTip').css('display', 'inline');
				$('#posToolTip').html(layout.selected.x+', '+layout.selected.y);
				$('#posToolTip').css('left', ( (layout.selected.x + layout.selected.w) * (config.scaleScreen * config.editorScale)) +70);
				$('#posToolTip').css('top', ( (layout.selected.y + layout.selected.h) * (config.scaleScreen * config.editorScale))+70);
				return;
			}
		}
	
		var rects = getSelectedRect(LayoutEditorModal.config, pos)
	
		if (rects.length > 0) {
			layout.selected = rects[rects.length - 1];
			config.curUnit = layout.selected;
			layout.zIndex++;
			layout.selected.z = layout.zIndex;
			layout.startPos = pos;
			config.dragging = true;
		}else{
			config.curUnit = undefined;
			$('#posToolTip').css('display', 'none');
		}

		if(config.dragging){
			$('#posToolTip').css('display', 'inline');
			$('#posToolTip').html(layout.selected.x+', '+layout.selected.y);
			$('#posToolTip').css('left', (pos.x * (config.scaleScreen * config.editorScale)) +70);
			$('#posToolTip').css('top', (pos.y * (config.scaleScreen * config.editorScale))+70);
			startPosition.x = layout.selected.x;
			startPosition.y = layout.selected.y;
		}
	},

	mouseUp: function(e) {
		var layout = LayoutEditorModal.layout;
		var config = LayoutEditorModal.config;

		refreshLed(config);

		if (!config.dragging || !layout.selected) {
			return
		}

		if(layout.selected.trim != undefined){
			var trim = layout.selected.trim;
			if( Number(trim.left) > 0 || Number(trim.right) > 0 ){
				if(layout.selected.x < trim.left){

					layout.selected.x = startPosition.x;
					refreshLed(config);

					$('#posToolTip').css('display', 'inline');
					$('#posToolTip').css('width', 400);
					$('#posToolTip').html('Units cut left edge can not be placed in the X: 0 positions.');
					setTimeout(function(){
						$('#posToolTip').css('display', 'none');
						$('#posToolTip').css('width', 70);
					}, 2000);
				}
			}
		}

		$('#frameX').val(config.viewPort.x/16);
		$('#frameY').val(config.viewPort.y/16);

		config.dragging = false;
		layout.selected = undefined;
		layout.startPos = undefined;
		refreshLed(config);

		compareLayout(config);
	},

	mouseMove: function(e) {
		var layout = LayoutEditorModal.layout;
		var config = LayoutEditorModal.config;

		if (layout.selected == config.viewPort) {
			layout.snapX = 16;
			layout.snapY = 16;
		}else{
			layout.snapX = 2;
			layout.snapY = 8;
		}

		if (!config.dragging || !layout.selected) {
			return;
		}
	
		clearSelection();
		var pos = getMousePos(e);

		if(config.dragging){
			$('#posToolTip').css('display', 'inline');
			$('#posToolTip').html(layout.selected.x+', '+layout.selected.y);
		}

		pos.x = pos.x / (config.scaleScreen * config.editorScale);
		pos.y = pos.y / (config.scaleScreen * config.editorScale);

		var prevX = layout.selected.x;
		var prevY = layout.selected.y;
		var diffX = Math.floor((pos.x - layout.startPos.x) / layout.snapX) * layout.snapX;
		var diffY = Math.floor((pos.y - layout.startPos.y) / layout.snapY) * layout.snapY;

		if (diffX != 0) {
			layout.selected.x += diffX;
			layout.startPos.x += diffX;
		}

		if (diffY != 0){
			layout.selected.y += diffY;
			layout.startPos.y += diffY;
		}

		if (diffX == 0 && diffY == 0) {
			return;
		}

		if( !restrictRect(layout.selected, config.viewPort) ){
			$('#posToolTip').css('left', (layout.selected.x + layout.selected.w) * (config.scaleScreen * config.editorScale) + 70);
			$('#posToolTip').css('top', (layout.selected.y + layout.selected.h) * (config.scaleScreen * config.editorScale) + 70);
		}
		restrictRect(config.viewPort, {x: 0, y: 0, w: outer.width, h: outer.height});

		if (layout.selected == config.viewPort) {
			diffX = config.viewPort.x - prevX;
			diffY = config.viewPort.y - prevY;
			config.rectList.forEach(function(rect) {
				rect.x += diffX;
				rect.y += diffY;
			});
		}

		draw(config);
		compareLayout(config);
	},
	mouseOut : function(e){
		if(e.toElement){
			if(e.toElement.className != 'posToolTip'){
				var config = LayoutEditorModal.config;
				config.dragging = false;
			}
		}
	},
	testMouseClick: function(e) {
		var layout = LayoutEditorModal.patternLayout;
		var config = LayoutEditorModal.patternConfig;

		clearSelection();

		var pos = getMousePos(e);
		var cur = new Date().getTime();
		var diff= cur - layout.prevClickTime;

		pos.x = pos.x / (config.scaleScreen * config.editorScale);
		pos.y = pos.y / (config.scaleScreen * config.editorScale);

		layout.prevClickTime = cur;
		layout.selected = undefined;

		if (diff < 300) {
			if (isInsideRect(pos, config.viewPort, config.unitScale)) {
				layout.selected = config.viewPort;
				layout.startPos = pos;
				config.dragging = true;
				return;
			}
		}

		var rects = getSelectedRect(config, pos)

		if (rects.length > 0) {
			rects.sort(compareZ);
			layout.selected = rects[rects.length - 1];
			config.curUnit = layout.selected;
			layout.zIndex++;
			layout.selected.z = layout.zIndex;
			layout.startPos = pos;

			if(config.curUnit.pattern == undefined){
				config.curUnit.pattern = true;
			}
			else{
				config.curUnit.pattern = !config.curUnit.pattern;
			}

			rects.forEach(function(rect){
				var brightness = $('#patternBrightness').val() * 10;
				showPattern( (rect.id*2), rect.pattern, brightness, brightness, brightness);
			});
		}

		draw(config);
	},

	keyup : function(e){
		var config = LayoutEditorModal.config;

		if( config.curUnit != undefined){

			if( e.key == 'ArrowUp' ){
				if( config.curUnit.y > config.viewPort.y ){
					config.curUnit.y = config.curUnit.y - 1;
					$('#posToolTip').css('top', Number($('#posToolTip').css('top').replace(/[^-\d\.]/g, '')) - config.scaleScreen );
				}
			}else if( e.key == 'ArrowDown' ){
				if( config.curUnit.y + config.curUnit.h < config.viewPort.y + config.viewPort.h ){
					config.curUnit.y = config.curUnit.y + 1;
					$('#posToolTip').css('top', Number($('#posToolTip').css('top').replace(/[^-\d\.]/g, '')) + config.scaleScreen );
				}
			}else if( e.key == 'ArrowLeft' ){
				if( config.curUnit.x > config.viewPort.x ){
					config.curUnit.x = config.curUnit.x - 1;
					$('#posToolTip').css('left', Number($('#posToolTip').css('left').replace(/[^-\d\.]/g, '')) - config.scaleScreen );
				}
			}else if( e.key == 'ArrowRight' ){
				if( config.curUnit.x + config.curUnit.w < config.viewPort.x + config.viewPort.w ){
					config.curUnit.x = config.curUnit.x + 1;
					$('#posToolTip').css('left', Number($('#posToolTip').css('left').replace(/[^-\d\.]/g, '')) + config.scaleScreen );
				}
			}

			$('#posToolTip').css('display', 'inline');
			$('#posToolTip').html(config.curUnit.x+', '+config.curUnit.y);

			draw(config);
		}
	},

	getNewPosition: function(newID) {
		var config = LayoutEditorModal.config;

		function backwardFirst(loc, size, resol) {
			return loc + Math.floor(size / resol) * resol- resol;
		}

		var from = config.from;
		var direction = config.direction;

		var startX = ([0, 2].indexOf(from) >= 0) ? config.viewPort.x : backwardFirst(config.viewPort.x, config.viewPort.w, config.resolX);
		var startY = ([0, 1].indexOf(from) >= 0) ? config.viewPort.y : backwardFirst(config.viewPort.y, config.viewPort.h, config.resolY);
	
		var diffX = ([0, 2].indexOf(from) >= 0) ? 1 : -1;
		var diffY = ([0, 1].indexOf(from) >= 0) ? 1 : -1;
	
	
		if (direction == 0) {		
			var dir = config.resolX * diffX * Math.pow(-1, Math.floor(newID / config.maxX));		
			startX = dir > 0 ? config.viewPort.x : backwardFirst(config.viewPort.x, config.viewPort.w, config.resolX);
			x = startX + (newID % config.maxX) * dir;
			y = startY + config.resolY * diffY * Math.floor(newID / config.maxX);		
		} else {
			var dir = config.resolY * diffY * Math.pow(-1, Math.floor(newID / config.maxY));	
			startY = dir > 0 ? config.viewPort.y : backwardFirst(config.viewPort.y, config.viewPort.h,config.resolY);
			x = startX + config.resolX * diffX * Math.floor(newID / config.maxY);
			y = startY + (newID % config.maxY) * dir;
		}

		return {x: x, y: y};
	},

	closePortSelect: function(){
		$('#portInfoPanel').toggle();
		$('#layoutInfoPanel').toggle();
	},

	addEventHandlers: function() {
		var prevPosX = 0;
		var prevPosY = 0;

		$('#autoDetectBtn').click(function(){
			getAutoDetect(function(detectCount){
				var index = 1;
				var modal = LayoutEditorModal;
				modal.config.rectList = [];
				for(var i=0; i< detectCount.totalUnit; i++){
					addAutoDetect(i+1, modal );
				}

				$('#frameRowSelect li:last-child a').last().click();
				$('#frameColumnSelect li:last-child a').last().click();
			});
		});

		$('#addUnitBtn').click(function() {
			var modal = LayoutEditorModal;
			var frameX = modal.config.viewPort.x;
			var frameY = modal.config.viewPort.y;
			var numUnits = modal.config.rectList.length;
			var newRect = {
				x: Number(modal.config.trim.left) + frameX,
				y: frameY,
				z: modal.zIndex,
				w: modal.config.resolX,
				h: modal.config.resolY,
				inputDir: modal.config.inputDir,
				portUse: modal.config.portUse,
				portLength: modal.config.portLength,
				id: modal.config.rectList.length + 1,
				trim : {
					left : modal.config.trim.left,
					right: modal.config.trim.right
				}
			};

			if(env.supportLedFilm){
				if(newRect.portUse.length > 0){
					if( newRect.inputDir < 2 ){
						newRect.w = parseInt(newRect.portLength) * 16;
						newRect.h = ( newRect.portUse.length > 0 ? newRect.portUse.length : 2 ) * 16;
					}else{
						newRect.h = parseInt(newRect.portLength) * 16;
						newRect.w = ( newRect.portUse.length > 0 ? newRect.portUse.length : 2 ) * 16;
					}
				}
			}

			modal.config.rectList.push(newRect);
			if(modal.config.rectList.length == maxUnitCnt){
				$('#addUnitBtn').attr('disabled', true);
			}

			refreshLed(modal.config);
			compareLayout(modal.config);
		});

		$('#removeUnitBtn').click(function() {
			var config = LayoutEditorModal.config;

			if (config.rectList.length <= 0) {
				return;
			}

			var index = config.rectList.indexOf(config.curUnit);
			if (index < 0) {
				index = config.rectList.length - 1;
				config.curUnit = config.rectList[index];
			}

			config.rectList.splice(index, 1);
			config.rectList.forEach(function(rect) {
				if (rect.id > config.curUnit.id) {
					rect.id--;
				}
			});
			config.curUnit = config.rectList.length == 0 ? undefined : config.rectList[config.rectList.length - 1];

			if(config.rectList.length < maxUnitCnt){
				$('#addUnitBtn').attr('disabled', false);
			}
			hideToolTip();
			refreshLed(config);
			compareLayout(config);
		});

		$('layoutRefresh').click(function(){
			var config = LayoutEditorModal.config;
			refreshLed(config);
		});

		$('#rearrBtn').click(function() {
			LayoutEditorModal.rearrange();
		});

		$('#portSetBtn').click(function() {

			var config = LayoutEditorModal.config;

			if(config.portUse.indexOf(1) < 0 ){
				$('#cutLeftBtn').css('visibility', 'hidden');
			}else{
				$('#cutLeftBtn').css('visibility', 'visible');
			}

			if(config.portUse.indexOf(4) < 0 ){
				$('#cutRightBtn').css('visibility', 'hidden');
			}else{
				$('#cutRightBtn').css('visibility', 'visible');
			}

			tempConfig.portLength = config.portLength;
			tempConfig.inputDir = config.inputDir;
			tempConfig.portUse = config.portUse;
			tempConfig.trim = {};
			tempConfig.trim.left = config.trim.left;
			tempConfig.trim.right = config.trim.right;
			disableLayoutPanel(true);
			LayoutEditorModal.closePortSelect();
		});

		$('#portCancelBtn').click(function() {
			var config = LayoutEditorModal.config;

			config.portLength = tempConfig.portLength;
			config.inputDir = tempConfig.inputDir;
			config.portUse = tempConfig.portUse;
			config.trim.left = tempConfig.trim.left;
			config.trim.right = tempConfig.trim.right;

			var edge = config.trim.left / 16;
			$('#leftEdge').width(37 * edge);

			var edge = config.trim.right / 16;
			$('#rightEdge').width(37 * edge);

			showPortUse(config);
			disableLayoutPanel(false);
			LayoutEditorModal.closePortSelect();
		});

		$('.cutting').click(function() {
			var config = LayoutEditorModal.config;

			$('#portInfoPanel').hide();
			$('#cuttingPanel').show();

			var direction = '';

			if( $(this).attr('id').indexOf('Left') > -1 ){
				direction = 'Left';
				$('#cuttingRange').val(config.trim.left);
				drawEdgeTable(direction, config.trim.left);
			}else{
				direction = 'Right';
				$('#cuttingRange').val(config.trim.right);
				drawEdgeTable(direction, config.trim.right);
			}

			$('#cuttingRange').css('direction', direction == 'Right' ? 'rtl':'ltr');
			checkRangeUI('cuttingRange', 0, 14, direction == 'Right');

			$('#cuttingSubject').html('Cut '+direction+' Edge');
			$('#cuttingRange').attr('name', direction);
			$('#cuttingRange').change(function() {
				var val = $(this).val();
				var direction = $(this).attr('name');
				drawEdgeTable(direction, val);
			}).on('input', function() {
				var val = $(this).val();
				var direction = $(this).attr('name');
				checkRangeUI('cuttingRange', 0, 14, direction=='Right');
			});
		});

		$('#cutCancelBtn').click(function(){
			$('#portInfoPanel').show();
			$('#cuttingPanel').hide();
		});

		$('#cutOKBtn').click(function(){

			var config = LayoutEditorModal.config;

			$('#portInfoPanel').show();
			$('#cuttingPanel').hide();

			var direction = $('#cuttingRange').attr('name');
			var val = $('#cuttingRange').val();

			if(direction == 'Left'){
				var edge = val / 16;
				$('#leftEdge').width(37 * edge);
				config.trim.left = Number(val);
			}else{
				var edge = val / 16;
				$('#rightEdge').width(37 * edge);
				$('#rightEdge').css('left', '');
				$('#rightEdge').css('right', 0);
				config.trim.right = Number(val);
			}

		});


		$('#portResetBtn').click(function() {
			var config = LayoutEditorModal.config;
			config.portLength = 2;
			config.inputDir = 2;
			config.portUse = [1,2];
			showPortUse(config);
		});


		$('#portOKBtn').click(function() {
			var config = LayoutEditorModal.config;
			disableLayoutPanel(false);
			LayoutEditorModal.closePortSelect();

			if(config.inputDir<=1){
				$('#layout_1').append($('#unitColumnSelect'));
				$('#layout_2').append($('#unitRowSelect'));
			}else{
				$('#layout_1').append($('#unitRowSelect'));
				$('#layout_2').append($('#unitColumnSelect'));
			}
		});

		$('[id^=Column]').click(function() {
			var portCount = $('[id^=Column]:checked').length;
			$('#portOKBtn').prop('disabled', portCount < 2);

			$('#cutLeftBtn, #leftEdge').css('visibility', $('#Column1').prop('checked')? 'visible' : 'hidden');
			$('#cutRightBtn, #rightEdge').css('visibility', $('#Column4').prop('checked')? 'visible' : 'hidden');

			var config = LayoutEditorModal.config;
			config.portUse = [];
			$('[id^=Column]:checked').each(function(){
				config.portUse.push( Number($(this).attr('name')) );
			});
			showPortUse(config);
		});

		$('#testBtn').click(function(){
			var config = LayoutEditorModal.config;
			var modal = LayoutEditorModal;

			tmpSelectedUnit = config.curUnit;
			config.curUnit = undefined;

			refreshLed(config);

			modal.patternConfig = jQuery.extend(true, {}, modal.config);
			modal.patternLayout = jQuery.extend(true, {}, modal.layout);

			disableLayoutPanel(true);
			checkRangeUI('patternBrightness', 0, 100);
			$('#patternBrightnessVal').html($('#patternBrightness').val());
			$('#testPatternPanel').css('display', 'inline');

			modal.config.canvas.removeEventListener('mousedown', modal.mouseDown );
			modal.config.canvas.removeEventListener("mouseup", modal.mouseUp);
			modal.config.canvas.removeEventListener("mousemove", modal.mouseMove);
			modal.config.canvas.addEventListener('mousedown', modal.testMouseClick, false);
			document.removeEventListener("keydown", modal.keyup, false);
		});

		$('#testDoneBtn').click(function(){
			var modal = LayoutEditorModal;
			var config = LayoutEditorModal.config;

			config.curUnit = tmpSelectedUnit;
			tmpSelectedUnit = undefined;

			refreshLed(config);

			modal.patternConfig = {};
			modal.patternLayout = {};

			disableLayoutPanel(false);
			$('#testPatternPanel').css('display', 'none');

			modal.config.canvas.addEventListener('mousedown', modal.mouseDown, false);
			modal.config.canvas.addEventListener("mouseup", modal.mouseUp, false);
			modal.config.canvas.addEventListener("mousemove", modal.mouseMove, false);
			modal.config.canvas.removeEventListener('mousedown', modal.testMouseClick, false);
			document.addEventListener("keydown", modal.keyup, false);

			modal.config.rectList.forEach(function(rect){
				delete rect.pattern;
				showPattern( (rect.id*2), false, 0, 0, 0);
			});
			draw(config);
		});

		$('#patternBrightness').change(function(){
			var modal = LayoutEditorModal;
			var patternVal = $(this).val() * 10;

			$('#patternBrightnessVal').html($(this).val());

			var color = Math.round($(this).val() * 255 / 100);
			var colorStr = color.toString(16);
			if (colorStr.length < 2) {
				colorStr = '0' + colorStr;
			}
			var RGBVal = '#'+colorStr+colorStr+colorStr;

			$('#patternColor').css('background-color', RGBVal);

			modal.patternConfig.rectList.forEach(function(rect){
				showPattern( (rect.id*2), rect.pattern, patternVal, patternVal, patternVal);
			});
		}).on('input', function() {
			checkRangeUI('patternBrightness', 0, 100);
		});

		$('#patternAll').click(function(){
			var modal = LayoutEditorModal;
			var patternVal = $('#patternBrightness').val() * 10;
			var applyAll = $(this).prop('checked');
			modal.patternConfig.rectList.forEach(function(rect){
				rect.pattern = applyAll;
				showPattern( (rect.id*2), applyAll, patternVal, patternVal, patternVal);
			});
			draw(modal.patternConfig);
		});

		$('#frameX').mousedown(function(){
			prevPosX = $(this).val();
		});

		$('#frameX').mouseup(function(){
			var config = LayoutEditorModal.config;

			if(config.viewPort.w + $(this).val()*16 > 1920){
				$(this).val(prevPosX);
			}

			var layout = LayoutEditorModal.layout;
			var diffX = ($(this).val() - prevPosX) * 16;

			config.viewPort.x = $(this).val() * 16;
			config.rectList.forEach(function(rect) {
				rect.x += diffX;
			});
			refreshLed(config);
			compareLayout(config);

			prevPosX = $(this).val();
		});

		$('#frameX').focusout(function(){
			var config = LayoutEditorModal.config;
			if(config.viewPort.w + $(this).val()*16 > 1920){
				$(this).val(prevPosX);
			}

			var layout = LayoutEditorModal.layout;
			var diffX = ($(this).val() - prevPosX) * 16;

			config.viewPort.x = $(this).val() * 16;
			config.rectList.forEach(function(rect) {
				rect.x += diffX;
			});
			refreshLed(config);
			compareLayout(config);


		});

		$('#frameY').mousedown(function(){
			prevPosY = $(this).val();
		});

		$('#frameY').mouseup(function(){
			var config = LayoutEditorModal.config;

			if(config.viewPort.h + $(this).val()*16 > 1080){
				$(this).val(prevPosY);
			}

			var layout = LayoutEditorModal.layout;
			var diffY = ($(this).val() - prevPosY) * 16;

			config.viewPort.y = $(this).val() * 16;
			config.rectList.forEach(function(rect) {
				rect.y += diffY;
			});
			refreshLed(config);
			compareLayout(config);

			prevPosY = $(this).val();
		});

		$('#frameY').focusout(function(){
			var config = LayoutEditorModal.config;

			if(config.viewPort.h + $(this).val()*16 > 1080){
				$(this).val(prevPosY);
			}

			var layout = LayoutEditorModal.layout;
			var diffY = ($(this).val() - prevPosY) * 16;

			config.viewPort.y = $(this).val() * 16;
			config.rectList.forEach(function(rect) {
				rect.y += diffY;
			});
			refreshLed(config);
			compareLayout(config);
		});

		$('#layoutZoomRange').mouseup(function(){
			var modal = LayoutEditorModal;
			var zoomEnd = 1 + $(this).val()*0.5;
			modal.config.editorScale = zoomEnd;
			$('#layoutEditorCanvas').css('width', parseInt($('#layoutEditorCanvasWraper').css('width')) * zoomEnd);
			refreshLed(modal.config);
		});

		$('#layoutZoomRange').on('input', function() {
			checkRangeUI('layoutZoomRange', 0, 4);
		});

		$('#layoutZoom-minus').click(function() {
			var value = $('#layoutZoomRange').val();
			if(value>0){
				value--;
				$('#layoutZoomRange').val(value);
				$('#layoutZoomRange').mousedown();
				$('#layoutZoomRange').mouseup();
				checkRangeUI('layoutZoomRange', 0, 4);
			}
		});

		$('#layoutZoom-plus').click(function() {
			var value = $('#layoutZoomRange').val();
			if(value<4){
				value++;
				$('#layoutZoomRange').val(value);
				$('#layoutZoomRange').mousedown();
				$('#layoutZoomRange').mouseup();
				checkRangeUI('layoutZoomRange', 0, 4);
			}
		});

		$('#layoutEditorModal #modalActionBtn').click(function(){

			LayoutEditorModal.config.physicalLayout = {
				x : $('#physicalX').val(),
				y : $('#physicalY').val(),
				pitch : $('#pitchSelectDrop').html()
			}

			applyConfig(LayoutEditorModal.config);

			getLedConfig(function(config){
				mainCanvas.rectList = config.unitList;
				LayoutEditorModal.config.rectList = config.unitList;
				drawFromConfig(config, mainCanvas);
				getOverallStatusHandler();
				overallStatusTimer = setInterval(getOverallStatusHandler, 5000);
			});

			$(this).attr('disabled', true);
			$('#layoutEditorModal').modal('hide');
		});

		$('#physicalX, #physicalY').change(function(){
			$('#layoutEditorModal #modalActionBtn').attr('disabled', false);
		});

		$('#posToolTip').mouseenter(function(){
			var config = LayoutEditorModal.config;
			config.dragging = false;
		});

		$('.custom, #frameX, #frameY, #physicalX, #physicalY, #unitRowSelectDrop, #unitRowSelect').keydown(function(event){
			var config = LayoutEditorModal.config;
			if( config.curUnit != undefined ){
				if( event.key == "ArrowUp" || event.key == "ArrowDown" || event.key == "ArrowLeft" || event.key == "ArrowRight" ){
					event.preventDefault();
				}
			}
		});

		$('#unitRowSelect, #unitColumnSelect, #frameRowSelect, #frameColumnSelect, #inputDirSelect, #portRowSelect').click(function(){
			$('.dropdown-toggle').blur();
			$('#layoutEditorCanvas').focus();
		});
	},

	init: function(param) {
		$.get('/detail/layoutEditor.html', function(data, success, type){
			$.get('/detail/testPattern.html', function(testPatternData, success, type){
				$('#layoutEditorModal .modal-content').append(testPatternData);
				modal.addEventHandlers();
			});

			var modal = LayoutEditorModal;
			addDetailModal(Locale.getText("layoutEditorModal"), 'Layout Editor', data);
			modal.config.canvas = document.getElementById("layoutEditorCanvas");
			modal.config.ctx = modal.config.canvas.getContext("2d");

			modal.config.canvas.width= 1920 * modal.config.scaleScreen;
			modal.config.canvas.height = 1080 * modal.config.scaleScreen;
			modal.config.ctx.scale(modal.config.scaleScreen, modal.config.scaleScreen);
			originalConfig = JSON.stringify(originalConfig);

			drawLayoutSelect();
			checkRangeUI('layoutZoomRange', 0, 4);

			if( navigator.userAgent.toLowerCase().indexOf("webkit") != -1 ){
				$('.cuttingPanel').css('height', 350);
			}else{
				$('#layoutZoomRangeDIV').css('margin-top', -6);
			}

			$('#layoutEditorModal #modalActionBtn').show();
			$('#layoutEditorModal #modalActionBtn').attr('disabled', true);
			$('#layoutEditorModal #modalactionName').text(Locale.getText("Save"));
	
			$('#layoutEditorModal').on('hidden.bs.modal', function () {
				modal.config.canvas.removeEventListener('mousedown', modal.mouseDown );
				modal.config.canvas.removeEventListener("mouseup", modal.mouseUp);
				modal.config.canvas.removeEventListener("mousemove", modal.mouseMove);
				document.removeEventListener("keydown", modal.keyup, false);
				hideToolTip();
			});

			if( param!=undefined ){
				modal.modal(param);
			}
		});
	},

	modal: function(param) {
		var modal = LayoutEditorModal;
		var config = modal.config;
		$('#layoutEditorModal').modal();
		clearInterval(overallStatusTimer);
		overallStatusTimer = undefined;
		getLedConfig(function(ret) {

			initLed(5, config);
			drawFromConfig(ret, config);

			config.resolX = ret.resolution.w;
			config.resolY = ret.resolution.h;
			config.maxX = ret.viewPort.w / ret.resolution.w;
			config.maxY = ret.viewPort.h / ret.resolution.h;
			config.physicalLayout = ret.physicalLayout;

			if(config.rectList.length == maxUnitCnt){
				$('#addUnitBtn').attr('disabled', true);
			}

			if(env.supportLedFilm){
				config.resolX = 32;
				config.resolY = 32;
			}
			drawRowColSelect(config);
			drawPitchSelect(config);
			drwaPortSelect(config);
			showPortUse(config);
			setDropButtonText('resolSelect', config.resolX + 'x' + config.resolY, true);
			$('#frameX').val(config.viewPort.x/16);
			$('#frameY').val(config.viewPort.y/16);
		});

		modal.config.canvas.addEventListener('mousedown', modal.mouseDown, false);
		modal.config.canvas.addEventListener("mouseup", modal.mouseUp, false);
		modal.config.canvas.addEventListener("mousemove", modal.mouseMove, false);
		modal.config.canvas.addEventListener("mouseout", modal.mouseOut, false);
		document.addEventListener("keydown", modal.keyup, false);
		if(env.supportLedFilm){
			$('.LEDSignageRes').hide();

			if( $('#portInfoPanel').css('display')!='none' )
				$('#portCancelBtn').click();

			if( $('#testPatternPanel').css('display')!='none' )
				$('#testDoneBtn').click();
		}

		if( param!=undefined ){

			setTimeout(function(){
				$('#testBtn').click();
			},500);
		}
	}
};