function isOn(unitID, config) {
	return unitID in config.onUnits;
}

function callLedFilmAPI(command, data, event, callback) {
	if (event && callback) {
		console.log('command here', command);
		var id = Math.round(Math.random() * 1000000);
		socket.on(event + id, function(msg) {
			callback(msg);
			socket.removeAllListeners(event + id);
		});
		data.eventID = id;
	}
	data.command = command;
	socket.emit("ledfilm", data);
}

function listDevices(callback) {
    callLedFilmAPI("listDevices", {}, "listDevices", callback);
}

function downloadApplication(params, callback) {
    callLedFilmAPI("downloadApplication", params, "downloadApplication", callback);
}

function upgradeApplication(params, callback) {
    callLedFilmAPI("upgradeApplication", params, "upgradeApplication", callback);
}

function installApplication(params, callback) {
	callLedFilmAPI("installApplication", params, "installApplication", callback);
}

function copyApplication(params, callback) {
	callLedFilmAPI("copyApplication", params, "copyApplication", callback);
}

function getFilmUnitConfig(callback) {
	callLedFilmAPI('getUnitConfig', {}, 'config', callback);
}

function setUnitConfig(config) {
	callLedFilmAPI('setUnitConfig', config, 'config');
}

function getDiskSpace(callback) {
	callLedFilmAPI('getDiskSpace', {}, 'getDiskSpace', callback);
}

function getNewLog(callback) {
	callLedFilmAPI('getNewLog', {}, 'getNewLog', callback);
}

function getUnitResolutionList(callback) {
	callLedFilmAPI('getUnitResolutionList', {}, 'config', callback);
}

function showPattern(slave, on, r, g, b) {
	callLedFilmAPI('showPattern', {
		slave: slave,
		on: on,
		r: r,
		g: g,
		b: b
	}, 'pattern');
}

function setSlaveAddr(numUnits, callback) {
	callLedFilmAPI('setSlaveAddr', {
		numUnits: numUnits
	}, 'config', callback);
}

function setPosition(slave, x, y, callback) {
	callLedFilmAPI('setPosition', {
		slave: slave,
		x: x,
		y: y
	}, 'config', callback);
}

function getPosition(slave, callback) {
	callLedFilmAPI('getPosition', {slave:slave}, 'getPosition', callback);
}

function setFilmPosition(slave, x, y, callback) {
	callLedFilmAPI('setPosition', {
		slave: slave,
		x: x,
		y: y
	}, 'config', callback);
}

function getGamma(slave, callback) {
	callLedFilmAPI('getGamma', {
		slave: slave
	}, 'unit', callback);
}

function setGamma(slave, gamma) {
	callLedFilmAPI('setGamma', {
		slave: slave,
		gamma: Number(gamma)
	}, 'unit');
}

function getGamut(slave, callback) {
	callLedFilmAPI('getGamut', {
		slave: slave
	}, 'unit', callback);
}

function setGamut(slave, array) {
	callLedFilmAPI('setGamut', {
		slave: slave,
		array: array
	}, 'unit');
}

function setGlobalBC(slave, bc){
	callLedFilmAPI('setGlobalBC', {
		slave: slave,
		bc: bc
	}, 'unit');
}

function getGlobalBC(slave, callback){
	callLedFilmAPI('getGlobalBC', {
		slave: slave
	}, 'globalBC', callback);
}

function setBC_CC(slave, bank, bc, r, g, b) {
	callLedFilmAPI('setBC_CC', {
		slave: slave,
		bank: bank,
		bc: bc,
		r: r,
		g: g,
		b: b
	}, 'module');
}

function getBC_CC(slave, bank, callback) {
	callLedFilmAPI('getBC_CC', {
		slave: slave,
		bank: bank
	}, 'module', callback);
}

function getLedFilmTempHumidity(fpgaNo, callback) {
	callLedFilmAPI('getTempHumidity', {
		fpgaNo: fpgaNo,
	}, 'unit', callback);
}

function getLedTemperature(slave, callback) {
	callLedFilmAPI('getTemperature', {
		slave: slave,
	}, 'unit', callback);
}

function getLedCurrent(slave, callback) {
	callLedFilmAPI('getCurrent', {
		slave: slave,
	}, 'unit', callback);
}


function getLedSignalStatus(slave, callback) {
	callLedFilmAPI('getSignalStatus', {
		slave: slave,
	}, 'unit', callback);
}

function getLedSignalState(fpgaNo, callback) {
	callLedFilmAPI('getSignalState', {
		fpgaNo: fpgaNo,
	}, 'unit', callback);
}

function getLedPowerState(fpgaNo, callback) {
	callLedFilmAPI('getPowerState', {
		fpgaNo: fpgaNo,
	}, 'unit', callback);
}

function getLedLOD(slave, callback) {
	callLedFilmAPI('getLOD', {
		slave: slave,
	}, 'unit', callback);
}

function getLedOverallStatus(slave, callback) {
	callLedFilmAPI('getOverallStatus', {
		slave: slave,
	}, 'unit', callback);
}

function saveConfig(slave) {
	callLedFilmAPI('saveConfig', {
		slave: slave
	}, 'config');
}

function loadConfig(slave) {
	callLedFilmAPI('loadConfig', {
		slave: slave
	}, 'config');
}

function setModuleCalibration(slave, row, col, data) {
	callLedFilmAPI('setModuleCalibration', {
		slave: slave,
		row : row,
		col : col,
		data: data
	}, 'module');
}

function saveModuleCalibration(slave) {
	callLedFilmAPI('saveModuleCalibration', {
		slave: slave
	}, 'module');
}

function reloadModuleCalibration(slave) {
	callLedFilmAPI('reloadModuleCalibration', {
		slave: slave
	}, 'module');
}

function getModuleCalibration(slave, row, col, callback) {
	callLedFilmAPI('getModuleCalibration', {
		slave: slave,
		row : row,
		col : col
	}, 'module', callback);
}

function saveCalibrationData(slave, bank) {
	callLedFilmAPI('saveCalibrationData', {
		slave: slave,
		bank: bank
	}, 'module');
}

function loadCalibrationData(slave, from, callback) {
	callLedFilmAPI('loadCalibrationData', {
		slave: slave,
		from: from
	}, 'module', callback);
}

function saveConfigAllFpga(slave, bank) {
	callLedFilmAPI('saveConfigAllFpga', {}, 'ConfigAllFpga');
}



function isOn(unitID, config) {
	return unitID in config.onUnits;
}

function ledFpgaUpdate(fileName, callback) {
	callLedFilmAPI("ledFpgaUpdate", {
		fileName: fileName
	}, 'ledFpga', callback);
}

function getFpgaVersion(numUnits, callback) {
	callLedFilmAPI("getFpgaVersion", {
		numUnits: numUnits
	}, 'ledFpga', callback);
}

function getFpgaUpdatePercent(callback) {
	callLedFilmAPI("getFpgaUpdatePercent", {
	}, 'ledFpga', callback);
}

function getFPGAUpdateErrorStatus(callback) {
	callLedFilmAPI("getFPGAUpdateErrorStatus", {
	}, 'ledFpga', callback);
}

function initConfigFpga(slave, callback) {
	callLedFilmAPI("initConfigFpga", {
		slave: slave
	}, 'ledFpga', callback);
}

function rebootFpga(slave, callback) {
	callLedFilmAPI("rebootFpga", {
		slave: slave
	}, 'ledFpga', callback);
}

function getFpgaCheckSum(name, callback) {
	callLedFilmAPI("getFpgaCheckSum", {
		name: name
	}, 'ledFpga', callback);
}

function startLiveLOD(start, callback) {
	callLedFilmAPI('liveLOD', { start: start }, 'liveLOD', callback);
}

function liveLOD(callback) {
	callLedFilmAPI('liveLOD', {}, 'liveLOD', callback);
}

function setPMControl(onOff) {
	callLedFilmAPI('setPMControl', {
		onOff: onOff
	}, 'factory');
}

function setInputDirection(slave, inputDirection) {
	callLedFilmAPI('setInputDirection', {
		slave: slave,
		inputDirection: inputDirection
	}, 'inputDirection');
}


function setWidthHeight(slave, width, height) {
	callLedFilmAPI('setWidthHeight', {
		slave: slave,
		width: width,
		height: height
	}, 'setWidthHeight');
}

function setDisplayPortSel(slave, displayPort) {
	callLedFilmAPI('setDisplayPortSel', {
		slave: slave,
		displayPort: displayPort
	}, 'setDisplayPortSel');
}

function getAutoDetect(callback) {
	callLedFilmAPI('getAutoDetect', {}, 'getAutoDetect', callback);
}

function getInputDirection(slave, callback) {
	callLedFilmAPI('getInputDirection', {slave:slave}, 'getInputDirection', callback);
}

function getWidthHeight(slave, callback) {
	callLedFilmAPI('getWidthHeight', {slave:slave}, 'getWidthHeight', callback);
}

function getDisplayPortSel(slave, callback) {
	callLedFilmAPI('getDisplayPortSel', {slave:slave}, 'getDisplayPortSel', callback);
}

function getTrim(slave, callback) {
	callLedFilmAPI('getTrim', {slave:slave}, 'getTrim', callback);
}

function setTrim(slave, width, height) {
	callLedFilmAPI('setDisplayPortSel', {
		slave: slave,
		width: width,
		height: height
	}, 'setTrim');
}

function firstUseDone(callback){
	callLedFilmAPI('firstUseDone', {}, 'firstUseDone', callback);
}

function ledTypeDone(callback){
	callLedFilmAPI('ledTypeDone', {}, 'ledTypeDone', callback);
}

function saveFirstUseFile(selectVal, callback){
	callLedFilmAPI('saveFirstUseFile', {selectVal:selectVal}, 'saveFirstUseFile', callback);
}

function saveLEDTypeFile(ledType, callback){
	callLedFilmAPI('saveLEDTypeFile', {ledType:ledType}, 'saveLEDTypeFile', callback);
}



// for Color Film
function getReceivedMac(slave, callback){
	callLedFilmAPI('getReceivedMac', {slave:slave}, 'getReceivedMac', callback);
}

function getSendingMAC(slave, callback){
	callLedFilmAPI('getSendingMAC', {slave:slave}, 'getSendingMAC', callback);
}

function getTempHumidityColor(senderIdx, receiverIdx, callback){
	callLedFilmAPI('getTempHumidityColor', {
		senderIdx:senderIdx,
		receiverIdx:receiverIdx
	}, 'getTempHumidityColor', callback);
}

function getFpgaStatus(index, callback){
	callLedFilmAPI('getFpgaStatus', {
		index:index
	}, 'getFpgaStatus', callback);
}

function getReceiverStatus(slave, rcvIdx, callback){
	callLedFilmAPI('getReceiverStatus', {
		slave : slave,
		rcvIdx:rcvIdx
	}, 'getReceiverStatus', callback);
}

function getFilmGain(slave, rcvIdx, filmIdx, callback){
	console.log('getFilmGain::'+slave+', '+rcvIdx+', '+filmIdx);
	callLedFilmAPI('getFilmGain', {
		slave:slave,
		rcvIdx:rcvIdx,
		filmIdx:filmIdx
	}, 'getFilmGain', callback);
}

function setSendingMAC(slave, rcvIdx, mac5th, mac6th) {
	callLedFilmAPI('setSendingMAC', {
		slave: slave,
		rcvIdx: rcvIdx,
		mac5th: mac5th,
		mac6th: mac6th
	}, 'setSendingMAC');
}

function clearSendingMAC(slave) {
	callLedFilmAPI('clearSendingMAC', {
		slave: slave
	}, 'clearSendingMAC');
}


function setSenderPos(slave, hValue, vValue) {
	callLedFilmAPI('setSenderPos', {
		slave: slave,
		hValue: hValue,
		vValue: vValue
	}, 'setSenderPos');
}

function setXYPos(slave, rcvIdx, xValue, yValue) {
	callLedFilmAPI('setXYPos', {
		slave: slave,
		rcvIdx: rcvIdx,
		xValue: xValue,
		yValue: yValue
	}, 'setXYPos');
}


function setInputDirectionColor(slave, inputDirection) {
	callLedFilmAPI('setInputDirectionColor', {
		slave: slave,
		inputDirection: inputDirection
	}, 'setInputDirectionColor');
}

function setDisplayPort(slave, rcvIdx, value) {
	callLedFilmAPI('setDisplayPort', {
		slave: slave,
		rcvIdx: rcvIdx,
		value: value
	}, 'setDisplayPort');
}

function setTrimColor(slave, rcvIdx, left, right) {
	callLedFilmAPI('setTrimColor', {
		slave: slave,
		rcvIdx: rcvIdx,
		left: left,
		right: right
	}, 'setTrimColor');
}

function setTestPattern(slave, onOff, patternR, patternG, patternB, mac5th, mac6th) {
	callLedFilmAPI('setTestPattern', {
		slave : slave,
		onOff : onOff,
		patternR : patternR,
		patternG : patternG,
		patternB : patternB,
		mac5th : mac5th,
		mac6th : mac6th
	}, 'setTestPattern');
}

function setHVNum(slave, rcvIdx, hValue, vValue) {
	callLedFilmAPI('setHVNum', {
		slave : slave,
		rcvIdx : rcvIdx,
		hValue : hValue,
		vValue : vValue
	}, 'setHVNum');
}

function setReceiverNum(slave, receiverNum) {
	callLedFilmAPI('setReceiverNum', {
		slave : slave,
		receiverNum : receiverNum
	}, 'setReceiverNum');
}

function setIP(slave, ip1st, ip2nd, ip3th, ip4th, serverIP, callback) {
	callLedFilmAPI('setIP', {
		slave : slave,
		ip1st : ip1st,
		ip2nd : ip2nd,
		ip3th : ip3th,
		ip4th : ip4th,
		serverIP : serverIP
	}, 'setIP', callback);
}

function setFilmGain(slave, rcvIdx, filmIdx, valueR, valueG, valueB) {
	callLedFilmAPI('setFilmGain', {
		slave : slave,
		rcvIdx : rcvIdx,
		filmIdx : filmIdx,
		valueR : valueR,
		valueG : valueG,
		valueB : valueB
	}, 'setFilmGain');
}

function saveFPGAConfig() {
	for(var i=0; i<4; i++){
		callLedFilmAPI('saveFPGAConfig', {
			slave : (i+1)*2,
			index : 2,
			value : 1
		}, 'saveFPGAConfig');
	}
}

function FPGAReset(slave, index, value, callback) {
	callLedFilmAPI('FPGAReset', {
		slave : slave,
		index : index,
		value : value
	}, 'FPGAReset', callback);
}

function execTFTP(callback){
	callLedFilmAPI('execTFTP', {}, 'execTFTP', callback);
}

function killTFTP(pid, callback){
	callLedFilmAPI('killTFTP', {pid:pid}, 'killTFTP', callback);
}

function moveUpdateFile(fileName){
	callLedFilmAPI('moveUpdateFile', {fileName : fileName}, 'moveUpdateFile');
}

function getFpgaVersionColor(slave, callback){
	callLedFilmAPI('getFpgaVersionColor', {slave:slave}, 'getFpgaVersionColor', callback);
}

function getPowerStatusColor(index, callback){
	callLedFilmAPI('getPowerStatusColor', {slave:slave}, 'getPowerStatusColor', callback);
}

function getUpgradeStatus(slave, callback){
	callLedFilmAPI('getUpgradeStatus', {slave:slave}, 'getUpgradeStatus', callback);
}

function setFpgaControl(slave, index, value, callback) {
	callLedFilmAPI('setFpgaControl', {
		slave : slave,
		index : index,
		value : value
	}, 'setFpgaControl', callback);
}

function saveFilmGain(slave, callback) {
	callLedFilmAPI('setFpgaControl', {
		slave : slave,
		index : 4,
		value : 1
	}, 'setFpgaControl', callback);
}

function loadFilmGain(slave, callback) {
	callLedFilmAPI('setFpgaControl', {
		slave : slave,
		index : 6,
		value : 1
	}, 'setFpgaControl', callback);
}

function getP14Status (callback) {
	callLedFilmAPI('getP14Status', {}, 'getP14Status', callback);
}