var currentVer = "";
var updatePercent = 0;
var updateStarted = false;

var updateMsg = "";

var updateFileName = undefined;

document.addEventListener("DOMContentLoaded", function() {
    // init UI change
    var head = $('head');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/control_input.css">');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/popup_software_common.css">');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/my_control_common.css">');
    $('.content-container').addClass('wide-padding');
    var menu2 = $('#menu-2-2');
    menu2.removeClass('active');
    menu2.css("display", "none");
    var menu1 = $('#menu-2-1');
    menu1.addClass('active');
    menu1.css("display", "block");
    $('.lnb-box').css("display", "block");
    $('#ROOT_CONTAINER').removeClass('dashboard');

    drawProgressModal('upload');
    drawProgressModal('micom');
    drawProgressModal('update');

    addAlertModals('update-warning-modal', '');
    // drawCompleteModal(true);
    drawCompleteModal('update');

    $('#micomUpdate').on('click', function () {
        var isChecked = $(this).prop('checked');
        $(this).attr('aria-checked', isChecked);
    });

});

function init(env) {
    updateMsg = getLanguageText(languageTable, "Please Keep this Signage on.") + "<br>";
    updateMsg += getLanguageText(languageTable, "The POWER remote controller key will not work during the update.") + "<br>";
    updateMsg += getLanguageText(languageTable, "Do not reboot in Device page.");


    addConfirmModals('.delete', 'deleteModal', getLanguageText(languageTable, "Delete?"), '', false, function () {
        deleteFile("update", updateFileName, function () {
            window.location.reload(true);
        });
    });

    socket.on('swupdate', function (msg) {
        var micomProgress = -1;
        if (!updateStarted) {
            getMicomUpdateProgress(function (msg) {
                $('#micom-popup-label').html(getLanguageText(languageTable, 'Micom Progress').replace('Micom', msg.type));
                $('#micom-content').html(updateMsg);
                $('#micom-field-label').html(getLanguageText(languageTable, 'Micom Update'));
                if (micomProgress === -1) {
                    // $('.progress-bar').removeClass("active progress-bar-striped");
                    $('.state-bar').css('width', '0%');
                    _closePopup('popup-update');
                    showMyModal('popup-micom');
                    // $('#update-prog').hide();
                    // $('#micom-prog').show();
                }

                micomProgress = msg.progress;

                $('#micom-percent').html(msg.progress + "%");
                $('#micom-state-bar').css('width', msg.progress + '%');

                // $('#overlay_progress').html(msg.type + " " + getLanguageText(languageTable, "Update") + " : " + msg.progress + "%");
            });
        }

        updateStarted = true;

        if (updatePercent > 0 && msg === 0) {
            // overlayControl(false);
            showMyModal('update-warning-modal', getLanguageText(languageTable, "Fail to update. Check xxx file please.").replace(/xxx/gi, updateFileName));
            return;
        }

        if (updatePercent > 0 && updatePercent === msg) {
            return;
        }

        updatePercent = msg;

        _closePopup('popup-micom');

        $('#update-popup-label').html(getLanguageText(languageTable, 'S/W Update'));
        $('#update-content').html(updateMsg);
        $('#update-field-label').html(getLanguageText(languageTable, 'Update'))

        showMyModal('popup-update');
        $('#update-percent').html(updatePercent + "%");
        $('#update-state-bar').css('width', updatePercent + '%');
        // $('#overlay_progress').html(msg + "%");

        if (msg === 100) {
            // var updateCompletedMsg = getLanguageText(languageTable, "Update completed. Signage is rebooting now.") + "<br>" + getLanguageText(languageTable, "Please login again.");
            // overlayControl(false);
            // overlayControl(true, updateCompletedMsg);
            _closePopup('popup-upload');
            _closePopup('popup-micom');
            _closePopup('popup-update');
            _openPopup('popup-update-complete');

            setTimeout(function () {
                setSWUpdateStatus(false);
                window.location.replace("/login");
            }, 2 * 60 * 1000);
        }
    });

    addConfirmModals('.update', 'updateModal', getLanguageText(languageTable, "Update?"), '', false, function () {
        var micomUpdate = $('#micomUpdate').prop('checked');

        updatePercent = 0;
        updateStarted = false;

        swupdate(updateFileName, micomUpdate, function (m) {
            if (m.returnValue) {
                setSWUpdateStatus(true);
            }
        });

        setTimeout(function () {
            getUpdateStatus(function (msg) {
                if (msg.status !== 'in progress' && !updateStarted) {
                    // overlayControl(false);
                    showMyModal('update-warning-modal', getLanguageText(languageTable, "Cannot start S/W update. Check xxx file please.").replace(/xxx/gi, updateFileName));
                    // showWarning(getLanguageText(languageTable, "Cannot start S/W update. Check xxx file please.").replace(/xxx/gi, updateFileName));
                }
            });
        }, 10 * 1000);
    });

    if (env.supportMicomFanControl) {
        addConfirmModals('', 'micomUpdateModal', getLanguageText(languageTable, "Fan Micom Update?"), '', function () {
            fanMicomUpdate(updateFileName, function (msg) {
                var warningMsg = "";
                if (msg.returnValue) {
                    warningMsg = getLanguageText(languageTable, "Update fan micom with xxx done").replace(/xxx/gi, updateFileName);
                } else {
                    warningMsg = getLanguageText(languageTable, "Failed to update fan micom with xxx. Check the file please").replace(/xxx/gi, updateFileName);
                }
                showMyModal('update-warning-modal', warningMsg);
                // showWarning(warningMsg, 10 * 1000);
                // overlayControl(false);
            });
        });

        env.uploadedFiles.forEach(function (file) {
            var ext = file.substr(file.length - 3, 3).toLowerCase();
            var button = $(".update[name='" + file + "']");
            if (ext === 'txt') {
                button.prop('disabled', false);
                button.removeClass('update');
                button.addClass('fanMicomUpdate');
            }
        });

        $('.fanMicomUpdate').on('click', function () {
            updateFileName = $(this).attr('name');
            var micomText = $('#micomUpdateModal-text-content');
            micomText.html(getLanguageText(languageTable, "Update fan micom with this S/W Update file?") + "<br>" + updateFileName);
            micomText.css('word-break', 'break-all');
            showMyModal('micomUpdateModal');
        });
    }

    getBasicInfo(function (msg) {
        var {firmwareVersion: firmwareVersion} = msg;
        currentVer = firmwareVersion;
    });

    listUpdates(function (msg) {
        var {fileinfo: fileinfo} = msg;
        if (!msg || !msg.returnValue) {
            return;
        }

        fileinfo.forEach(function (file) {
            var button = $(".update[name='" + file.name + "']");
            var newVersion = file.version.toString(16);
            newVersion = '0' + newVersion.substring(0, 1) + '.' + newVersion.substring(1, 3) + '.' + newVersion.substring(3, 5);
            button.prop('id', newVersion);
            button.prop('disabled', false);
            $('.file-info[name="' + file.name + '"]').append('(' + newVersion + ')');

            var curVerInteger = parseInt(currentVer.replace(/[.]/g, ""));
            var newVerInteger = parseInt(newVersion.replace(/[.]/g, ""));

            if (isNaN(curVerInteger)) {
                curVerInteger = 0;
            }

            var elem = $('.file-list[name="' + file.name + '"]');
            if (curVerInteger === newVerInteger) {
                elem.addClass('bg-gray');
                $('.btn-update[name="' + file.name + '"]').css("visibility", "hidden");
            } else if (curVerInteger > newVerInteger) {
                elem.removeClass('bg-gray');
            }
        });
    });

    $('.file-area :file').on('change', function () {
        var file = document.getElementById('upload').files[0];
        var fileName = filteringXSS(file.name);

        var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
        if (supportExt.indexOf(ext) < 0) {
            showMyModal('update-warning-modal', getLanguageText(languageTable, "Unsupported File type."));
            resetInputFile($(this));
            return;
        }

        // check maxsize
        if (file.size > env.maxSize) {
            showMyModal('update-warning-modal', getLanguageText(languageTable, "Flash memory is not enough."));
            resetInputFile($(this));
            return;
        }

        $('#fileName').html(fileName);
        $('#upload-submit').css("display", "inline-block");
        $('#upload-label').css("display", "none");
    });

    $(".update").on('click',function () {
        updateFileName = $(this).attr('name');
        var newVer = $(this).attr('id');
        if (newVer === undefined) {
            newVer = '-';
        }
        var modalMsg = getLanguageText(languageTable, "Do you want to install this S/W Update file?") + "<br>";
        modalMsg += "(" + getLanguageText(languageTable, "Newer Ver.") + ") " + newVer + "<br>";
        modalMsg += "(" + getLanguageText(languageTable, "Current Ver.") + ") " + currentVer + "<br>";

        showMyModal('updateModal', modalMsg);
    });

    $(".delete").on('click',function () {
        updateFileName = $(this).attr('name');
        showMyModal('deleteModal', getLanguageText(languageTable, "Do you want to delete this S/W Update file?") + "<br>" + updateFileName);
        // $('#deleteModalContent').css('word-break', 'break-all');
        // $('#deleteModal').modal();
    });
    // here

    function uploadFile(file) {
        var formData = new FormData();
        var newname = filteringXSS(file.name); //filter for XSS

        formData.append('target', 'update');
        formData.append('newname', newname);
        formData.append('update', file);

        $('#upload-popup-label').html(getLanguageText(languageTable, 'Upload'));
        $('#upload-content').html('');
        $('#upload-field-label').html(getLanguageText(languageTable, 'Uploading'));
        showMyModal('popup-upload');
        // overlayControl(true, getLanguageText(languageTable, "Uploading") + "\n" + newname);
        document.onkeydown = function (e) {
            return false;
        }

        $('input').prop('disabled', true);
        $('button').prop('disabled', true);

        var xhr = new XMLHttpRequest();

        xhr.open('post', '/upload', true);
        //xhr.setRequestHeader("Content-type","multipart/form-data");

        xhr.upload.onprogress = function (e) {
            if (e.lengthComputable) {
                var per = Math.round((e.loaded / e.total) * 100);
                $('#upload-percent').html(per + "%");
                $('#upload-state-bar').css('width', per + '%');
                // $('#overlay_progress').html(per + "%");
            }
        };

        xhr.upload.onload = function (e) {
            setTimeout(function () {
                // overlayControl(false);
                window.location.reload(true);
            }, 2000);
        }

        xhr.onerror = function (e) {
            // overlayControl(false);
            showMyModal('update-warning-modal', getLanguageText(languageTable, "An error occurred while submitting the form."));
        };

        xhr.onload = function () {
            console.log(this.statusText);
        };

        xhr.send(formData);
    }

    $('#upload-confirm').on('click', function () {
        // evt.preventDefault();
        var file = document.getElementById('upload').files[0];

        if (!file) {
            showMyModal('update-warning-modal', getLanguageText(languageTable, "Please choose a file to upload"));
            return;
        }

        var fileName = filteringXSS(file.name);
        if (env.uploadedFiles.indexOf(fileName) >= 0) {
            var modalTitle = getLanguageText(languageTable, "Overwrite?");
            var modalMsg = getLanguageText(languageTable, "Overwrite xxx?").replace(/xxx/gi, fileName);
            addConfirmModals('', "overwrite", modalTitle, modalMsg, false,
                function () {
                    setTimeout(function () {
                        uploadFile(file);
                    }, 500);
                    _closePopup('overwrite');
                    // $('#overwrite').close();
                    $('#overwrite').remove();
                }, function () {
                    resetInputFile($('.file-area :file'));
                }
            );
            showMyModal('overwrite');
            // $('#overwrite').modal();
            return;
        }

        uploadFile(file);
    });

    _closePopup('popup-upload');
    _closePopup('popup-micom');
    _closePopup('popup-update');

    displayLoading(false);

    if (locale === "ar-SA") {
        changeRtl();
    }
}

function resetInputFile(elm) {
    if (getInternetExplorerVersion() > -1) {
        elm.attr('type', 'radio');
        elm.attr('type', 'file');
    } else {
        elm.val("");
    }

    $('#fileName').html(getLanguageText(languageTable, 'Select a file to upload'));
    $('#upload-label').css('display', "inline-block");
    $('#upload-submit').css('display', "none");
}

// function changeRtl() {
//     $(".table>tbody>tr>th").css({"text-align": "start"});
//     $(".table-update-file .file-info").attr("dir", "ltr");
//     $(".table-update-file .file-info").css("text-align", "end");
// }