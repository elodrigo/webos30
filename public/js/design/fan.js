document.addEventListener("DOMContentLoaded", function() {
    // init UI change
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/control_info_state_fan.css">');
    $('.content-container').addClass('wide-padding');
    var menu1 = $('#menu-2-1');
    menu1.removeClass('active');
    menu1.css("display", "none");
    var menu2 = $('#menu-2-2');
    menu2.addClass('active');
    menu2.css("display", "block");
    $('.lnb-box').css("display", "block");
    $('#ROOT_CONTAINER').removeClass('dashboard');

    displayLoading(false);

    $('.side-logo').css('top', '35px');
});


function refreshSensor() {
    updateFanStatus();
    updateTemperature();
}

function updateFanStatus() {
    getFanStatus(function (res) {
        var types = res.group;
        types.forEach(function (type) {
            var fan = res[type];
            fan.status.forEach(function (status, index) {
                var rpm = ' - ';
                var selector = '#' + type + (index + 1) + 'rpm';
                if (status) {
                    rpm = fan.rpm[index];
                    $(selector).removeClass('color-point');
                } else {
                    rpm = 'Not OK';
                    $(selector).addClass('color-point');
                }
                $(selector).text(rpm);
            });
        });
    });
}

function updateTemperature() {
    getTemp(function (res) {
        var types = res.group ? res.group : [];
        types.forEach(function (type) {
            var sensor = res[type];
            var temperature = sensor.celsius;

            readTempType(function (tempType) {
                if (!tempType.isCelsius) {
                    $('#' + type + 'Temp').html(Math.floor((temperature) * 1.8 + 32) + '°F');
                } else {
                    $('#' + type + 'Temp').html(Number(temperature) + '°C');
                }
            });
        });
    });


}