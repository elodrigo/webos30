var ampm = '';
var continent = '';
var country = '';
var city = '';
var setAuto = false;
var var_dstStartMonth = '';
var var_dstStartWeek = '';
var var_dstStartWeekday = '';
var var_dstStartHour = '';
var var_dstEndMonth = '';
var var_dstEndWeek = '';
var var_dstEndWeekday = '';
var var_dstEndHour = '';
var timeZoneInited = false;
var isCityState = false;
var agent = navigator.userAgent.toLowerCase();

var ampmName = {
    am: getLanguageText(languageTable, "AM"),
    pm: getLanguageText(languageTable, "PM")
};

var continentNameList = ["Africa", "Asia", "CIS", "Europe", "MiddleEast", "NorthAmerica", "Oceania",
    "Pacific", "SouthAmerica"];

var continentNameMap = {
    Africa: getLanguageText(languageTable, "Africa"),
    Asia: getLanguageText(languageTable, "Asia"),
    CIS: getLanguageText(languageTable, "CIS"),
    Europe: getLanguageText(languageTable, "Europe"),
    MiddleEast: getLanguageText(languageTable, "Middle East"),
    NorthAmerica: getLanguageText(languageTable, "North America"),
    Oceania: getLanguageText(languageTable, "Oceania"),
    Pacific: getLanguageText(languageTable, "Pacific"),
    SouthAmerica: getLanguageText(languageTable, "South America")
};

var monthList = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
// var monthMap = new Map();
var monthMap = {
    1: "1", 2: "2", 3: "3", 4: "4", 5: "5", 6: "6", 7: "7", 8: "8", 9: "9", 10: "10", 11: "11", 12: "12"
};

var weekList = ["1", "2", "3", "4", "5"];
var weekMap = {
    "1": getLanguageText(languageTable, "1st"),
    "2": getLanguageText(languageTable, "2nd"),
    "3": getLanguageText(languageTable, "3rd"),
    "4": getLanguageText(languageTable, "4th"),
    "5": getLanguageText(languageTable, "Last")
};

var weekdayList = ["0", "1", "2", "3", "4", "5", "6"];
var weekdayMap = {
    "0": getLanguageText(languageTable, "Sunday"),
    "1": getLanguageText(languageTable, "Monday"),
    "2": getLanguageText(languageTable, "Tuesday"),
    "3": getLanguageText(languageTable, "Wednesday"),
    "4": getLanguageText(languageTable, "Thursday"),
    "5": getLanguageText(languageTable, "Friday"),
    "6": getLanguageText(languageTable, "Saturday")
};

var hourList = [];
var hourMap = new Map();

var timeZoneContainer = document.getElementById('applyTimeZoneModal');
var timeZoneFocusTrap =  focusTrap.createFocusTrap('applyTimeZoneModal', {
    onActivate: function () {timeZoneContainer.classList.add('is-active')},
    onDeactivate: function () {timeZoneContainer.classList.remove('is-active')},
    escapeDeactivates: false,
});
var timeContainer = document.getElementById('applyTimeModal');
var timeFocusTrap =  focusTrap.createFocusTrap('applyTimeModal', {
    onActivate: function () {timeContainer.classList.add('is-active')},
    onDeactivate: function () {timeContainer.classList.remove('is-active')},
    escapeDeactivates: false,
});
var timeErrorContainer = document.getElementById('applyTimeErrorModal');
var timeErrorFocusTrap =  focusTrap.createFocusTrap('applyTimeErrorModal', {
    onActivate: function () {timeErrorContainer.classList.add('is-active')},
    onDeactivate: function () {timeErrorContainer.classList.remove('is-active')},
    escapeDeactivates: false,
});

document.addEventListener("DOMContentLoaded", function() {
    // init UI change
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/control_time.css">');
    $('.content-container').addClass('wide-padding');
    var menu2 = $('#menu-2-2');
    menu2.removeClass('active');
    menu2.css("display", "none");
    var menu1 = $('#menu-2-1');
    menu1.addClass('active');
    menu1.css("display", "block");
    $('.lnb-box').css("display", "block");
    $('#ROOT_CONTAINER').removeClass('dashboard');

    addAlertModals('applyTimeErrorModal', 'Input time correctly', function () {
        timeErrorFocusTrap.deactivate();
    });

    getNTPStatus(function (ret) {
       var {networkTimeSource: networkTimeSource, useNetworkTime: useNetworkTime} = ret;
       if (networkTimeSource) {
           $('#useSNTP').attr('checked', networkTimeSource.useSNTP);
       }

       $('#useNTP').prop('checked', useNetworkTime);
       checkDisable();
       if (useNetworkTime && !timeZoneInited) {
           initTimeZone();
           timeZoneInited = true;
       }
    });

    $('#useNTP').on('change', function () {
        checkDisable();
        var useNTP = $('#useNTP');
        var useNTPChecked = useNTP.prop('checked');
        $('#useNTP').attr('aria-checked', useNTPChecked);
        var param = {useNTP: useNTPChecked};
        useNTP.prop('disabled', true);
        setNTPStatus(param, function (ret) {
            useNTP.prop('disabled', false);
            if (!timeZoneInited) {
                initTimeZone();
                timeZoneInited = true;
            }

            getDSTInfo(function (dstInfo) {
                var useDst = dstInfo.dstMode === "on";
                $('#useDST').prop('checked', useDst ? 'true' : 'false');
                $('.timing-saving').css('display', useDst ? 'block' : 'none');
            });
            showSuccess('time-setting-success');
        });
    });

    $('#useDST').on('change', function () {
        var useDST = $('#useDST');
        var useDSTChecked = useDST.prop('checked');
        $('#useDST').attr('aria-checked', useDSTChecked);
        $('.timing-saving').css('display', useDSTChecked ? 'block' : 'none');
        setDisableControlElement(true);
        setDstOnOff(useDSTChecked ? 'on' : 'off', function () {
            initCurrentTime(function () {
                setDisableControlElement(false);
            });
        });
    });

    $('#time-apply').on('click', function () {
        var isValid = checkDateValid();
        if (!isValid[0]) {
            initCurrentTime();
            showMyModal('applyTimeErrorModal', isValid[1]);
            timeErrorFocusTrap.activate();
            return;
        }
        if ($("#useNTP").prop('checked')) {
            // timezone
            if (country === undefined) {
                showMyModal('applyTimeErrorModal', getLanguageText(languageTable, "Please select country"));
                timeErrorFocusTrap.activate();
                return;
            } else if (city === undefined) {
                showMyModal('applyTimeErrorModal', getLanguageText(languageTable, "Please select city"));
                timeErrorFocusTrap.activate();
                return;
            }
            togglePopup('time-apply', 'applyTimeZoneModal');
            timeZoneFocusTrap.activate();
            // time
        } else {
            togglePopup('time-apply', 'applyTimeModal');
            timeFocusTrap.activate();
        }
        showSuccess('time-date-success');
    });

    initCurrentTime();

    drawAmpm();
    makeKeyDownDropdown("ampm-dropdown");
    initDST();

    displayLoading(false);

    if (locale === "ar-SA") {
        changeRtl();
    }

    makeKeyDownDropdown("continent-dropdown");
    makeKeyDownDropdown("country-dropdown");
    makeKeyDownDropdown("city-dropdown");
    makeKeyDownDropdown("summer-start-week");
    makeKeyDownDropdown("summer-start-weekday");
    makeKeyDownDropdown("summer-start-month");
    makeKeyDownDropdown("summer-start-hour");
    makeKeyDownDropdown("summer-end-week");
    makeKeyDownDropdown("summer-end-weekday");
    makeKeyDownDropdown("summer-end-month");
    makeKeyDownDropdown("summer-end-hour");

    $('.side-logo').css('top', '35px');

});

function timeModalOK() {
    setDisableControlElement(true);
    var hourVal = $('#label-hour').val();
    var currentHour = ampm === "am" ? hourVal : parseInt(hourVal) + 12;
    if (hourVal === 12) {
        currentHour -= 12;
    }

    var timeInfo = {
        hour: addZero(currentHour, 2),
        minute: addZero($('#label-minute').val(), 2),
        year: $('#label-year').val(),
        month: addZero($('#label-month').val(), 2),
        day: addZero($('#label-date').val(), 2)
    };
    setCurrentTime(timeInfo);

    if ($("#useDST").prop('checked')) {
        var Month = var_dstStartMonth;
        var Week = var_dstStartWeek;
        var Weekday = var_dstStartWeekday;
        var Hour = var_dstStartHour;
        setDstStartTime(Month, Week, Weekday, Hour);

        Month = var_dstEndMonth;
        Week = var_dstEndWeek;
        Weekday = var_dstEndWeekday;
        Hour = var_dstEndHour;
        setDstEndTime(Month, Week, Weekday, Hour);
    }

    timeFocusTrap.deactivate();
    _closePopup('applyTimeModal');

    setTimeout(function () {
        initCurrentTime(function () {
            setDisableControlElement(false);
        });
    }, 500);
}

function timeModalCancel() {
    initCurrentTime();
    if ($("#useDST").prop('checked')) {
        initDST();
    }
    timeFocusTrap.deactivate();
    _closePopup('applyTimeModal');
}

function timeZoneModalOK() {
    setContinent(continent, function (msg) {
    });

    setCountry(country, function (msg) {
    });

    getCityList(country, function (msg) {
        var timeZone = undefined;

        msg.forEach(function (item) {
            var {City: City, Country: Country} = item;
            if (!isCityState) {
                if (City === city) {
                    timeZone = item;
                }
            } else {
                if (Country === city) {
                    timeZone = item;
                }
            }
        });

        setCity(timeZone, function (msg) {
        });
    });
    timeZoneFocusTrap.deactivate();
    _closePopup('applyTimeZoneModal');
}

function timeZoneModalCancel() {
    initTimeZone();
    timeZoneFocusTrap.deactivate();
    _closePopup('applyTimeZoneModal');
}

function timeErrorOK() {
    timeErrorFocusTrap.deactivate();
    _closePopup('applyTimeErrorModal');
}

function checkDisable() {
    var useNTP = $("#useNTP").prop('checked');
    setAuto = useNTP;
    var controlTime = $('#control-time');
    var controlTimezone = $('#control-timezone');
    if (useNTP) {
        controlTime.css('display', 'none');
        controlTimezone.css('display', 'block');
    } else {
        controlTime.css('display', 'block');
        controlTimezone.css('display', 'none');
        initCurrentTime();
    }
}

function drawAmpm() {
    var ampmDropdown = $('#ampm-dropdown');
    var dropdownMenu = $(ampmDropdown).children(".dropdown-menu");
    var ampmValList = ['am', 'pm'];

    var ul = createDropdownChildren(ampmValList, ampmName, 'ampm-dropdown');

    var lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            ampm = ampmValList[i];
            var ariaId = 'option-ampm-' + ampmValList[i]
            changeDropdownLabel("ampm-dropdown", ampmName[ampmValList[i]], ariaId);
            changeDropdownAria("ampm-dropdown", i);
        });
    });

    $(dropdownMenu).html(ul);
}


function initTimeZone() {
    getlocaleContinent(function (ret) {
        continent = ret.localeContinent;
        drawContinent(continent);
    });

    getlocaleCountry(function (ret) {
        country = ret.localeCountry;
        drawCountry(continent, country);

        getTimeZone(function (timeZone) {
            drawCity(country, timeZone);
        });
    });
}

function drawContinent(defaultVal) {
    var continentDropdown = $('#continent-dropdown');
    var dropdownMenu = $(continentDropdown).children(".dropdown-menu");

    var ul = createDropdownChildren(continentNameList, continentNameMap, 'continent-dropdown');

    if (locale === "ar-SA") {
        dropdownMenu.find(".dropdown-menu").css("right", 0);
    }

    var lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            continent = continentNameList[i];
            var ariaId = 'option-continent-' + continentNameList[i];
            changeDropdownLabel("continent-dropdown", continentNameMap[continentNameList[i]], ariaId);
            changeDropdownAria("continent-dropdown", i);
            drawCountry(continentNameList[i]);
        });
    });

    $(dropdownMenu).html(ul);
    var myIndex = continentNameList.findIndex(function (item) {return item === defaultVal});
    var ariaDefaultId = 'option-continent-' + continentNameList[myIndex]
    changeDropdownLabel("continent-dropdown", continentNameMap[defaultVal], ariaDefaultId);
    changeDropdownAria("continent-dropdown", myIndex);
}

function drawCountry(continent, defaultVal) {
    var countryDropdown = $('#country-dropdown');
    var dropdownMenu = $(countryDropdown).children(".dropdown-menu");

    getCountryList(continent, function (msg) {
        var countryNameList =[];
        var countryNameMap = new Map();
        msg.forEach(function (item) {
            var {shortName: shortName, fullName: fullName} = item;
            countryNameList.push(shortName);
            countryNameMap.set(shortName, getLanguageText(languageTable, fullName));
        });

        var ul = createDropdownChildren(countryNameList, countryNameMap, 'country-dropdown');

        var lis = $(ul).children('li');
        lis.each(function (i) {
            $(this).on('click', function () {
                country = countryNameList[i];
                var ariaId = 'option-country-' + countryNameList[i];
                changeDropdownLabel("country-dropdown", countryNameMap.get(countryNameList[i]), ariaId);
                changeDropdownAria("country-dropdown", i);
                drawCity(countryNameList[i]);
            });
        });

        $(dropdownMenu).html(ul);

        if (defaultVal) {
            var myIndex = countryNameList.findIndex(function (item) {return item === defaultVal});
            var ariaId = 'option-country-' + countryNameList[myIndex];
            changeDropdownLabel("country-dropdown", countryNameMap.get(defaultVal), ariaId);
            changeDropdownAria("country-dropdown", myIndex);
        } else {
            changeDropdownLabel("country-dropdown", '', ' ');
            changeDropdownAria("country-dropdown", 0, true);
            drawCity();
        }
    });

    if (locale === "ar-SA") {
        dropdownMenu.find(".dropdown-menu").css("right", 0);
    }
}

function drawCity(country, timeZone) {
    var cityDropdown = $('#city-dropdown');
    var dropdownMenu = $(cityDropdown).children(".dropdown-menu");

    if (country) {
        getCityList(country, function (msg) {
            var cityNameList = [];
            var cityNameMap = new Map();
            msg.forEach(function (item, index) {
                var {City: City, Country: Country} = item;
                var ariaId = 'option-city-' + cityNameList[index];
                if (City) {
                    cityNameList.push(City);
                    cityNameMap.set(City, getLanguageText(languageTable, City));
                    changeDropdownLabel('city-dropdown', cityNameMap.get(City), ariaId);
                    changeDropdownAria('city-dropdown', index);
                    isCityState = false;
                    city = City;
                } else {
                    cityNameList.push(Country);
                    cityNameMap.set(Country, getLanguageText(languageTable, Country));
                    isCityState = true;
                    changeDropdownLabel('city-dropdown', cityNameMap.get(Country), ariaId);
                    changeDropdownAria('city-dropdown', index)
                    city = Country;
                }
            });

            var ul = createDropdownChildren(cityNameList, cityNameMap, 'city-dropdown');

            var lis = $(ul).children('li');
            lis.each(function (i) {
                $(this).on('click', function () {
                    var ariaId = 'option-city-' + cityNameList[i];
                    changeDropdownLabel('city-dropdown', cityNameMap.get(cityNameList[i]), ariaId);
                    changeDropdownAria('city-dropdown', i);
                    city = cityNameList[i];
                });
            });

            if (timeZone) {
                var {City: timeZoneCity, ZoneID: ZoneID, Description: Description} = timeZone;

                var defaultValue = timeZone ? timeZoneCity : undefined;
                if (!isCityState) {
                    var myIndex = cityNameList.findIndex(function (item) {return item === defaultValue});
                    var ariaId1 = 'option-city-' + cityNameList[myIndex]
                    changeDropdownLabel('city-dropdown', cityNameMap.get(defaultValue), ariaId1);
                    changeDropdownAria('city-dropdown', myIndex);
                    city = defaultValue;
                }

                if (timeZone && ZoneID.slice(0,7) === 'Etc/GMT') {
                    var text = getLanguageText(languageTable, 'Custom') + ' (' + Description + ')';
                    if (Description.indexOf(':') < 0) {
                        text = getLanguageText(languageTable, 'Custom') + ' (' + Description + ':00)';
                    }
                    console.log(text);
                    var ariaId2 = 'option-city-' + '00'
                    changeDropdownLabel('city-dropdown', text, ariaId2);
                    city = undefined;
                }
            }

            $(dropdownMenu).html(ul);
        });
        enableDropdownClickable('city-dropdown', true);
    } else {
        changeDropdownLabel('city-dropdown', '', ' ');
        changeDropdownAria('city-dropdown', 0, true);
        enableDropdownClickable('city-dropdown', false);
    }

    if (locale === "ar-SA") {
        dropdownMenu.find(".dropdown-menu").css("right", 0);
    }
}

function drawDaylightSaving() {
    // for (var i = 1; i <= 12; i++) {
    //     monthList.push(i);
    //     monthMap.set(i, String(i));
    // }
    for (var h = 0; h < 24; h++) {
        hourList.push(h);
        hourMap.set(h, String(h));
    }

    for (let k = 0; k < 2; k++) {
        var when = k === 0 ? 'start': 'end'
        const weekId = `summer-${when}-week`;
        var weekDropdown = $(`#${weekId}`).children(".dropdown-menu");
        var weekUl = createDropdownChildren(weekList, weekMap, weekId);
        var weekLis = $(weekUl).children('li');
        weekLis.each(function (i) {
            $(this).on('click', function () {
                var ariaId = 'option-' + weekId + '-' + weekList[i];
                changeDropdownLabel(weekId, weekMap[weekList[i]], ariaId);
                changeDropdownAria(weekId, i);
                if (k === 0) {
                    var_dstStartWeek = weekList[i];
                } else {
                    var_dstEndWeek = weekList[i];
                }
            });
        });
        $(weekDropdown).html(weekUl);
        var ariaWeekId = 'option-' + weekId + '-' + weekList[0];
        changeDropdownLabel(weekId, weekMap[weekList[0]], ariaWeekId);
        changeDropdownAria(weekId, 0);

        const weekdayId = `summer-${when}-weekday`;
        var weekdayDropdown = $(`#${weekdayId}`).children(".dropdown-menu");
        var weekdayUl = createDropdownChildren(weekdayList, weekdayMap, weekdayId);
        var weekdayLis = $(weekdayUl).children('li');
        weekdayLis.each(function (i) {
            $(this).on('click', function () {
                var ariaId = 'option-' + weekdayId + '-' + weekdayList[i];
                changeDropdownLabel(weekdayId, weekdayMap[weekdayList[i]], ariaId);
                changeDropdownAria(weekdayId, i);
                if (k === 0) {
                    var_dstStartWeekday = weekdayList[i];
                } else {
                    var_dstEndWeekday = weekdayList[i];
                }
            });
        });
        $(weekdayDropdown).html(weekdayUl);
        var ariaWeekdayId = 'option-' + weekdayId + '-' + weekList[0];
        changeDropdownLabel(weekdayId, weekdayMap[weekdayList[0]], ariaWeekdayId);
        changeDropdownAria(weekdayId, 0);

        const monthId = `summer-${when}-month`;
        var monthDropdown = $(`#${monthId}`).children(".dropdown-menu");
        var monthUl = createDropdownChildren(monthList, monthMap, monthId);
        var monthLis = $(monthUl).children('li');
        monthLis.each(function (i) {
            $(this).on('click', function () {
                var ariaId = 'option-' + monthId + '-' + monthList[i];
                changeDropdownLabel(monthId, monthMap[monthList[i]], ariaId);
                changeDropdownAria(monthId, i);
                if (k === 0) {
                    var_dstStartMonth = monthList[i];
                } else {
                    var_dstEndMonth = monthList[i];
                }
            });
        });
        $(monthDropdown).html(monthUl);
        var ariaMonthId = 'option-' + monthId + '-' + monthList[0];
        changeDropdownLabel(monthId, monthMap[monthList[0]], ariaMonthId);
        changeDropdownAria(monthId, 0);

        const hourId = `summer-${when}-hour`;
        var hourDropdown = $(`#${hourId}`).children(".dropdown-menu");
        var hourUl = createDropdownChildren(hourList, hourMap, hourId);
        var hourLis = $(hourUl).children('li');
        hourLis.each(function (i) {
            $(this).on('click', function () {
                var ariaId = 'option-' + hourId + '-' + hourList[i];
                changeDropdownLabel(hourId, hourMap.get(hourList[i]), ariaId);
                changeDropdownAria(hourId, i);
                if (k === 0) {
                    var_dstStartHour = hourList[i];
                } else {
                    var_dstEndHour = hourList[i];
                }
            });
        });
        $(hourDropdown).html(hourUl);
        var ariaId = 'option-' + hourId + '-' + hourList[0];
        changeDropdownLabel(hourId, hourMap.get(hourList[0]), ariaId);
        changeDropdownAria(hourId, 0);
    }
}

function initDST() {
    drawDaylightSaving();
    getDSTInfo(function (dstInfo) {

        var_dstStartMonth = dstInfo.dstStartMonth;
        var_dstStartWeek = dstInfo.dstStartWeek;
        var_dstStartWeekday = dstInfo.dstStartDayOfWeek;
        var_dstStartHour = dstInfo.dstStartHour;

        var_dstEndMonth = dstInfo.dstEndMonth;
        var_dstEndWeek = dstInfo.dstEndWeek;
        var_dstEndWeekday = dstInfo.dstEndDayOfWeek;
        var_dstEndHour = dstInfo.dstEndHour;

        var useDST = $('#useDST');
        useDST.attr('checked', dstInfo.dstMode === "on" ? "true" : "false");
        useDST.change();

        var i1 = monthList.findIndex(function (item) {return item === dstInfo.dstStartMonth});
        var ariaId = 'option-summer-start-month-' + monthList[i1];
        changeDropdownLabel('summer-start-month', monthMap[dstInfo.dstStartMonth], ariaId);
        changeDropdownAria('summer-start-month', i1);

        i1 = weekList.findIndex(function (item) {return item === dstInfo.dstStartWeek});
        ariaId = 'option-summer-start-week-' + weekList[i1];
        changeDropdownLabel('summer-start-week', weekMap[dstInfo.dstStartWeek], ariaId);
        changeDropdownAria('summer-start-week', i1);

        i1 = weekdayList.findIndex(function (item) {return item === dstInfo.dstStartDayOfWeek});
        ariaId = 'option-summer-start-weekday-' + weekdayList[i1];
        changeDropdownLabel('summer-start-weekday', weekdayMap[dstInfo.dstStartDayOfWeek], ariaId);
        changeDropdownAria('summer-start-weekday', i1);

        i1 = hourList.findIndex(function (item) {return item === dstInfo.dstStartHour});
        ariaId = 'option-summer-start-hour-' + hourList[i1];
        changeDropdownLabel('summer-start-hour', hourMap.get(dstInfo.dstStartHour), ariaId);
        changeDropdownAria('summer-start-hour', i1);

        i1 = monthList.findIndex(function (item) {return item === dstInfo.dstEndMonth});
        ariaId = 'option-summer-end-month-' + monthList[i1];
        changeDropdownLabel('summer-end-month', monthMap[dstInfo.dstEndMonth], ariaId);
        changeDropdownAria('summer-end-month', i1);

        i1 = weekList.findIndex(function (item) {return item === dstInfo.dstEndWeek});
        ariaId = 'option-summer-end-week-' + weekList[i1];
        changeDropdownLabel('summer-end-week', weekMap[dstInfo.dstEndWeek], ariaId);
        changeDropdownAria('summer-end-week', i1);

        i1 = weekdayList.findIndex(function (item) {return item === dstInfo.dstEndDayOfWeek});
        ariaId = 'option-summer-end-weekday-' + weekdayList[i1];
        changeDropdownLabel('summer-end-weekday', weekdayMap[dstInfo.dstEndDayOfWeek], ariaId);
        changeDropdownAria('summer-end-weekday', i1);

        i1 = hourList.findIndex(function (item) {return item === dstInfo.dstEndHour});
        ariaId = 'option-summer-end-hour-' + hourList[i1];
        changeDropdownLabel('summer-end-hour', hourMap.get(dstInfo.dstEndHour), ariaId);
        changeDropdownAria('summer-end-hour', i1);

    });
}

function checkDateValid() {

    var number1to12Regex = /(^[1-9]{1}$|^[1]{1}[0-2]{1}$)/;
    var number0to23Regex = /(^[0-9]{1}$|^[1]{1}[0-9]{1}$|^[2]{1}[0-3]{1}$)/;
    var number1to31Regex = /(^[1-9]{1}$|^[1-2]{1}[0-9]{1}$|^[3]{1}[0-1]{1}$)/;
    var number1to59Regex = /(^[1-9]{1}$|^[0-5]{1}[0-9]{1}$)/;
    // To change year can be from current year to 2037.
    // This is same with setting of set.
    var number2017to2037Regex = /(^[2]{1}[0]{1}[1]{1}[7-9]{1}$|^[2]{1}[0]{1}[2]{1}[0-9]{1}$|^[2]{1}[0]{1}[3]{1}[0-7]{1}$)/;
    var maxDaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    var yearElem = $('#label-year');
    if (!number2017to2037Regex.test(yearElem.val())) {
        // alert(getLanguageText(languageTable, "Invalid year value"));
        return [false, getLanguageText(languageTable, "Invalid year value")];
    }

    if (yearElem.val() % 4 === 0 && yearElem.val() % 100 !== 0 || yearElem.val() % 400 === 0)
        maxDaysInMonth[1] = 29;

    var monthElem = $('#label-month');
    if (!number1to12Regex.test(monthElem.val())) {
        // alert(getLanguageText(languageTable, "Invalid month value"));
        return [false, getLanguageText(languageTable, "Invalid month value")];
    }

    var dayElem = $('#label-date');
    if (( !number1to31Regex.test(dayElem.val()) ) || ( dayElem.val() <= 0 || dayElem.val() > maxDaysInMonth[monthElem.val() - 1] )) {
        // alert(getLanguageText(languageTable, "Invalid day value"));
        return [false, getLanguageText(languageTable, "Invalid day value")];
    }

    if (!number1to12Regex.test($('#label-hour').val())) {
        // alert(getLanguageText(languageTable, "Invalid hour value"));
        return [false, getLanguageText(languageTable, "Invalid hour value")];
    }

    if (!number1to59Regex.test($('#label-minute').val())) {
        // alert(getLanguageText(languageTable, "Invalid minute value"));
        return [false, getLanguageText(languageTable, "Invalid minute value")];
    }

    return [true, ''];
}

function addZero(n, digits) {
    var zero = '';
    n = n.toString();

    if (n.length < digits) {
        for (var i = 0; i < digits - n.length; i++)
            zero += '0';
    }
    return zero + n;
}

function initCurrentTime(callback) {
    getCurrentTime(function (msg) {
        $('#label-year').val(msg.year);
        $('#label-month').val(msg.month);
        $('#label-date').val(msg.day);

        ampm = msg.hour < 12 ? 'am' : 'pm';
        var ampmAriaId = 'option-ampm-' + ampm
        changeDropdownLabel("ampm-dropdown", ampmName[ampm], ampmAriaId);
        changeDropdownAria("ampm-dropdown", ampm === 'am' ? 0 : 1);

        $('#label-hour').val((msg.hour % 12) === 0 ? 12 : (msg.hour % 12));
        var myMin = $('#label-minute');
        msg.minute.toString().length === 1 ? myMin.val('0' + msg.minute) : myMin.val(msg.minute);

        if (callback instanceof  Function) {
            callback();
        }
    });
}

function setDisableControlElement(disable) {
    $("input[type=checkbox]").prop("disabled", disable);
    $('#time-apply').prop("disabled", disable);
}

// function changeRtl() {
//     $(".dropdown-ampm").css({"float": "left", "margin-right": "0", "margin-left": "120px"});
//     $(".dropdown-ampm.mobile").css({"float": "left", "margin-right": "0", "margin-left": "0px"});
// }