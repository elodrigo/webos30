document.addEventListener("DOMContentLoaded", function() {
    // init UI change
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/control_display_and_sound.css">');
    $('.content-container').addClass('wide-padding');
    var menu2 = $('#menu-2-2');
    menu2.removeClass('active');
    menu2.css("display", "none");
    var menu1 = $('#menu-2-1');
    menu1.addClass('active');
    menu1.css("display", "block");
    $('.lnb-box').css("display", "block");
    $('#ROOT_CONTAINER').removeClass('dashboard');

    getData();

    displayLoading(false);

    $('.side-logo').css('top', '35px');
});

function getData() {

    getTileModeValue(function (msg) {
        var {tileRow: tileRow, tileCol: tileCol, tileId: tileId, tileNaturalSize: tileNaturalSize} = msg;
        $("#network-mode").val(getLanguageText(languageTable, toCapitalize(msg.tileMode)));
        $("#network-row").val(tileRow);
        $("#network-col").val(tileCol);
        $("#network-id").val(tileId);
        $("#network-natural").val(getLanguageText(languageTable, toCapitalize(msg.naturalMode)));
        $("#network-size").val(tileNaturalSize);

        if (msg.tileMode === "off")
            $("#tileModeViewSpace").css('display', 'none');

        // drawTileMode(tileRow, tileCol, tileId);
    });

}

drawTileMode()



function drawTileMode(row, column, tileID) {

    var divs = "";
    var width = 0;
    var height = 0;

    if ((row < 6) && (column < 6)) {

        if (row > column) {
            height = 186 / row;
            width = (height / 2) * 3;
        } else {
            width = 280 / column;
            height = (width / 3) * 2;
        }

        for (var i = 0; i < row; i++) {
            divs += "<tr>"
            for (var j = 0; j < column; j++) {
                console.log("tileID::" + tileID + ", X::" + ( (i * column) + j + 1 ));
                divs += "<td style='border:2px solid " + ( tileID === ( (i * column) + j + 1 ) ? "red" : "black")
                    + ";width:" + width + "px;height:" + height + "px; text-align: center;'>" + ( (i * column) + j + 1 ) + "</td>";
            }
            divs += "</tr>"
        }

    } else {
        divs += "<td style='border:2px solid black;width:280px;height:186px; text-align: center;'  valign=middle>" + row + " X " + column + "</td>";
    }

    $("#tileModeView").append(divs);

}