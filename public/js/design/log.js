var path = location.protocol + '//' + location.host + location.pathname;

var logDisplayName = {
    ALL: getLanguageText(languageTable, "All"),
    TIME: getLanguageText(languageTable, "Hours"),
    FAN: getLanguageText(languageTable, "Fan"),
    TEMP: getLanguageText(languageTable, "Temperature"),
    SIGNAL: getLanguageText(languageTable, "Signal"),
    SHAKE: getLanguageText(languageTable, "Motion"),
    PANEL: getLanguageText(languageTable, "Panel")
};

var logValList = ['ALL'];

var logBody = [];
var logBodyM = [];
var logHead;

document.addEventListener("DOMContentLoaded", function() {
    // init UI change
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/control_log.css">');
    $('.content-container').addClass('wide-padding');
    var menu1 = $('#menu-2-1');
    menu1.removeClass('active');
    menu1.css("display", "none");
    var menu2 = $('#menu-2-2');
    menu2.addClass('active');
    menu2.css("display", "block");
    $('.lnb-box').css("display", "block");
    $('#ROOT_CONTAINER').removeClass('dashboard');

    displayLoading(false);

    $('.side-logo').css('top', '35px');
});

function drawLogFilterSelect(curFile) {
    var filterDropdown = $('#filter-dropdown');
    var dropdownMenu = $(filterDropdown).children(".dropdown-menu");

    if (env.logList && env.logList.length > 0) {
        for (var i=0; i < env.logList.length; i++) {
            var title = env.logList[i].toUpperCase();
            logValList.push(title);
            logDisplayName[title] = getLanguageText(languageTable, toCapitalize(env.logList[i]));
        }
    }

    var ul = createDropdownChildren(logValList, logDisplayName, 'filter-dropdown');

    var lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            var selected = logValList[i];
            changeDropdownLabel("filter-dropdown", logDisplayName[selected]);
            changeDropdownAria("filter-dropdown", i);
            var newPath = path + "?filter=" + selected + "&file=" + curFile;
            window.location.replace(newPath);
        });
    });

    $(dropdownMenu).html(ul);

    changeDropdownLabel("filter-dropdown", logDisplayName[logValList[0]]);
    changeDropdownAria("filter-dropdown", 0);
}

function initLog(curFile, filter) {
    parseLogJsonScroll(logJson.log);

    drawLogFilterSelect(curFile);
    makeKeyDownDropdown("filter-dropdown");

    $("#field-label-filename").text($("#field-label-filename").text().replace(".txt", ".csv"));

    $('#download-btn').on('click', function () {
        if (curFile) {
            window.location.replace('/logDown?name=' + curFile);
        }
    });

    // drawTable();
    drawTableScroll();
    // drawReflowTable();
    drawReflowTableScroll();
    drawLogPagerScroll();

    if (filter) {
        var myIndex = logValList.findIndex(function (item) {return item === filter});
        changeDropdownLabel("filter-dropdown", logDisplayName[filter]);
        changeDropdownAria("filter-dropdown", myIndex);
    }

    if (locale == "ar-SA") {
        changeRtl();
    }
}

function parseLogJsonScroll(data) {
    var allRows = data.split(/\r?\n|\r/);

    var totalPagesM = [];
    var rowCountM = 0;
    var pageFive = [];
    var pageScroll = [];

    for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
        if (allRows[singleRow]) {
            var rowCells = allRows[singleRow].split(',');
            if (singleRow === 0) {
                logHead = rowCells;
            } else {
                pageScroll.push(rowCells);

                if (rowCountM > 4) {
                    totalPagesM.push(pageFive);
                    rowCountM = 0;
                    pageFive = [];
                } else {
                    pageFive.push(rowCells);
                    rowCountM ++;
                }
            }
        }
    }
    logBody = pageScroll;
    logBodyM = pageScroll;
}

function drawTableScroll() {
    var table = `<table><caption>Log Table – Consists of ${getLanguageText(languageTable, "Category")}, ${getLanguageText(languageTable, "Sensor")}, ${getLanguageText(languageTable, "Event")}, ${getLanguageText(languageTable, "State")} ${getLanguageText(languageTable, "Occurence Time")}</caption><thead>`;

    table += "<tr>";
    for (var head of logHead) {
        table += "<th scope=\"col\"><div class=\"text-block\">";
        table += head.replace(/^ */g, "");
        table += "</div></th>";
    }
    table += "</tr>";
    table += "</thead>";
    table += "<tbody>";

    for (var row of logBody) {
        table += "<tr>";
        for (var col of row) {
            var letter = col.replace(/^ */g, "");
            table += "<td>";
            table += letter === 'NG' ? 'Not OK' : letter;
            table += "</td>";
        }
        table += "</tr>";
    }

    table += "</tbody>";

    table += "</table>";

    $("#new-table-box").html(table);
}

function drawReflowTableScroll() {
    var head = logHead

    var reflowHead = `<div class="table-header">
                <div class="row">
                    <div class="thead">${head[0].replace(/^ */g, "")}</div>
                    <div class="thead">${head[1].replace(/^ */g, "")}</div>
                </div>
    </div>`;

    var tableBody = "<ul class='table-body'>";

    for (var rowM of logBodyM) {
        var tableRow = "<li class='table-row'>"

        var summaryData = `<div class="summary-data">
        <div class="row">
            <div class="table-cell center">${rowM[0].replace(/^ */g, "")}</div>
            <div class="table-cell">
                <button type="button" role="listbox" class="btn btn-expand" aria-expanded="false">${rowM[1].replace(/^ */g, "")}</button>
            </div>
        </div>
    </div>`;

        var dataBox = "<div class='all-data-box'><ul class='all-data'>"
        for (var i = 2; i < rowM.length; i++) {
            var letter = rowM[i].replace(/^ */g, "");
            dataBox += "<li><span class='field-label'>";
            dataBox += head[i].replace(/^ */g, "");
            dataBox += "</span><span class='field-content'>";
            dataBox += letter === 'NG' ? 'Not OK' : letter
            dataBox += "</span></li>";
        }
        dataBox += "</ul></div>";

        tableRow += summaryData;
        tableRow += dataBox;
        tableRow += "</li>";

        tableBody += tableRow;
    }

    tableBody += "</ul>";
    reflowHead += tableBody;

    $('#reflow-table-to-list').html(reflowHead);
    // return reflowHead;
}

function drawLogPagerScroll() {
    var pagers = $('.pager');

    var pagerPC = $(pagers[0]);
    var pagerPCA = $(pagerPC).children('a');
    var prevPagers = $('.go-prev');
    var prevPagersPC = $(prevPagers[0]);
    var nextPagers = $('.go-next');
    var nextPagersPC = $(nextPagers[0]);
    var beforePagers = $('.go-before');
    var beforePagersPC = $(beforePagers[0]);
    var afterPagers = $('.go-after');
    var afterPagersPC = $(afterPagers[0]);

    var currentNum = 0;
    var prevFile = 'log00000.txt';
    var nextFile = 'log00000.txt';
    var pageSet = 10;

    for (var i=0; i < pagerPCA.length; i++) {
        var isCur = $(pagerPCA[i]).hasClass('active');
        var num = $(pagerPCA[i]).data('num');
        $(pagerPCA[i]).attr('aria-pressed', isCur === true ? 'true' : 'false');
        $(pagerPCA[i]).prop('id', `page-${num}`);
        $(pagerPCA[i]).addClass('page');
        $(pagerPCA[i]).addClass(`page-${num}`);
        if (isCur === true) {
            $(pagerPCA[i]).addClass('current');
            currentNum = num;
            if (i > 0) {
                prevFile = $(pagerPCA[i-1]).prop('href');
            }
            if (i < pagerPCA.length-1) {
                nextFile = $(pagerPCA[i+1]).prop('href');
            }
        }
    }
    var maxPage = pagerPCA.length - 1;
    var startSet = (Math.ceil((currentNum + 1) / pageSet) - 1) * pageSet;
    var endSet = startSet + pageSet - 1;
    if (endSet > maxPage) {
        endSet = maxPage;
    }
    for (var m=0; m < pagerPCA.length; m++) {
        if (m < startSet || endSet < m) {
            $(pagerPCA[m]).remove();
        }
    }

    prevPagersPC.on('click', function () {
        if (currentNum > 0) {
            window.location.replace(prevFile);
        }
    });
    nextPagersPC.on('click', function () {
        if (currentNum < pagerPCA.length -1) {
            window.location.replace(nextFile);
        }
    });
    beforePagersPC.on('click', function () {
        window.location.replace($(pagerPCA[0]).prop('href'));
    });
    afterPagersPC.on('click', function () {
        window.location.replace($(pagerPCA[pagerPCA.length-1]).prop('href'));
    });

    var pagerM = $(pagers[1]);
    var pagerMA = $(pagerM).children('a');
    var prevPagersM = $(prevPagers[1]);
    var nextPagersM = $(nextPagers[1]);
    var beforePagersM = $(beforePagers[1]);
    var afterPagersM = $(afterPagers[1]);

    var currentNumM = 0;
    var prevFileM = 'log00000.txt';
    var nextFileM = 'log00000.txt';
    var pageSetM = 3;

    for (var j=0; j < pagerMA.length; j++) {
        var isCurM = $(pagerMA[j]).hasClass('active');
        var numM = $(pagerMA[j]).data('num');
        $(pagerMA[j]).attr('aria-pressed', isCur === true ? 'true' : 'false');
        $(pagerMA[j]).prop('id', `page-${num}`);
        $(pagerMA[j]).addClass('page');
        $(pagerMA[j]).addClass(`page-${num}`);
        if (isCurM === true) {
            $(pagerMA[j]).addClass('current');
            currentNumM = numM;
            if (j > 0) {
                prevFileM = $(pagerMA[j-1]).prop('href');
            }
            if (j < pagerMA.length-1) {
                nextFileM = $(pagerMA[j+1]).prop('href');
            }
        }
    }
    var maxPageM = pagerMA.length - 1;
    var startSetM = (Math.ceil((currentNumM + 1) / pageSetM) - 1) * pageSetM;
    var endSetM = startSetM + pageSetM - 1;
    if (endSetM > maxPageM) {
        endSetM = maxPageM;
    }
    for (var n=0; n < pagerMA.length; n++) {
        if (n < startSetM || endSetM < n) {
            $(pagerMA[n]).remove();
        }
    }

    prevPagersM.on('click', function () {
        if (currentNumM > 0) {
            window.location.replace(prevFileM);
        }
    });
    nextPagersM.on('click', function () {
        if (currentNumM < pagerMA.length -1) {
            window.location.replace(nextFileM);
        }
    });
    beforePagersM.on('click', function () {
        window.location.replace($(pagerMA[0]).prop('href'));
    });
    afterPagersM.on('click', function () {
        window.location.replace($(pagerMA[pagerMA.length-1]).prop('href'));
    });
}
