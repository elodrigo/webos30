var movie = ["avi", "mp4", "wmv", 'divx', 'asf', 'm4v', 'mov',
    '3gp', '3g2', 'mkv', 'ts', 'trp', 'tp', 'mts',
    'mpg', 'mpeg', 'dat', 'vob'
];
//var music = ["mp3", "wav"];
var image = ['png', 'bmp', 'jpg', 'jpeg', 'jpe'];

var MediaFilterDisplayName = {
    all: getLanguageText(languageTable, "All"),
    video: getLanguageText(languageTable, "Video"),
    image: getLanguageText(languageTable, "Image"),
};

var mediaFilterValList = ['all', 'video', 'image'];

var toDeleteFilename = undefined;

document.addEventListener("DOMContentLoaded", function() {
    // init UI change
    var head = $('head');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/popup_software_common.css">');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/media_library.css">');
    $('#ROOT_CONTAINER').removeClass('dashboard');

    drawMediaFilterDropdown();
    makeKeyDownDropdown("media-filter-dropdown");

    addFileFilter();

    changeDropdownLabel("media-filter-dropdown", MediaFilterDisplayName[mediaFilterValList[0]]);
    changeDropdownAria("media-filter-dropdown", 0);

    addAlertModals('media-warning-modal', '');

    $('.btn-download').on('click', function () {
        window.location.href = $(this).data('uri');
    })

    $('.side-logo').css('top', '35px');
});

function addRenameModals(id, title, content, button1, button2, renameCallback, overwriteCallback) {

    function createBtn() {
        return `<div class="button-container">
          <button type="button" id="${id}-rename" class="btn btn-cancel">${button1}</button>
          <button type="button" id="${id}-overwrite" class="btn btn-yes btn-primary">${button2}</button>
        </div>`;
    }

    var html = `<div id="${id}" class="popup-wrapper">
      <div class="dim">&nbsp;</div>
      <div class="popup" role="dialog" aria-labelledby="popup-label" aria-modal="true">
        <div class="popup-container">
          <div class="popup-header">
            <h1 id="popup-label">${title}</h1>
          </div>
          <div class="popup-content">
            <div class="text-content" role="alert">
              <P id="${id}-text-content" class="alet-text">${content}</P>
            </div>
          </div>
          <div class="popup-footer">
            ${createBtn()}
          </div>
        </div>
      </div>
    </div>`

    var modal = $(html);
    $('body').append(modal);

    var myContainer = document.getElementById(id);
    var myFocusTrap = focusTrap.createFocusTrap(`#${id}`, {
        onActivate: function () {myContainer.classList.add('is-active')},
        onDeactivate: function () {myContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });

    $(`#${id}-rename`).on('click', function () {
        myFocusTrap.deactivate();
        _closePopup(id);
        if (renameCallback) {
            renameCallback();
        }
    });

    $(`#${id}-overwrite`).on('click', function () {
        myFocusTrap.deactivate();
        _closePopup(id);
        if (overwriteCallback) {
            overwriteCallback();
        }
    });

    $(`#${id}`).on('cssClassChanged', function () {
        myFocusTrap.activate();
    });
}

function init(curDir, filter) {
    if (filter === '') {
        filter = ' ';
    }

    $('.file-area :file').on('change', function (e) {
        var name = e.target.files[0].name;
        $('#fileName').html(name);
    });

    $('.btn-to-list').on('click', function () {
        var dir = $(this).val();
        if (!dir) {
            window.location.replace('/media')
        } else {
            window.location.replace(window.location.pathname + "?dir=" + curDir + '/' + dir + '&filter=' + filter);
        }
    });

    $('.img-thumb').each(function (index) {
        var myThis = $(this);
        var id = myThis.attr('id');
        var media = myThis.attr('name');
        var format = media.substr(media.length - 3, 3).toLowerCase();
        var filePath = curDir + '/' + media;
        getThumbnail(id, filePath, function (msg) {
            getThumbnailResponse(msg, myThis);
        });
    });

    $(document).on('change', '.file-area :file', function () {
        var filename = $(this).val();
        $('#fileName').html(filteringXSS(filename));
    });

    function uploadFile(file, newname) {
        var formData = new FormData();

        formData.append('dir', curDir);
        formData.append('newname', newname);
        formData.append('media', file);

        if (!file) {
            showMyModal('media-warning-modal', 'Please choose a file to upload');
            return;
        }

        var fileName = file.name;
        var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();

        if (movie.indexOf(ext) < 0 && image.indexOf(ext) < 0) {
            showMyModal('media-warning-modal', getLanguageText(languageTable, 'Unsupported File type. Only Image/Video files are allowed.'));
            return;
        }

        if (file.size > maxSize) {
            showMyModal('media-warning-modal', getLanguageText(languageTable, 'Flash memory is not enough.'));
            return;
        }

        var xhr = new XMLHttpRequest();

        xhr.open('post', '/upload', true);

        xhr.upload.onprogress = function (e) {
            if (e.lengthComputable) {
                var newContents = `<p class="percent"><strong class="strong" id="media-percent">0</strong> %</p>
      <p><span id="media-filename"></span> ${getLanguageText(languageTable, "File Upload Progress")}</p>`;
                $('#loading-contents').html(newContents);

                displayLoading(true);

                var per = Math.round((e.loaded / e.total) * 100);
                $('#media-percent').html(String(per));
                $('#media-filename').html(fileName);
            }
        }

        xhr.upload.onload = function (e) {
            setTimeout(function () {
                _closePopup('popup-update');
                displayLoading(false);
                $('#loading-contents').html('');
                window.location.reload(true);
            }, 2000);
        }

        xhr.onerror = function (e) {
            _closePopup('popup-update');
            displayLoading(false);
            $('#loading-contents').html('');
            alert('An error occurred while submitting the form.');
            console.log('An error occurred while submitting the form. Maybe your file is too big');
        };

        xhr.onload = function () {
            console.log(this.statusText);
        };

        xhr.send(formData);
    }

    $('#media-upload-submit').on('click', function () {
        var file = document.getElementById('file-upload').files[0];
        var newname = filteringXSS(file.name);

        if (allFiles.indexOf(newname.replace(/&/gi, "&amp;")) >= 0) {
            var modalID = 'rename' + (new Date().valueOf());
            var tempName = rename(newname);
            addRenameModals(modalID,
                getLanguageText(languageTable, 'Rename or Overwrite?'),
                getLanguageText(languageTable, '{{filename}} already exists.').replace("{{filename}}", newname) + '<br>' +
                getLanguageText(languageTable, 'Rename to {{filename (2)}} or Overwrite?').replace("{{filename (2)}}", tempName) + '<br>',
                getLanguageText(languageTable, 'Rename'),
                getLanguageText(languageTable, 'Overwrite'),
                function () {
                    newname = tempName;
                    setTimeout(function () {
                        uploadFile(file, newname);
                    }, 500);
                    $(`#${modalID}`).remove();
                },
                function () {
                    setTimeout(function () {
                        uploadFile(file, newname);
                    }, 500);
                    $(`#${modalID}`).remove();
                });
            showMyModal(modalID);
            return;
        }

        uploadFile(file, newname);
    });

    addConfirmModals('.btn-delete', 'delete-file-modal', getLanguageText(languageTable, "information message"), `${getLanguageText(languageTable, "Delete?")}<br>`, false, function () {
            deleteFile("signage", toDeleteFilename, function () {
                window.location.reload(true);
            });
        }, null,
        function (myThis) {
            var fileName = curDir + $(myThis).attr('name');
            toDeleteFilename = fileName;
            $('#delete-file-modal').find('.alet-text').html(getLanguageText(languageTable, 'Delete?') + "<br>" + fileName.replace('/', ''));

        });

    displayLoading(false);
}

function addFileFilter() {
    var filter = '';

    var addFilter = function (ext) {
        if (filter !== '') {
            filter += ',';
        }
        filter += '.' + ext;
    };

    movie.forEach(addFilter);
    image.forEach(addFilter);

    $('#upload').attr('accept', filter);
}

function drawMediaFilterDropdown() {
    var filterDropdown = $('#media-filter-dropdown');
    var dropdownMenu = $(filterDropdown).children(".dropdown-menu");

    var ul = createDropdownChildren(mediaFilterValList, MediaFilterDisplayName, 'media-filter-dropdown');

    var lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            changeDropdownLabel("media-filter-dropdown", MediaFilterDisplayName[mediaFilterValList[i]]);
            changeDropdownAria("media-filter-dropdown", i);
        });
    });

    $(dropdownMenu).html(ul);
}

function rename(filename) {
    var split = filename.lastIndexOf('.');
    var name = filename.substring(0, split);
    var ext = split >= 0 ? filename.substr(split + 1) : '';

    var regex = /\(\d+\)$/;
    if (regex.test(name)) {
        name = name.substring(0, name.lastIndexOf('(')).trim();
    }

    var newname = ''
    var i = 2;
    do {
        newname = name + ' (' + i + ').' + ext;
        ++i;
    } while (allFiles.indexOf(newname.replace(/&/gi, "&amp;")) >= 0);

    return newname;
}

function getThumbnailResponse(msg, myThis) {
    var newSrc = "/tmp/tnm/" + encodeURIComponent(msg.fileName);
    if (msg.result === false) {
        // refer to photo&video
        switch (msg.result.errorCode) {
            case 'COMMON_ERROR(-3)':
            case 'COMMON_ERROR(-51)':
                newSrc = msg.mediaType === 'image' ? '/images/photo_invalid.png' : '/images/video_invalid.png';
                break;

            case 'COMMON_ERROR(-55)': //video only
                newSrc = '/images/video_invalid.png;'
                break;

            case 'COMMON_ERROR(-61)':
            case 'COMMON_ERROR(-62)':
            case 'COMMON_ERROR(-63)': //video only
                //#IF_COMMERCIAL add fhd option
                newSrc = '/images/HEVC_H264thumb.png';
                break;

            default:
                newSrc = msg.mediaType === 'image' ? '/images/photo_default.png' : '/images/video_default.png';
                break;
        }
    }

    var m = $(myThis).children('img')[0];
    var s = $(myThis).siblings();
    var icon = $(s[0]).find('img');
    if (icon.length > 0) {
        var iconSrc = msg.mediaType === 'image' ? '/assets/images/icon_picture.svg' : '/assets/images/icon_video.svg';
        $(icon[0]).attr('src', iconSrc);
    }

    $(m).attr('src', newSrc);
}