var currentSetting = {
    ipv4: '0.0.0.0',
    url: ''
};

function initNtpServerSetting() {
    setupNtpServerSetting();
    getNtpServerSetting();
}

function getNtpServerSetting () {
    getSystemSettings('commercial', ['ntpServerMode', 'ntpServerType', 'ntpServerIpv4', 'ntpServerUrl'], function(res) {
        var isAutoMode = res.ntpServerMode === 'auto';

        setManualSettingDisability(isAutoMode);
        $('#ntp-server-switch').prop('checked', isAutoMode);
        $('input[name=ntpServerType][value=' + res.ntpServerType+ ']').prop('checked', true);
        $('#ntpServerAddr').val(res['ntpServer' + capitalizeFirstLetter(res.ntpServerType)]);

        currentSetting.ipv4 = res.ntpServerIpv4;
        currentSetting.url = res.ntpServerUrl;
    });
}

function setupNtpServerSetting () {
    // var bNtpManualSettingModal = addNtpServerSettingModal('ntpManualSettingModal');
    // $('body').append(bNtpManualSettingModal);

    $('#btn-ntp-server').on('click', function () {
        getNtpServerSetting();
        showMyModal('ntp-server-popup');
    });

    // modal
    $('#ntp-server-switch').on('change', function() {
        var val = $(this).prop('checked');
        $(this).attr('aria-checked', val);
        setSystemSettings('commercial', {
            'ntpServerMode': val ? 'auto' : 'manual'
        }, function() {
            setManualSettingDisability(val);
        });
    });

    $('input:checkbox[name="ntpServerType"]').on('change',function() {
        var addrType = $(this).val();

        $('#ntpManualSettingModalOK').prop('disabled', true);
        $('#btn-now-update').prop('disabled', true);
        setSystemSettings('commercial', {
            ntpServerType: addrType,
        }, function() {
            $('#ntpServerAddr').val(currentSetting[addrType]);
        });
    });

    if (getBrowserType() === 'msie') {
        $('#ntpServerAddr').on('keyup', function(e) {
            if (e.keyCode === 13) {
                $(this).trigger('change');
            }
        });
    }

    $('#ntpServerAddr').on('input change', function () {
        var addrType = $('input:checkbox[name="ntpServerType"]:checked').val(),
            val = $(this).val(),
            prevVal = currentSetting[addrType];

        if (val === prevVal || !validateManualAddr(addrType, val)) {
            $('#ntpManualSettingModalOK').prop('disabled', true);
            $('#btn-now-update').prop('disabled', true);
        } else {
            $('#ntpManualSettingModalOK').prop('disabled', false);
            $('#btn-now-update').prop('disabled', false);
        }
    });

    $('#ntpManualSettingModalOK').on('click',function () {
        var addrType = $('input:checkbox[name="ntpServerType"]:checked').val(),
            addrValue = $('#ntpServerAddr').val();

        if (!validateManualAddr(addrType, addrValue)) {
            console.log('invalid address');
            return;
        }

        setNTPServerAddress(addrType, addrValue, function() {
            currentSetting[addrType] = addrValue;
            $('#ntpManualSettingModalOK').prop('disabled', true);
            $('#btn-now-update').prop('disabled', true);
        });
    });

    $('#btn-now-update').on('click',function () {
        var addrType = $('input:checkbox[name="ntpServerType"]:checked').val(),
            addrValue = $('#ntpServerAddr').val();

        if (!validateManualAddr(addrType, addrValue)) {
            console.log('invalid address');
            return;
        }

        setNTPServerAddress(addrType, addrValue, function() {
            currentSetting[addrType] = addrValue;
            $('#ntpManualSettingModalOK').prop('disabled', true);
            $('#btn-now-update').prop('disabled', true);
        });
    });

    $('#ipv4').on('click', function () {
        $(this).attr("checked", true);
        $('#url').prop('checked', false);
        $('#url').attr("checked", false);
        $('#ntpServerAddr').prop('placeholder', '0.0.0.0');
        const ipv4 = $('#ipv4');
        const isIt = ipv4.prop('checked');
        if (!isIt) {
            ipv4.prop('checked', true);
        }
    });

    $('#url').on('click', function () {
        $(this).attr("checked", true);
        $('#ipv4').prop('checked', false);
        $('#ipv4').attr("checked", false);
        $('#ntpServerAddr').prop('placeholder', '');
        const isIt = $('#url').prop('checked');
        if (!isIt) {
            $('#url').prop('checked', true);
        }
    });
}

function setManualSettingDisability(autoMode) {
    $('input:checkbox[name="ntpServerType"]').prop('disabled', autoMode);
    $('#ntpServerAddr').prop('disabled', autoMode);

    if (autoMode) {
        $('#ntpManualSettingModalOK').prop('disabled', true);
        $('#btn-now-update').prop('disabled', true);
    }
}

function validateManualAddr (type, addr) {
    if (type === 'ipv4') {
        return checkIPAddress(addr);
    } else {
        var result = false;
		if( !addr || addr.length<=0 || addr.length > 512 || addr.trim() === '') {
			return result;
		}

		if (!result) {
			var special_key ="!,*&^%$#@~;";
			for (var inx = 0; inx < special_key.length; inx++) {
				if (addr.indexOf(special_key.charAt(inx)) !== -1)
					return false;
			}
			result = true;
		}
		return result;
    }
}

function addNtpServerSettingModal(id) {
    var rtl = env.locale == 'ar-SA' ? 'dir=rtl" style="direction: rtl;"' : '';

    var titleFloatDir = env.locale == 'ar-SA' ? 'right': 'left';
    var valueFloatDir = env.locale == 'ar-SA' ? 'left': 'right';

	var html =
	'<div class="modal fade" id="' + id + '" tabindex="-1" role="dialog">' +
		'<div class="modal-dialog modal-top30" role="document">' +
			'<div class="modal-content"'+ rtl + '>' +
				'<div class="modal-body">' +
                    '<table id="' + id + 'Table" style="width: 100%">' +
                        '<tr>' +
                            '<th  style="float: ' + titleFloatDir + ';"><div>' + Locale.getText('Default NTP Server') + '</div></th>' +
                            '<td style="float:' + valueFloatDir + ';">' +
                                '<input id="defaultNtpServerEnable" type="checkbox" class="switch">' +
                                '<label for="defaultNtpServerEnable">&nbsp;</label>' +
                            '</td>' +
                        '</tr>' +
                        '<tr style="height: 25px"></tr>' +
                        '<tr>' +
                            '<th style="float:' + titleFloatDir + ';"><span style="font-size: 15px; color: #3b3b3b;">' + Locale.getText('Manual Setting') + '</span></th>' +
                        '</tr>' +
                        '<tr>' +
                            '<th colspan="2"><hr style="margin-top: 5px; margin-bottom: 10px;" /></th>' +
                        '</tr>' +
                        '<tr>' +
                            '<th style="float:' + titleFloatDir + ';"><span>' + Locale.getText('Type') + '</span></th>' +
                            '<td style="float:' + valueFloatDir + ';">' +
                                '<div id="ntpServerTypeSelect">' +
                                    '<label><input type="radio" name="ntpServerType" value="ipv4"><span>IPv4</span></label>' +
                                    '<label><input type="radio" name="ntpServerType" value="url"><span>URL</span></label>' +
                                '</div>' +
                            '<td>' +
                        '</tr>' +
                        '<tr>' +
                            '<th style="float:' + titleFloatDir + ';"><span>' + Locale.getText('Address') + '</span></th>' +
                            '<td style="float:' + valueFloatDir + ';">' +
                                '<div>' +
                                    '<input type="text" id="ntpServerAddr" class="custom" maxlength="512" style="width:180px; height:32px;"/>' +
                                    '<button type="button" class="btn whiteCardBtn" id="' + id + "OK" + '" disabled>' + Locale.getText('Update Now') + '</button>' +
                                '</div>' +
                            '<td>' +
                        '</tr>' +
					'</table>' +
				'</div>' +
				'<div class="modal-footer">' +
					'<button type="button" class="btn whiteCardBtn" data-dismiss="modal" id="' + id + "Cancel" + '">' + Locale.getText('Close') + '</button>' +
				'</div>' +
			'</div>' +
		'</div>' +
	'</div>';
	return html;
}
