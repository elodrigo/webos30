document.addEventListener("DOMContentLoaded", function() {
    // init UI change
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/control_display_and_sound.css">');
    $('.content-container').addClass('wide-padding');
    var menu1 = $('#menu-2-1');
    menu1.removeClass('active');
    menu1.css("display", "none");
    var menu2 = $('#menu-2-2');
    menu2.addClass('active');
    menu2.css("display", "block");
    $('.lnb-box').css("display", "block");
    $('#ROOT_CONTAINER').removeClass('dashboard');

    getData();

    if (locale === 'ar-SA') {
        changeRtl();
    }

    displayLoading(false);

    $('.side-logo').css('top', '35px');
});

function getData() {
    getSetID(function(msg){
        $('#id-setting').val(msg.setId);
    });

    getBasicInfo(function(msg) {
        var {firmwareVersion, micomVersion, serialNumber, bootLoaderVersion, modelName} = msg;
        $('#model-name').val(modelName);
        $('#code-num').val(serialNumber);
        $('#software-varsion').val(firmwareVersion);

        $('#boot-loader').val(bootLoaderVersion);
        $('#micom-varsion').val(micomVersion);
    });

    getWebOSInfo(function(msg) {
        var {core_os_release, core_os_release_codename} = msg;
        $('#webos-varsion').val(core_os_release + " (" + core_os_release_codename + ")");
    });

    if (env.supportMicomFanControl) {
        getFanMicomInfo(function (msg) {
            $('#fanmicom').html(msg.fanMicomVersion);
        });
    }
}