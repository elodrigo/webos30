function compareFirstUse(){
    var modified = JSON.stringify(originalVal)!=JSON.stringify(selectVal);
    $('#firstUseModal #modalActionBtn').prop('disabled', !modified);
}

function saveFirstUse(){
    setSystemSettings('commercial', {'ledLayout' : {'pixelPitch' : selectVal.pixelPitch}});
    setSignageName(selectVal.signageName);
    setHdmiInput(selectVal.input);
    setContinent(selectVal.continent, function(msg){
        setCountry(selectVal.country,function(msg){
            getCityList(selectVal.country, function(msg){
                var timeZone = undefined;

                msg.forEach(function(item) {
                    if(!isCityState){
                        if( item.City == selectVal.city ){
                            timeZone = item;
                        }
                    }else{
                        if( item.Country == selectVal.city ){
                            timeZone = item;
                        }
                    }
                });
                setCity(timeZone, function(msg){
                });
                saveFirstUseFile(selectVal);
                if(selectVal.language != originalVal.language){
                    setMenuLanguage(selectVal.language);
                }else{
                    $('#firstUseModal').modal('hide');
                    location.reload();
                }
                originalVal = JSON.parse(JSON.stringify(selectVal));
            });
        });
    });

    if( originalVal.ledFilmType != selectVal.ledFilmType ){
        overlayControl20(true, Locale.getText('Reboot') + '......');
        setSystemSettings('commercial', {'ledFilmType' : selectVal.ledFilmType});

        rebootScreen();

        setTimeout(function(){
            window.location.replace("/login.html");
        }, 60 * 1000);
    }
}

var myFirstUseContainer = undefined;
var myFirstUseFocusTrap = undefined;

var firstUseModal = {

    init: function() {
        var modal = this;
        /*$.get('/detail/firstUse.html', function(data, success, type){
            addDetailModal('firstUseModal', Locale.getText('First Use'), data);
        });*/
        addAlertModals('pitchChangeModal', 'If the pitch is changed, all of the configuration settings are initialized.');

        myFirstUseContainer = document.getElementById('firstUseModal');
        myFirstUseFocusTrap = focusTrap.createFocusTrap('#firstUseModal', {
            onActivate: function () {myFirstUseContainer.classList.add('is-active')},
            onDeactivate: function () {myFirstUseContainer.classList.remove('is-active')},
            escapeDeactivates: false,
        });
    },

    modal: function() {
        firstUseModal.getFirstUseInfo(firstUseModal);
        showMyModal('firstUseModal');
        $('#firstUseModal #modalActionBtn').show();
        myFirstUseFocusTrap.activate();
        firstUseDone(function(done){
            $('#firstUseModal #modalActionBtn').prop('disabled', done);
        });
        $('#firstUseModal #modalActionBtn').unbind().click(function(){
            myFirstUseFocusTrap.deactivate();
            saveFirstUse();
        });

        $('#firstUseModal #modalactionName').text(Locale.getText('Save'));
        makeKeyDownDropdown('languageSelect');
        makeKeyDownDropdown('ledPitchSelect');
        makeKeyDownDropdown('inputSelect');
        makeKeyDownDropdown('ledTypeSelect');
        makeKeyDownDropdown('continentSelect');
        makeKeyDownDropdown('countrySelect');
        makeKeyDownDropdown('citySelect');
    },

    getFirstUseInfo: function(modal){
        getSignageName(function(ret) {
            $("#signageName").val(ret);
            originalVal.signageName = ret;
            $("#signageName").keyup(function(){
                selectVal.signageName = $(this).val();
                compareFirstUse();
            });
        });

        getMenuLanguage(function(currLanguage){
            var langParse = currLanguage.split('-');
            var langCode = langParse[0];
            if (langCode == 'pt') {
                langCode = currLanguage;
            }
            langCode += (langParse.length == 2) ? '' : '-' + langParse[1];

            originalVal.language = langCode;
            modal.drawLanguageSelect();
        });

        getHdmiInput(function(currentInput){
            originalVal.input = currentInput;
            modal.drawInputSelect();
        });

        getlocaleContinent(function(ret){
            originalVal.continent = ret.localeContinent;
            getlocaleCountry(function(ret){
                originalVal.country = ret.localeCountry;
                getTimeZone(function(timeZone){
                    originalVal.timeZone = timeZone;
                    selectVal = JSON.parse(JSON.stringify(originalVal));
                    modal.drawContinentSelect(modal);
                });
            });
        });

        getSystemSettings('commercial', ['ledFilmType'], function(ret){
            originalVal.ledFilmType = ret.ledFilmType;
            modal.drawLedTypeSelect(modal);
        });

        getSystemSettings('commercial', ['ledLayout'], function (ret) {
            originalVal.pixelPitch = ret.ledLayout.pixelPitch;

            // var pitchList = [14, 24];
            // var pitchNameList = ["14mm", "24mm"];
            var pitchValList = [14, 24];
            var pitchNameMap = {"14": "14mm", "24": "24mm"}

            var ledPitchDropdown = $('#ledPitchSelect');
            var dropdownMenu = $(ledPitchDropdown).children(".dropdown-menu");

            var ul = createDropdownChildren(pitchValList, pitchNameMap, 'ledPitchSelect');

            var lis = $(ul).children('li');
            lis.each(function (i) {
                var selected = pitchValList[i];
                $(this).on('click', function () {
                    changeDropdownLabel("ledPitchSelect", pitchNameMap[selected]);
                    changeDropdownAria("ledPitchSelect", i);
                    selectVal.pixelPitch = selected;
                    if (originalVal.pixelPitch !== Number(selected)) {
                        showMyModal('pitchChangeModal');
                    }
                    compareFirstUse();
                });
            });

            $(dropdownMenu).html(ul);

            changeDropdownLabel("ledPitchSelect", pitchNameMap[originalVal.pixelPitch]);
            var myIndex = pitchValList.findIndex(function (item) {return item === originalVal.pixelPitch});
            changeDropdownAria("ledPitchSelect", myIndex);

            /*drawDropDownBox('ledPitchSelect', pitchList, pitchNameList, 'dropboxType2', function (selected) {
                selectVal.pixelPitch = selected;
                if (originalVal.pixelPitch !== Number(selected)) {
                    var pitchChangeModal = addTwoButtonModal('pitchChangeModal', 'If the pitch is changed, all of the configuration settings are initialized.', 'OK', '');
                    $('body').append(pitchChangeModal);
                    $('#pitchChangeModalOK').remove(); // Delete unused buttons
                    $('#pitchChangeModal').modal({
                        backdrop: 'static',
                        keyboard: false,
                        show: true
                    });
                }
                compareFirstUse();
            }, originalVal.pixelPitch);*/
            $('#ledPitchSelectDrop').attr('disabled', true);
        });
    },

    drawLanguageSelect: function(){
        $.getJSON('/resources/languageTable.json', function(table) {
            var languageList = [];
            var codeList = [];
            var languageNameMap = new Map();
            table.languages.forEach(function(lang) {
                if( (lang.languageCode == 'ar') && (env.addLanguage != 'ar') ){
                    return;
                }
                languageList.push(lang.name);
                codeList.push(lang.languageCode);
                languageNameMap.set(lang.languageCode, lang.name);
            });

            var languageDropdown = $('#languageSelect');
            var dropdownMenu = $(languageDropdown).children(".dropdown-menu");

            var ul = createDropdownChildren(codeList, languageNameMap, 'languageSelect');

            var lis = $(ul).children('li');
            lis.each(function (i) {
                var selected = codeList[i];
                $(this).on('click', function () {
                    changeDropdownLabel("languageSelect", languageNameMap.get(selected));
                    changeDropdownAria("languageSelect", i);
                    selectVal.language = selected;
                    compareFirstUse();
                });
            });

            $(dropdownMenu).html(ul);

            changeDropdownLabel("languageSelect", languageNameMap.get(codeList));
            var myIndex = codeList.findIndex(function (item) {return item === originalVal.language});
            changeDropdownAria("languageSelect", myIndex);

        });
    },

    drawInputSelect: function(){
        getInputList(function(msg){
            var inputNameList = [];
            var inputIDList = [];
            var inputNameMap = new Map();
            msg.forEach(function(input){
                inputNameList.push(input.name);
                inputIDList.push(input.id);
                inputNameMap.set(input.id, input.name);
            });

            var inputDropdown = $('#inputSelect');
            var dropdownMenu = $(inputDropdown).children(".dropdown-menu");

            var ul = createDropdownChildren(inputIDList, inputNameMap, 'inputSelect');

            var lis = $(ul).children('li');
            lis.each(function (i) {
                var selected = inputIDList[i];
                $(this).on('click', function () {
                    changeDropdownLabel("inputSelect", inputNameMap.get(selected));
                    changeDropdownAria("inputSelect", i);
                    selectVal.input = selected;
                    compareFirstUse();
                });
            });

            $(dropdownMenu).html(ul);

            changeDropdownLabel("inputSelect", inputNameMap.get(inputIDList));
            var myIndex = inputIDList.findIndex(function (item) {return item === originalVal.input});
            changeDropdownAria("inputSelect", myIndex);
        });
    },

    drawLedTypeSelect: function(){
        /*var filmTypeList = [1, 2];
        var filmNameList = [Locale.getText('MONO').toLowerCase(), Locale.getText('color')];
        $('#ledTypeSelect').empty();
        drawDropDownBox('ledTypeSelect', filmTypeList, filmNameList, 'dropboxType2', function (selected) {
            var originType = originalVal.ledFilmType  == 'color' ? 2 : 1;
            if( originType != selected){
                addFilmTypeModal('ledTypeModal', Locale.getText('warning'), 'The browser should be restarted to reflect the selected transparent LED type. Continue?', function() {
                    selectVal.ledFilmType = (selected == 2 ? 'color' : 'mono');
                    compareFirstUse();
                }, function(){
                    setDropButtonText('ledTypeSelect', originType, true);
                    selectVal.ledFilmType = (originType == 2 ? 'color' : 'mono');
                    compareFirstUse();
                });
                $('#ledTypeModal').modal();
            }else{
                selectVal.ledFilmType = (selected == 2 ? 'color' : 'mono');
                compareFirstUse();
            }
        }, (originalVal.ledFilmType == 'color' ? 2 : 1) );*/
    },

    drawContinentSelect: function(modal){
        modal.drawCountrySelect(modal);

        var continentDropdown = $('#continentSelect');
        var dropdownMenu = $(continentDropdown).children(".dropdown-menu");

        var ul = createDropdownChildren(continentList, continentNameMap, 'continentSelect');

        var lis = $(ul).children('li');
        lis.each(function (i) {
            var selected = continentList[i];
            $(this).on('click', function () {
                changeDropdownLabel("continentSelect", continentNameMap[selected]);
                changeDropdownAria("continentSelect", i);
                selectVal.continent = selected;
                $('#firstUseModal #modalActionBtn').prop('disabled', true);
                modal.drawCountrySelect(modal, selected);
            });
        });

        $(dropdownMenu).html(ul);

        changeDropdownLabel("continentSelect", originalVal.continent);
        var myIndex = continentList.findIndex(function (item) {return item === originalVal.continent});
        changeDropdownAria("continentSelect", myIndex);
    },

    drawCountrySelect: function(modal, continent){
        if(continent){
            $('#citySelect .dropdown-menu').empty();
            changeDropdownLabel('citySelect', ' ');
            changeDropdownAria('citySelect' -1);
        }else{
            modal.drawCitySelect();
        }

        getCountryList(continent ? continent : originalVal.continent, function(msg){
            var shortNameList = [];
            var fullNameList = [];
            var countryNameMap = new Map();
            msg.forEach(function(ret) {
                shortNameList.push(ret.shortName);
                fullNameList.push(ret.fullName);
                countryNameMap.set(ret.shortName, ret.fullName);
            });

            var countryDropdown = $('#countrySelect');
            var dropdownMenu = $(countryDropdown).children(".dropdown-menu");

            var ul = createDropdownChildren(shortNameList, countryNameMap, 'countrySelect');

            var lis = $(ul).children('li');
            lis.each(function (i) {
                var selected = shortNameList[i];
                $(this).on('click', function () {
                    changeDropdownLabel("countrySelect", countryNameMap.get(selected));
                    changeDropdownAria("countrySelect", i);
                    selectVal.country = selected;
                    modal.drawCitySelect(selected);
                });
            });

            $(dropdownMenu).html(ul);

            changeDropdownLabel("countrySelect", continent ? '' : countryNameMap.get(originalVal.country));
            if (continent) {
                changeDropdownAria("countrySelect", -1);
            } else {
                var myIndex = shortNameList.findIndex(function (item) {return item === originalVal.country});
                changeDropdownAria("countrySelect", myIndex);
            }
        });
    },

    drawCitySelect: function(country){
        getCityList(country ? country : originalVal.country, function(msg){
            var cityList = [];
            var cityNameMap = new Map();

            msg.forEach(function(item) {
                if( item.City == ''){
                    cityList.push(item.CountryCode);
                    cityNameMap.set(item.CountryCode, item.Country);
                    isCityState = true;
                }else{
                    cityList.push(item.City);
                    cityNameMap.set(item.City, item.City);
                }
            });

            var cityDropdown = $('#citySelect');
            var dropdownMenu = $(cityDropdown).children(".dropdown-menu");

            var ul = createDropdownChildren(cityList, cityNameMap, 'citySelect');

            var lis = $(ul).children('li');
            lis.each(function (i) {
                var selected = cityList[i];
                $(this).on('click', function () {
                    changeDropdownLabel("citySelect", selected);
                    changeDropdownAria("citySelect", i);
                    selectVal.city = selected;
                    compareFirstUse();
                });
            });

            $(dropdownMenu).html(ul);

            if (isCityState) {
                changeDropdownLabel("citySelect", cityNameMap.get(cityList[0]));
                changeDropdownAria("citySelect", 0);
            } else {
                // changeDropdownLabel("citySelect", originalVal.timeZone.City);
                // var myIndex = cityList.findIndex(function (item) {return item === originalVal.timeZone.City});
                // changeDropdownAria("citySelect",myIndex);
                changeDropdownLabel("citySelect", ' ');
                changeDropdownAria("citySelect", -1);
            }
        });
    }
};