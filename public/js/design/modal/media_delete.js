document.addEventListener("DOMContentLoaded", function() {
    const myContainer = document.getElementById('media-delete-popup');
    const myFocusTrap = focusTrap.createFocusTrap('#media-delete-popup', {
        onActivate: function () {myContainer.classList.add('is-active')},
        onDeactivate: function () {myContainer.classList.remove('is-active')},
    });

    $('#btn-media-delete-cancel').on('click', function () {
        myFocusTrap.deactivate();
        _closePopup('media-delete-popup');
    });

    $('#btn-media-delete-ok').on('click', function () {
        myFocusTrap.deactivate();
        selectedFile.forEach(function(fileName){
            deleteFile("signage", fileName, function() {
            });
        });
        setTimeout(function(){
            window.location.reload(true);
        }, 500);
        _closePopup('media-delete-popup');
    });

    $('#media-delete-popup').on('cssClassChanged', function (e) {
        // e.stopPropagation();
        myFocusTrap.activate();
        var ul = $('#media-delete-popup .list-items');
        var fileName = ''
        selectedFile.forEach(function(file){
            fileName += '<li>'+ file;
        });

        ul.html("<br>" + fileName.replace('/',''));
    });

});