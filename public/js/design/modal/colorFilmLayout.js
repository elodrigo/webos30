var scaleRatio = 0;
var senderColor = ['red', 'blue', 'green', 'yellow'];
var layout = undefined;
var currSender = undefined;
var currUnit = undefined;
var senderList = new Array(4)
var backScreen = '#backgroundImg';
var unitRows = 2;
var unitPort = 0;
var prevVal = 0;
var captureInterval = undefined;
var senderPos = {
    x : 0,
    y : 0
}

var macList = {
    sender1 : [],
    sender2 : [],
    sender3 : [],
    sender4 : []
};

var senderResolutionW = 0;
var senderResolutionH = 0;
var maxRows = 0;
var unitPixels = {
    width: 0,
    height: 0,
    miniHeight: 0
};

var inputDisplayName = {
    0: getLanguageText(languageTable, "Right"),
    1: getLanguageText(languageTable, "Left"),
    2: getLanguageText(languageTable, "Bottom"),
    3: getLanguageText(languageTable, "Top"),
}
var inputValList = [0,1,2,3];

var senderDisplayName = {};
var senderValList = [];

var rowDisplayName = {};
var rowValList = [];
var portDisplayName = {
    0: getLanguageText(languageTable, "Both"),
    1: getLanguageText(languageTable, "Left"),
    2: getLanguageText(languageTable, "Right"),
}
var portValList = [0,1,2];

var FilmLayoutEditorModal = {
    config: {
        scaleScreen: 702 / 1920
    },

    addEventHandlers: function () {
        $('#addSenderBtn').on('click', function () {
            var newID = getNewSenderId();
            var newSender = {
                senderId : newID,
                inputDirection : 2,
                width : senderResolutionW,
                height : 540,
                x : 0,
                y : 0,
                unitList : [],
            };
            layout.senderList.push(newSender);
            setSenderPos(newID*2, 0, 0);
            setInputDirectionColor(newID*2, 2);
            drawSender(layout.senderList);
            drawSenderSelect(layout.senderList.length);

            saveLayout();
        });

        $('#removeSenderBtn').on('click', function () {
            var index = findSender(currSender);

            clearSendingMAC(currSender*2);
            layout.senderList.splice(index,1);
            currSender = layout.senderList[layout.senderList.length];
            drawSender(layout.senderList);
            drawSenderSelect(layout.senderList.length);
            saveLayout();
            $(this).attr('disabled', true);
            $(this).addClass('disabled');
        });

        $('#unitSetting').on('click', function () {
            // $('#layoutEditorModal .popup-edit-layout').css('width', 1250);
            // $('#layoutEditorModal .popup-edit-layout').css('height', 700);
            // flex space-between 이기 때문에 left 줄필요 없을듯
            // $('#layoutControlPanel').css('left', 150);
            $('#layoutEditorModalTitle').text(getLanguageText(languageTable, "Unit Settings"));
            $('.unitPropertyTxt').html(`<marquee>${getLanguageText(languageTable, "Unit Property")}</marquee>`);

            var sender = layout.senderList[findSender(currSender)];
            var arrInputdir = ['Right', 'Left', 'Bottom', 'Top'];
            var inputDir = arrInputdir[parseInt(sender.inputDirection)];
            var senderStatus = getLanguageText(languageTable, 'Sender') + sender.senderId + ' : ';
            senderStatus += getLanguageText(languageTable, 'Position') + '(' + sender.x + ',' + sender.y + '),';
            senderStatus += getLanguageText(languageTable, 'Resolution') + '(960 X 540),';
            senderStatus += getLanguageText(languageTable, 'Input Direction') + '(' + getLanguageText(languageTable, inputDir) + ')';
            $('#subTitle').text(senderStatus);

            senderPos.x = sender.x;
            senderPos.y = sender.y;

            backScreen = '#unitBackgroundImg';

            var senderWidth = sender.width;
            var senderHeight = sender.height;
            var senderX = sender.x;
            var senderY = sender.y;

            // flex 이므로
            // $('#unitBackgroundImg').css('width', 1920);
            // $('#unitBackgroundImg').css('height', 1080);
            // $('#unitBackgroundImg').css('left', senderX*(-1) );
            // $('#unitBackgroundImg').css('top', senderY*(-1) );

            drawUnit(sender.unitList);

            // $('.senderSetting').css('display', 'none');
            $('.senderSetting').hide();
            // $('.unitSetting').css('display', 'block');
            $('.unitSetting').show();
            $('#layoutEditorModal .btn-cancel').on('click', function () {
                setTimeout(function () {
                    $('#layoutEditorModalTitle').text(getLanguageText(languageTable, 'Layout Editor'));
                    $('#subTitle').text('');
                    // $('#layoutEditorModal .popup-edit-layout').css('width', 1200);
                    // $('#layoutEditorModal .popup-edit-layout').css('height', 680);

                    backScreen = '#backgroundImg';
                    currUnit = undefined;
                    // $('.unitSetting').css('display', 'none');
                    $('.unitSetting').hide();
                    // $('.senderSetting').css('display', 'block');
                    $('.senderSetting').show();
                    if($('#patternPanel').css('display') !== 'none'){
                        $('#patternDone').trigger('click');
                    }
                }, 100);
            });
        });

        $('#addUnitBtn').on('click', function () {
            $(this).attr('disabled', true);

            var sender = layout.senderList[findSender(currSender)];
            unitPort = 0;
            var newUnit = {
                unitId : -1,
                portSelection : unitPort,
                rows : 2,
                mini : 0,
                x : 0,
                y : 0,
                trimming : {
                    left : 0,
                    right: 0
                },
                width : 0,
                height : 0,
                mac : []
            };

            getReceivedMac(currSender*2, function(retRcv) {
                var rcvMac = retRcv.result.receiverMAC;
                getSendingMAC(currSender*2, function(retSnd) {
                    var sndMac = retSnd.result.sendingMAC;
                    var str = JSON.stringify(sndMac);

                    for(var i=0; i<18; i++){
                        var mac = sndMac['index'+i];
                        if( (mac[0]=='00') && (mac[1]=='00') ){
                            newUnit.unitId = (i+1);
                            break;
                        }
                    }

                    for(var j=0; j<18; j++){
                        if( (rcvMac['index'+j][0]=='00')&&(rcvMac['index'+j][1]=='00') ){
                            break;
                        }
                        var targetStr = '"'+rcvMac['index'+j][0]+'","'+rcvMac['index'+j][1]+'"';

                        if( str.indexOf(targetStr) < 0 ){
                            var idleMac = rcvMac['index'+i];
                            console.log(idleMac);
                            var mac5th = idleMac[0];
                            var mac6th = idleMac[1];
                            var slaveIdx = currSender*2;
                            var unitIdx = newUnit.unitId - 1;

                            newUnit.mac.push(mac5th);
                            newUnit.mac.push(mac6th);
                            setSendingMAC(parseInt(currSender)*2, unitIdx, mac5th, mac6th);
                            setXYPos(parseInt(currSender)*2, unitIdx, 0, 0);
                            sender.unitList.push(newUnit);
                            setReceiverNum(slaveIdx, sender.unitList.length);
                            setHVNum(slaveIdx, unitIdx, 2, 2);
                            setDisplayPort(slaveIdx, unitIdx, newUnit.portSelection);

                            setTimeout(function(){
                                drawUnit(sender.unitList);
                                saveLayout();
                            }, 100);
                            break;
                        }
                    }
                });
            });
        });

        $('#removeUnitBtn').on('click', function() {
            var sender = layout.senderList[findSender(currSender)];
            setSendingMAC(parseInt(currSender)*2, currUnit, '00', '00');
            sender.unitList.splice(findUnit(sender, (Number(currUnit)+1) ),1);
            currUnit = sender.unitList[sender.unitList.length];
            drawUnit(sender.unitList);
            saveLayout();
        });

        $('.cutting').on('click', function () {
            var sender = layout.senderList[findSender(currSender)];

            $('#layoutControlPanel').css('display', 'none');
            $('#cuttingPanel').show();

            var direction = '';
            var unitList = sender.unitList;
            var unitIdx = findUnit(sender, (Number(currUnit)+1) );
            var trimming = sender.unitList[unitIdx].trimming;

            if( $(this).attr('id').indexOf('Left') > -1 ){
                direction = 'Left';
                $('#cuttingRange').val(trimming.left);
                drawEdgeTable(direction, trimming.left);
            }else{
                direction = 'Right';
                $('#cuttingRange').val(trimming.right);
                drawEdgeTable(direction, trimming.right);
            }

            $('#cuttingRange').css('direction', direction === 'Right' ? 'rtl':'ltr');
            checkRangeUI('cuttingRange', 0, unitPixels.width-1, direction === 'Right');

            $('#cuttingSubject').html("<marquee>"+Locale.getText('Cut '+direction+' Edge')+"</marquee>");
            $('#cuttingRange').attr('name', direction);
            $('#cuttingRange').on('change',function() {
                var val = $(this).val();
                var direction = $(this).attr('name');
                drawEdgeTable(direction, val);
            }).on('input', function() {
                var val = $(this).val();
                var direction = $(this).attr('name');
                checkRangeUI('cuttingRange', 0, unitPixels.width-1, direction==='Right');
            });
        });

        $('#cutCancelBtn').on('click', function () {
            $('#layoutControlPanel').show();
            $('#cuttingPanel').css('display', 'none')
        });

        $('#cutOKBtn').on('click',function(){
            var sender = layout.senderList[findSender(currSender)];
            var unitList = sender.unitList;
            var unitIdx = findUnit(sender, (Number(currUnit)+1) );
            var trimming = sender.unitList[unitIdx].trimming;
            $('#layoutControlPanel').show();
            $('#cuttingPanel').hide();

            var direction = $('#cuttingRange').attr('name');
            var val = $('#cuttingRange').val();

            if(direction === 'Left'){
                $('#trimLeft').val(val);
                trimming.left = val;
            }else{
                $('#trimRight').val(val);
                trimming.right = val;
            }

            drawUnit(sender.unitList);
            saveLayout();
        });

        $('#patternOn').on('click',function(){
            openPatternPanel(false);
        });

        $('#patternAllOn').on('click',function(){
            openPatternPanel(true);
        });

        $('#patternDone').on('click',function(){
            closePatternPanel();
        });

        $('input[type="number"]').on('click',function(){
            if( $(this).attr('name') !== undefined ){
                prevVal = $(this).val();
            }
        });

        $('input[type="number"]').on('keypress',function(){
            console.log('pressing')
            if( $(this).attr('id').indexOf('sender') > -1 ){
                var param = $(this).attr('name');
                if( param === 'left' ){
                    if( (prevVal>1920-senderResolutionW) || (prevVal<0) ){
                        prevVal = 1920-senderResolutionW;
                    }
                }else{
                    if( (prevVal>540) || (prevVal<0) ){
                        prevVal = 540;
                    }
                }
            }
        });

        $('input[type="number"]').on('change',function() {
            var id = $(this).attr('id');
            var sender = layout.senderList[findSender(currSender)];

            if( id.indexOf('sender') > -1 ){
                if( $(this).attr('name') !== undefined ){
                    var val = $(this).val();
                    var param = $(this).attr('name');

                    if( param === 'left' ){
                        if( (val>1920-senderResolutionW) || (val<0) ){
                            $(this).val(prevVal)
                        }
                        sender.x = Number( $(this).val() );
                    }else{
                        if( (val>540) || (val<0) ){
                            $(this).val(prevVal)
                        }
                        sender.y = Number( $(this).val() );
                    }

                    prevVal = Number( $(this).val() );
                    var currX = sender.x;
                    var currY = sender.y
                    setSenderPos(currSender*2, currX, currY);
                    $('#sender'+currSender).css(param, $(this).val()/scaleRatio );

                    sender.x = currX;
                    sender.y = currY;

                    saveLayout();
                }
            } else if (id.indexOf('unit') > -1) {
                if( $(this).attr('name') !== undefined ){
                    var val = $(this).val();
                    var param = $(this).attr('name');
                    var sender = layout.senderList[findSender(currSender)];
                    var unitList = sender.unitList;
                    var unitIdx = findUnit(sender, (Number(currUnit)+1) );
                    var unit = sender.unitList[unitIdx];
                    var unitWidth = unit.portSelection == 0 ? unitPixels.width * 2 : unitPixels.width;
                    var unitHeight = (Number(unit.rows) + (unit.mini ? 1 : 0)) * unitPixels.height;
                    var min, max = 0;
                    var currVal = 0;

                    var adjustTrimmingX = Number(unit.portSelection) !== 2 ? Number(unit.trimming.left) : 0;
                    var adjustTrimmingXR = Number(unit.portSelection) !== 1 ? Number(unit.trimming.right) : 0;
                    var adjustTrimmingY = 0;
                    var adjustTrimmingYR = 0;

                    if (sender.inputDirection < 2) {
                        var tmpVal = unitWidth;
                        unitWidth = unitHeight;
                        unitHeight = tmpVal;

                        tmpVal = adjustTrimmingX;
                        adjustTrimmingX = adjustTrimmingYR;
                        adjustTrimmingYR = tmpVal;

                        tmpVal = adjustTrimmingXR;
                        adjustTrimmingXR = adjustTrimmingY;
                        adjustTrimmingY = tmpVal;
                    }

                    if (param === 'left') {
                        senderX = sender.x;
                        min = sender.x + adjustTrimmingX;
                        max = sender.x + senderResolutionW - unitWidth + adjustTrimmingX + adjustTrimmingXR;

                        console.log('left::min::'+min+', max::'+max);

                        if( (val>max) || (val<min) ){
                            $(this).val(prevVal)
                        }
                        unit.x = Number( $(this).val() ) - senderX;
                        currVal = unit.x;
                    } else {
                        senderY = sender.y;
                        min = sender.y + adjustTrimmingY;
                        max = sender.y + 540 - unitHeight + adjustTrimmingY + adjustTrimmingYR;

                        if( (val>max) || (val<min) ){
                            $(this).val(prevVal)
                        }
                        unit.y = Number( $(this).val() ) - senderY;
                        currVal = unit.y
                    }

                    prevVal = Number( $(this).val() );

                    var currX = unit.x
                    var currY = unit.y

                    if (sender.inputDirection < 2) {
                        currY -= adjustTrimmingY;
                    } else {
                        currX -= adjustTrimmingX;
                    }

                    setXYPos((parseInt(currSender))*2, currUnit, currX, currY);
                    drawUnit(sender.unitList);
                    saveLayout();
                }
            }
        });

        $('input[type="number"]').on('focus',function(){
            if( $(this).attr('id').indexOf('unit') > -1 ){
                prevVal = Number( $(this).val() );
            }
        });

        $('[id^=physical]').on('keyup',function(){
            if( $(this).attr('id') === 'physicalX'){
                layout.physicalLayout.x = Number($(this).val());
            }else{
                layout.physicalLayout.y = Number($(this).val());
            }

            saveLayout();
        });

    },

    init: function(param) {
        const myContainer = document.getElementById('layoutEditorModal');
        const myFocusTrap = focusTrap.createFocusTrap('#layoutEditorModal', {
            onActivate: function () {myContainer.classList.add('is-active')},
            onDeactivate: function () {myContainer.classList.remove('is-active')},
        });

        $('#layoutEditorModal .popup-edit-layout').css('max-width', '130rem');


        getSystemSettings('commercial', ['ledLayout'], function (ret) {
            layout = ret.ledLayout;
            $('#pixelPitchTxt').text(layout.pixelPitch + "mm");

            if (Number(layout.pixelPitch) === 14) { // set 14mm condition
                maxRows = 15;
                senderResolutionW = 960;
                senderResolutionH = 540;
                unitPixels.width = 48;
                unitPixels.height = 36;
                unitPixels.miniHeight = 9;
                // $('.miniTxt').hide();
                // $('#miniSelect').hide();
                // $('#cuttingRange').attr('max', unitPixels.width - 1);
            } else { // set 24mm condition
                maxRows = 27;
                senderResolutionW = 980;
                senderResolutionH = 540;
                unitPixels.width = 28;
                unitPixels.height = 20;
                unitPixels.miniHeight = 5;
                // $('.miniTxt').show();
                // $('#miniSelect').show();
            }

            drawInputDir();
            drawPortSelection();
            drawRowSelection();
            drawMiniSelection(false);

            if(layout.pixelPitch === undefined){
                console.log("[ERR] : pixel pitch(14mm/24mm) is not selected, Set default value 24mm");
                layout.pixelPitch = 24;

                maxRows = 27;
                senderResolutionW = 980;
                senderResolutionH = 540;
                unitPixels.width = 28;
                unitPixels.height = 20;
                unitPixels.miniHeight = 5;
            }

            setScale(layout.pixelPitch);

            if (layout.senderList !== undefined) {
                drawSender(layout.senderList);
                currSender = layout.senderList.length - 1;
                drawSenderSelect(layout.senderList.length);
                changeDropdownLabel("senderSelect", senderDisplayName[currSender]);
                var myIndex = senderValList.findIndex(function (item) {return item === currSender});
                changeDropdownAria("senderSelect", myIndex);
            } else {
                layout.senderList = [];
                for (var i = 0; i < 4; i++) {
                    clearSendingMAC( (i+1)*2 );
                }
            }

            if (layout.physicalLayout === undefined) {
                layout.physicalLayout = {x:0, y:0}
                $('#physicalX').val(0);
                $('#physicalY').val(0);
            } else {
                $('#physicalX').val(layout.physicalLayout.x);
                $('#physicalY').val(layout.physicalLayout.y);
            }

            // $('#unitEditWraper').css('width', senderResolutionW);
        });

        $('#btn-layout-cancel').on('click', function () {
            myFocusTrap.deactivate();
            var isUnit = $('#unitEditWraper').css('display');
            if (isUnit !== 'none') {
                $('#unitEditWraper').css('display', 'none');
                $('#layoutEditorCanvasWraper').css('display', 'block');
            } else {
                _closePopup('layoutEditorModal');
            }
        });

        $('#btn-layout-save').on('click', function () {
            myFocusTrap.deactivate();

            if($('#patternPanel').css('display') !== 'none'){
                $('#patternDone').trigger('click');
            } else {
                var isUnit = $('#unitEditWraper').css('display');
                if (isUnit !== 'none') {
                    $('#unitEditWraper').css('display', 'none');
                    $('.unitSetting').hide();
                    $('#layoutEditorCanvasWraper').css('display', 'block');
                    $('.senderSetting').show();
                } else {
                    clearInterval(captureInterval);
                    _closePopup('layoutEditorModal');
                }
            }

            getSystemSettings('commercial', ['ledLayout'], function(ret){
                drawColorUnit(ret);
            });

            // clearInterval(captureInterval);
            // _closePopup('layoutEditorModal');
        });

        makeKeyDownDropdown('senderSelect');
        makeKeyDownDropdown('inputSelect');
        makeKeyDownDropdown('portSelect');
        makeKeyDownDropdown('miniSelect');
        makeKeyDownDropdown('rowSelect');

        var modal = LayoutEditorModal;
        modal.addEventHandlers();

        $('#layoutEditorModal').on('cssClassChanged', function (e) {
            // e.stopPropagation();
            myFocusTrap.activate();
            getSystemSettings('commercial', ['ledLayout'], function(ret){
                if (param !== undefined) {
                    if( ret.ledLayout.senderList.length > 0 ){
                        currSender = 1;
                        selectSender(currSender);
                        setTimeout(function(){
                            $('#patternAllOn').click();
                        },500);
                    }
                }
            });

            captureInterval = setInterval(function(){
                showScreen();
            },200);
        });
    }
}

function drawSender(senderList) {
    if (senderList === undefined){
        return;
    }

    var maxSenderCnt = (env.numOfExtInputs === 1 ? 1 : 4);		// regard as One-Box model if no.of ex.input is 1

    $('#addSenderBtn').attr('disabled', senderList.length === maxSenderCnt ? true : false);

    if(senderList.length === 0){
        $('#unitSetting').attr('disabled', true);
    }

    $('.view-item').remove();

    resetMacList();

    senderList.forEach(function(sender, index){
        var senderId = layout.senderList[index].senderId;
        var ID = 'sender'+senderId;
        var arrDirection = ['dirImgRight', 'dirImgLeft', 'dirImgBottom', 'dirImgTop'];
        var dirClass = arrDirection[parseInt(sender.inputDirection)];
        var senderTxt = `<div class="view-item draggable ui-widget-content" id="${ID}" name="${senderId}" style="position: absolute"
                    <div class="title">${getLanguageText(languageTable, "Sender")} ${senderId}</div> 
            </div>`;
        $('#layoutEditorCanvasWraper').append(senderTxt);
        var myId = $('#'+ID);
        myId.css('left', sender.x/scaleRatio);
        myId.css('top', sender.y/scaleRatio);
        myId.css('width', sender.width/scaleRatio);
        myId.css('height', sender.height/scaleRatio);
        // myId.append('<div class="senderTag">'+Locale.getText('Sender')+senderId+'<div>');
        // myId.append('<div class="'+dirClass+'"><div>');
        myId.on('mousedown', function(){
            currSender = $(this).attr('name');
            selectSender(currSender);
        });

        $('#'+ID).draggable({
            containment: "#layoutEditorCanvasWraper",
            stack:".view-item",
            scroll: false,
            drag: function( event, ui ) {
                var index = $(this).attr('name');
                var x, y;

                if (Number(layout.pixelPitch) === 14) {
                    x = Math.round( scaleRatio * ui.position.left );
                    y = Math.floor( scaleRatio * ui.position.top );
                } else {
                    x = Math.floor( scaleRatio * ui.position.left );
                    y = Math.ceil( scaleRatio * ui.position.top );
                }
                var sender = layout.senderList[findSender(currSender)];

                $('#senderX').val( x );
                if (y > 540) {
                    y = 540
                }
                $('#senderY').val( y );

                sender.x = x;
                sender.y = y;
            },
            stop : function( event, ui ){
                var currX
                var currY
                if (Number(layout.pixelPitch) === 14) {
                    currX = Math.round( scaleRatio * ui.position.left );
                    currY = Math.floor( scaleRatio * ui.position.top );
                } else {
                    currX = Math.floor( scaleRatio * ui.position.left );
                    currY = Math.ceil( scaleRatio * ui.position.top );
                }

                setSenderPos((parseInt(currSender))*2, currX, currY);
                saveLayout();
            }
        });
    });

}

function showScreen() {
    getCapture(464, function(m) {
        if(m.indexOf('noimage')<0){
            cpatureFileName = m;
        }else{
            m = cpatureFileName;
        }
        // console.log('this here', m)
        $(backScreen).attr('src',m);
    });

}

function setScale(pitch) {
    //825, 464
    //464, 261
    var width = Number($('#layoutEditorCanvasWraper').css('width').replace(/[^-\d\.]/g, ''));

    var scaledWidth = Number(pitch) === 14 ? 824 : 825;

    scaleRatio = 1920 / scaledWidth;
    var height = 1080 / scaleRatio;
    $('#layoutEditorCanvasWraper').css('height', height);
    $('#unitEditWraper').css('height', height);

    scaleRatio = 1920 / scaledWidth;
    $('#backgroundImg').removeClass('portraitImg');
    $('#layoutEditorCanvasWraper').css('width', scaledWidth);
    $('#unitEditWraper').css('width', scaledWidth);
}

function resetMacList() {
    macList = {
        sender1 : [],
        sender2 : [],
        sender3 : [],
        sender4 : []
    };
}

function selectSender(id) {
    var sender = layout.senderList[findSender(id)];

    $('.view-item').css('border', 'none');
    $('#sender'+id).css('border', '2px solid #cf0652');
    $('#senderX').val( sender.x );
    $('#senderY').val( sender.y );
    $('#senderW').val(senderResolutionW);
    $('#senderH').val(senderResolutionH);

    var top = 0;
    $('.view-item').each(function(item, index){
        if( Number($('#sender'+item).css('z-index')) > top ){
            top = Number($('#sender'+item).css('z-index'))
        }
    });
    $('#sender'+id).css('z-index', top+1);

    var dir = sender.inputDirection;
    changeDropdownLabel("inputSelect", inputDisplayName[dir]);
    changeDropdownAria("inputSelect", dir);
    changeDropdownLabel("senderSelect", senderDisplayName[currSender]);
    var myIndex = senderValList.findIndex(function (item) {return item === currSender});
    changeDropdownAria("senderSelect", myIndex);

    getReceivedMac((id)*2, function(msg) {
        if(msg.returnValue) {
            var tmpMac = msg.result.receiverMAC;
            var receiverCount = 0;
            macList['sender'+(id)] = [];
            while(true){
                if( tmpMac['index'+ receiverCount ] !== undefined ){
                    if( (tmpMac['index'+receiverCount][0] != '00') || (tmpMac['index'+receiverCount][1] != '00') ){
                        macList['sender'+(id)].push(tmpMac['index'+ receiverCount]);
                    }
                    receiverCount++;
                }else{
                    break;
                }
            }
        } else {
            console.log('getReceivedMac false');
        }

        var hasUnit = (macList['sender'+ Number(id) ].length > 0);
        // console.log('macList', macList)
        getFpgaStatus(parseInt(id)-1, function(ret) {
            var connected = ret.result.ethLinkStatus;
            if(connected){
                $('#unitSetting').attr('disabled', !hasUnit );
                $('#patternAllOn').attr('disabled', !hasUnit );
                if (hasUnit === true) {
                    $('#unitSetting').removeClass('disabled');
                    $('#patternAllOn').removeClass('disabled');
                } else {
                    $('#unitSetting').addClass('disabled');
                    $('#patternAllOn').addClass('disabled');
                }
            }else{
                $('#unitSetting').attr('disabled', true );
                $('#patternAllOn').attr('disabled', true );
                $('#unitSetting').addClass('disabled');
                $('#patternAllOn').addClass('disabled');
            }
        });

        $('#unitCount').html(getLanguageText(languageTable, "X Units").replace('X', sender.unitList.length));
        const rmBtn = $('#removeSenderBtn');
        rmBtn.attr('disabled', false);
        rmBtn.removeClass('disabled');
    });

}

function selectUnit(id) {
    var sender = layout.senderList[findSender(currSender)];
    var unitList = sender.unitList;
    var unitIdx = findUnit(sender, (Number(currUnit)+1) );
    var unit = unitList[ unitIdx ];
    var unitScaleRatio = scaleRatio / 2;

    $('.unit .film').css('background-color', 'white');
    $('#unit'+id+' .film').css('background-color', '#cf0652');
    $('#unit'+id+' .virtual').css('background-color', 'white');

    var x = parseInt($('#unit'+id).css('left'));
    var y = parseInt($('#unit'+id).css('top'));

    if(unit.mini > 0){
        if(sender.inputDirection == '0'){
            x += ( 4 - unit.mini ) * unitPixels.miniHeight;
        }else if(sender.inputDirection == '2'){
            y += ( 4 - unit.mini ) * unitPixels.miniHeight;
        }
    }

    $('#unitX').val( x + sender.x);
    $('#unitY').val( y + sender.y);

    $('#trimLeft').val(unit.trimming.left);
    $('#trimRight').val(unit.trimming.right);

    var top = 0;
    $('.unit').each(function(item, index){
        if( Number($('#unit'+item).css('z-index')) > top ){
            top = Number($('#unit'+item).css('z-index'));
        }
    });
    $('#unit'+id).css('z-index', top+1);

    $('#removeUnitBtn').attr('disabled', false);
    $('#patternOn').attr('disabled', false);
    $('#cutRight').attr('disabled', false);
    $('#cutLeft').attr('disabled', false);

    var rows = unit.rows;
    var mini = unit.mini;
    var port = unit.portSelection;
    // setDropButtonText('rowSelect', parseInt(rows), true);
    changeDropdownLabel('rowSelect', rowDisplayName[parseInt(rows)]);
    changeDropdownAria('rowSelect', parseInt(rows)-1);

    if( parseInt(rows) === maxRows ){
        drawMiniSelection(true);
    }else{
        drawMiniSelection(false);
        // setDropButtonText('miniSelect', parseInt(mini), true);
    }
    // setDropButtonText('portSelect', parseInt(port), true);
    console.log('port num:', parseInt(port));
    changeDropdownLabel('portSelect', portDisplayName[parseInt(port)]);
    changeDropdownAria('portSelect', parseInt(port));
}

function drawSenderSelect(senderCount) {
    var senderList = layout.senderList;

    for (const top of senderList) {
        var id = top.senderId;
        senderValList.push(id);
        senderDisplayName[id] = getLanguageText(languageTable, "Sender") + id;
    }

    const senderDropdown = $('#senderSelect');
    const dropdownMenu = $(senderDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(senderValList, senderDisplayName, 'senderSelect');

    const lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            var selected = senderValList[i];
            changeDropdownLabel("senderSelect", senderDisplayName[selected]);
            changeDropdownAria("senderSelect", i);
            currSender = selected;
            selectSender(currSender);
        });
    });

    $(dropdownMenu).html(ul);

    changeDropdownLabel("senderSelect", senderDisplayName[currSender]);
    var myIndex = senderValList.findIndex(function (item) {return item === currSender});
    changeDropdownAria("senderSelect", myIndex);

    if(senderCount === 0){
        $('#senderSelect .dropdown-menu').css('display', 'none');
    }
}

function drawInputDir() {
    const inputDropdown = $('#inputSelect');
    const dropdownMenu = $(inputDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(inputValList, inputDisplayName, 'inputSelect');

    const lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            var selected = inputValList[i]
            changeDropdownLabel("inputSelect", inputDisplayName[selected]);
            changeDropdownAria("inputSelect", i);
            var sender = layout.senderList[findSender(currSender)];
            var arrDirection = ['dirImgRight', 'dirImgLeft', 'dirImgBottom', 'dirImgTop'];
            sender.inputDirection = selected;
            sender.unitList = [];
            setInputDirectionColor( (parseInt(currSender))*2, selected );
            clearSendingMAC( parseInt(currSender)*2 );
            saveLayout();
            // 아래 로직은 하단의 화살표 표시하는거로 보임
            // $('#sender'+currSender+' [class^=dirImg]').remove();
            // var dirClass = arrDirction[parseInt(sender.inputDirection)];
            // $('#sender'+currSender).append('<div class="'+dirClass+'"><div>');
        });
    });

    $(dropdownMenu).html(ul);

    changeDropdownLabel("inputSelect", inputDisplayName[2]);
    changeDropdownAria("inputSelect", 2);
}

function drawPortSelection() {
    const portDropdown = $('#portSelect');
    const dropdownMenu = $(portDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(portValList, portDisplayName, 'portSelect');

    const lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            var selected = portValList[i];
            changeDropdownLabel("portSelect", portDisplayName[selected]);
            changeDropdownAria("portSelect", i);
            unitPort = selected;
            var sender = layout.senderList[findSender(currSender)];
            var inputDir = sender.inputDirection;
            var unitList = sender.unitList;
            var unitIdx = findUnit(sender, (Number(currUnit)+1) );
            var unit = unitList[ unitIdx ];
            var slave = currSender*2;
            var hValue = selected === 0 ? 2 : 1;
            var vValue = Number(unit.rows) + (unit.mini > 0 ? 1 : 0 );

            if(inputDir < 2){
                setHVNum(slave, currUnit, vValue, hValue);
            }else{
                setHVNum(slave, currUnit, hValue, vValue);
            }

            setDisplayPort(slave, currUnit, selected);
            unit.portSelection = selected;
            drawUnit(sender.unitList);
            saveLayout();
        });
    });

    $(dropdownMenu).html(ul);

    changeDropdownLabel("portSelect", '-');
    changeDropdownAria("portSelect", -1);
}

function drawMiniSelection(fullRows) {
    // var displayName = {};
    var displayName = new Map();
    var valList = [];
    var miniMax = fullRows ? 0 : 4;

    for (var i = 0; i < miniMax; i++) {
        valList.push(i);
        // displayName[i] = i;
        displayName.set(i, String(i))
    }

    $('#miniSelect').empty();

    const rowDropdown = $('#miniSelect');
    const dropdownMenu = $(rowDropdown).children(".dropdown-menu");

    if (!displayName || displayName.length < 1) {
        return;
    }
    const ul = createDropdownChildren(valList, displayName, 'miniSelect');
    if (!ul) {
        return;
    }

    const lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            var selected = valList[i]
            changeDropdownLabel("miniSelect", displayName[selected]);
            changeDropdownAria("miniSelect", i);
            var sender = layout.senderList[findSender(currSender)];
            var unitList = sender.unitList;
            var unitIdx = findUnit(sender, (Number(currUnit)+1) );
            var unit = unitList[ unitIdx ];
            if (unit !== undefined) {

                unit.mini = parseInt(selected);

                var slave = currSender*2;
                var dir = parseInt(sender.inputDirection);
                var hValue = unitPort === 0 ? 2 : 1;
                var vValue = parseInt(unit.rows) + (unit.mini > 0 ? 1 : 0 );

                if(dir < 2){
                    var tmpVal = hValue;
                    hValue = vValue;
                    vValue = tmpVal;
                }

                setHVNum(slave, currUnit, hValue, vValue);

                if(dir < 2) {
                    if( (unit.x + (unit.rows * unitPixels.height) + ( unit.mini * unitPixels.miniHeight ) ) > senderResolutionW ){
                        unit.x = 0;
                        setXYPos(slave, currUnit, unit.x, unit.y);
                    }
                } else {
                    if( (unit.y + ( unit.rows * unitPixels.height ) + ( unit.mini * unitPixels.miniHeight ) ) > 540 ){
                        unit.y = 0;
                        setXYPos(slave, currUnit, unit.x, unit.y);
                    }
                }

                drawUnit(sender.unitList);
                saveLayout();
            }
        });
    });

    $(dropdownMenu).html(ul);

    changeDropdownLabel("miniSelect", displayName[0]);
    changeDropdownAria("miniSelect", 0);

    if(fullRows){
        $('#miniSelect .dropdown-menu').css('display', 'none');
    }

}

function drawRowSelection() {
    for (var i = 1; i <= maxRows; i++) {
        rowValList.push(i);
        rowDisplayName[i] = i;
    }

    const rowDropdown = $('#rowSelect');
    const dropdownMenu = $(rowDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(rowValList, rowDisplayName, 'rowSelect');

    const lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            var selected = rowValList[i]
            changeDropdownLabel("rowSelect", rowDisplayName[selected]);
            changeDropdownAria("rowSelect", selected);
            unitRows = selected;
            var sender = layout.senderList[findSender(currSender)];
            var unitList = sender.unitList;
            var unitIdx = findUnit(sender, (Number(currUnit)+1) );
            var unit = unitList[ unitIdx ];
            if (unit !== undefined) {
                var slave = currSender*2;
                var dir = parseInt(sender.inputDirection);
                var hValue = unitPort === 0 ? 2 : 1;
                var vValue = parseInt(selected) + (unit.mini > 0 ? 1 : 0);

                if( parseInt(selected) === maxRows ){
                    unit.mini = 0;
                }

                drawMiniSelection(parseInt(selected) === maxRows);

                if(dir < 2){
                    var tmpVal = hValue;
                    hValue = vValue;
                    vValue = tmpVal;
                }
                setHVNum(slave, currUnit, hValue, vValue);
                unit.rows = selected;

                var realRows = parseInt(unit.rows) + (unit.mini > 0 ? 1 : 0);

                if(dir < 2){
                    if( (unit.x + realRows * unitPixels.height) > senderResolutionW ){
                        unit.x = 0;
                        setXYPos(slave, currUnit, unit.x, unit.y);
                    }
                }else{
                    if( (unit.y + realRows * unitPixels.height) > 540 ){
                        unit.y = 0;
                        setXYPos(slave, currUnit, unit.x, unit.y);
                    }
                }

                drawUnit(sender.unitList);
                saveLayout();
            }
        });
    });

    $(dropdownMenu).html(ul);

    changeDropdownLabel("rowSelect", rowDisplayName[2]);
    changeDropdownAria("rowSelect", 1);
}

function drawUnit(unitList){
    console.log('drawing unit')
    if( unitList === undefined ){
        return;
    }

    $('.unit').remove();
    var unitMax = ( unitList.length >= macList['sender'+(Number(currSender))].length );
    var filmWidth = unitPixels.width;
    var filmHeight = unitPixels.height;
    var miniHeight = unitPixels.miniHeight;

    setTimeout(function(){
        $('#addUnitBtn').attr('disabled', unitMax);
    }, 1000);
    $('#removeUnitBtn').attr('disabled', true);

    var inputDir = layout.senderList[findSender(currSender)].inputDirection;

    console.log('unit list:', unitList)
    unitList.forEach(function(unit, index){
        setTrimColor(parseInt(currSender)*2, index, unit.trimming.left, unit.trimming.right);
        var ID = 'unit'+ (unit.unitId -1);
        var filmLeftWidth = filmWidth - unit.trimming.left;
        var filmRightWidth = filmWidth - unit.trimming.right;
        var port = getLanguageText(languageTable, "Port") + ':';
        if (unit.portSelection === 0) {
            port += getLanguageText(languageTable, "Both");
        } else if (unit.portSelection === 1) {
            port += getLanguageText(languageTable, "Left");
        } else if (unit.portSelection === 2) {
            port += getLanguageText(languageTable, "Right");
        }

        // 예전 UI의 테이블 그리기
        var unitTxt = '<div class="view-item draggable ui-widget-content unit" id="'+ID+'" name="'+(unit.unitId -1)+'" style="position:absolute;">';
        var tableTxt = '<table id="unitTable'+(unit.unitId -1)+'" style="border: 0 solid black;" title="'+port+'">';

        if( inputDir < 2 ){
            var miniTxtRight = '';
            var miniTxtLeft = '';

            if( unit.mini > 0 ){
                var align = inputDir == 0 ? 'right' : 'left';
                if(unit.mini > 0){
                    miniTxtRight += '<td class="film virtual" align="'+align+'" style="height:'+filmRightWidth+'px;"><table><tr>';
                    for(var i=0; i<unit.mini; i++){
                        miniTxtRight += '<td class="mini" style="height:'+filmRightWidth+'px;"></td>';
                    }
                    miniTxtRight += '</tr></table></td>';
                }

                if(unit.mini > 0){
                    miniTxtLeft += '<td class="film virtual" align="'+align+'" style="height:'+filmRightWidth+'px;"><table><tr>';
                    for(var i=0; i<unit.mini; i++){
                        miniTxtLeft += '<td class="mini" style="height:'+filmLeftWidth+'px;"></td>';
                    }
                    miniTxtLeft += '</tr></table></td>';
                }
            }


            if(unit.portSelection == 0){

                tableTxt+= '<tr>';

                if(inputDir == 0){
                    tableTxt += miniTxtRight;
                }

                for(var i=0; i<unit.rows; i++){
                    tableTxt += '<td class="film" style="height:'+filmRightWidth+'px;"></td>';
                }

                if(inputDir == 1){
                    tableTxt += miniTxtRight;
                }

                tableTxt += '</tr>';
                tableTxt += '<tr>';
                if(inputDir == 0)
                    tableTxt += miniTxtLeft;
                for(var i=0; i<unit.rows; i++){
                    tableTxt += '<td class="film" style="height:'+filmLeftWidth+'px;"></td>';
                }
                if(inputDir == 1)
                    tableTxt += miniTxtRight;
                tableTxt+= '</tr>';
            }else{
                tableTxt += '<tr>';
                if(inputDir == 0){
                    tableTxt += (unit.portSelection == 1 ? miniTxtLeft : miniTxtRight );
                }
                for(var i=0; i<unit.rows; i++){
                    tableTxt += '<td class="film" style="height:'+ (unit.portSelection == 1 ? filmLeftWidth : filmRightWidth ) +'px"></td>';
                }
                if(inputDir == 1){
                    tableTxt += (unit.portSelection == 1 ? miniTxtLeft : miniTxtRight );
                }
                tableTxt+= '</tr>';
            }

        }else{
            var miniTxt = '';
            if( unit.mini > 0 ){
                miniTxt += '<tr>';

                var valign = inputDir == 2 ? 'bottom' : 'top';

                if(unit.portSelection == 0){

                    miniTxt += '<td class="film virtual" valign="'+valign+'" style="width:'+filmLeftWidth+'px;">';
                    miniTxt += '<table>';
                    for(var i=0; i<unit.mini; i++){
                        miniTxt += '<tr>';
                        miniTxt += '<td class="mini" style="width:'+filmLeftWidth+'px;"></td>';
                        miniTxt += '</tr>';
                    }
                    miniTxt += '</table>';
                    miniTxt += '</td>';

                    miniTxt += '<td class="film virtual" valign="'+valign+'" style="width:'+filmRightWidth+'px;">';
                    miniTxt += '<table>';
                    for(var i=0; i<unit.mini; i++){
                        miniTxt += '<tr>';
                        miniTxt += '<td class="mini" style="width:'+filmRightWidth+'px;"></td>';
                        miniTxt += '</tr>';
                    }
                    miniTxt += '</table>';
                    miniTxt += '</td>';

                }else{
                    miniTxt += '<td class="film virtual" valign="'+valign+'" style="width:'+ (unit.portSelection == 1 ? filmLeftWidth : filmRightWidth ) +'px;">';
                    miniTxt += '<table>';
                    for(var i=0; i<unit.mini; i++){
                        miniTxt += '<tr>';
                        miniTxt += '<td class="mini" style="width:'+(unit.portSelection == 1 ? filmLeftWidth : filmRightWidth )+'px;"></td>';
                        miniTxt += '</tr>';
                    }
                    miniTxt += '</table>';
                    miniTxt += '</td>';
                }

                miniTxt += '</tr>';
            }

            if(inputDir == 2)
                tableTxt += miniTxt;

            for(var i=0; i<unit.rows; i++){
                tableTxt+= '<tr>';

                if(unit.portSelection == 0){
                    tableTxt += '<td class="film" style="width:'+filmLeftWidth+'px;"></td>';
                    tableTxt += '<td class="film" style="width:'+filmRightWidth+'px;"></td>';
                }else{
                    tableTxt += '<td class="film" style="width:'+ (unit.portSelection == 1 ? filmLeftWidth : filmRightWidth ) +'px"></td>';
                }

                tableTxt+= '</tr>';
            }

            if(inputDir == 3){
                tableTxt += miniTxt;
            }
        }

        tableTxt+= '</table>';
        unitTxt += tableTxt + '</table></div>';

        $('#unitEditWraper').append(unitTxt);
        // 테이블 그리기 End
        // 각종 css 변경
        $('#'+ID).css('left', unit.x);
        $('#'+ID).css('top', unit.y);

        if( inputDir < 2 ){

            if( inputDir == 0){
                $('.mini').css('border-left', '1px solid black');
            }else{
                $('.mini').css('border-right', '1px solid black');
            }

            $('.film').css('width', filmHeight);
            $('.mini').css('width', miniHeight);

            $('#'+ID).css('width', ( (parseInt(unit.rows) +(unit.mini>0?1:0))  * filmHeight));
            $('#unitTable'+(unit.unitId -1) ).css('width', ( (parseInt(unit.rows) + (unit.mini>0?1:0)) * filmHeight));

            var height = 0;
            if(unit.portSelection == 0){
                height = filmLeftWidth+filmRightWidth;
            }else if(unit.portSelection ==1){
                height = filmLeftWidth;
            }else{
                height = filmRightWidth;
            }
            $('#'+ID).css('height', height );
            $('#unitTable'+(unit.unitId -1) ).css('height', height );
        }else{

            if( inputDir == 2){
                $('.mini').css('border-top', '1px solid black');
            }else{
                $('.mini').css('border-bottom', '1px solid black');
            }

            $('.film').css('height', filmHeight);
            $('.mini').css('height', miniHeight);

            $('#'+ID).css('height', ( (parseInt(unit.rows) +(unit.mini>0?1:0)) * filmHeight));
            $('#unitTable'+(unit.unitId -1) ).css('height', ( (parseInt(unit.rows) +(unit.mini>0?1:0)) * filmHeight));

            var width = 0;
            if(unit.portSelection == 0){
                width = filmLeftWidth+filmRightWidth;
            }else if(unit.portSelection ==1){
                width = filmLeftWidth;
            }else{
                width = filmRightWidth;
            }
            $('#'+ID).css('width', width );
            $('#unitTable'+(unit.unitId -1) ).css('width', width );
        }

        $('#'+ID).mousedown(function(){
            currUnit = $(this).attr('name');
            console.log('currUnit::'+currUnit);
            selectUnit(currUnit);

            if($('#patternPanel').css('display') !== 'none'){
                $('#patternDone').trigger('click');
            }
        });

        $('#'+ID).tooltip();

        $('#'+ID).draggable({
            containment: "#unitEditWraper",
            stack:".unit",
            scroll: false,
            drag: function( event, ui ) {
                var index = $(this).attr('name');
                var x = Math.floor( ui.position.left );
                var y = Math.floor( ui.position.top );

                var sender = layout.senderList[findSender(currSender)];
                var unit = unitList[ findUnit(sender, (Number(currUnit)+1) ) ];

                if(unit.mini > 0){
                    if(sender.inputDirection == '0'){
                        x += ( 4 - unit.mini ) * unitPixels.miniHeight;
                    }else if(sender.inputDirection == '2'){
                        y += ( 4 - unit.mini ) * unitPixels.miniHeight;
                    }
                }

                $('#unitX').val( x + senderPos.x );
                $('#unitY').val( y + senderPos.y );
            },
            stop : function( event, ui ){
                var sender = layout.senderList[findSender(currSender)];
                var unitX = Math.floor( ui.position.left );
                var unitY = Math.floor( ui.position.top );
                var x = unitX + senderPos.x;
                var y = unitY + senderPos.y;
                var unit = unitList[ findUnit(sender, (Number(currUnit)+1) ) ];
                var unitIdx = unit.unitId-1;

                unit.x = unitX;
                unit.y = unitY;

                if( sender.inputDirection >= 2 && unit.x - unit.trimming.left < 0 && Number(unit.portSelection) !== 2){
                    unit.x = unit.trimming.left;

                    var text1 = getLanguageText(languageTable, "Units cut left edge can not be placed in the X:(Cut Pixel) positions.");
                    $("#modalWarningMessage").empty();
                    $("#modalWarningMessage").html(text1);

                    setTimeout(function(){
                        $("#modalWarningMessage").empty();
                    }, 5000);

                    drawUnit(sender.unitList);
                } else if( sender.inputDirection < 2 && unit.y - unit.trimming.right < 0 && Number(unit.portSelection) !== 1){
                    unit.y = unit.trimming.right;

                    var text2 = getLanguageText(languageTable, "Units cut right edge cannot be placed in the Y:(Cut Pixel) positions.");
                    $("#modalWarningMessage").empty();
                    $("#modalWarningMessage").html(text2);

                    setTimeout(function(){
                        $("#modalWarningMessage").empty();
                    }, 5000);

                    drawUnit(sender.unitList);
                }

                if (sender.inputDirection < 2) {
                    setXYPos((parseInt(currSender))*2, unitIdx, unit.x, unit.y - (Number(unit.portSelection) !== 1 ? unit.trimming.right : 0));
                } else {
                    setXYPos((parseInt(currSender))*2, unitIdx, unit.x - (Number(unit.portSelection) !== 2 ? unit.trimming.left : 0), unit.y);
                }

                saveLayout();
            }
        });
        // css 변경 End
    });

    if( currUnit !== undefined ){
        selectUnit(currUnit);
    }

}

function findSender(senderId){
    if( (layout.senderList !== undefined) && (layout.senderList.length > 0) ) {
        for(var i=0; i<layout.senderList.length; i++){
            // string 과 int 므로 ==
            if(layout.senderList[i]["senderId"] == senderId){
                return i;
            }
        }
        return -1;
    } else{
        return -1;
    }
}

function findUnit(sender, targetId){
    var unitList = sender.unitList;
    for(var i=0; i<unitList.length; i++){
        // string 과 int 므로 ==
        if( targetId == unitList[i].unitId ){
            return i;
        }
    }
}

function getNewSenderId(){
    if( findSender(1) < 0 ){
        console.log('not exist::1');
        return 1;
    }else if( findSender(2) < 0 ){
        console.log('not exist::2');
        return 2;
    }else if( findSender(3) < 0 ){
        console.log('not exist::3');
        return 3;
    }else{
        console.log('not exist::4');
        return 4;
    }
}

function openPatternPanel(isAll){
    $('#patternRangeR').val(0);
    $('#patternRangeG').val(0);
    $('#patternRangeB').val(0);
    $('.patternVal').html(0);
    checkRangeUI('patternRangeR', 0, 255);
    checkRangeUI('patternRangeG', 0, 255);
    checkRangeUI('patternRangeB', 0, 255);

    if(isAll){
        setTestPattern( currSender*2, '1', 0, 0, 0, 'ff', 'ff' );
    }else{
        var sender = layout.senderList[findSender(currSender)];
        var unitList = sender.unitList;
        var unitIdx = findUnit(sender, (Number(currUnit)+1) );
        var unit = unitList[unitIdx];
        var mac5th = unit.mac[0];
        var mac6th = unit.mac[1];
        setTestPattern( currSender*2, '1', 0, 0, 0, mac5th, mac6th );
    }

    $('[id^=patternRange]').unbind('change');
    $('[id^=patternRange]').change(function(){
        checkRangeUI($(this).attr('id'), 0, 255);
        $('#'+$(this).attr('id')+'Val').html($(this).val());

        var R = $('#patternRangeR').val();
        var G = $('#patternRangeG').val();
        var B = $('#patternRangeB').val();

        var hexR = parseInt(R).toString(16);
        var hexG = parseInt(G).toString(16);
        var hexB = parseInt(B).toString(16);

        if(hexR.length < 2){
            hexR = '0'+hexR;
        }

        if(hexG.length < 2){
            hexG = '0'+hexG;
        }

        if(hexB.length < 2){
            hexB = '0'+hexB;
        }

        var patternColor = '#'+hexR+hexG+hexB;
        $('.printPattern').css('background-color', patternColor);

        if(isAll){
            setTestPattern( currSender*2, '1', R, G, B, 'ff', 'ff' );
        }else{
            var sender = layout.senderList[findSender(currSender)];
            var unitList = sender.unitList;
            var unitIdx = findUnit(sender, (Number(currUnit)+1) );
            var unit = unitList[unitIdx];
            var mac5th = unit.mac[0];
            var mac6th = unit.mac[1];
            setTestPattern( currSender*2, '1', R, G, B, mac5th, mac6th );
        }
    }).on('input', function(){
        checkRangeUI($(this).attr('id'), 0, 255);
    });

    $('#layoutControlPanel').hide();
    $('#patternPanel').show();
}

function closePatternPanel(){
    setTestPattern( (currSender)*2, '0', 0, 0, 0, 'ff', 'ff' );
    $('#layoutControlPanel').show();
    $('#patternPanel').hide();
}


function drawEdgeTable(dir, count){
    var tableText = '';
    var width = env.ledFilmType === 'color' ? unitPixels.width : 16;
    var height = env.ledFilmType === 'color' ? unitPixels.height : 16;
    var cellWidth = 100 / width;
    var cellHeight = 100 / height;

    for(var i=0; i<height; i++){
        tableText += '<tr style="">'
        for(var j=0; j<width; j++){
            if( dir === 'Left' ){
                if(j < Number(count) ){
                    tableText += '<td class="cutted" style="height:'+cellHeight+'%; width:'+cellWidth+'%; border: 1px solid black;"></td>'
                }else{
                    tableText += '<td class="notcutted" style="height:'+cellHeight+'%; width:'+cellWidth+'%; border: 1px solid black;"></td>'
                }
            }else{
                if(j+ Number(count) >= unitPixels.width){
                    tableText += '<td class="cutted" style="height:'+cellHeight+'%; width:'+cellWidth+'%; border: 1px solid black;"></td>'
                }else{
                    tableText += '<td class="notcutted" style="height:'+cellHeight+'%; width:'+cellWidth+'%; border: 1px solid black;"></td>'
                }
            }
        }
        tableText += '</tr>'
    }

    $('#edgeTable').empty();
    $('#edgeTable').append(tableText);
}
function saveLayout(){
    layout.senderList.forEach(function(sender, index){
        var portrait = sender.inputDirection >= 2;
        sender.unitList.forEach(function(unit){
            if(portrait){
                unit.width = unit.portSelection === 0 ? unitPixels.width * 2 : unitPixels.width;
                unit.height = (unit.rows * unitPixels.height) + (unit.mini * unitPixels.miniHeight);
            }else{
                unit.width = (unit.rows * unitPixels.height) + (unit.mini * unitPixels.miniHeight);
                unit.height = unit.portSelection === 0 ? unitPixels.width * 2 : unitPixels.width;
            }
        });

        if (index === layout.senderList.length - 1) {
            saveFPGAConfig();
        }
    });

    setSystemSettings('commercial', {'ledLayout' : layout });
}