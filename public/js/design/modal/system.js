document.addEventListener("DOMContentLoaded", function() {
    const myContainer = document.getElementById('system-popup');
    const myFocusTrap = focusTrap.createFocusTrap('#system-popup', {
        onActivate: function () {myContainer.classList.add('is-active')},
        onDeactivate: function () {myContainer.classList.remove('is-active')},
    });

    addProgressModal('sw-progress-modal');
    addCompleteModal('sw-complete-modal');

    $('#goUpdateBtn').on('click', function () {
        myFocusTrap.deactivate();
        _closePopup('system-popup');
        showMyModal('update-popup');

        const a = $('#update1-wrapper');
        setTimeout(function () {
            resizeTableHead(a);
        },250);
    });

    $('#btn-system-ok').on('click', function () {
        myFocusTrap.deactivate();
        _closePopup('system-popup');
    });

    $('#system-popup').on('cssClassChanged', function (e) {
        // e.stopPropagation();
        getInformation();
        myFocusTrap.activate();
    });

    $('#goFirstUseBtn').on('click', function () {
        myFocusTrap.deactivate();
        _closePopup('system-popup');
        firstUseModal.modal();
    });
});

function getInformation() {
    getSignageName(function(ret) {
        $("#modalSignageName").html(ret);
    });

    getBasicInfo(function(msg) {
        $('#modalModelName').html(msg.modelName);
        $('#modalSerialNumber').html(msg.serialNumber);

        $('#modalFirmwareVersion').html(msg.firmwareVersion);
        $('#modalMicomVersion').html(msg.micomVersion);
        $('#modalBootLoaderVersion').html(msg.bootLoaderVersion);
    });

    getWebOSInfo(function(msg) {
        $('#modalWebosVersion').html(msg.core_os_release + " (" + msg.core_os_release_codename + ")");
    });

    if (env.supportLedFilm) {
        $('#fpgaVer').show();

        if(env.ledFilmType === 'color' ){
            $('#FPGAVersion').html('');
            for(var i=0; i<4; i++){
                getFPGAVersionText(i);
            }
        }else{
            getFpgaVersion(1, function(msg){
                $('#FPGAVersion').html(msg.result.strArray[0]);
            });
        }
    }
}

function getFPGAVersionText(index){
    getFpgaVersionColor((index+1)*2, function(msg){
        console.log(msg.result);

        if (msg.result) {
            var result = getLanguageText(languageTable, "Sender") + (index+1) + '('+ msg.result.version1st + 'x' + msg.result.version2nd.toString(16) +') ';
            $('#FPGAVersion').append(result);
        }
    });
}