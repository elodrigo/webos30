//for Chart
var chartRefresh = 5;
var currChartPage = 0;
var currHistoryCount = 0;
var totalChartPage = 0;
var historyCountPerPage = [];
var useHistoryCount = 0;
var chartOffset = 0;
var currtime = undefined;
var currHour = 0;
var currMinute = 0;
var initOffset = 0;
var currOffset = 0;
var originAM = false;
var isAM = true;
var isCelsius = true;

document.addEventListener("DOMContentLoaded", function() {
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/popup_temperature.css">');

    const myContainer = document.getElementById('temperature-popup');
    const myFocusTrap = focusTrap.createFocusTrap('#temperature-popup', {
        onActivate: function () {myContainer.classList.add('is-active')},
        onDeactivate: function () {myContainer.classList.remove('is-active')},
    });

    drawTempSelect();
    drawTimeSelect();

    $('#btn-temp-ok').on('click', function () {
        myFocusTrap.deactivate();
        _closePopup('temperature-popup');
    });

    $('#btn-temp-modal-more').on('click', function () {
        var isChart = $(this).data('name') === 'chart';
        if (isChart) {
            $(this).data('name', 'table');
            $(this).text("Charts");
            $('#temperature-modal-chart').css('display', 'none');
            $('#temperature-modal-table').css('display', 'block');
        } else {
            $(this).data('name', 'chart');
            $(this).text("Table");
            $('#temperature-modal-chart').css('display', 'block');
            $('#temperature-modal-table').css('display', 'none');
        }
    });

    $('#temperature-popup #prevChartBtn').on('click', function () {
        currChartPage++;
        console.log(currChartPage)
        setTimeout(function () {
            showModalTempChart();
        }, 200);
    });

    $('#temperature-popup #nextChartBtn').on('click', function () {
        currChartPage--;
        console.log(currChartPage)
        setTimeout(function () {
            showModalTempChart();
        }, 200);
    });

    $('#chart-modal-refresh').on('click', function () {
        setTimeout(function () {
            showModalTempChart();
        }, 200);
    });

    getCurrentTime(function (msg) {
        originAM = (msg.hour < 12);
    });

    $('#temperature-popup').on('cssClassChanged', function (e) {
        // e.stopPropagation();
        myFocusTrap.activate();
        setTimeout(function() {
            showModalTempChart();
        }, 200);
    });

    makeKeyDownDropdown('temp-select-dropdown');
    makeKeyDownDropdown('time-select-dropdown');
});

function getOffsetTime() {

    getCurrentTime(function(msg){
        currtime = msg;
        currtime.hour = currtime.hour - ( currChartPage*12 );
        if( currtime.hour < 0 ){
            currtime.hour += 24;
            currtime.day = currtime.day-1;
        }

        if( currtime.hour > 11 ){
            isAM = false;
            currtime.hour = currtime.hour%12;
        }else{
            isAM = true;
        }

        currentTime = msg.current;
        currHour = msg.hour;
        currMinute = msg.minute;
        initOffset = currMinute%chartRefresh;
        currOffset = initOffset;

        var offSetDate = new Date(currtime.current.split(' ').splice(0, 5).join(' '));
        currentTime = new Date(offSetDate.getTime() - (currOffset*60*1000 + currChartPage*(60*1000*60*12) ) ).toString();
        setHistoryOffset();
    });
}

function setHistoryOffset() {
    getTempHistory('main', 0, 10000, function(tempHistory){

        var totalHistoryCount = tempHistory.history.length;


        var offsetedHistory = tempHistory.history.splice(currOffset, 10000);

        var filteredHistory = [];
        var resultHistory = [];
        currMinute -= currOffset;

        for(var i=0; i<offsetedHistory.length; i++){
            if( i%chartRefresh == 0 ){

                var tmpVal = {};
                tmpVal.value = offsetedHistory[i];

                if(i>0){
                    currMinute = currMinute - chartRefresh;
                }

                if(currMinute < 0){
                    currMinute += 60;

                    if(--currHour < 0){
                        currHour += 12;
                        tmpVal.am = !isAM;
                    }
                }

                tmpVal.minute = currMinute;
                tmpVal.hour = currHour;
                tmpVal.am = isAM;

                filteredHistory.push(tmpVal);

                if( currMinute==0 ){
                    if( currHour==0 ){
                        resultHistory.push(filteredHistory);
                        filteredHistory = [];
                    }
                }
            }
        }

        if(filteredHistory.length > 0)
            resultHistory.push(filteredHistory);

        var selectedHistory = [];
        for(var j=0; j<resultHistory[currChartPage].length; j++){
            if( isCelsius == true ){
                selectedHistory.push(resultHistory[currChartPage][j].value);
            }else{
                selectedHistory.push(roundToOne((resultHistory[currChartPage][j].value) * 1.8 + 32)  );
            }
        }

        var tmpTime = currtime.current.split(' ').splice(0, 5);
        tmpTime[4] = resultHistory[currChartPage][0].hour + ':' + resultHistory[currChartPage][0].minute + ':00';
        tmpTime[2] = currentTime.split(' ')[2];
        var offSetDate = new Date(tmpTime.join(' '));

        var year = offSetDate.getFullYear();
        var month = offSetDate.getMonth() + 1;
        var day = offSetDate.getDate();

        if(month<10) month = '0'+month;
        if(day<10) day = '0'+day;

        var currentDate = year+'-'+month+'-'+day;

        var myInterval = tempHistory.interval*chartRefresh

        var tempTableHistory = {
            main: selectedHistory
        };

        drawTempChart('myTempModal', tempTableHistory, myInterval, isCelsius);
        drawChartPaging(resultHistory[currChartPage][0].am, currentDate, currChartPage==(resultHistory.length-1))
    });

}

function showModalTempChart() {
    readTempType(function(tempType){
        isCelsius = tempType.isCelsius;
        getOffsetTime();
    });
}

function drawChartPaging(isAM, currentDate, lastPage){
    $('#chartDate').text(currentDate);

    if (originAM) {
        if (currChartPage%2 == 0) {
            $('#temperature-popup #prevChartBtn').text(getLanguageText(languageTable, "Prev DAY"));
            $('#temperature-popup #nextChartBtn').text(getLanguageText(languageTable, "PM"));
        } else {
            $('#temperature-popup #prevChartBtn').text(getLanguageText(languageTable, "AM"));
            $('#temperature-popup #nextChartBtn').text(getLanguageText(languageTable, "Next Day"));
        }
    } else {
        if (currChartPage%2 == 0) {
            $('#temperature-popup #prevChartBtn').text(getLanguageText(languageTable, "AM"));
            $('#temperature-popup #nextChartBtn').text(getLanguageText(languageTable, "Next Day"));
        } else {
            $('#temperature-popup #prevChartBtn').text(getLanguageText(languageTable, "Prev DAY"));
            $('#temperature-popup #nextChartBtn').text(getLanguageText(languageTable, "PM"));
        }
    }

    $('#temperature-popup #prevChartBtn, #nextChartBtn').prop('disabled', false);

    if (lastPage) {
        $('#temperature-popup #prevChartBtn').prop('disabled', true);
    }

    if (currChartPage === 0) {
        $('#temperature-popup #nextChartBtn').prop('disabled', true);
    }
}

function drawTempSelect() {
    const displayName = {
        true: "℃",
        false: "℉"
    };
    const valList = [true, false];

    readTempType(function (tempType) {
        isCelsius = tempType.isCelsius;

        const tempDropdown = $('#temp-select-dropdown');
        const dropdownMenu = $(tempDropdown).children(".dropdown-menu");

        const ul = createDropdownChildren(valList, displayName, 'temp-select-dropdown');

        const lis = $(ul).children('li');
        lis.each(function (i) {
            $(this).on('click', function () {
                var selected = valList[i];
                changeDropdownLabel("temp-select-dropdown", displayName[selected]);
                changeDropdownAria("temp-select-dropdown", i);
                writeTempType(selected);
                showModalTempChart();
            });
        });

        $(dropdownMenu).html(ul);

        changeDropdownLabel('temp-select-dropdown', displayName[valList[tempType.isCelsius ? 0 : 1]]);
        changeDropdownAria('temp-select-dropdown', tempType.isCelsius ? 0 : 1);
    });
}

function drawTimeSelect() {
    autoInterval = setInterval(function() {
        showModalTempChart();
    }, 600*1000 );

    var addChar = '';
    if (env.locale === 'ar-SA') {
        addChar = "\u202A";
    }

    const displayName = {
        '300': addChar + getLanguageText(languageTable, "5Min"),
        '600': addChar + getLanguageText(languageTable, "10Min")
    };
    const valList = ['300', '600'];

    const timeDropdown = $('#time-select-dropdown');
    const dropdownMenu = $(timeDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(valList, displayName, 'time-select-dropdown');

    const lis = $(ul).children('li');
    for (let i = 0; i < valList.length; i++) {
        $(lis[i]).on('click', function () {
            var selected = valList[i]
            changeDropdownLabel("time-select-dropdown", displayName[selected]);
            changeDropdownAria("time-select-dropdown", i);
            clearInterval(autoInterval);
            autoInterval = setInterval(function() {
                showModalTempChart();
            }, selected*1000 );
        });
    }

    $(dropdownMenu).html(ul);

    changeDropdownLabel('time-select-dropdown', displayName['600']);
    changeDropdownAria('time-select-dropdown', 1);
}