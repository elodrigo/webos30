document.addEventListener("DOMContentLoaded", function() {
    var title = `${getLanguageText(languageTable, "{apptitle} will be deleted.").replace('{apptitle}', 'LG ConnectedCare')}<br>${getLanguageText(languageTable, "Do you want to proceed?")}`;
    $('#connected-reset-popup .text-content').html(title);

    const myContainer = document.getElementById('connected-reset-popup');
    const myFocusTrap = focusTrap.createFocusTrap('#connected-reset-popup', {
        onActivate: function () {myContainer.classList.add('is-active')},
        onDeactivate: function () {myContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });

    $('#connected-reset-popup').on('cssClassChanged', function (e) {
        // e.stopPropagation();
        myFocusTrap.activate();
    });

    $('#btn-reset-ok').on('click', function () {
        appReset();
    });

    $('#btn-reset-cancel').on('click', function () {
        _closePopup('connected-reset-popup');
    });


});

function appReset() {
    care365Disable( function() {
        remove365CareApp( function() {
            setSystemSettings('commercial', {
                care365Enable: "off",
                signage365CareAccountNumber: "",
                signage365CareAccountName: ""
            }, function() {
                setTimeout( function() {
                    // $('#resetModal').modal('hide');
                    _closePopup('connected-reset-popup');
                    location.reload();
                }, 2500);
            });
        });
    });
}