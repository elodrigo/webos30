var updateStarted = false;
var updatePercent = 0;

document.addEventListener("DOMContentLoaded", function() {
    const myContainer = document.getElementById('update-popup');
    const myFocusTrap = focusTrap.createFocusTrap('#update-popup', {
        onActivate: function () {myContainer.classList.add('is-active')},
        onDeactivate: function () {myContainer.classList.remove('is-active')},
    });

    addAlertModals('update-warning-modal', getLanguageText(languageTable, 'Unsupported File type.'));
    addProgressModal('upload-progress-modal', getLanguageText(languageTable, 'Uploading'));

    var sw = $('#update-sw');
    var fpga = $('#update-fpga');

    sw.on('click', function () {
        sw.attr("aria-selected", "true");
        fpga.attr("aria-selected", "false");
    });

   fpga.on('click', function () {
        sw.attr("aria-selected", "false");
        fpga.attr("aria-selected", "true");
    });

    function uploadFile(file) {
        var formData = new FormData();
        formData.append('update', file);
        formData.append('target', 'update');

        showMyModal('upload-progress-modal')

        var xhr = new XMLHttpRequest();

        xhr.open('post', '/upload', true);

        xhr.upload.onprogress = function(e) {
            if (e.lengthComputable) {
                var per = Math.round((e.loaded / e.total) * 100);
                updateProgressModal('upload-progress-modal', per);
            }
        }

        xhr.upload.onload = function(e) {
            setTimeout(function() {
                _closePopup('upload-progress-modal');
                window.location.reload();
            }, 2000);
        }

        xhr.onerror = function(e) {
            _closePopup('upload-progress-modal');
            showMyModal('update-warning-modal', getLanguageText(languageTable, "An error occurred while submitting the form."));
        }

        xhr.onload = function() {
            _closePopup('upload-progress-modal');
        }

        xhr.send(formData);
    }

   $('#update-file-find').on('change', function () {
       var fileName = $(this).val();

       if( fileName.indexOf('epk') > 0 ){
           $('#update-fileName').html($(this).val());
       } else {
           $(this).val('');
           showMyModal('update-warning-modal', getLanguageText(languageTable, 'Unsupported File type.'));
       }
   });

   $('#update-fpga-file-find').on('change', function () {
       var fileName = $(this).val();

       if (env.ledFilmType === 'mono') {
           if (fileName.indexOf('hexout') > 0) {
               $('#FPGAfileName').html($(this).val());
           } else {
               $(this).val('');
               showMyModal('update-warning-modal', getLanguageText(languageTable, 'Unsupported File type.'));
           }
       } else {
           if( fileName.indexOf('img') > 0  ){
               $('#FPGAfileName').html($(this).val());
           }else{
               $(this).val("");
               showMyModal('update-warning-modal', getLanguageText(languageTable, 'Unsupported File type.'));
           }
       }
   });

   $('#btn-update-cancel').on('click', function () {
       myFocusTrap.deactivate();
       _closePopup('update-popup');
   });

    $('#btn-update-upload').on('click', function () {
        myFocusTrap.deactivate();
        _closePopup('update-popup');
        var file = document.getElementById('update-file-find').files[0];

        var isFpga = fpga.attr('aria-selected');
        if (isFpga) {
            file = document.getElementById('update-fpga-file-find').files[0];
        }

        if (!file) {
            showMyModal('update-warning-modal', getLanguageText(languageTable, 'Please choose a file to upload'));
            return;
        }

        var fileName = file.name;
        var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
        var supportExt = ['epk', 'hexout', 'img'];

        if (supportExt.indexOf(ext) < 0) {
            showMyModal('update-warning-modal', getLanguageText(languageTable, 'Unsupported File type.'));
            return;
        }

        if (file.size > env.maxSize) {
            showMyModal('update-warning-modal', getLanguageText(languageTable, 'Flash memory is not enough.'));
            return;
        }

        if (env.updateFiles.indexOf(fileName) >= 0) {
            $('#overwrite-modal').remove();
            addConfirmModals('', 'overwrite-modal', getLanguageText(languageTable, 'information message'), getLanguageText(languageTable, 'Overwrite?'),
                false, function () {
                    setTimeout(function () {
                        uploadFile(file);
                    }, 500);
                });
            showMyModal('overwrite-modal');
            return;
        }

        uploadFile(file);
    });

    $('#update-popup').on('cssClassChanged', function (e) {
        e.stopPropagation();
        myFocusTrap.activate();
        $('#updateFileTable > tbody').empty();
        $('#FPGAFileTable > tbody').empty();

        var reflowHeader = `<div class="table-header">
                            <div class="row">
                                <div class="thead">${getLanguageText(languageTable, "File Name")}</div>
                                <div class="thead">${getLanguageText(languageTable, "Update")}</div>
                            </div>
                        </div>`;

        let updateUl = document.createElement('ul');
        updateUl.classList.add('table-body');
        let fpgaUl = document.createElement('ul');
        fpgaUl.classList.add('table-body');

        env.updateFiles.forEach(function (updateFile) {
            var fileHTML = `<tr class="file-list">
            <td>${updateFile}</td>
            <td class="update" data-name="${updateFile}">03.22.0</td>
            <td>
                <button type="button" class="btn btn-delete" data-name="${updateFile}">${getLanguageText(languageTable, "Delete")}</button>
            </td>`;

            var reflowHTML =
                `<li class="table-row">
                    <div class="summary-data">
                        <div class="row">
                            <div class="table-cell center">${updateFile}</div>
                            <div class="table-cell">
                                <button type="button" role="listbox" class="btn btn-expand" aria-expanded="false">03.23.20</button>
                            </div>
                        </div>
                    </div>
                    <div class="all-data-box">
                        <ul class="all-data">
                            <li>
                                <span class="field-label">${getLanguageText(languageTable, "Delete")}</span>
                                <span class="field-content align-center">
                                    <button type="button" class="btn btn-delete">${getLanguageText(languageTable, "Delete")}</button>
                                </span>
                            </li>
                        </ul>
                    </div>
                </li>`;

            if (updateFile.indexOf('epk') > 0) {
                $('#updateFileTable > tbody').append(fileHTML);
                $(updateUl).append(reflowHTML);
            } else {
                $('#FPGAFileTable > tbody').append(fileHTML);
                $(fpgaUl).append(reflowHTML);
            }
        });
        var nodataHTML = `<div class="nodata-text">
                            ${getLanguageText(languageTable, "Select a file to upload")} (EPK file)
                        </div>`;

        var reflowUpdate = $('#updateFileTable-reflow');
        if ($(updateUl).children('li').length > 0) {
            reflowUpdate.empty();
            reflowUpdate.append(reflowHeader);
            reflowUpdate.append(updateUl);
        } else {
            reflowUpdate.empty();
            reflowUpdate.append(nodataHTML);
        }

        var reflowFpga = $('#FPGAFileTable-reflow');
        if ($(fpgaUl).children('li').length > 0) {
            reflowFpga.empty();
            reflowFpga.append(reflowHeader);
            reflowFpga.append(fpgaUl);
        } else {
            reflowFpga.empty();
            reflowFpga.append(nodataHTML);
        }

        var btnUpdate = $('.convert-table-to-list').find('button');
        $(btnUpdate).on('click', function () {
            var myRow = $(this).parents('li');
            var isExpanded = $(myRow[0]).hasClass('expand');
            if (isExpanded) {
                $(myRow[0]).removeClass('expand');
                $(this).attr('aria-expanded', "false");
            } else {
                $(myRow[0]).addClass('expand');
                $(this).attr('aria-expanded', "true");
            }
        });
    });

    $('.update').on('click', function () {
        var fileName = $(this).data("name");
        if (fileName.indexOf('.epk') > 0) {
            showMyModal('sw-progress-modal');
            // displayLoading(true);
            swupdate(fileName, false);
            setTimeout(function () {
                getUpdateStatus(function(msg) {

                    if (msg.status !== 'in progress' && !updateStarted) {
                        // displayLoading(false);
                        _closePopup('sw-progress-modal');
                        showMyModal('update-warning-modal', getLanguageText(languageTable, 'Cannot start S/W update. Check xxx file please.').replace('xxx', fileName));
                    }
                });
            }, 10 * 1000);
        } else if (fileName.indexOf('.epk') > 0) {
            // displayLoading(true);
            showMyModal('sw-progress-modal');
            ledFpgaUpdate(fileName, function(msg) {
                setTimeout(function() {
                    var fpgaProgressTimer = setInterval(function() {
                        getFpgaUpdatePercent(function(ret) {
                            getFPGAUpdateErrorStatus(function(retErrorStatus){

                                if( !(ret.returnValue && retErrorStatus.returnValue) ) {
                                    clearInterval(fpgaProgressTimer);
                                    _closePopup('sw-progress-modal');
                                    // displayLoading(false);
                                }

                                var percent = ret.Percent;

                                if (percent === undefined) {
                                    // displayLoading(false);
                                    _closePopup('sw-progress-modal');
                                    showMyModal('sw-complete-modal');
                                } else {
                                    updateProgressModal('sw-progress-modal', percent);
                                }
                            });
                        });
                    }, 2 * 1000);
                }, 11 * 1000);
            });
        } else {
            overlaySenderUpdate(true, 'update-popup');
            $('.overlayBtn').click(function(){
                if( $(this).prop('id').length == 0 ){
                    var slave = Number( $(this).attr('name') ) * 2;
                    updateSender(slave, fileName);
                    $('#overlayBtnTable').hide();
                    $('#updateImage').show();
                }
            });
        }
    });

    $('.btn-delete').on('click', function () {
        var fileName = $(this).data('name');
        deleteFile("update", fileName, function() {
            _closePopup('update-popup');
            window.location.reload();
            showMyModal('update-popup');
        });
    });

    socket.on('swupdate', function(msg) {

        updateStarted = true;
        if (updatePercent > 0 && msg === 0) {
            _closePopup('sw-progress-modal');
            showMyModal('update-warning-modal', getLanguageText(languageTable, "Fail to update. Check xxx file please.").replace('xxx', fileName));
            return;
        }

        updatePercent = msg;
        updateProgressModal('sw-progress-modal', msg);
        if (msg == 100) {
            _closePopup('sw-progress-modal');
            showMyModal('sw-complete-modal');
        }
    });
});

function updateSender(slave, fileName){
    moveUpdateFile(fileName);

    getNetworkStatus(function(msg) {
        var connection = msg.wired;
        var ip = connection.ipAddress.split('.');
        var tftp = '';

        setIP( slave, ip[0], ip[1], ip[2], (50+slave) , ip,  function(ret){
            console.log('setIP::');
            console.log(ret);
        });

        execTFTP(function(excuteMsg){
            console.log(excuteMsg);
            tftp = excuteMsg+1;
        });

        setFpgaControl(slave, 1 , 1, function(triggerMsg){
            console.log(triggerMsg);
        });

        var interval = setInterval(function(){
            getUpgradeStatus(slave, function(upgradeRet){
                console.log('update Status'+upgradeRet.result.value);
                if(upgradeRet.result.value == 10){
                    clearInterval(interval);

                    var overlayInterval = setInterval(function(){
                        getFpgaVersionColor(slave, function(ver){
                            console.log('version :: '+ver.result.version2nd);
                            if(ver.result.version2nd > 0){
                                $('#overlay_msg').html(Locale.getText("Success"));
                                setTimeout(function(){
                                    overlaySenderUpdate(false);
                                }, 5000);
                                clearInterval(overlayInterval);
                                killTFTP(tftp);
                                $('.overlayBtn').unbind('click');
                            }
                        });
                    }, 1000);

                }else if(upgradeRet.result.value == 128){
                    $('#overlay_msg').html(Locale.getText("Fail"));
                    setTimeout(function(){
                        overlaySenderUpdate(false);
                    }, 5000);
                    clearInterval(interval);
                    killTFTP(tftp);
                    $('.overlayBtn').unbind('click');
                }
            });
        }, 1000);
    });
}