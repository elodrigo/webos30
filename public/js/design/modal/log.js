var index = 0;
var order = false; // 0 descending / 1 ascending
var sortOrder = [false,false,false,false];

document.addEventListener("DOMContentLoaded", function() {
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/popup_log_report.css">');

    var myContainer = document.getElementById('log-popup');
    var myFocusTrap = focusTrap.createFocusTrap('#log-popup', {
        onActivate: function () {myContainer.classList.add('is-active')},
        onDeactivate: function () {myContainer.classList.remove('is-active')},
    });

    sepLogString = log.split('\n');
    sepLogString.splice(sepLogString.length-1, 1);
    logRowCount = sepLogString.length;

    drawRowSelect();
    drawFilterSelect();
    drawPagination();

    $('#btn-log-cancel').on('click', function () {
        $('body').css('overflow', 'auto');
        myFocusTrap.deactivate();
        _closePopup('log-popup');
    });

    $('#btn-log-ok').on('click', function () {
        $('body').css('overflow', 'auto');
        myFocusTrap.deactivate();
        _closePopup('log-popup');
    });

    $('#log-popup').on('cssClassChanged', function (e) {
        // e.stopPropagation();
        myFocusTrap.activate();
        $('body').css('overflow', 'hidden');
        currLogPage = 0;
        currLogPageM = 0;
        pagingLog();
        pagingLogM();
    });

    $('#btn-log-download').on('click', function () {
        var logForDown = 'Date,	Sensor,	Event,  State' + '\n';

        sepLogString.forEach(function(data){
            data = data.replace(/^ */g, "").replace(/\n[ ]+/g, "\n").replace(/\t/g, " ").replace(/ {2,}/g, " ");
            data = data.split(',')[4]+','+data.split(',')[0]+','+data.split(',')[1]+','+data.split(',')[3];
            logForDown += data.replace(/Please, Set a time/g, "Please Set a time") + '\n'
        });

        logForDown.replace(/Please, Set a time/g, "Please Set a time");
        downloadFile('log.csv', 'data:text/csv;charset=UTF-8,' + encodeURIComponent(logForDown), logForDown);
    });

    makeKeyDownDropdown('row-dropdown');
    makeKeyDownDropdown('filter-dropdown');
});

function printLog(rowPerPage) {
    var tbodyStr = '';

    for(var i=currLogPage*rowPerPage; i<(currLogPage+1)*rowPerPage; i++){
        if( i === logRowCount ) {
            break;
        }
        if( sepLogString[i].length === 0 ) {
            break;
        }

        tbodyStr += '<tr>';

        tbodyStr += '<td>' + sepLogString[i].replace(/Please, Set a time/g, "Please Set a time").split(',')[4].trim() + '</td>';
        tbodyStr += '<td>' + sepLogString[i].split(',')[0].trim() + '</td>';
        tbodyStr += '<td>' + sepLogString[i].split(',')[1].trim() + '</td>';
        var statusStr = sepLogString[i].split(',')[3].trim().includes('NG') ? 'Not OK' : sepLogString[i].split(',')[3].trim();
        tbodyStr += '<td>' + statusStr + '</td>';

        tbodyStr += '</tr>';
    }

    $('#log-table > tbody').empty();
    $('#log-table > tbody').append(tbodyStr);
}

function printReflowLog(rowPerPage) {
    var tableBody = '';

    for (var i=currLogPageM*rowPerPage; i<(currLogPageM+1)*rowPerPage; i++) {
        if( i === logRowCount ) {
            break;
        }
        if( sepLogString[i].length === 0 ) {
            break;
        }

        var tableRow = `<li class="table-row">
            <div class="summary-data">
                <div class="row">
                    <div class="table-cell center">${sepLogString[i].replace(/Please, Set a time/g, "Please Set a time").split(',')[4]}</div>
                    <div class="table-cell">
                        <button type="button" role="listbox" class="btn btn-expand"
                                aria-expanded="false">${sepLogString[i].split(',')[0]}</button>
                    </div>
                </div>
            </div>
            <div class="all-data-box">
                <ul class="all-data">
                    <li>
                        <span class="field-label">${getLanguageText(languageTable, "Event")}</span>
                        <span class="field-content">${sepLogString[i].split(',')[1]}</span>
                    </li>
                    <li>
                        <span class="field-label">${getLanguageText(languageTable, "State")}</span>
                        <span class="field-content">${(sepLogString[i].split(',')[3].includes('NG') ? 'Not OK' : sepLogString[i].split(',')[3])}</span>
                    </li>
                </ul>
            </div>
        </li>`;

        tableBody += tableRow;
    }

    $('#log-table-reflow').html(tableBody);

    $('#log-table-reflow .summary-data').on('click', function () {
        var parent = $(this).parent();
        var isExpanded = $(parent).hasClass('expand');
        if (isExpanded) {
            $(parent).removeClass('expand');
        } else {
            $(parent).addClass('expand');
        }
    });
}

function pagingLog() {
    var rowPerPage = Number($('#row-dropdown-label').text().split(' ')[0]);
    var currPageText = (currLogPage*rowPerPage + 1)+'-'+(currLogPage*rowPerPage + rowPerPage);
    var lastPage = parseInt(logRowCount/rowPerPage);
    var pageTxt = getLanguageText(languageTable, 'XX of YY');
    // var startSet = currLogPage + 1;
    var logPageSet = 5;
    var startSet = (Math.ceil((currLogPage + 1) / logPageSet) - 1) * logPageSet + 1;

    var endSet = startSet + 5;
    if (endSet > lastPage) {
        endSet = lastPage + 2;
    }

    $('#log-popup .go-prev').prop('disabled', false);
    $('#log-popup .go-next').prop('disabled', false);
    if (currLogPage <= 0) {
        $('#log-popup .go-prev').prop('disabled', true);
    }
    if (currLogPage > lastPage) {
        $('#log-popup .go-next').prop('disabled', true);
        currPageText = (currLogPage * rowPerPage + 1) + '-' + logRowCount;
    }

    var pageHtml = '';
    for (var i=startSet; i < endSet; i++) {
        var li = `<a href="javascript:void(0);" role="button" data-num=${i} id="page-${i}" class="page page-${i} ${i === currLogPage + 1 ? 'current' : ''}"
                    aria-label="${i} page." aria-pressed="${i === currLogPage + 1 ? 'true' : 'false'}"><span>${i}</span></a>`;
        pageHtml += li;
    }

    var pagers = $('#log-popup .pager');
    var pagersPC = $(pagers[0]);
    pagersPC.html(pageHtml);
    var prevPagers = $('#log-popup .go-prev');
    var prevPagersPC = $(prevPagers[0]);
    prevPagersPC.off('click');
    var nextPagers = $('#log-popup .go-next');
    var nextPagersPC = $(nextPagers[0]);
    var beforePagers = $('#log-popup .go-before');
    var beforePagersPC = $(beforePagers[0]);
    beforePagersPC.off('click');
    var afterPagers = $('#log-popup .go-after');
    var afterPagersPC = $(afterPagers[0]);
    afterPagersPC.off('click');
    nextPagersPC.off('click');
    var pagesPC = pagersPC.children('.page');

    $(pagesPC).each(function (i, elem) {
        $(elem).on('click', function () {
            paginationClicker(pagesPC, i);
            currLogPage = $(this).data('num') - 1;
            pagingLog();
        });
    });

    prevPagersPC.on('click', function () {
        if (currLogPage > 0) {
            currLogPage--;
            goPrevClicker(pagesPC);
            pagingLog();
        }
    });

    nextPagersPC.on('click', function () {
        if (currLogPage < lastPage) {
            currLogPage++;
            goNextClicker(pagesPC);
            pagingLog();
        }
    });

    beforePagersPC.on('click', function () {
        if (currLogPage > 0) {
            currLogPage = 0;
            pagingLog();
        }
    });

    afterPagersPC.on('click', function () {
        if (currLogPage < lastPage) {
            currLogPage = lastPage;
            pagingLog();
        }
    });

    pageTxt = pageTxt.replace('XX', currPageText);
    pageTxt = pageTxt.replace('YY', logRowCount);

    printLog(rowPerPage);
}

function pagingLogM() {
    var rowPerPage = Number($('#row-dropdown-label').text().split(' ')[0]);
    var currPageText = (currLogPageM*rowPerPage + 1)+'-'+(currLogPageM*rowPerPage + rowPerPage);
    var lastPage = parseInt(logRowCount/rowPerPage);
    var pageTxt = getLanguageText(languageTable, 'XX of YY');
    // var startSet = currLogPage + 1;
    var logPageSet = 3;
    var startSet = (Math.ceil((currLogPageM + 1) / logPageSet) - 1) * logPageSet + 1;

    var endSet = startSet + 3;
    if (endSet > lastPage) {
        endSet = lastPage + 2;
    }

    $('#log-popup .go-prev').prop('disabled', false);
    $('#log-popup .go-next').prop('disabled', false);
    if (currLogPageM <= 0) {
        $('#log-popup .go-prev').prop('disabled', true);
    }
    if (currLogPageM >= lastPage) {
        $('#log-popup .go-next').prop('disabled', true);
        currPageText = (currLogPageM * rowPerPage + 1) + '-' + logRowCount;
    }

    var pageHtml = '';
    for (var i=startSet; i < endSet; i++) {
        var li = `<a href="javascript:void(0);" role="button" data-num=${i} id="page-${i}" class="page page-${i} ${i === currLogPageM + 1 ? 'current' : ''}"
                    aria-label="${i} page." aria-pressed="${i === currLogPageM + 1 ? 'true' : 'false'}"><span>${i}</span></a>`;
        pageHtml += li;
    }

    var pagers = $('#log-popup .pager');
    var pagersM = $(pagers[1]);
    pagersM.html(pageHtml);
    var prevPagers = $('#log-popup .go-prev');
    var prevPagersM = $(prevPagers[1]);
    prevPagersM.off('click');
    var nextPagers = $('#log-popup .go-next');
    var nextPagersM = $(nextPagers[1]);
    var beforePagers = $('#log-popup .go-before');
    var beforePagersM = $(beforePagers[1]);
    beforePagersM.off('click');
    var afterPagers = $('#log-popup .go-after');
    var afterPagersM = $(afterPagers[1]);
    afterPagersM.off('click');
    nextPagersM.off('click');
    var pagesM = pagersM.children('.page');

    $(pagesM).each(function (i, elem) {
        $(elem).on('click', function () {
            paginationClicker(pagesM, i);
            currLogPageM = $(this).data('num') - 1;
            pagingLogM();
        });
    });

    prevPagersM.on('click', function () {
        if (currLogPageM > 0) {
            currLogPageM--;
            goPrevClicker(pagesM);
            pagingLogM();
        }
    });

    nextPagersM.on('click', function () {
        if (currLogPageM < lastPage) {
            currLogPageM++;
            goNextClicker(pagesM);
            pagingLogM();
        }
    });

    beforePagersM.on('click', function () {
        if (currLogPageM > 0) {
            currLogPageM = 0;
            pagingLogM();
        }
    });

    afterPagersM.on('click', function () {
        if (currLogPageM < lastPage) {
            currLogPageM = lastPage;
            pagingLogM();
        }
    });

    pageTxt = pageTxt.replace('XX', currPageText);
    pageTxt = pageTxt.replace('YY', logRowCount);

    printReflowLog(rowPerPage);
}

function drawRowSelect() {
    var displayName = {
        0: "10 " + getLanguageText(languageTable, "Row"),
        1: "30 " + getLanguageText(languageTable, "Row"),
        2: "50 " + getLanguageText(languageTable, "Row")
    };
    var valList = [0, 1, 2];

    var rowDropdown = $('#row-dropdown');
    var dropdownMenu = $(rowDropdown).children(".dropdown-menu");

    var ul = createDropdownChildren(valList, displayName, 'row-dropdown');

    var lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            changeDropdownLabel("row-dropdown", displayName[valList[i]]);
            changeDropdownAria("row-dropdown", i);
            currLogPage = 0;
            pagingLog();
            pagingLogM();
        });
    });

    $(dropdownMenu).html(ul);

    changeDropdownLabel("row-dropdown", displayName[valList[1]]);
    changeDropdownAria("row-dropdown", 1);
}

function drawFilterSelect() {
    var displayName = {
        ALL: getLanguageText(languageTable, "All"),
        TIME: getLanguageText(languageTable, "Time"),
        SIGNAL: getLanguageText(languageTable, "Signal"),
        LEDUNIT: getLanguageText(languageTable, "LED Unit"),
        TEMPERATURE: getLanguageText(languageTable, "Temperature")
    };
    var valList = ['ALL', 'TIME', 'SIGNAL', 'LEDUNIT', 'TEMPERATURE'];

    var filterDropdown = $('#filter-dropdown');
    var dropdownMenu = $(filterDropdown).children(".dropdown-menu");

    var ul = createDropdownChildren(valList, displayName, 'filter-dropdown');

    var lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            var selected = valList[i];
            changeDropdownLabel("filter-dropdown", displayName[selected]);
            changeDropdownAria("filter-dropdown", i);
            currLogPage = 0;
            filteringLog(selected);
        });
    });

    $(dropdownMenu).html(ul);

    changeDropdownLabel("filter-dropdown", displayName[valList[0]]);
    changeDropdownAria("filter-dropdown", 0);
}

function downloadFile(fileName, urlData, logForDown) {
    if (navigator.msSaveBlob){
        var blob = new Blob([logForDown], { type: 'text/csv;charset=utf-8;' });
        navigator.msSaveBlob(blob, fileName);
    }else{
        var aLink = document.createElement('a');
        aLink.download = fileName;
        aLink.href = urlData;
        document.body.appendChild(aLink);
        var event = new MouseEvent('click');
        aLink.dispatchEvent(event);
    }
}

function filteringLog(filter){
    if(filter === 'ALL'){
        sepLogString = log.split('\n');
        sepLogString.splice(sepLogString.length-1, 1);
        sepLogString = sepLogString.reverse();
        logRowCount = sepLogString.length;
    }else{
        var tmpLog = [];
        log.split('\n').forEach(function(item){
            if(item.indexOf(filter) > 0)
                tmpLog.push(item);
        });

        sepLogString = tmpLog;
        sepLogString = sepLogString.reverse();
        logRowCount = sepLogString.length;
    }

    pagingLog();
    pagingLogM();
}