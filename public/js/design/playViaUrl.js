document.addEventListener("DOMContentLoaded", function() {
    // init UI change
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/control_input.css">');
    $('.content-container').addClass('wide-padding');
    var menu2 = $('#menu-2-2');
    menu2.removeClass('active');
    menu2.css("display", "none");
    var menu1 = $('#menu-2-1');
    menu1.addClass('active');
    menu1.css("display", "block");
    $('.lnb-box').css("display", "block");
    $('#ROOT_CONTAINER').removeClass('dashboard');

    addAlertModals('url-warning-modal', getLanguageText(languageTable, "The URL format is invalid."));

    init();

    displayLoading(false);

    $('.side-logo').css('top', '35px');
});

function init() {
    getPlayViaUrl(function (msg) {
        $('#loader').prop('checked', msg.playViaUrlMode === 'on');
        $('#url-setting').val(msg.playViaUrl);
        onChangedLoader();
    });

    $("#apply").on('click',function () {
        var playViaUrlMode = $('#loader').prop('checked') ? "on" : "off";
        var url = $('#url-setting').val();
        if (checkInvalidUrl(url)) {
            if (playViaUrlMode === 'on') {
                $('#url-setting-success').css('display', 'none');
                showMyModal('url-warning-modal', getLanguageText(languageTable, "The URL format is invalid."));
                return;
            } else {
                getPlayViaUrl(function (msg) {
                    $('#url-setting').val(msg.playViaUrl);
                    setPlayViaUrl(playViaUrlMode, msg.playViaUrl);
                    showSuccess('url-setting-success');
                    showMyModal('url-warning-modal', 'URL is saved.');
                });
                return;
            }
        }
        setPlayViaUrl(playViaUrlMode, url);
        showSuccess('url-setting-success');
        showMyModal('url-warning-modal', 'URL is saved.');
    });

    $("#preview").on('click',function () {
        var url = $('#url-setting').val();
        if (checkInvalidUrl(url)) {
            // alert(getLanguageText(languageTable, "The URL format is invalid."));
            $('#url-setting-success').css('display', 'none');
            showMyModal('url-warning-modal');
            return;
        }

        var protocols = ['http:', 'https:', 'ftp:'];
        for (var i = 0; i < protocols.length; i++) {
            var matchingResult = false;
            if (url.toLowerCase().indexOf(protocols[i]) > -1) {
                matchingResult = true;
                break;
            }

            if (i === protocols.length -1 && matchingResult === false) {
                url = "http://" + url;
            }
        }
        console.log("url : " + url);
        window.open(url, '_blank');
    });

    $('#loader').on('change',function (e) {
        var isChecked = $(this).prop('checked');
        $(this).attr('aria-checked', isChecked);
        onChangedLoader();
        e.preventDefault();
    });
}

function onChangedLoader() {
    var isLoaderOn = $('#loader').prop('checked');
    if (isLoaderOn) {
        $('#url-setting').prop('disabled', false);
        $("#preview").prop('disabled', false);
    } else {
        $('#url-setting').prop('disabled', true);
        $("#preview").prop('disabled', true);
    }
}

function checkInvalidUrl(url) {
    var isInvalidUrl = false;
    if (url === undefined || url.length === 0 || url.trim().length === 0) {
        isInvalidUrl = true;
    }

    return isInvalidUrl;
}