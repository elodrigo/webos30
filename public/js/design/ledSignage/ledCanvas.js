var isDashboard = false;
var inited = false;
var curUnit = undefined;
var cpatureFileName = '';

var interval = 1;

function initLed(sec, config) {
    interval = sec;
}

function compareZ(a, b) {
    return a.z - b.z;
}

function rgba(r, g, b, a) {
    return 'rgba(' + r + ',' + g + ',' + b + ',' + a + ')';
}

function drawFromConfig(ledConfig, config) {
    config.rectList = [];
    ledConfig.unitList.forEach(function(unit) {
        var rect = {
            x: unit.x,
            y: unit.y,
            w: unit.w,
            h: unit.h,
            id: unit.id,
            inputDir: unit.inputDir,
            portLength: unit.portLength,
            portUse: unit.portUse,
            trim : unit.trim,
            z: 0
        }
        config.rectList.push(rect);
    });

    config.viewPort = ledConfig.viewPort;
    config.from = ledConfig.from;
    config.direction = ledConfig.direction;

    var scaleW =  config.viewPort.w / outer.width;
    var scaleH =  config.viewPort.h / outer.height;

    if(env.supportLedFilm){
        config.unitScale = scaleW > scaleH ? scaleW : scaleH;
    }else{
        config.unitScale = 1;
    }

    refreshLed(config);
}

function setupInputList() {
    getInputList(function(msg) {
        for (var i = 0; i < msg.length; i++) {
            var itsId = msg[i].id;
            var checkbox = `<div class="checkbox">
                    <input type="radio" name="external-radio-type" id="${itsId}" value="${itsId}">
                    <label for="${itsId}">${msg[i].name}</label>
            </div>`;
            $(checkbox).appendTo('#ctrl-external');
        }

        var ctrlExternal = $('[name=external-radio-type]');
        ctrlExternal.on('click', function () {
            var thisVal = $(this).val();
            setHdmiInput(thisVal);

            ctrlExternal.each(function (j, elem) {
                var elemVal = $(elem).val();
                if (thisVal === elemVal) {
                    $(this).attr("checked", true);
                } else {
                    $(elem).attr("checked", false);
                }
            });
        });

        getHdmiInput(function (msg) {
            $("input:radio[name=external-radio-type][value='" + msg + "']").attr("checked", true);
            $("input:radio[name=external-radio-type][value='" + msg + "']").prop("checked", "checked");
        });
    });
}

function refreshLed(config) {
    if (!config) {
        config = mainCanvas;
    }
    var height = env.supportLedFilm ? 1080 : Math.floor(1080 * config.scaleScreen);
    getCapture(height, function(m) {
        if(m.indexOf('noimage')<0){
            cpatureFileName = m;
        }else{
            m = cpatureFileName;
            setTimeout(function(){
                refreshLed(config);
            }, 1000);
            return;
        }

        inited = true;
        config.img.onload = function() {
            draw(config);
        }
        config.img.src = m;
    });
}

function getOverallStatusHandler() {
    var counter = 0;
    mainCanvas.rectList.forEach(function(rect, idx) {
        getLedPowerStatus(rect.id * 2, function(ret1) {
            if (!ret1.isPowerOk) {
                rect.status = 'off';
                if (++counter === mainCanvas.rectList.length) {
                    draw(mainCanvas);
                }
                return;
            }
            getLedOverallStatus(rect.id * 2, function(ret2) {
                rect.status = ret2.isOverallOk ? 'ok' : 'ng';
                if (++counter === mainCanvas.rectList.length) {
                    draw(mainCanvas);
                }
            });
        });
    });
}

function getMousePos(evt) {
    var outer = evt.target.getBoundingClientRect();
    if(env.supportLedFilm){
        return {
            x: (evt.clientX - outer.left),
            y: (evt.clientY - outer.top)
        };
    }else{
        return {
            x: (evt.clientX - outer.left) / mainCanvas.scaleScreen,
            y: (evt.clientY - outer.top) / mainCanvas.scaleScreen
        };
    }
}

function isInsideRect(pos, rect) {
    var x = rect.x;
    var y = rect.y;
    var w = rect.w;
    var h = rect.h;
    return x <= pos.x && y <= pos.y && (x + w) >= pos.x && (y + h) >= pos.y;
}

function getSelectedRect(config, pos) {
    return config.rectList.filter(function(rect) {
        return isInsideRect(pos, rect, config.unitScale);
    });
}

function draw(config) {
    if (!inited) {
        return;
    }

    config.ctx.clearRect(0, 0, outer.width, outer.height);
    config.ctx.drawImage(config.img, 0, 0, outer.width, outer.height);

    config.ctx.strokeStyle = 'black';
    config.ctx.lineWidth = 1;
    config.ctx.strokeRect(0, 0, outer.width, outer.height);

    if (config !== mainCanvas) {
        config.ctx.strokeStyle = '#cf0652';
        config.ctx.lineWidth = 10;
        config.ctx.strokeRect(config.viewPort.x, config.viewPort.y,
            config.viewPort.w, config.viewPort.h);
    }

    config.ctx.strokeStyle = 'black';
    config.ctx.lineWidth = 1;

    var sorted = config.rectList;
    sorted.forEach(function(rect) {
        drawRect(config, rect);
    });
}