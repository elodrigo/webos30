const GnbURL = [{
    url: 'ledDashboard',
    title: 'Dashboard',
    menuClass: 'menu-led-dashboard'
}, {
    url: 'ledControl',
    title: 'Control',
    menuClass: 'menu-led-control',
}, {
    url: 'ledMedia',
    title: 'Media',
    menuClass: 'menu-led-media'
}, {
    url: 'ledSignage365Care',
    title: 'LG ConnectedCare',
    menuClass: 'menu-lgc',
}, {
    url: 'ledSiserver',
    title: 'SI Server',
    menuClass: 'menu-led-server',
}];

var isMobile = false;

document.addEventListener("DOMContentLoaded", function() {

    window.onload = function () {
        // createCurrentMenu();
        addMenuEvent();

        $('.side-logo').css('top', '33px');
    }

    $('.btn-signout').on('click', function () {
        location.href = "/logout"
    });

    // $( "#sender-1" ).draggable({ containment: "#containment-wrapper", scroll: false });
    // $( "#sender-2" ).draggable({ containment: "#containment-wrapper", scroll: false });
    // $( "#sender-3" ).draggable({ containment: "#containment-wrapper", scroll: false });
    // $( "#sender-4" ).draggable({ containment: "#containment-wrapper", scroll: false });

    $('.btn-dropdown').on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        var $parent = $this.closest('.dropdown');
        var $parentTable = $this.closest('table').not('.no-scroll, has-not-header');
        var test = $(this).is(':focus')

        const lis = $(this).siblings('.dropdown-menu').find('li');
        lis.each(function (n) {
            var cur = $(this).attr('data-cur');
            if (cur == 'true') {
                $(this).css('background-color', '#cccccc');
            } else {
                $(this).css('background-color', '');
            }
        });

        $('.btn-dropdown').not($this).attr('aria-expanded', 'false');
        // console.log($this.attr('aria-expanded'))
        if($this.attr('aria-expanded') === 'false') {
            // preventScroll();
            if($parentTable.length > 0) {
                var scrollTop = $(document).scrollTop();
                var offsetTop = $parent.offset().top;
                var offsetLeft = $parent.offset().left;
                var parentWidth = $parent.outerWidth();
                var parentHeight = $parent.outerHeight();

                $this.next('.dropdown-menu').css({
                    top:offsetTop + parentHeight - scrollTop,
                    left:offsetLeft,
                    width:parentWidth,
                });
            }
            $this.attr('aria-expanded', 'true');
        } else {
            $this.attr('aria-expanded', 'false');
        }
    });

    if (env.locale === 'ar-SA') {
        changeRtl();
    }

});

function createCurrentMenu(index, myUrl) {
    var gnb = $(".parent-menu");
    gnb.each(function (index) {
        $(this).removeClass("focus");
        var btn = $(this).children('button');
        var myName = $(btn[0]).attr("name");
        if (myName === myUrl) {
            $(this).addClass("focus");
            $(this).children("button").attr("aria-selected", "true");
        }
    });
}

function addMenuEvent() {
    var btnMenu = $(".btn-main-menu");
    btnMenu.each(function (index) {
        var myBtn = ($(this)[0]);
        var myUrl = $(myBtn).attr('name');
        $(this).on("click", function () {
            location.href = "/ledSignage/" + myUrl;
        });
    });
}

function getCaretClass() {
    let caretClass = 'caret' + (isMobile ? ' mobile' : '');
    // if (locale === "ar-SA") {
    //     caretClass = caretClass + " arabic";
    // }
    return caretClass
}

/*function createDropdownChildren(valueList, textMap, ariaId) {
    var caretClass = getCaretClass();
    var ul = document.createElement('ul');
    ul.classList.add('lists');
    $(ul).prop('tabindex', "-1");
    $(ul).prop('role', 'listbox');
    $(ul).prop('aria-labelledby', ariaId);
    var selected = true;
    for (var i=0; i < valueList.length; i++) {
        var top = valueList[i];
        var textVal = textMap[top];
        if (!textVal) {
            textVal = textMap.get(top);
        }
        var myId = ariaId ? 'option-' + ariaId + '-' + i : undefined;
        var item;

        /!*if (myId) {
            item = `<li class="list" ><a href="javascript:void(0);" class="${caretClass}"
                        role="option" aria-selected="${selected}" data-value="${top}" id="${myId}" tabindex="-1">${textVal}</a></li>`
        } else {
            item = `<li class="list"><a href="javascript:void(0);" class="${caretClass}"
                        role="option" aria-selected="${selected}" data-value="${top}" tabindex="-1">${textVal}</a></li>`
        }*!/

        if (myId) {
            item = `<li class="list ${caretClass}" role="option" aria-selected="${selected}" data-value="${top}" 
                    id="${myId}" tabindex="-1">${textVal}</li>`
        } else {
            item = `<li class="list ${caretClass}" role="option" aria-selected="${selected}" data-value="${top}" 
                    tabindex="-1">${textVal}</li>`
        }

        $(ul).append(item);
        selected = false;
    }
    return ul;
}

function changeDropdownLabel(id, text, ariaId) {
    var element = $('#' + id);
    var newBtn = `<button type="button" id="field-capture-btn" class="btn-dropdown" aria-haspopup="listbox" 
                aria-labelledby="label-${id} ${id}-btn" aria-activedescendent="option-0101" aria-expanded="false">text</button>`
    var label = $(element).children('button');
    if (label.length > 0) {
        $(label).html(text);
        $(label).prop('id', id + '-btn');
        $(label).prop('aria-labelledby', `"label-${id} ${id}-btn"`);
        $(label).prop('aria-expanded', "false");
    } else {
        var button = $(element).children('a');
        $(button).html(text);
    }
}

function changeDropdownAria(id, index, deactive) {
    var element = $('#' + id);
    var lists = $(element).find('li');
    var label = $(element).children('button');

    var ariaId = 'option-' + id + '-' + index;

    if (label.length > 0) {
        $(label).attr('aria-activedescendent', ariaId)
    } else {
        var button = $(element).children('a');
        $(button).attr('aria-activedescendent', ariaId)
    }

    for (var i = 0; i < lists.length; i++) {
        if (i === index && !deactive) {
            $(lists[i]).attr('aria-selected', "true");
        } else {
            $(lists[i]).attr('aria-selected', "false");
        }
    }
}

function makeKeyDownDropdown(dropdown) {
    $(`#${dropdown}`).on('keydown', function (evt) {
        if (evt.key !== 'Tab') {
            evt.preventDefault();
            // evt.stopPropagation();
            const lis = $(this).find('li');

            // let myIndex = lis.index(lis.filter(':focus'));
            var myIndex = 0;
            lis.each(function (i) {
                var a = $(this).attr('aria-selected');
                if (a == 'true') {
                    myIndex = i;
                }
            })

            if (evt.key === "ArrowUp" && myIndex > 0) {
                myIndex--
            }
            if (evt.key === "ArrowDown" && myIndex < lis.length - 1) {
                myIndex++
            }
            if (!~myIndex) {
                myIndex = 0;
            }

            if (evt.key === "Enter" || evt.key === " ") {
                const label = $(this).children('button');
                if (label.attr('aria-expanded') == 'true') {
                    lis.eq(myIndex).trigger('click');
                    return;
                } else {
                    $(label).attr('aria-expanded', true)
                    return;
                }

            }

            lis.eq(myIndex).trigger('focus');
            lis.each(function (j) {
                if (j === myIndex) {
                    $(this).attr('aria-selected', 'true');
                } else {
                    $(this).attr('aria-selected', 'false');
                }
            })
            if (myIndex >= 0) {
                const myId = lis.eq(myIndex).prop('id');
                const label = $(this).children('button');
                $(label).attr('aria-activedescendent', myId);
            }
        }

    });
}*/

function createDropdownChildren(valueList, textMap, ariaId) {
    var caretClass = getCaretClass();
    var ul = document.createElement('ul');
    ul.classList.add('lists');
    $(ul).prop('tabindex', "-1");
    $(ul).attr('role', 'listbox');
    $(ul).prop('aria-labelledby', ariaId);
    var selected = true;
    for (var i=0; i < valueList.length; i++) {
        var top = valueList[i];
        var textVal = textMap[top];
        if (!textVal) {
            textVal = textMap.get(top);
        }
        var myId = ariaId ? 'option-' + ariaId + '-' + i : undefined;
        var item;

        /*if (myId) {
            item = `<li class="list" ><a href="javascript:void(0);" class="${caretClass}"
                        role="option" aria-selected="${selected}" data-value="${top}" id="${myId}" tabindex="-1">${textVal}</a></li>`
        } else {
            item = `<li class="list"><a href="javascript:void(0);" class="${caretClass}"
                        role="option" aria-selected="${selected}" data-value="${top}" tabindex="-1">${textVal}</a></li>`
        }*/

        if (myId) {
            item = `<li class="list ${caretClass}" role="option" data-selected="${selected}" data-cur="${selected}" data-value="${top}" 
                    id="${myId}" tabindex="-1">${textVal}</li>`
        } else {
            item = `<li class="list ${caretClass}" role="option" data-selected="${selected}" data-cur="${selected}" data-value="${top}" 
                    tabindex="-1">${textVal}</li>`
        }

        $(ul).append(item);
        selected = false;
    }
    return ul;
}

function changeDropdownLabel(id, text, ariaId) {
    var element = $('#' + id);
    var newBtn = `<button type="button" id="field-capture-btn" class="btn-dropdown" aria-haspopup="listbox" 
                aria-labelledby="label-${id} ${id}-btn" aria-activedescendent="option-0101" aria-expanded="false">text</button>`
    var label = $(element).children('button');
    if (label.length > 0) {
        $(label).html(text);
        $(label).prop('id', id + '-btn');
        // $(label).attr('aria-labelledby', `label-${id} ${text}`);
        $(label).prop('aria-expanded', "false");
    } else {
        var button = $(element).children('a');
        $(button).html(text);
    }
}

function changeDropdownAria(id, index, deactive) {
    var element = $('#' + id);
    var lists = $(element).find('li');
    var label = $(element).children('button');

    var ariaId = 'option-' + id + '-' + index;
    var labelled = `label-${id} ${ariaId}`

    if (label.length > 0) {
        $(label).attr('aria-activedescendent', ariaId);
        $(label).attr('aria-labelledby', labelled);
    } else {
        var button = $(element).children('a');
        $(button).attr('aria-activedescendent', ariaId)
    }

    for (var i = 0; i < lists.length; i++) {
        if (i === index && !deactive) {
            $(lists[i]).attr('data-selected', "true");
            $(lists[i]).attr('data-cur', "true");
        } else {
            $(lists[i]).attr('data-selected', "false");
            $(lists[i]).attr('data-cur', "false");
        }
    }
}

function makeKeyDownDropdown(dropdown) {
    $(`#${dropdown}`).on('keydown', function (evt) {
        if (evt.key !== 'Tab') {
            evt.preventDefault();
            // evt.stopPropagation();
            const lis = $(this).find('li');

            lis.unbind('mouseenter mouseleave');

            // let myIndex = lis.index(lis.filter(':focus'));
            var myIndex = 0;
            lis.each(function (i) {
                var a = $(this).attr('data-selected');
                if (a == 'true') {
                    myIndex = i;
                }
            });

            if (evt.key === "ArrowUp" && myIndex > 0) {
                myIndex--
            }
            if (evt.key === "ArrowDown" && myIndex < lis.length - 1) {
                myIndex++
            }
            if (!~myIndex) {
                myIndex = 0;
            }

            if (evt.key === "Enter" || evt.key === " ") {
                const label = $(this).children('button');
                if (label.attr('aria-expanded') == 'true') {
                    lis.eq(myIndex).trigger('click');
                    lis.eq(myIndex).css('background-color', '#cccccc');
                    return;
                } else {
                    lis.each(function (n) {
                        var cur = $(this).attr('data-cur');
                        if (cur == 'true') {
                            $(this).css('background-color', '#cccccc');
                        } else {
                            $(this).css('background-color', '');
                        }
                    });
                    $(label).attr('aria-expanded', true)
                    return;
                }
            }

            lis.eq(myIndex).trigger('focus');
            lis.each(function (j) {
                if (j === myIndex) {
                    if ($(this).attr('data-cur') != 'true') {
                        $(this).css('background-color', '#efefef');
                    }
                    $(this).attr('data-selected', 'true');
                } else if (j !== myIndex && $(this).attr('data-cur') == 'true') {
                    $(this).attr('data-selected', 'false');
                    $(this).css('background-color', '#cccccc');
                } else {
                    $(this).attr('data-selected', 'false');
                    $(this).css('background-color', '');
                }
            });
            if (myIndex >= 0) {
                const myId = lis.eq(myIndex).prop('id');
                const label = $(this).children('button');
                $(label).attr('aria-activedescendent', myId);
            }

            $(this).children('button').trigger('focus');

            lis.hover(function () {
            }, function () {
                lis.each(function (l) {
                    var selected = $(this).attr('data-selected');
                    var cur = $(this).attr('data-cur');
                    if (selected == 'true' && cur == 'false') {
                        $(this).css('background-color', '');
                    }

                });
                lis.unbind('mouseenter mouseleave');
            })
        }

    });
}

function myDropdownBinding(id) {
    var myDropdown = $('#' + id).children('.btn-dropdown');
    $(myDropdown).off('click');
    $(myDropdown).on('click', function(){
        var isExpanded = $(myDropdown).attr('aria-expanded');
        if (isExpanded === 'true' || isExpanded === true) {
            $(myDropdown).attr('aria-expanded', "false");
        } else {
            $(myDropdown).attr('aria-expanded', "true");
        }
    });
    $('#' + id).css('opacity', '1');
}

function myDropdownUnBinding(id) {
    var myDropdown = $('#' + id).children('.btn-dropdown');
    $(myDropdown).off('click');
    $(myDropdown).on('click', function () {
        $(this).attr('aria-expanded', 'false')
    });
    $('#' + id).css('opacity', '0.5');
}

function _openPopup(id) {
    const popup = $(`#${id}`);
    $(popup).addClass('show');
}

function _closePopup(id) {
    const popup = $(`#${id}`);
    $(popup).removeClass('show');
}

function fileThis(id) {
    document.getElementById(id).click();
}

function displayLoading(show) {
    const loading = $('.loading');
    if (show) {
        $(loading).show();
    } else {
        $(loading).hide();
    }
}

function addConfirmModals(btnId, id, title, content, isOneBtn, callback, callbackCancel, btnCallback) {

    function createBtn() {
        if (isOneBtn) {
            return `<div class="button-container">
              <button type="button" id="${id}-cancel" class="btn btn-yes btn-primary">${getLanguageText(languageTable, "Confirm")}</button>
            </div>`;
        } else {
            return `<div class="button-container">
              <button type="button" id="${id}-cancel" class="btn btn-cancel">${getLanguageText(languageTable, "No")}</button>
              <button type="button" id="${id}-ok" class="btn btn-yes btn-primary">${getLanguageText(languageTable, "Yes")}</button>
            </div>`;
        }
    }

    const html = `<div id="${id}" class="popup-wrapper">
      <div class="dim">&nbsp;</div>
      <div class="popup" role="dialog" aria-labelledby="popup-label" aria-modal="true">
        <div class="popup-container">
          <div class="popup-header">
            <h1 id="popup-label">${title}</h1>
          </div>
          <div class="popup-content">
            <div class="text-content" role="alert">
              <P id="${id}-text-content" class="alet-text">${content}</P>
            </div>
          </div>
          <div class="popup-footer">
            ${createBtn()}
          </div>
        </div>
      </div>
    </div>`

    const modal = $(html);
    $('body').append(modal);

    const myContainer = document.getElementById(id);
    const myFocusTrap = focusTrap.createFocusTrap(`#${id}`, {
        onActivate: function () {myContainer.classList.add('is-active')},
        onDeactivate: function () {myContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });

    $(`#${id}-cancel`).on('click', function () {
        myFocusTrap.deactivate();
        _closePopup(id);
        if (callbackCancel) {
            callbackCancel();
        }
    });

    $(`#${id}-ok`).on('click', function () {
        myFocusTrap.deactivate();
        _closePopup(id);
        if (callback) {
            callback();
        }
    });

    const newBtnId = btnId[0] === '#' || btnId[0] === '.' ? btnId.slice(1, btnId.length) : btnId;

    $(btnId).on('click', function () {
        if (btnCallback) {
            btnCallback($(this));
        }
        togglePopup(newBtnId, id);
        myFocusTrap.activate();
    });

    $(`#${id}`).on('cssClassChanged', function () {
        myFocusTrap.activate();
    });
}

function addAlertModals(id, content, callback) {

    const html = `<div id="${id}" class="popup-wrapper">
      <div class="dim">&nbsp;</div>
      <div class="popup popup-alert" role="dialog" aria-labelledby="popup-label" aria-modal="true">
        <div class="popup-container">
          <h1 id="popup-label" class="ir">${getLanguageText(languageTable, "information message")}</h1>
          <div class="popup-content">
            <div class="alet-text">
              ${content}
            </div>
          </div>
          <div class="popup-footer">
            <div class="button-container">
              <button type="button" id="${id}-ok" class="btn btn-yes btn-primary">${getLanguageText(languageTable, "OK")}</button>
            </div>
          </div>
        </div>
      </div>
    </div>`

    const modal = $(html);
    $('body').append(modal);

    const myContainer = document.getElementById(id);
    const myFocusTrap = focusTrap.createFocusTrap(`#${id}`, {
        onActivate: function () {myContainer.classList.add('is-active')},
        onDeactivate: function () {myContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });

    $(`#${id}-ok`).on('click', function () {
        myFocusTrap.deactivate();
        if (callback) {
            callback();
        }
        _closePopup(id);
    });

    $(`#${id}`).on('cssClassChanged', function (e) {
        // e.stopPropagation();
        myFocusTrap.activate();
    });
}

function showMyModal(id, content, title ) {
    const m = $(`#${id}`);
    if (title) {
        $(m).find('#popup-label').html(title);
    }

    if (content) {
        $(m).find('.alet-text').html(content);
    }
    $(m).addClass('show');
    $(m).trigger('cssClassChanged');
}

function addCompleteModal(id, msg1, msg2) {
    var message1 = getLanguageText(languageTable, "Update completed. Signage is rebooting now.");
    var message2 = getLanguageText(languageTable, "Please login again.");

    var html = `<div id="${id}" class="popup-wrapper">
                <div class="dim">&nbsp;</div>
                <div class="popup popup-sw-complate" role="dialog" aria-labelledby="popup-label" aria-modal="true">
                    <div class="popup-container">
                        <div class="popup-content" id="${id}-popup-label-complete">
                            <div class="sw-content">
                              ${msg1 ? msg1 : message1} <br>
                              ${msg2 ? msg2 : message2}
                              <button id="${id}-btn-hidden"></button>
                            </div>
                        </div>
                    </div>
                </div>
             </div>`;

    var modal = $(html);
    $('body').append(modal);

    const myContainer = document.getElementById(id);
    const myFocusTrap = focusTrap.createFocusTrap(`#${id}`, {
        onActivate: function () {myContainer.classList.add('is-active')},
        onDeactivate: function () {myContainer.classList.remove('is-active')},
        initialFocus: function () {document.getElementById(`${id}-btn-hidden`)},
        escapeDeactivates: false,
    });

    $(`#${id}`).on('cssClassChanged', function (e) {
        var isShow = $(this).hasClass('show')
        // e.stopPropagation();
        if (isShow) {
            myFocusTrap.activate();
        } else {
            myFocusTrap.deactivate();
        }
    });
}

function addProgressModal(id, content, title) {
    var contentText = ``

    var html = `<div id="${id}" class="popup-wrapper">
            <div class="dim">&nbsp;</div>
            <div class="popup popup-sw-ing" role="dialog" aria-labelledby="popup-label" aria-modal="true">
                <div class="popup-container">
                  <div class="popup-header">
                    <h1 id="${id}-popup-label">${title ? title : getLanguageText(languageTable, "S/W Update")}</h1>
                  </div>
                  <div class="popup-content">
                    <div class="field field-update">
                      <h1 class="field-label">${content ? content : getLanguageText(languageTable, "Update Progress")}</h1>
                      <div class="field-form">
                        <div class="update-bar"><div id="${id}-state-bar" style="width:34%;" class="state-bar"></div></div>
                        <div id="${id}-percent" class="update-percent">34<span>%</span></div>
                      </div>
                    </div>
                    <div class="sw-content">
                      ${getLanguageText(languageTable, "Please Keep this Signage on.")} ${getLanguageText(languageTable, "The POWER remote controller key will not work during the update.")} <br>
                      ${getLanguageText(languageTable, "Do not reboot in Device page.")}
                      <button id="${id}-btn-hidden"></button>
                    </div>
                  </div>
                </div>
            </div>
           </div>`;

    var modal = $(html);
    $('body').append(modal);

    const myContainer = document.getElementById(id);
    const myFocusTrap = focusTrap.createFocusTrap(`#${id}`, {
        onActivate: function () {myContainer.classList.add('is-active')},
        onDeactivate: function () {myContainer.classList.remove('is-active')},
        initialFocus: function () {document.getElementById(`${id}-btn-hidden`)},
        escapeDeactivates: false,
    });

    $(`#${id}`).on('cssClassChanged', function (e) {
        var isShow = $(this).hasClass('show')
        // e.stopPropagation();
        if (isShow) {
            myFocusTrap.activate();
        } else {
            myFocusTrap.deactivate();
        }
    });
}

function updateProgressModal(id, per) {
    $(`#${id}-state-bar`).css('width', per + '%');
    $(`#${id}-percent`).html(per + '<span>%</span>');
}

function moveHere(url) {
    window.location = url;
}

function drawPagination() {
    const pagination = $('.pagination');

    for (const top of pagination) {
        const goBefore = $(top).children('.go-before')[0];
        const goPrev = $(top).children('.go-prev')[0];
        const goNext = $(top).children('.go-next')[0];
        const goAfter = $(top).children('.go-after')[0];
        const pager = $(top).children('.pager')[0];
        const pages = $(pager).children('.page');

        for (let i = 0; i < pages.length; i++) {
            $(pages[i]).on('click', function () {
                paginationClicker(pages, i);
            });
        }
        $(goPrev).on('click', function () {
            goPrevClicker(pages);
        });
        $(goNext).on('click', function () {
            goNextClicker(pages);
        });
    }
}

/*function paginationClicker(pages, index) {
    for (let i = 0; i < pages.length; i++) {
        if (index === i) {
            $(pages[i]).addClass('current');
        } else {
            $(pages[i]).removeClass('current');
        }
    }
}

function goPrevClicker(pages) {
    for (let i = 0; i < pages.length; i++) {
        if ($(pages[i]).hasClass('current')) {
            const n = pages[i].dataset.num;
            if (n > 1) {
                $(pages[i]).removeClass('current');
                $(pages[i - 1]).addClass('current');
            }
        }

    }
}

function goNextClicker(pages) {
    for (let i = pages.length - 1; i >= 0; i--) {
        if ($(pages[i]).hasClass('current')) {
            const n = pages[i].dataset.num;
            if (n < pages.length) {
                $(pages[i]).removeClass('current');
                $(pages[i + 1]).addClass('current');
            }
        }
    }
}*/

function paginationClicker(pages, index) {
    for (var i = 0; i < pages.length; i++) {
        if (index === i) {
            $(pages[i]).addClass('current');
            $(pages[i]).attr("aria-pressed", "true");
        } else {
            $(pages[i]).removeClass('current');
            $(pages[i]).attr("aria-pressed", "false");
        }
    }
}

function goPrevClicker(pages) {
    for (var i = 0; i < pages.length; i++) {
        if ($(pages[i]).hasClass('current') && i > 0) {
            $(pages[i]).removeClass('current');
            $(pages[i]).attr('aria-pressed', 'false');
            $(pages[i - 1]).addClass('current');
            $(pages[i - 1]).attr('aria-pressed', 'true');
            break;
        }
    }
}

function goNextClicker(pages) {
    for (var i = 0; i < pages.length; i++) {
        if ($(pages[i]).hasClass('current') && i < pages.length) {
            $(pages[i]).removeClass('current');
            $(pages[i]).attr('aria-pressed', 'false');
            $(pages[i+1]).addClass('current');
            $(pages[i+1]).attr('aria-pressed', 'true');
            break;
        }
    }
}

function getLanguageText(languageTable, text) {
    var text = text + "";

    for (var i = languageTable.length - 1; i >= 0; i--) {
        if (languageTable[i][text]) {
            text = languageTable[i][text];
        }
    }
    return text;
}

function unmountCSS(item) {
    $(`link[rel=stylesheet][href='${cssMap[item]}']`).remove();

}

function mountCSS(item) {
    $('head').append(`<link rel="stylesheet" type="text/css" href="${cssMap[item]}">`);
}

function disableSlide(id) {
    var container = $(`#${id}`);
    var slide = $(container.find('input'));
    var bar = $(container.find(".slide-color-bar"));
    bar.removeClass('redslider');
    bar.addClass('greyslider');
    slide.prop('disabled', true);
    slide.css('cursor', 'not-allowed');
    slide.css('background', '#f1f1f1');

    // $(container.find('label')).html(getLanguageText(languageTable, "Energy Saving"));
    // $('#contrast').prop('disabled', true);
    // $('#contrastVal').html(getLanguageText(languageTable, "Energy Saving"));
}

function enableSlide(id) {
    var container = $(`#${id}`);
    var slide = $(container.find('input'));
    slide.prop('disabled', false);
    slide.css('cursor', 'pointer');
    var bar = $(container.find(".slide-color-bar"));
    bar.removeClass('greyslider');
    bar.addClass('redslider');
    // var contrast = $('#contrast');
    // $(contrast).prop('disabled', false);
    // $('#contrastVal').html($(contrast).val());
}

function disableDropButton(id, disabled) {
    var li = $('#' + id).find('div button').attr('disabled', disabled);
}

function dropdownBinding(id) {
    var myDropdown = $('#' + id).children('.btn-dropdown');
    $(myDropdown).on('click', function(){
        var $this = $(this);
        var $parent = $this.closest('.dropdown');
        var $parentTable = $this.closest('table').not('.no-scroll, has-not-header');

        $('.btn-dropdown').not($this).attr('aria-expanded', 'false');
        if($this.attr('aria-expanded') === 'false') {
            // preventScroll();
            if($parentTable.length > 0) {
                var scrollTop = $(document).scrollTop();
                var offsetTop = $parent.offset().top;
                var offsetLeft = $parent.offset().left;
                var parentWidth = $parent.outerWidth();
                var parentHeight = $parent.outerHeight();

                $this.next('.dropdown-menu').css({
                    top:offsetTop + parentHeight - scrollTop,
                    left:offsetLeft,
                    width:parentWidth,
                });
            }
            $this.attr('aria-expanded', 'true');
        } else {
            $this.attr('aria-expanded', 'false');
        }
    });
}

function dropdownUnBinding(id) {
    var myDropdown = $('#' + id).children('.btn-dropdown');
    // $(myDropdown).off('click', '**');
    $(myDropdown).on('click', function () {
        $(this).attr('aria-expanded', 'false')
    })
}

function overlaySenderUpdate(on, from) {
    var maxSenderCnt = env.numOfExtInputs === 1 ? 1 : 4;
    var senderBtns = '';

    for (var i=1; i<=maxSenderCnt; i++) {
        senderBtns += '<td><button class="overlayBtn" name="'+ i + '">Sender' + i + '</button></td>'
    }

    if (on === true) {
        var over = '<div id="overlay" class="update_overlay"> '
            + '<div class="update_in_progress">'
            + '<center>'
            + '<table id="overlayBtnTable">'
            + '<tr>'
            + senderBtns
            + '<td><span id=overlay_progress></span><br>'
            + '</tr>'
            + '<tr>'
            + '<td colspan="4">'
            + '<center>'
            + '<button class="overlayBtn" id="closeOverlay">close</button>'
            + '</center>'
            + '</td>'
            + '</tr>'
            + '</table>'
            + '<div id="updateImage" style="display:none">'
            + '<img src="../images/loading.gif"><br>'
            + '<span id="overlay_msg">'+ Locale.getText("LED FPGA Updating ......") + '</span>'
            + '</div>'
            + '</center>'
            + '</div>'
            + '</div>';
        if(!from)
            $(over).prependTo('.wrapper');
        else
            $(over).prependTo('#'+from);
    } else {
        $('#overlay').remove();
    }

    $('#closeOverlay').click(function(){
        $('#overlay').remove();
    });
}

function capitalizeFirstLetter(str) {
    var str = str.substring(0, 1).toUpperCase() + str.substring(1, str.length).toLowerCase();
    return str;
}

function changeRtl() {
    $('#ROOT_CONTAINER').addClass('lang_rtl');
}

function roundToOne(num) {
    return +(Math.round(num + "e+1")  + "e-1");
}