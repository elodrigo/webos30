var installPopUpTimer;
var updatePopupTimer;
var signature ='';
var downloadUrl ='';
var appVersion='00.00.00';
var accountName='UnKnown';
var accountNo='000000';

var networkErrorFocusTrap;
var appTimeZoneErrorFocusTrap;
var connectedResetFocusTrap;
var accountInputFocusTrap;
var accountCheckFocusTrap;
var appInstallFocusTrap;
var appInstallSucceededFocusTrap;
var appInstallFailedFocusTrap;
var appUpdateFocusTrap;
var appUpdateSucceededFocusTrap;
var appUpdateFailedFocusTrap;

document.addEventListener("DOMContentLoaded", function() {
// init UI change
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/control_led_w3_input.css">');
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/popup_enter_account_number.css">');
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/popup_confirm_account_number.css">');
    // $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/popup_require_network_settings.css">');

    createCurrentMenu(3, "ledSignage365Care");

    var networkErrorContainer = document.getElementById('networkError-popup');
    networkErrorFocusTrap =  focusTrap.createFocusTrap('#networkError-popup', {
        onActivate: function () {networkErrorContainer.classList.add('is-active')},
        onDeactivate: function () {networkErrorContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });
    var appTimeZoneErrorContainer = document.getElementById('appTimeZoneError');
    appTimeZoneErrorFocusTrap =  focusTrap.createFocusTrap('#appTimeZoneError', {
        onActivate: function () {appTimeZoneErrorContainer.classList.add('is-active')},
        onDeactivate: function () {appTimeZoneErrorContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });
    var connectedResetContainer = document.getElementById('connected-reset-popup');
    connectedResetFocusTrap =  focusTrap.createFocusTrap('#connected-reset-popup', {
        onActivate: function () {connectedResetContainer.classList.add('is-active')},
        onDeactivate: function () {connectedResetContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });
    var accountInputContainer = document.getElementById('accountInput-popup');
    accountInputFocusTrap =  focusTrap.createFocusTrap('#accountInput-popup', {
        onActivate: function () {accountInputContainer.classList.add('is-active')},
        onDeactivate: function () {accountInputContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });
    var accountCheckContainer = document.getElementById('accountCheck-popup');
    accountCheckFocusTrap =  focusTrap.createFocusTrap('#accountCheck-popup', {
        onActivate: function () {accountCheckContainer.classList.add('is-active')},
        onDeactivate: function () {accountCheckContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });
    var appInstallContainer = document.getElementById('appInstall-popup');
    appInstallFocusTrap =  focusTrap.createFocusTrap('#appInstall-popup', {
        onActivate: function () {appInstallContainer.classList.add('is-active')},
        onDeactivate: function () {appInstallContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });
    var appInstallSucceededContainer = document.getElementById('appInstallSucceeded-popup');
    appInstallSucceededFocusTrap =  focusTrap.createFocusTrap('#appInstallSucceeded-popup', {
        onActivate: function () {appInstallSucceededContainer.classList.add('is-active')},
        onDeactivate: function () {appInstallSucceededContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });
    var appInstallFailedContainer = document.getElementById('appInstallFailed-popup');
    appInstallFailedFocusTrap =  focusTrap.createFocusTrap('#appInstallFailed-popup', {
        onActivate: function () {appInstallFailedContainer.classList.add('is-active')},
        onDeactivate: function () {appInstallFailedContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });
    var appUpdateContainer = document.getElementById('appUpdate-popup');
    appUpdateFocusTrap =  focusTrap.createFocusTrap('#appUpdate-popup', {
        onActivate: function () {appUpdateContainer.classList.add('is-active')},
        onDeactivate: function () {appUpdateContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });
    var appUpdateSucceededContainer = document.getElementById('appUpdateSucceeded-popup');
    appUpdateSucceededFocusTrap =  focusTrap.createFocusTrap('#appUpdateSucceeded-popup', {
        onActivate: function () {appUpdateSucceededContainer.classList.add('is-active')},
        onDeactivate: function () {appUpdateSucceededContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });
    var appUpdateFailedContainer = document.getElementById('appUpdateFailed-popup');
    appUpdateFailedFocusTrap =  focusTrap.createFocusTrap('#appUpdateFailed-popup', {
        onActivate: function () {appUpdateFailedContainer.classList.add('is-active')},
        onDeactivate: function () {appUpdateFailedContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });

    $('#appEnable').on('change', function () {
        const isChecked = $(this).prop('checked');
        $(this).attr('aria-checked', isChecked);
    });

    var title = getLanguageText(languageTable, "Enable or disable {apptitle} service on this device").replace('{apptitle}', "LG ConnectedCare");
    $('.connected-service-box-title').text(title);

    var appIdVal = `${getLanguageText(languageTable, "Number")} : 099916 / ${getLanguageText(languageTable, "Name")} : qetest1`;
    $('#app-id').val(appIdVal);

    var appVerVal = `${getLanguageText(languageTable, "Ver")}`;
    $('#appVersion').val(appVerVal);

    $('#btn-reset-ok').on('click', function () {
        appReset();
    });

    $('#btn-account-cancel').on('click', function () {
        accountInputFocusTrap.deactivate();
        _closePopup('accountInput-popup');
    });

    $('#btn-timezoneError-cancel').on('click', function () {
        appTimeZoneErrorFocusTrap.deactivate();
        _closePopup('appTimeZoneError');
    });

    $('#btn-accountCheck-cancel').on('click', function () {
        accountCheckFocusTrap.deactivate();
        _closePopup('accountCheck-popup');
    });

    getContents365Care();
    getSystemSetting365Care();
    change365Enable();
    getModalonNetwork();

    var updatePrgTxt = `${getLanguageText(languageTable, "The update of the {apptitle} is in progress.").replace('{apptitle}', getLanguageText(languageTable, "LG ConnectedCare app"))}`;
    $('update-progress-txt').text(updatePrgTxt);
    var updateSucceedTxt = `${getLanguageText(languageTable, "The {apptitle} has been successfully updated.").replace('{apptitle}', getLanguageText(languageTable, "LG ConnectedCare app"))}`;
    $('update-succeed-txt').text(updateSucceedTxt);
    var updateFailedTxt = `${getLanguageText(languageTable, "The update of the {apptitle} has failed.").replace('{apptitle}', getLanguageText(languageTable, "LG ConnectedCare app"))}`;
    $('update-failed-txt').text(updateFailedTxt);
    var networkErrorTxt = `${getLanguageText(languageTable, "To use {apptitle}, you need to connect the device to the network. Do you want to set the network connection?").replace('{apptitle}', 'LG ConnectedCare')}`;
    $('network-settings-error-txt').text(networkErrorTxt);
    displayLoading(false);
});

function init() {

}

function autoTimeSetting() {
    setAutomaticallyTime( function(ret) {
        $('#installModal').modal('hide');
        $('#updateModal').modal('hide');
    });
}

function initInstallModal() {
    $('#btn-install').on('click', function () {
        accountCheckFocusTrap.deactivate();
        _closePopup('accountCheck-popup');
        appInstallFocusTrap.deactivate();
        _closePopup('appInstall-popup');
        appInstallSucceededFocusTrap.deactivate();
        _closePopup('appInstallSucceeded-popup');
        appInstallFailedFocusTrap.deactivate();
        _closePopup('appInstallFailed-popup');
        appTimeZoneErrorFocusTrap.deactivate();
        _closePopup("appTimeZoneError");
        networkErrorFocusTrap.deactivate();
        _closePopup("networkError-popup");
        refreshPinPassWord();
        showMyModal('accountInput-popup');
        accountInputFocusTrap.activate();
        $("#txtCategoryPin").css("color", "#4D4D4D");
    });

    $('#accountNameChk').on('click', function () {
        var currentPin = '';
        $('#currentPin input').each(function () {
            currentPin += $(this).val();
        });
        $('#accountPasswd').val(currentPin);
        accountChk(currentPin);
    });

    $('#btn-appInstallSucceeded-ok').on('click', function () {
        clearTimeout(installPopupTimer);
        care365Enable( function() {
            setSystemSettings('commercial', {
                care365Enable: "on"
            }, function() {
                getServerStatusCare365Subscribe( function (ret) {
                });
                location.reload();
            });
        });
    });

    $('#accountNameChkBack').on('click', function () {
        accountCheckFocusTrap.deactivate();
        _closePopup('accountCheck-popup');
        showMyModal('accountInput-popup');
        accountInputFocusTrap.activate();
        $("#txtCategoryPin").css("color", "#4D4D4D");
        refreshPinPassWord();
    });

    $('#accountConfirm').on('click', function () {
        accountCheckFocusTrap.deactivate();
        _closePopup('accountCheck-popup');
        showMyModal('appInstall-popup');
        appInstallFocusTrap.activate();
        appInstall();
    });

    $('#btn-timezoneError-ok').on('click', function () {
        autoTimeSetting();
        appTimeZoneErrorFocusTrap.deactivate();
        _closePopup('appTimeZoneError');
    });
}

// Device is connected firstly to set ip, focus/blue event is not occurred.
// This api include exception handling about it.
function initInputPin() {

    /*$(".input_circle_pin").on("keyup", function () {
        var pin = $(this);
        console.log(pin)
        var nextPin = $(this).nextAll("input");
        console.log('next', nextPin)
        if (pin.val() && nextPin.length >= 1) {

            pin.next(".input_circle_pin").val("");
            pin.next(".input_circle_pin").focus();
        } else if (pin.val() && !(nextPin.length >= 1)) {

        } else if (!(nextPin.length >= 1)){

        }
    });*/

    $('.input-box').on("keyup", function () {
        var pin = $(this).children("input");
        var nextPin = $(this).next();
        if (pin.val() && nextPin.length >= 1) {
            nextPin.children("input").val("");
            nextPin.children("input").focus();
        }
    });

}

function accountChk(currentPin) {
    var accountName = '(Unknown)';

    if (currentPin.length !== 6) {
        $("#txtCategoryPin").css("color", "#ff0000");
        refreshPinPassWord();
    } else {
        searchAccountName(currentPin, function(ret) {
            accountInputFocusTrap.deactivate();
            _closePopup('accountInput-popup');
            if (ret === "ERROR") {
                showMyModal('networkError-popup');
                networkErrorFocusTrap.activate();
            } else if (ret === "TIME") {
                getUseNetworkTime( function(ret) {
                    if (!ret.useNetworkTime) {
                        showMyModal('appTimeZoneError');
                        appTimeZoneErrorFocusTrap.activate();
                    } else {
                        showMyModal('networkError-popup');
                        networkErrorFocusTrap.activate();
                    }
                });
            } else {
                if (ret.resultCode == 0) {
                    accountName = ret.companyInfo.name;
                    $('#accountConfirm').attr('disabled', false);

                    setSystemSettings('commercial', {
                        signage365CareAccountName: accountName,
                        signage365CareAccountNumber: currentPin
                    }, function(ret){} );
                } else {
                    $('#accountConfirm').attr('disabled', true);
                }
                // $('#accountName').val(accountName);
                $('#accountName').text(accountName);

                // $('#accountName').html("<h2><font color='#4D4D4D'>" + accountName + "</font></h2>");
                // $('#accountCheck').show();
                showMyModal('accountCheck-popup');
                accountCheckFocusTrap.activate();
            }
        });
    }
}

function refreshPinPassWord() {
    $("#currentPin input").each(function () {
        $(this).val("");
        // onBlurInputPin($(this));
    });
}

function appInstall() {
    // overlayControlSC(true, Locale.getText('Loading…'), '#appInstallOverlay');

    get365CareServiceMode( function(mode) {
        appDownloadURL( mode.commer365CareServiceMode, function(ret) {
            if (ret === "ERROR") {
                appInstallFocusTrap.deactivate();
                _closePopup('appInstall-popup');
                showMyModal('networkError-popup');
                networkErrorFocusTrap.activate();
            } else {
                signature = ret.signature;
                installIpkDownload(ret.downloadUrl, function(ret) {
                });
            }
        });
    });

    socket.on('installIpk', function(ret) {
        if (ret.completionStatusCode == 200) {
            care365Disable( function() {
                setSystemSettings('commercial', {
                    care365Enable: "off"
                }, function() {
                    install365CareApp(signature, ret.destFile,  function(ret) {
                    });
                });
            });
        } else {
            appInstallFocusTrap.deactivate();
            _closePopup('appInstall-popup');
            showMyModal('appInstallFailed');
            appInstallFailedFocusTrap.activate();
        }
    });

    socket.on('careInstall', function(ret) {
        appInstallFocusTrap.deactivate();
        _closePopup('appInstall-popup');

        if (ret.details.state === "installed") {
            showMyModal('appInstallSucceeded-popup');
            appInstallSucceededFocusTrap.activate();
            installPopupTimer = setTimeout(function() {
                // $('#installModal').modal('hide');
                // overlayControl20(true, Locale.getText("Loading…"));
                care365Enable( function() {
                    setSystemSettings('commercial', {
                        care365Enable: "on"
                    }, function() {
                        getServerStatusCare365Subscribe( function (ret) {
                        });
                        location.reload();
                    });
                });
            }, 5000);
        } else {
            showMyModal('appInstallFailed-popup');
            appInstallFailedFocusTrap.activate();
        }
    });
}

function appUpdate() {
    // overlayControlSC(true, Locale.getText('Loading…'),'#appUpdateOverlay');

    installIpkDownload(downloadUrl, function(ret) {
    });

    socket.on('installIpk', function(ret) {
        if (ret.completionStatusCode == 200) {
            install365CareApp(signature, ret.destFile,  function(ret) {
            });
        } else {
            // overlayControlSC(false);
            appUpdateFocusTrap.deactivate();
            _closePopup('appUpdate-popup');
            showMyModal('appUpdateFailed-popup');
            appUpdateFailedFocusTrap.activate();
        }
    });

    socket.on('careInstall', function(ret) {
        // overlayControlSC(false);
        appUpdateFocusTrap.deactivate();
        _closePopup('appUpdate-popup');

        if (ret.details.state === "installed") {
            showMyModal('appUpdateSucceeded-popup');
            appUpdateSucceededFocusTrap.activate();
            updatePopupTimer = setTimeout(function() {
                // $('#updateModal').modal('hide');
                // overlayControl20(true, Locale.getText("Loading…"));
                care365Enable( function() {
                    setSystemSettings('commercial', {
                        care365Enable: "on"
                    }, function() {
                        getServerStatusCare365Subscribe( function (ret) {
                        });
                        location.reload();
                    });
                });
            }, 5000);
        } else {
            showMyModal('appUpdateFailed-popup');
            appUpdateFailedFocusTrap.activate();
        }
    });
}

function appReset() {
    care365Disable( function() {
        remove365CareApp( function() {
            setSystemSettings('commercial', {
                care365Enable: "off",
                signage365CareAccountNumber: "",
                signage365CareAccountName: ""
            }, function() {
                setTimeout( function() {
                    connectedResetFocusTrap.deactivate();
                    _closePopup('connected-reset-popup');
                    location.reload();
                }, 2500);
            });
        });
    });
}

function initResetModal() {
    $('#connected-reset').on('click', function () {
        showMyModal('connected-reset-popup');
        connectedResetFocusTrap.activate();
    });

    $('#btn-reset-ok').on('click',function() {
        appReset();
    });
}

function initNetworkModal() {
    $('#scUpdate').on('click', function () {
        showMyModal('networkError-popup');
        networkErrorFocusTrap.activate();
    });

    $('#scUpdateCheck').on('click', function () {
        showMyModal('networkError-popup');
        networkErrorFocusTrap.activate();
    });

    $('#btn-install').on('click', function () {
        showMyModal('networkError-popup');
        networkErrorFocusTrap.activate();
    });
}

function initUpdateModal() {
    $('#scUpdate').on('click', function () {
        showMyModal('appUpdate-popup');
        appUpdateFocusTrap.activate();
        appTimeZoneErrorFocusTrap.deactivate();
        _closePopup('appTimeZoneError');
        networkErrorFocusTrap.deactivate();
        _closePopup('networkError-popup');
        appUpdateSucceededFocusTrap.deactivate();
        _closePopup('appUpdateSucceeded-popup');
        appUpdateFailedFocusTrap.deactivate();
        _closePopup('appUpdateFailed-popup');
        appUpdate();

    });

    $('#btn-appUpdateSucceeded-ok').on('click', function () {
        _closePopup('appUpdateSucceeded-popup');
        clearTimeout(updatePopupTimer);
        care365Enable( function() {
            setSystemSettings('commercial', {
                care365Enable: "on"
            }, function() {
                getServerStatusCare365Subscribe( function (ret) {
                });
                location.reload();
            });
        });
    });

    $('#scUpdateCheck').on('click', function () {
        // overlayControl20(true, Locale.getText("Checking for update..."));

        get365CareServiceMode( function(mode) {
            appDownloadURL( mode.commer365CareServiceMode, function(ret) {
                if (ret === "ERROR" || ret === "TIME") {
                    appUpdateFocusTrap.deactivate();
                    _closePopup('appUpdate-popup');
                    appTimeZoneErrorFocusTrap.deactivate();
                    _closePopup("appTimeZoneError");
                    appUpdateSucceededFocusTrap.deactivate();
                    _closePopup('appUpdateSucceeded-popup');
                    appUpdateFailedFocusTrap.deactivate();
                    _closePopup('appUpdateFailed-popup');
                    networkErrorFocusTrap.deactivate();
                    _closePopup("networkError-popup");
                }

                if (ret === "ERROR") {
                    showMyModal("networkError-popup");
                    networkErrorFocusTrap.activate();
                } else if (ret === "TIME") {
                    getUseNetworkTime( function(ret) {
                        if (!ret.useNetworkTime) {
                           showMyModal("appTimeZoneError");
                           appTimeZoneErrorFocusTrap.activate();
                        } else {
                            showMyModal("networkError-popup");
                            networkErrorFocusTrap.activate();
                        }
                    });
                } else {
                    // $('#updateModal').modal('hide');
                    var newVersion = ret.downloadUrl.split('/');
                    if (newVersion[5] != appVersion) {
                        $('#appVersion').html(getLanguageText(languageTable, 'Ver') + '.' + appVersion + ' (' + getLanguageText(languageTable, 'Ver') + '.' + newVersion[5] + ')');
                        $('#scUpdateCheck').hide();
                        $('#scUpdate').show();
                        signature = ret.signature;
                        downloadUrl = ret.downloadUrl;
                    }
                }
                // overlayControl20('false');
            });
        });
    });

    $('#btn-timezoneError-ok').on('click', function () {
        autoTimeSetting();
        appTimeZoneErrorFocusTrap.deactivate();
        _closePopup('appTimeZoneError');
    });
}

function getSystemSetting365Care() {
    getSystemSettings('commercial', ['signage365CareAccountNumber', 'signage365CareAccountName','care365Enable'], function(ret) {
        accountName = ret.signage365CareAccountName;
        accountNo = ret.signage365CareAccountNumber;

        var appIdVal = `${getLanguageText(languageTable, "Number")} : ${accountNo ? accountNo : ''} / ${getLanguageText(languageTable, "Name")} : ${accountName ? accountName : ''}`;
        $('#app-id').val(appIdVal);

        // $('#appAccountNo').text(accountNo);
        // $('#appAccount').text(accountName);

        var care365Enable = ret.care365Enable == "on" ? true : false;
        $('#appEnable').prop('checked', care365Enable);

        isFirstBoot( function(ret) {
            if (ret && care365Enable) {
                getServerStatusCare365Subscribe( function (ret) {
                });
            }
        });

        if (!$('#appEnable').prop('checked')) {
            $('#serverStatus').val(getLanguageText(languageTable, "Not Connected"));
        } else {
            getServerStatusCare365( function(ret) {
                if (ret.serverStatus == "Waiting") {
                    $('#serverStatus').val(getLanguageText(languageTable, "Waiting for Approval"));
                } else {
                    $('#serverStatus').val(getLanguageText(languageTable, ret.serverStatus));
                }
            });
        }
    });
}

function getContents365Care() {
    getListApps( function(ret) {
        for (var i=0; i < ret.apps.length; i++) {
            if (ret.apps[i].id === "com.webos.app.commercial.care365") {
                appVersion = ret.apps[i].version;
                $('#appVersion').val(getLanguageText(languageTable, 'Ver') + '.' + appVersion);
                // $('#365CareNotInstall').hide();
                // $('#365CareInstalled').show();
                break;
            }
        }
    });

    socket.on('serverStatus', function(ret) {
        if (ret.serverStatus == "Waiting") {
            // console.log(getLanguageText(languageTable, "Waiting for Approval"))
            $('#serverStatus').val(getLanguageText(languageTable, "Waiting for Approval"));
        } else {
            // console.log(getLanguageText(languageTable, ret.serverStatus))
            $('#serverStatus').val(getLanguageText(languageTable, ret.serverStatus));
        }
    });
}

function change365Enable() {
    $('#appEnable').on('change', function() {
        displayLoading(true);
        if($('#appEnable').prop('checked')) {
            care365Enable( function() {
                setSystemSettings('commercial', {
                    care365Enable: "on"
                }, function() {
                    getServerStatusCare365Subscribe( function (ret) {
                    });
                    displayLoading(false);
                });
            });
        } else {
            care365Disable( function() {
                setSystemSettings('commercial', {
                    care365Enable: "off"
                }, function() {
                    $('#serverStatus').val(getLanguageText(languageTable, "Not Connected"));
                    displayLoading(false);
                });
            });
        }
    });
}

function getModalonNetwork() {
    getNetworkStatus(function (ret) {
        if (ret.wired.onInternet == "yes") {
            initInstallModal();
            initUpdateModal();
            initInputPin();
        } else {
            initNetworkModal();
            showMyModal('networkError-popup');
            networkErrorFocusTrap.activate();
        }
        $("#btn-networkError-ok").on('click',function() {
            location.href = "/ledSignage/ledControl#network";
        });

        initResetModal();
    });
}


