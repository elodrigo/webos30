// LED SI Server Setting for LCLG002
var defaultVal = {
    siServerIp: "0.0.0.0",
    serverIpPort: "0",
    secureConnection: "off",
    fqdnMode: "off",
    fqdnAddr: "http",
    appLaunchMode: "none",
    appType: "zip",
    autoSet: "on",
    proxyEnable: "off",
    proxySingleAddress: "0.0.0.0",
    proxySinglePassword: "",
    proxySinglePort: "0",
    proxySingleUsername: ""
};

var currVal = defaultVal;
var siAppName = 'com.lg.app.signage.ipk';
var updateFrom, updateTo;

var regExps = {
    portNumFormat: new RegExp('^([0-9]|[1-9][0-9]|[1-9][0-9][0-9]|[1-9][0-9][0-9][0-9]|[1-5][0-9][0-9][0-9][0-9]|6[0-4][0-9][0-9][0-9]|65[0-4][0-9][0-9]|655[0-2][0-9]|6553[0-5])$'),
    ipAddrFormat: new RegExp('^([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$'),
    urlFormat: new RegExp('^(https?)\://')
}


document.addEventListener("DOMContentLoaded", function() {
// init UI change
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/control_led_w3_si_server.css">');
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/popup_si_server_proxy.css">');

    createCurrentMenu(4, "ledSiserver");
    siServerInit();

    displayLoading(false);
});

function init() {

}

function siServerInit() {
    modalInit();
    btnInit();
    getSISetting();
}

function getSISetting() {
    var keys = ["siServerIp", "serverIpPort", "fqdnMode", "fqdnAddr", "appLaunchMode", "appType", "autoSet", "secureConnection", "proxyEnable"];
    getSystemSettings('commercial', keys, function (ret) {
        currVal = ret;
        $("#siServerIPAddr").val(ret.siServerIp);
        $("#pnum").val(ret.serverIpPort);
        ret.secureConnection === "on" ? $("#scEnable").prop('checked', true) : $("#scEnable").removeAttr('checked');
        $('input[name="appLaunchModeRadio"][value="' + ret.appLaunchMode + '"]').prop('checked', true)
        ret.fqdnMode === "on" ? ($("#fqdnEnable").prop('checked', true), $('#fqdnAddr').removeAttr("disabled")) : ($("#fqdnEnable").removeAttr('checked'), $('#fqdnAddr').prop("disabled", true));
        ret.fqdnMode === "on" ? ($('#siServerIPAddr').prop("disabled", true), $('#pnum').prop("disabled", true)) : ($('#siServerIPAddr').removeAttr("disabled"), $('#pnum').removeAttr("disabled"));
        $('#fqdnAddr').val(ret.fqdnAddr);
        $('input[name="appTypeRadio"][value="' + ret.appType + '"]').prop('checked', true);
        ret.autoSet === "on" ? $("#asEnable").prop('checked', true) : $("#asEnable").removeAttr('checked');
        ret.proxyEnable === "on" ? ($("#proxyEnable").prop('checked', true), $('#proxySetting').removeAttr("disabled")) : ($("#proxyEnable").removeAttr('checked'), $('#proxySetting').prop("disabled", true));
        subBtnInit(ret.appLaunchMode);
    });
}

function modalInit() {
    addConfirmModals('#btnReset', 'reset-modal', getLanguageText(languageTable, 'All setting data will be deleted.'),
        getLanguageText(languageTable, 'Do you want to process?'), false, function () {
            resetSystemSettings('commercial', Object.keys(defaultVal), function (ret) {
                if(ret.returnValue){
                    location.reload();
                } else {
                    console.log("Reset Error");
                }
            });
        });

    addConfirmModals('.btn-upgrades', 'upgradeModal', getLanguageText(languageTable, 'information message'),
        getLanguageText(languageTable, "Applications will be upgraded from {text}").replace('{text}', 'USB'), false, function () {
            displayLoading(true);
            if (updateFrom === 'usb') {
                usbUpdate();
            } else if (currVal.appType === 'zip') {
                if (currVal.appLaunchMode === 'usb') {
                    usbUpdate();
                } else {
                    zipUpdate({
                        "from": updateFrom,
                        "to": updateTo
                    });
                }
            } else if (updateFrom === 'remote') {
                remoteUpdate();
            }

        }, function () {

        }, function (i) {
            updateFrom = i.attr('from');
            updateTo = i.attr('to');
            var trans = {
                "none": "None",
                "local": "Local",
                "remote": "Remote",
                "usb": "USB"
            }; // for translation
            var upgradeModalTxt = getLanguageText(languageTable, 'Applications will be upgraded from {text}').replace('{text}', getLanguageText(languageTable, trans[updateFrom]));
            $('#upgradeModal-text-content').text(upgradeModalTxt);
        });
}

function btnInit() {
    $('#btn-proxy-confirm').on('click', function () {
        var proxySetVal = {
            proxySingleAddress: $("#proxyIPAddr").val(),
            proxySinglePort: $('#proxyPortNum').val(),
            proxySingleUsername: $('#userName').val(),
            proxySinglePassword: $('#proxyPassword').val()
        }
        if(!regExps.ipAddrFormat.test(proxySetVal.proxySingleAddress)) proxySetVal.proxySingleAddress = currVal.proxySingleAddress;
        if(!regExps.portNumFormat.test(proxySetVal.proxySinglePort)) proxySetVal.proxySinglePort = currVal.proxySinglePort;
        if(proxySetVal.proxySingleUsername.length > 255 || proxySetVal.proxySinglePassword.length > 255){
            proxySetVal.proxySingleUsername = currVal.proxySingleUsername;
            proxySetVal.proxySinglePassword = currVal.proxySinglePassword;
        }
        setSystemSettings('commercial', proxySetVal);
    });

    $('#proxySetting').on('click', function () {
        var keys = ["proxySingleAddress", "proxySinglePort", "proxySingleUsername", "proxySinglePassword"];
        getSystemSettings('commercial', keys, function (ret) {
            $("#proxyIPAddr").val(ret.proxySingleAddress);
            $("#proxyPortNum").val(ret.proxySinglePort);
            $("#userName").val(ret.proxySingleUsername);
            $("#proxyPassword").val(ret.proxySinglePassword);
            showMyModal('proxy-popup');
        });
    });

    $('#siServerIPAddr').on('change', function () {
        var ip = $("#siServerIPAddr").val();
        if (!regExps.ipAddrFormat.test(ip)) {
            $("#siServerIPAddr").val(currVal.siServerIp);
            return;
        }
        currVal.siServerIp = ip;
        setSystemSettings('commercial', {
            siServerIp: ip
        });
    });

    $('#pnum').on('change', function () {
        var port = $("#pnum").val();
        if (!regExps.portNumFormat.test(port)) {
            $("#pnum").val(currVal.serverIpPort);
            return;
        }
        currVal.serverIpPort = port;
        setSystemSettings('commercial', {
            serverIpPort: port
        });
    });

    $( 'input:radio[name=appTypeRadio]').on('change', function () {
        var selected = $(this).val();
        var validation = ['zip', 'ipk'];
        if (validation.indexOf(selected) < 0) {
            $('input[name="appTypeRadio"][value="' + currVal.appType + '"]').prop('checked', true);
            return;
        }
        currVal.appType = selected;
        setSystemSettings('commercial', {
            appType: selected
        });
    });
    var keys = {
        scEnable : "secureConnection",
        asEnable : "autoSet",
        fqdnEnable : "fqdnMode",
        proxyEnable : "proxyEnable"
    };

    /*$('#scEnable, #asEnable, #fqdnEnable, #proxyEnable').on('change', function () {
        var keys = {
            scEnable : "secureConnection",
            asEnable : "autoSet",
            fqdnEnable : "fqdnMode",
            proxyEnable : "proxyEnable"
        };
        var settings = {};

        var btnid = $(this).attr('id');
        const isChecked = $(this).prop('checked');
        $(this).attr('aria-checked', isChecked);
        var enable = $("input:checkbox[id=" + btnid + "]").is(":checked") ? "on" : "off";
        if (btnid === "fqdnEnable") {
            enable === "on" ? ($('#fqdnAddr').removeAttr("disabled"), $('#siServerIPAddr').prop("disabled", true), $('#pnum').prop("disabled", true)) : ($('#fqdnAddr').prop("disabled", true), $('#siServerIPAddr').removeAttr("disabled"), $('#pnum').removeAttr("disabled"));
        }
        if (btnid === "proxyEnable") {
            enable === "on" ? $('#proxySetting').removeAttr("disabled") : $('#proxySetting').prop("disabled", true);
        }
        currVal[keys[btnid]] = enable;
        settings[keys[btnid]] = enable;
        setSystemSettings('commercial', settings);
    });*/

    $('#scEnable').on('change', function () {
        const isChecked = $(this).prop('checked');
        $(this).attr('aria-checked', isChecked);

        var settings = {};
        var btnid = $(this).attr('id');
        var enable = isChecked ? "on" : "off";
        currVal[keys[btnid]] = enable;
        settings[keys[btnid]] = enable;
        setSystemSettings('commercial', settings);
    });

    $('#asEnable').on('change', function () {
        const isChecked = $(this).prop('checked');
        $(this).attr('aria-checked', isChecked);

        var settings = {};
        var btnid = $(this).attr('id');
        var enable = isChecked ? "on" : "off";
        currVal[keys[btnid]] = enable;
        settings[keys[btnid]] = enable;
        setSystemSettings('commercial', settings);
    });

    $('#proxyEnable').on('change', function () {
        const isChecked = $(this).prop('checked');
        $(this).attr('aria-checked', isChecked);
        const proxy = $('#proxySetting');
        if (isChecked) {
            $(proxy).prop('disabled', false);
            $(proxy).removeClass('disabled');
        } else {
            $(proxy).prop('disabled', true);
            $(proxy).addClass('disabled');
        }

        var settings = {};
        var btnid = $(this).attr('id');
        var enable = isChecked ? "on" : "off";
        currVal[keys[btnid]] = enable;
        settings[keys[btnid]] = enable;
        setSystemSettings('commercial', settings);
    });

    $('#fqdnEnable').on('change', function () {
        const isChecked = $(this).prop('checked');
        $(this).attr('aria-checked', isChecked);
        const inputField = $('#fqdnAddr');
        const siIpAddr = $('#siServerIPAddr');
        const pnum = $('#pnum');
        if (isChecked) {
            inputField.prop('disabled', false);
            siIpAddr.prop('disabled', true);
            pnum.prop('disabled', true);
        } else {
            inputField.prop('disabled', true);
            siIpAddr.prop('disabled', false);
            pnum.prop('disabled', false);
        }

        var settings = {};
        var btnid = $(this).attr('id');
        var enable = isChecked ? "on" : "off";
        currVal[keys[btnid]] = enable;
        settings[keys[btnid]] = enable;
        setSystemSettings('commercial', settings);
    });

    $('#pnum').on('blur', function () {
        const port = $(this).val();
        if (port >= 0 && port <= 65535) {
            console.log('yes');
        } else {
            $(this).val(0);
        }

        var settings = {};
        var btnid = $(this).attr('id');
        var enable = isChecked ? "on" : "off";
        currVal[keys[btnid]] = enable;
        settings[keys[btnid]] = enable;
        setSystemSettings('commercial', settings);
    });

    var radio = $('input[name=appLaunchModeRadio]');
    radio.on('change', function () {

        const myId = $(this).prop('id');

        radio.each(function (j, elem) {
            var hereId = $(elem).prop('id');
            if (myId === hereId) {
                $(this).attr("checked", true);
            } else {
                $(elem).attr("checked", false);
            }
        });

        var selected = $(this).val();
        var validation = ['none', 'local', 'remote', 'usb'];
        if (validation.indexOf(selected) < 0) {
            $('input[name="appLaunchModeRadio"][value="' + currVal.appLaunchMode + '"]').prop('checked', true);
            return;
        }
        if (selected === 'remote' || selected === 'usb') {
            $('input[name="appTypeRadio"][value="zip"]').prop('checked', true).trigger('change');
        }
        subBtnInit(selected);
        currVal.appLaunchMode = selected;
        setSystemSettings('commercial', {
            appLaunchMode: selected
        });
    });
}

function subBtnInit(launchMode) {
    const upgrade = $('#local-upgrade-field');
    const app = $('#app-time-field');
    const usb = $('#usb-field');

    switch (launchMode) {
        case "local":
            $('#localUpgradeUSB').removeAttr('disabled');
            $('#localUpgradeRemote').removeAttr('disabled');
            $('#usbUpgradeRemote').prop('disabled', true);
            $('input[name=appTypeRadio]').removeAttr('disabled');
            $('#fqdnEnable').removeAttr('disabled');

            upgrade.css('display', 'flex');
            app.css('display', 'flex');
            usb.css('display', 'none');
            break;
        case "remote":
            $('#localUpgradeUSB').prop('disabled', true);
            $('#localUpgradeRemote').prop('disabled', true);
            $('#usbUpgradeRemote').prop('disabled', true);
            $('input[name=appTypeRadio]').prop('disabled', true);
            $('#fqdnEnable').removeAttr('disabled');

            upgrade.css('display', 'none');
            app.css('display', 'none');
            usb.css('display', 'none');
            break;
        case "usb":
            $('#localUpgradeUSB').prop('disabled', true);
            $('#localUpgradeRemote').prop('disabled', true);
            $('#usbUpgradeRemote').removeAttr('disabled');
            $('input[name=appTypeRadio]').prop('disabled', true);
            $('#fqdnEnable').removeAttr('disabled');

            upgrade.css('display', 'none');
            app.css('display', 'none');
            usb.css('display', 'flex');
            break;
        default:
            $('#siServerIPAddr').removeAttr('disabled');
            $('#pnum').removeAttr('disabled');
            $('#localUpgradeUSB').prop('disabled', true);
            $('#localUpgradeRemote').prop('disabled', true);
            $('#usbUpgradeRemote').prop('disabled', true);
            $('input[name=appTypeRadio]').prop('disabled', true);
            $('#fqdnEnable').prop('disabled', true);
            $("#fqdnEnable").removeAttr('checked');
            $('#fqdnAddr').prop("disabled", true);

            upgrade.css('display', 'none');
            app.css('display', 'none');
            usb.css('display', 'none');
            break;
    }
}

function usbUpdate() {
    listDevices(function (ret) {
        if (ret.returnValue) {
            var devices = ret.devices;
            for (var i = 0; i < devices.length; i++) {
                if (devices[i].deviceType === 'usb') {
                    var usb = devices[i];
                    break;
                }
            }
            if (usb === undefined || usb === null) {
                displayLoading(false);
                alert(getLanguageText(languageTable, "Cannot connect to the USB"));
                return;
            }

            if (currVal.appType === "zip") {
                zipUpdate({
                    "from": updateFrom,
                    "to": updateTo
                });
            } else {
                copyApplication(usb, function (ret) {
                    if (ret.returnValue) {
                        ipkUpdate(ret.destinationPath);
                    } else {
                        displayLoading(false);
                        alert(getLanguageText(languageTable, "Failed, try again."));
                    }
                });
            }
        } else {
            displayLoading(false);
            alert(getLanguageText(languageTable, "Cannot connect to the USB"));
        }
    });
}

function remoteUpdate() {
    var sc = currVal.secureConnection === 'on' ? "https://" : "http://";
    var url = currVal.fqdnMode === "on" ? currVal.fqdnAddr : sc + currVal.siServerIp + ":" + currVal.serverIpPort + '/application/' + siAppName;
    downloadApplication({
        targetURL: url
    }, function () {});

    socket.on("downloadIPK", function (ret) {
        if (ret.completionStatusCode === 200) {
            ipkUpdate(ret.target);
        } else {
            displayLoading(false);
            alert(getLanguageText(languageTable, "Download failed, try again."));
        }
    });
}

function zipUpdate(params) {
    upgradeApplication(params, function (ret) {
        displayLoading(false);
        if (ret.returnValue === false) {
            alert(getLanguageText(languageTable, "Failed, try again."));
        } else {
            alert(getLanguageText(languageTable, "Upgrade Complete"));
        }
    });
}

function ipkUpdate(target) {
    installApplication({
        ipkUrl: target
    }, function () {});
    socket.on("installIPK", function (ret) {
        displayLoading(false);
        if (ret.details.state === "installed") {
            alert(getLanguageText(languageTable, "Install Succeeded"));
        } else {
            alert(getLanguageText(languageTable, "Install Failed."));
        }
    });
}


