var currentTime = undefined;
var selectTime = {};

const yearDisplayName = new Map();
let yearValList = [];

const monthDisplayName = new Map();
let monthValList = [];

const dayDisplayName = new Map();
let dayValList = [];

const hourDisplayName = new Map();
let hourValList = [];

const minDisplayName = new Map();
let minValList = [];

document.addEventListener("DOMContentLoaded", function() {
    // init UI change

    $('#time-switch').on('change', function () {
        const isChecked = $(this).prop('checked');
        $(this).attr('aria-checked', isChecked);
        const timeGroup = $('#time-group');
        const save = $('#btn-ctrl-save');
        const pc = $('#btn-pc-save');
        const date = document.getElementById('time-date-group');
        const time = document.getElementById('time-group');
        if (isChecked) {
            timeGroup.prop('display', 'none');
            save.prop('disabled', true);
            pc.prop('disabled', true);
            date.style.pointerEvents ='none';
            date.style.opacity = '0.4';
            time.style.pointerEvents ='none';
            time.style.opacity = '0.4';
        } else {
            timeGroup.prop('display', 'block');
            save.prop('disabled', false);
            pc.prop('disabled', false);
            date.style.pointerEvents ='auto'
            date.style.opacity = '1';
            time.style.pointerEvents ='auto'
            time.style.opacity = '1';
        }
    });

    displayLoading(false);
});

function drawYearSelect() {
    for (let i = 2016; i <= 2021 + 16; i++) {
        yearValList.push(i);
        yearDisplayName.set(i, String(i));
    }

    const yearDropdown = $('#year-dropdown');
    const dropdownMenu = $(yearDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(yearValList, yearDisplayName, 'year-dropdown');

    const lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            const selected = yearValList[i];
            const ariaId = 'option-year-' + selected;
            selectTime.year = selected
            changeDropdownLabel("year-dropdown", yearDisplayName.get(selected), ariaId);
            changeDropdownAria("year-dropdown", i);
            drawDaySelect();
        });
    });

    $(dropdownMenu).html(ul);

    changeDropdownLabel("year-dropdown", currentTime.year, "option-year-" + currentTime.year);
    const myIndex = yearValList.findIndex(function (item) {return item === currentTime.year});
    changeDropdownAria("year-dropdown", myIndex);
}

function drawMonthSelect() {
    for (let i = 1; i <= 12; i++) {
        monthValList.push(i);
        monthDisplayName.set(i, String(i));
    }

    const monthDropdown = $('#month-dropdown');
    const dropdownMenu = $(monthDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(monthValList, monthDisplayName, 'month-dropdown');

    const lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            const selected = monthValList[i];
            selectTime.month = selected;
            const ariaId = 'option-month-' + selected;
            changeDropdownLabel("month-dropdown", monthDisplayName.get(selected), ariaId);
            changeDropdownAria("month-dropdown", i);
            drawDaySelect(selected);
        });
    });

    $(dropdownMenu).html(ul);

    changeDropdownLabel("month-dropdown", currentTime.month, "option-month-" + currentTime.month);
    const myIndex = monthValList.findIndex(function (item) {return item === currentTime.month});
    changeDropdownAria("month-dropdown", myIndex);
}

function drawDaySelect(month) {
    var dayCount = [31,28,31,30,31,30,31,31,30,31,30,31];

    const year = selectTime.year;
    var currMonth = month === undefined ? currentTime.month : month;

    if( ((year % 4 === 0) && (year % 100 !== 0)) || year % 400 === 0){
        dayCount[1] = 29;
    }

    for (let i = 1; i <= dayCount[currMonth-1]; i++) {
        dayValList.push(i);
        dayDisplayName.set(i, String(i));
    }

    const dayDropdown = $('#day-dropdown');
    const dropdownMenu = $(dayDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(dayValList, dayDisplayName, 'day-dropdown');

    const lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            const selected = dayValList[i];
            const ariaId = 'option-day-' + selected;
            changeDropdownLabel("day-dropdown", dayDisplayName.get(selected), ariaId);
            changeDropdownAria("day-dropdown", i);
            selectTime.day = selected;
        });
    });

    $(dropdownMenu).html(ul);

    if (currentTime.day > dayCount[currentTime.month-1]) {
        const myDay = dayCount[currentTime.month-1]
        currentTime.day = myDay;
        changeDropdownLabel("day-dropdown", myDay, "option-day-" + myDay);
        const myIndex = dayValList.findIndex(function (item) {return item === myDay});
        changeDropdownAria("day-dropdown", myIndex);
    } else {
        changeDropdownLabel("day-dropdown", currentTime.day, "option-day-" + currentTime.day);
        const myIndex2 = dayValList.findIndex(function (item) {return item === currentTime.day});
        changeDropdownAria("day-dropdown", myIndex2);
    }

}

function drawHourSelect() {
    for (let i = 0; i <= 23; i++) {
        hourValList.push(i);
        hourDisplayName.set(i, String(i));
    }

    const hourDropdown = $('#hour-dropdown');
    const dropdownMenu = $(hourDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(hourValList, hourDisplayName, 'hour-dropdown');

    const lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            const selected = hourValList[i];
            const ariaId = 'option-hour-' + selected;
            changeDropdownLabel("hour-dropdown", hourDisplayName.get(selected), ariaId);
            changeDropdownAria("hour-dropdown", i);
            selectTime.hour = selected;
        });
    });

    $(dropdownMenu).html(ul);

    changeDropdownLabel("hour-dropdown", currentTime.hour, "option-hour-" + currentTime.hour);
    const myIndex = hourValList.findIndex(function (item) {return item === currentTime.hour});
    changeDropdownAria("hour-dropdown", myIndex);
}

function drawMinSelect() {
    for (let i = 0; i <= 59; i++) {
        minValList.push(i);
        minDisplayName.set(i, String(i));
    }

    const minDropdown = $('#min-dropdown');
    const dropdownMenu = $(minDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(minValList, minDisplayName, 'min-dropdown');

    const lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            const selected = minValList[i]
            const ariaId = 'option-min-' + selected;
            changeDropdownLabel("min-dropdown", minDisplayName.get(selected), ariaId);
            changeDropdownAria("min-dropdown", i);
            selectTime.minute = selected;
        });
    });

    $(dropdownMenu).html(ul);

    changeDropdownLabel("min-dropdown", currentTime.minute, "option-min-" + currentTime.minute);
    const myIndex = minValList.findIndex(function (item) {return item === currentTime.minute});
    changeDropdownAria("min-dropdown", myIndex);
}

function setUpTimePanel() {
    getCurrentTime(function (msg) {
        currentTime = msg;
        selectTime = currentTime;
        drawYearSelect();
        drawMonthSelect();
        drawDaySelect();
        drawHourSelect();
        drawMinSelect();

        getNTPStatus(function (ret) {
            var auto = ret.useNetworkTime;
            var timeSwitch = $('#time-switch');
            timeSwitch.prop('checked', auto);
            timeSwitch.attr('aria-checked', auto);
            $('#year-dropdown').attr('disabled', auto);
            $('#month-dropdown').attr('disabled', auto);
            $('#day-dropdown').attr('disabled', auto);
            $('#hour-dropdown').attr('disabled', auto);
            $('#min-dropdown').attr('disabled', auto);
            $('#btn-ctrl-save').attr('disabled', auto);
            $('#btn-pc-save').attr('disabled', auto);
            $('#btn-ntp-server').attr('disabled', !auto);
        });

        initNtpServerSetting();
    });

    $('#time-switch').on('click', function () {
        var auto = $(this).prop('checked');
        displayLoading(true);
        var param = {useNTP: auto}
        setNTPStatus(param, function(ret) {
            getCurrentTime(function(msg) {
                currentTime = msg;
                selectTime = currentTime;
                drawYearSelect();
                drawMonthSelect();
                drawDaySelect();
                drawHourSelect();
                drawMinSelect();
                $('#year-dropdown').attr('disabled', auto);
                $('#month-dropdown').attr('disabled', auto);
                $('#day-dropdown').attr('disabled', auto);
                $('#hour-dropdown').attr('disabled', auto);
                $('#min-dropdown').attr('disabled', auto);
                $('#btn-ctrl-save').attr('disabled', auto);
                $('#btn-pc-save').attr('disabled', auto);
                $('#btn-ntp-server').attr('disabled', !auto);
            });
            displayLoading(false);
        });
    });

    $('#btn-ctrl-save').on('click', function () {
        setCurrentTime(selectTime);
        showMyModal('time-saved-modal');
    });

    $('#btn-pc-save').on('click', function () {
        var currDate = new Date();
        selectTime.year = currDate.getFullYear();
        selectTime.month = currDate.getMonth()+1;
        selectTime.day = currDate.getDate();
        selectTime.hour = currDate.getHours();
        selectTime.minute = currDate.getMinutes();

        const yearSelected = yearValList[currDate.getFullYear()];
        const yearAriaId = 'option-year-' + yearSelected;
        changeDropdownLabel("year-dropdown", yearDisplayName.get(yearSelected), yearAriaId);
        var yearIndex = yearValList.findIndex(function (item) {return item === yearSelected});
        changeDropdownAria("year-dropdown", yearIndex);

        const monthSelected = monthValList[currDate.getMonth()+1];
        const monthAriaId = 'option-month-' + monthSelected;
        changeDropdownLabel("month-dropdown", monthDisplayName.get(monthSelected), monthAriaId);
        var monthIndex = monthValList.findIndex(function (item) {return item === monthSelected});
        changeDropdownAria("month-dropdown", monthIndex);

        const daySelected = dayValList[currDate.getDate()];
        const dayAriaId = 'option-day-' + daySelected;
        changeDropdownLabel("day-dropdown", dayDisplayName.get(daySelected), dayAriaId);
        var dayIndex = dayValList.findIndex(function (item) {return item === daySelected});
        changeDropdownAria("day-dropdown", dayIndex);

        const hourSelected = hourValList[i];
        const hourAriaId = 'option-hour-' + hourSelected;
        changeDropdownLabel("hour-dropdown", hourDisplayName.get(hourSelected), hourAriaId);
        var hourIndex = hourValList.findIndex(function (item) {return item === hourSelected});
        changeDropdownAria("hour-dropdown", hourIndex);

        const minSelected = minValList[i]
        const minAriaId = 'option-min-' + minSelected;
        changeDropdownLabel("min-dropdown", minDisplayName.get(minSelected), minAriaId);
        var minIndex = minValList.findIndex(function (item) {return item === minSelected});
        changeDropdownAria("min-dropdown", minIndex);

        setNTPStatus({useNTP: false}, function(ret){
            setCurrentTime(selectTime);
        });
    });

    makeKeyDownDropdown('year-dropdown');
    makeKeyDownDropdown('month-dropdown');
    makeKeyDownDropdown('day-dropdown');
    makeKeyDownDropdown('hour-dropdown');
    makeKeyDownDropdown('min-dropdown');
}