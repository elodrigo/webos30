document.addEventListener("DOMContentLoaded", function() {
    // init UI change
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/control_led_w3_media.css">');
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/popup_media_delete.css">');
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/popoup_preview.css">');
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/popup_software_common.css">');

    var checkCount = 0;

    addAlertModals('media-warning-modal', getLanguageText(languageTable, 'Please choose a file to upload'));
    addProgressModal('media-progress-modal', getLanguageText(languageTable, 'Uploading'));
    addCompleteModal('media-complete-modal');

    $('input[type=checkbox]').on('change', function () {
        var isChecked = $(this).prop('checked');
        $(this).attr("checked", isChecked);
        if (isChecked) {
            checkCount += 1;
        } else if (!isChecked && checkCount > 0) {
            checkCount -= 1;
        }

        if (checkCount < 1) {
            $('#btn-media-delete').prop('disabled', true);
            $('#btn-media-export').prop('disabled', true);
        } else {
            $('#btn-media-delete').prop('disabled', false);
            $('#btn-media-export').prop('disabled', false);
        }
    });

    $('#btn-media-delete').on('click', function () {
        showMyModal('media-delete-popup');
    });

    displayLoading(false);
});

function init() {

}

function mediaInit(curDir) {
    createCurrentMenu(2, "ledMedia");

    $('#media-add-file').on('change', function (e) {
        if (e.target.files[0].name) {
            var file = e.target.files[0];
            var newname = file.name;
            if (mediaFiles.indexOf(newname.replace(/&/gi, "&amp;")) >= 0) {
                var modalID = 'rename' + (new Date().valueOf());
                var tempName = rename(newname);
                newname = tempName;
                setTimeout(function () {
                    uploadFile(file, newname);
                },500);
                return;
            }
            uploadFile(file, newname);
        }
    });

    $("img.img").each(function (index) {
        var id = $(this).attr('id');
        var media = $(this).attr('name');
        var format = media.substr(media.length - 3, 3).toLowerCase();
        getThumbnail(id, media);

        getVideoInfo(media, function(msg){
            // console.log(msg);
            if(msg.returnValue){
                $('#'+id+'_res').text(msg.originalWidth + 'X' + msg.originalHeight);
            }else{
                $('#'+id+'_res').text('Unknown');
            }
        });
    });

    $('.media-checkBox').on('click', function () {
        if ($(this).prop("checked")) {
            selectedFile.push($(this).attr("id"));
        } else {
            selectedFile.splice(selectedFile.indexOf($(this).attr("id")), 1);
        }

        // 아래 버튼 disable은 따로 구현
        // if( selectedFile.length > 0 ){
        //     $('#normalToolBar').hide();
        //     $('#multiToolBar').show();
        //     $('#selectedCount').text(selectedFile.length+' item selected');
        // }else{
        //     $('#normalToolBar').show();
        //     $('#multiToolBar').hide();
        //     $('#selectedCount').text('');
        // }
    });

    const imgElem = $('.element').find('.icon-scale');
    $(imgElem).on('click', function () {
        showMyModal('thumbnail-popup')
        var myImg = $(this).prev();
        var filename = $(myImg).prop('name');
        console.log('/download?name='+encodeURIComponent(curDir + '/' + filename))

        getThumbnail('bigThumb', filename)
        // togglePopup('icon-scale', 'thumbnail-popup');
    });

    $('#btn-media-export').on('click', function () {
        var link = document.createElement('a');
        link.setAttribute('download','');
        link.style.display = 'none';
        document.body.appendChild(link);

        console.log(selectedFile)

        var index = 0;
        selectedFile.forEach(function(fileName, index, array){
            setTimeout(function(){
                link.setAttribute('href', '/download?name='+encodeURIComponent(curDir + '/' + fileName));
                link.click();
                if(index === array.length - 1){
                    setTimeout(function(){
                        document.body.removeChild(link);
                    }, (index++)*1000);
                }
            }, (index++)*1000)
        });
    });

    $("button[id^='down']").on('click',function() {
        var fileName = $(this).attr('name');
        var link = document.createElement('a');
        link.setAttribute('download','');
        link.style.display = 'none';
        document.body.appendChild(link);
        link.setAttribute('href', '/download?name='+encodeURIComponent(curDir + '/' + fileName));
        link.click();
    });
}

function mediaAddClick() {
    document.getElementById('media-add-file').click();
}

function thumbnailClick() {

}

function uploadFile(file, newname) {
    var formData = new FormData();

    formData.append('dir', curDir);
    formData.append('newname', newname);
    formData.append('media', file);

    if (!file) {
        showMyModal('media-warning-modal', getLanguageText(languageTable, 'Please choose a file to upload'));
        return;
    }

    var fileName = file.name;
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();

    if (movie.indexOf(ext) < 0 && image.indexOf(ext) < 0) {
        showMyModal('media-warning-modal', getLanguageText(languageTable, 'Unsupported File type. Only Image/Video files are allowed.'));
        return;
    }

    if (file.size > maxSize) {
        showMyModal('media-warning-modal', getLanguageText(languageTable, 'Flash memory is not enough.'));
        return;
    }

    showMyModal('media-progress-modal');

    var xhr = new XMLHttpRequest();

    xhr.open('post', '/upload', true);

    xhr.upload.onprogress = function (e) {
        if (e.lengthComputable) {
            var per = Math.round((e.loaded / e.total) * 100);
            updateProgressModal('media-progress-modal', per);
        }
    }

    xhr.upload.onload = function (e) {
        setTimeout(function () {
            _closePopup('media-progress-modal');
            window.location.reload(true);
        },2000);
    }

    xhr.onerror = function (e) {
        _closePopup('media-progress-modal');
        alert('An error occurred while submitting the form.');
        console.log('An error occurred while submitting the form. Maybe your file is too big');
    }

    xhr.onload = function() {
        console.log(this.statusText);
    };

    xhr.send(formData);
}