//for Dashboard
var refreshInterval = undefined;

//for TimeZone
var continentNameList = ["Africa", "Asia", "CIS", "Europe", "Middle East", "North America", "Oceania", "Pacific", "South America"];
var continentNameMap = {"Africa": "Africa", "Asia": "Asia", "CIS": "CIS", "Europe": "Europe",
    "MiddleEast": "Middle East", "NorthAmerica": "North America", "Oceania": "Oceania",
    "Pacific": "Pacific", "SouthAmerica": "South America"};
var continentList = ["Africa", "Asia", "CIS", "Europe", "MiddleEast", "NorthAmerica", "Oceania", "Pacific", "SouthAmerica"];
var currContinent = '';
var currCountry = '';
var currCity = '';
var isCityState = false;

var currentTime;

var myWeekDays = {
    Sun: getLanguageText(languageTable, "Sunday"),
    Mon: getLanguageText(languageTable, "Monday"),
    Tue: getLanguageText(languageTable, "Tuesday"),
    Wed: getLanguageText(languageTable, "Wednesday"),
    Thu: getLanguageText(languageTable, "Thursday"),
    Fri: getLanguageText(languageTable, "Friday"),
    Sat: getLanguageText(languageTable, "Saturday")
};
var myMonths = {
    Jan: getLanguageText(languageTable, "January"),
    Feb: getLanguageText(languageTable, "February"),
    Mar: getLanguageText(languageTable, "March"),
    Apr: getLanguageText(languageTable, "April"),
    May: getLanguageText(languageTable, "May"),
    Jun: getLanguageText(languageTable, "June"),
    Jul: getLanguageText(languageTable, "July"),
    Aug: getLanguageText(languageTable, "August"),
    Sep: getLanguageText(languageTable, "September"),
    Oct: getLanguageText(languageTable, "October"),
    Nov: getLanguageText(languageTable, "November"),
    Dec: getLanguageText(languageTable, "December")
};

var pointStyles = [
    'circle',
    'triangle',
    'star',
    'rect',
    'cross',
    'dash',
    'line',
    'rectRounded',
    'crossRot',
    'rectRot',
];

//for FirstUse
var originalVal = {};
var selectVal = {};

//for Log
var currLogPage = 0;
var currLogPageM = 0;
var logRowCount = 0;
var sepLogString = undefined;

// LED Status
var unitStatus = {
    ok: 0,
    ng: 0,
    off: 0
};

var color = [
    "#4572a7", "#aa4643", "#89a54e", "#71588f", "#4198af", "#db843d",
    "#93a9cf", "#d19392", "#b9cd96", "#a99bbd", "#88c8da", "#f3b07a",
    "#0d62f5", "#ec1f1c", "#a1ec20", "#6e1ce2", "#1eb6e0", "#f3740c"
];

document.addEventListener("DOMContentLoaded", function() {
// init UI change
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/control_led_w3_dashboard.css">');
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/popup_system_info.css">');
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/popup_update.css">');
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/popup_first_user.css">');

    createCurrentMenu(0, "ledDashboard");
    drawAutoRefresh();

    addConfirmModals('#rebootBtn', 'reboot-popup', getLanguageText(languageTable, 'Reboot'), getLanguageText(languageTable, 'Reboot?'), false,
        function () {
            displayLoading(true);
            rebootScreen();
            setTimeout(function(){
                window.location.replace("/login.html");
            }, 60 * 1000);
    });

    $('#btn-temp-more').on('click', function () {
        var isChart = $(this).data('name') === 'chart';
        if (isChart) {
            $(this).data('name', 'table');
            $(this).text(getLanguageText(languageTable, "Charts"));
            $('#temperature-chart').css('display', 'none');
            $('#temperature-table').css('display', 'block');
        } else {
            $(this).data('name', 'chart');
            $(this).text("Table");
            $('#temperature-chart').css('display', 'block');
            $('#temperature-table').css('display', 'none');
        }
    });

    $('#btn-temp-detail').on('click', function () {
        showMyModal('temperature-popup');
    });

    $('#btn-system-more').on('click', function () {
        showMyModal('system-popup');
    });

    $('#btn-log-more').on('click', function () {
        showMyModal('log-popup');
    });

    setTimeout(function () {
        showTempTable();
    }, 200);

    makeKeyDownDropdown('auto-refresh-dropdown');

    displayLoading(false);
});

function showTempTable() {
    if (chartPage < 1) {
        chartPage = 1;
    }

    var pageSet = 10;

    var start = (chartPage - 1) * defaultHistorySize;
    var end = start + defaultHistorySize;

    function startTemperatureTable(tempSensors) {
        var index = 0;
        var tempTableHistory = {};
        /*for (const top of tempSensors) {
            tempTableHistory[top.id] = [];
        }*/
        var tempHistory = [];
        function tempHistoryCallback(msg) {

            readTempType(function (tempType) {
                var isCelsius = tempType.isCelsius;

                if(isCelsius == true) {
                    tempHistory.push(msg.history);
                } else {
                    for(var i=0; i<msg.history.length;i++) {
                        msg.history[i] = msg.history[i]* 1.8 + 32
                    }
                    tempHistory.push(msg.history);
                }
                console.log('history', tempHistory)
                tempTableHistory[tempSensors[index].id] = tempHistory[0];

                if (index === tempSensors.length - 1) {
                    totalHistorySize = msg.queueSize;
                    // drawTempChart(tempHistory, msg.interval, true, id);
                    drawTempChart('myTemp', tempTableHistory, msg.interval, tempType.isCelsius);
                    index = 0;
                    return;
                }
                ++index;
                getTempHistory(tempSensors[index].id, start, end, tempHistoryCallback);

            });
        }
        getTempHistory(tempSensors[index].id, start, end, tempHistoryCallback);
    }
    startTemperatureTable(tempSensors)
}

function drawTempChart(id, data, interval, isCelsius) {
    if (!data) {
        alert(getLanguageText(languageTable, "Fan RPM history is not yet collected"));
        return;
    }

    var chartData = {
        labels: [],
        datasets: []
    };

    var colorIndex = 0;
    for (const top of tempSensors) {
        if (data[top.id] && data[top.id].length > 0) {
            chartData.labels = createLabels(data[top.id].length, interval);
            var dataset = {
                label: top.name,
                data: data[top.id].reverse(),
                borderWidth: 1,
                borderColor: color[colorIndex],
                backgroundColor: color[colorIndex],
                labelUnit: '',
                pointStyle: pointStyles[colorIndex],
                pointRadius: 6,
            };
            chartData.datasets.push(dataset);
        }
        ++colorIndex;
    }

    var stepWidth = 10;
    var maxTemp = isCelsius ? 60 : 130;

    var opt = {
        maintainAspectRatio: false,
        responsive: true,
        layout: {
            padding: 0
        },
        scales: {
            yAxes: [
                {
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        min: 0,
                        max: maxTemp,
                        stepSize: stepWidth,
                    }
                }
            ]
        },
        plugins: {
            legend: {
                position: 'bottom',
            },
            title: {
                display: false,
                text: 'Chart.js Line Chart'
            },
            tooltip: {
                usePointStyle: true,
            }
        }
    };

    const ctx = document.getElementById(id+'-chart').getContext('2d');
    const myChart = new Chart(ctx, {type: 'line', data: chartData, options: opt});

    drawTempTable(id, chartData, isCelsius);
}

function createLabels(length, interval) {
    if (!currentTime) {
        return
    }
    var labels = [];

    var curTime = currentTime.split(' ').splice(0, 5).join(' ');
    var curDate = new Date(curTime);
    var tempDate = new Date();
    interval = interval * 1000;
    for (var i = 0; i !== length; ++i) {
        tempDate.setTime(curDate.getTime() - interval * (defaultHistorySize * (chartPage - 1) + (length - i - 1)));
        var timeStr = toTimeStr(tempDate);
        labels.push(timeStr);
    }
    return labels;
}

function toTimeStr(date) {
    var timeStr = date.toTimeString();
    timeStr = timeStr.substr(0, timeStr.indexOf(" "));
    timeStr = timeStr.substr(0, timeStr.lastIndexOf(":"));
    return timeStr;
}

function drawTempTable(id, chartData, isCelsius) {
    function makeCols() {
        var colHtml = '';

        for (var label of chartData.labels) {
            colHtml += '<col style="width: 120px;">';
        }
        return colHtml;
    }

    function makeLabels() {
        var labelHtml = '';

        for (var label of chartData.labels) {
            labelHtml += `<th scope="col"><div class="text-block">${label}</div></th>`
        }
        return labelHtml;
    }

    function makeTbody() {
        var tbodyHtml = '';

        for (var trData of chartData.datasets) {
            var tr = '<tr>';
            var th = `<th scope="row" colspan="2">${trData.label}</th>`;
            tr += th;

            for (var tdData of trData.data) {
                var td = `<td>${tdData}${trData.labelUnit ? trData.labelUnit : ''}${isCelsius ? '°C' : '°F'}</td>`
                tr += td
            }

            tr += '</tr>';
            tbodyHtml += tr;
        }

        return tbodyHtml;
    }

    var html = `
            <caption>Temperature Sensor (Celsius/Fahrenheit) Data</caption>
            <colgroup>
                <col style="width: 77px;">
                <col style="width: 77px;">
                
                ${makeCols()}
            </colgroup>
            
            <thead>
                <tr>
                    <th scope="col">Item</th>
                    <th scope="col">${getLanguageText(languageTable, "Time")}</th>
                   
                    ${makeLabels()}
                </tr>
            </thead>

            <tbody>
                ${makeTbody()}
            </tbody>
        `;

    $('#'+id+'-table').html(html);
}

function setCurTimeDash(locale) {
    getCurrentTime(function(msg) {
        currentTime = msg.current;
        var dateArr = currentTime.split(' ');
        var weekDay = myWeekDays[dateArr[0]];
        var year = `${dateArr[3]}${locale === 'en-US' ? '' : getLanguageText(languageTable, "year")}`;
        var month = `${myMonths[dateArr[1]]}`;
        var day = `${dateArr[2]}${locale === 'en-US' ? '' : getLanguageText(languageTable, "Days")}`;
        var timeArr = dateArr[4].split(':');

        var span = `<span>${year} ${month} ${day} <strong>${weekDay}</strong></span>`;

        $('#current-date').html(span);
        $('.hour').text(timeArr[0]);
        $('.min').text(timeArr[1]);
        $('.sec').text(timeArr[2]);
        $('#uptime').html(msg.uptime);
    });
}

function init() {

}

function drawAutoRefresh() {
    var addChar = '';

    if (env.locale === 'ar-SA') {
        addChar = "\u202A";
    }

    const displayName = {
        '0': addChar + getLanguageText(languageTable, "None"),
        '30': addChar + getLanguageText(languageTable, "30sec"),
        '300': addChar + getLanguageText(languageTable, "5Min"),
    };
    const valList = ['0', '30', '300'];

    const autoDropdown = $('#auto-refresh-dropdown');
    const dropdownMenu = $(autoDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(valList, displayName, 'auto-refresh-dropdown');

    const lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            var selected = valList[i];
            changeDropdownLabel("auto-refresh-dropdown", displayName[selected]);
            changeDropdownAria("auto-refresh-dropdown", i);
            var interval = parseInt(selected);
            if(interval === 0){
                clearInterval(refreshInterval);
            }else{
                clearInterval(refreshInterval);
                refreshInterval = setInterval(function(){
                    initInformation();
                }, interval * 1000);
            }
        });
    });

    $(dropdownMenu).html(ul);

    changeDropdownLabel('auto-refresh-dropdown', displayName[valList[0]]);
    changeDropdownAria('auto-refresh-dropdown', 0);
}

function getLog() {
    getCurrentTime(function(msg) {
        sepLogString = log.split('\n');
        sepLogString.splice(sepLogString.length-1, 1);
        sepLogString = sepLogString.reverse();

        var tableTxt = '';
        var max = 20;

        if( sepLogString.length < 20 ){
            max = sepLogString.length;
        }

        for(var i = 0; i < max; i++){
            tableTxt += '<tr>';
            tableTxt += '<td>' + sepLogString[i].replace(/Please, Set a time/g, "Please Set a time").split(',')[4] + '</td>'
            tableTxt += '<td>' + sepLogString[i].split(',')[0] + '</td>'
            tableTxt += '<td>' + sepLogString[i].split(',')[1] + '</td>'
            tableTxt += '<td>' + (sepLogString[i].split(',')[3].includes('NG') ? 'Not OK' : sepLogString[i].split(',')[3]) + '</td>'
            tableTxt += '</tr>';
        }

        $('#incidentTable > tbody').empty();
        $('#incidentTable > tbody:last').prepend(tableTxt);

        drawReflowIncident();
    });
}

function drawReflowIncident() {
    var reflowMax = 20;

    var tableBody = '';

    if (sepLogString.length < 20) {
        reflowMax = sepLogString.length;
    }

    for (var i=0; i < reflowMax; i++) {

        var tableRow = `<li class="table-row">
            <div class="summary-data">
                <div class="row">
                    <div class="table-cell center">${sepLogString[i].replace(/Please, Set a time/g, "Please Set a time").split(',')[4]}</div>
                    <div class="table-cell">
                        <button type="button" role="listbox" class="btn btn-expand"
                                aria-expanded="false">${sepLogString[i].split(',')[0]}</button>
                    </div>
                </div>
            </div>
            <div class="all-data-box">
                <ul class="all-data">
                    <li>
                        <span class="field-label">${getLanguageText(languageTable, "Event")}</span>
                        <span class="field-content">${sepLogString[i].split(',')[1]}</span>
                    </li>
                    <li>
                        <span class="field-label">${getLanguageText(languageTable, "State")}</span>
                        <span class="field-content">${(sepLogString[i].split(',')[3].includes('NG') ? 'Not OK' : sepLogString[i].split(',')[3])}</span>
                    </li>
                </ul>
            </div>
        </li>`;

        tableBody += tableRow;
    }

    $('#incidentTableReflow').html(tableBody);

    $('#incidentTableReflow .summary-data').on('click', function () {
        var parent = $(this).parent();
        var isExpanded = $(parent).hasClass('expand');
        if (isExpanded) {
            $(parent).removeClass('expand');
        } else {
            $(parent).addClass('expand');
        }
    });

}

function showLedStatusMessage() {
    console.log('here is message', unitStatus)
    // $('#normalUnit').html(unitStatus.ok);
    // $('#NGUnit').html(unitStatus.ng);
    // $('#offUnit').html(unitStatus.off);
}

function checkUnitOk(unitID) {
    if(env.supportLedFilm){
        getLedPowerState( (unitID-1), function(msg) {
            if (!msg.powerState) {
                unitStatus.off++;
                showLedStatusMessage();
                return;
            }
            getLedOverallStatus(unitID * 2, function(msg) {
                unitStatus[msg.isOverallOk ? 'ok' : 'ng']++;
                showLedStatusMessage();
            });
        });
    } else {
        getLedPowerStatus(unitID * 2, function(msg) {
            if (!msg.isPowerOk) {
                unitStatus.off++;
                showLedStatusMessage();
                return;
            }
            getLedOverallStatus(unitID * 2, function(msg) {
                unitStatus[msg.isOverallOk ? 'ok' : 'ng']++;
                showLedStatusMessage();
            });
        });
    }
}

function checkUnitStatusHandler() {
    unitStatus.ok = 0;
    unitStatus.ng = 0;
    unitStatus.off = 0;

    if(env.supportLedFilm){
        if( env.ledFilmType === 'color' ){
            getSystemSettings('commercial', ['ledLayout'], function(ret) {
                console.log("ledLayout Check : " + ret.ledLayout);
                if(ret.ledLayout.pixelPitch === undefined) { // for select pitch modal(14mm/24mm)
                    getP14Status(function (msg) {
                        var cpuRunStatus = msg.result && msg.result.cpuRunStatus ? msg.result.cpuRunStatus : false;
                        var initialPitch = 0;

                        if (cpuRunStatus) {
                            initialPitch = msg.result && msg.result.p14Status === true ? 14 : 24;
                        }

                        addConfirmModals('selectPitchModal-btn', 'selectPitchModal', getLanguageText(languageTable, 'Select LED Film Pitch'),
                            'input box here', false, function () {
                                var ledLayout = $("input[id=selectPitchModalVal]:checked").val();
                                console.log("Set LED Layout : " + ledLayout);
                                setSystemSettings('commercial', {'ledLayout' : {'pixelPitch' : ledLayout}});
                            });

                        $('#selectPitchModal-ok').attr('disabled', !cpuRunStatus);
                    });
                }
                if(ret.ledLayout.senderList !== undefined){
                    var senderList = ret.ledLayout.senderList;
                    for(var i=0; i<senderList.length; i++){
                        for(var j=0; j<senderList[i].unitList.length; j++){
                            var slave = parseInt(senderList[i].senderId) * 2
                            var unit = parseInt(senderList[i].unitList[j].unitId) - 1;

                            console.log('sender::'+slave+', unit::'+unit);
                            getReceiverStatus( slave, unit, function(status){
                                if(status.returnValue){
                                    if(!status.result.power){
                                        unitStatus.off++;
                                    }else if(!status.result.signal){
                                        unitStatus.ng++;
                                    }else{
                                        unitStatus.ok++;
                                    }
                                }
                                showLedStatusMessage();
                            });
                        }
                    }
                }
            });
        } else {
            getLedConfig(function(curConfig){
                for (var i = 0; i !== curConfig.unitList.length; ++i) {
                    checkUnitOk(i + 1);
                }
            });
        }
    } else {
        getLedConfig(function(curConfig) {
            for (var i = 0; i !== curConfig.unitList.length; ++i) {
                checkUnitOk(i + 1);
            }
        });
    }
}

function setBackground(card, isOK) {
    if (isOK) {
        card.removeClass('bg-red');
        card.addClass('bg-gray');
    } else {
        card.removeClass('bg-gray');
        card.addClass('bg-red');
    }
}