var pictureMode = undefined;
var currentTime = undefined;
var selectTime = {};
var wired = undefined;
var ipType = 4;

var pictureDisplayName = {
    vivid: getLanguageText(languageTable, 'Vivid'),
    normal: getLanguageText(languageTable, 'Standard'),
    eco: 'APS',
    cinema: getLanguageText(languageTable, 'Cinema'),
    sports: getLanguageText(languageTable, 'Sports'),
    game: getLanguageText(languageTable, 'Game'),
    expert1: getLanguageText(languageTable, 'Expert')
}
var pictureValList = ['vivid', 'normal', 'eco', 'cinema', 'sports', 'game', 'expert1'];

var savingDisplayName = {
    auto: getLanguageText(languageTable, 'Auto'),
    off: getLanguageText(languageTable, 'Off'),
    min: getLanguageText(languageTable, 'Minimum'),
    med: getLanguageText(languageTable, 'Medium'),
    max: getLanguageText(languageTable, 'Maximum'),
    screen_off: getLanguageText(languageTable, 'Screen Off'),
}
var savingValList = ['auto', 'off', 'min', 'med', 'max', 'screen_off'];

var gammaDisplayName = {
    medium: getLanguageText(languageTable, 'Medium'),
}
var gammaValList = ['medium'];

var blackDisplayName = {
    auto: getLanguageText(languageTable, 'Auto'),
    low: getLanguageText(languageTable, 'Low'),
    high: getLanguageText(languageTable, 'High'),
};
var blackValList = ['auto', 'low', 'high'];

var colorDisplayName = {
    '3200K': '3200K',
    '4000K': '4000K',
    '5200K': '5200K',
    '6500K': '6500K',
    '9300K': '9300K'
}
var colorValList = ['3200K', '4000K', '5200K', '6500K', '9300K'];

document.addEventListener("DOMContentLoaded", function() {
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/control_common.css">');
    // init UI change
    drawPictureModeSelect();
    drawSavingSelect();
    drawGammaSelect();
    drawBlackSelect();

    $('#brightnessScheduleAdd').on('click', function () {
        showMyModal('schedule-popup');
    });

    addConfirmModals('#pictureResetBtn', 'pictureResetModal', getLanguageText(languageTable, 'information message'),
        getLanguageText(languageTable, 'Are you sure to change Settings?'), false, function () {
            resetPictureDB(function (msg) {
                if (msg.returnValue) {
                    pictureCallback();
                    getPictureMode(function (msg) {
                        if (msg.pictureMode === 'expert1') {
                            setSystemSettings('commercial', {
                                ledColorTempDegree: '6500K'
                            });
                            refreshPictureDropDownVal();
                            checkChanged();
                        }
                    })
                }
            });
        });

    makeKeyDownDropdown('picture-mode-dropdown');
    makeKeyDownDropdown('saving-mode-dropdown');
    makeKeyDownDropdown('gamma-dropdown');
    makeKeyDownDropdown('colorTempSelect');
    makeKeyDownDropdown('black-level-dropdown');
    // displayLoading(false);
});

function drawPictureModeSelect() {
    const pictureDropdown = $('#picture-mode-dropdown');
    const dropdownMenu = $(pictureDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(pictureValList, pictureDisplayName, 'picture-mode-dropdown');

    const lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            var selected = pictureValList[i];
            changeDropdownLabel("picture-mode-dropdown", pictureDisplayName[selected]);
            changeDropdownAria("picture-mode-dropdown", i);
            setPictureMode(selected);
            getPictureModeVal();
            pictureCallback();
        });
    });

    $(dropdownMenu).html(ul);

    getPictureMode(function(msg) {
        var modified = msg.pictureSettingModified;
        var mode = msg.pictureMode;
        changeDropdownLabel("picture-mode-dropdown", pictureDisplayName[mode]);
        var myIndex = pictureValList.findIndex(function (item) {return item === mode});
        changeDropdownAria("picture-mode-dropdown", myIndex);
        $('#pictureUser').html(mode !== 'expert1' && modified[mode] ? '(' + getLanguageText(languageTable, 'User') + ')' : '');
    });
}

function getPictureModeVal() {
    getPictureMode(function(msg) {
        var modified = msg.pictureSettingModified;
        var mode = msg.pictureMode;
        $('#pictureUser').html(mode !== 'expert1' && modified[mode] ? '(' + getLanguageText(languageTable, 'User') + ')' : '');
        checkUiDisableAndDraw();
        refreshPictureDropDownVal();
    });
}

function refreshPictureDropDownVal() {
    var colorTemp;

    getPictureDBVal(['gamma', 'blackLevel'], function (msg) {
        var gamma = msg.gamma;
        var title = gammaDisplayName[gamma];
        changeDropdownLabel("gamma-dropdown", title ? title : gammaDisplayName['medium']);
        var myIndex = gammaValList.findIndex(function (item) {return item === gamma});
        changeDropdownAria("gamma-dropdown", myIndex >= 0 ? myIndex : 0);

        getSystemSettings('dimensionInfo', ['colorSystem'], function(msg2) {
            var selected = msg.blackLevel[msg2.colorSystem];
            changeDropdownLabel("black-level-dropdown", blackDisplayName[selected]);
            var myIndex2 = blackValList.findIndex(function (item) {return item === selected});
            changeDropdownAria("black-level-dropdown", myIndex2);
        });
    });

    getSystemSettings("commercial", ["ledColorTempDegree"], function(ret) {
        colorTemp = '6500K'
        if (ret !== null) {
            colorTemp = ret.ledColorTempDegree;
        }
        changeDropdownLabel("colorTempSelect", colorDisplayName[colorTemp]);
        var myIndex3 = colorValList.findIndex(function (item) {return item === colorTemp});
        changeDropdownAria("colorTempSelect", myIndex3);
    });
}

function pictureCallback() {
    refreshLed();
    getEnergySavingVal();
    getBrightnessMode();
    getPictureDBVal(['brightness', 'contrast', 'hSharpness', 'vSharpness', 'sharpness', 'color', 'tint'], function (msg) {
        for (var key in msg) {
            if (msg.hasOwnProperty(key)) {
                if(key === 'tint'){
                    if(msg[key] === 0){
                        $('#' + key + 'Val').html(msg[key]);
                    } else {
                        var tintVal = ''
                        msg[key] < 0 ? tintVal += 'R' : tintVal += 'G'
                        $('#' + key + 'Val').html(tintVal + Math.abs(msg[key]) );
                    }
                    var tintValue2 = msg[key];
                    tintValue2 += 50;
                    $('#' + key).val(tintValue2);
                } else if (key === 'sharpness' || key === 'hSharpness' || key === 'vSharpness') {
                    $('#' + key).val(msg[key] * 2);
                    $('#' + key + 'Val').html(msg[key]);
                } else {
                    $('#' + key).val(msg[key]);
                    $('#' + key + 'Val').html(msg[key]);
                }
            }
        }
        checkRangeUI('brightness', 0, 100);
        checkRangeUI('contrast', 0, 100);
        checkRangUISharpness();
        checkRangeUI('color', 0, 100);
        checkRangeUI('tint', 0, 100);
        refreshPictureDropDownVal();
        checkChanged();
    });
}

function checkUiDisableAndDraw() {
    drawPictureModeSelect();
    drawSavingSelect();
    drawGammaSelect();
    drawColorTempSelect();
    drawBlackSelect();
}

function drawSavingSelect() {
    const savingDropdown = $('#saving-mode-dropdown');
    const dropdownMenu = $(savingDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(savingValList, savingDisplayName, 'saving-mode-dropdown');

    const lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            var selected = savingValList[i];
            changeDropdownLabel("saving-mode-dropdown", savingDisplayName[selected]);
            changeDropdownAria("saving-mode-dropdown", i);
            setSystemSettings('picture', {energySavingModified: true, energySaving: selected}, function(ret) {
                getPictureDBVal(['contrast'], function(msg) {
                   $('#contrast').val(Number(msg.contrast));
                    checkRangeUI('contrast', 0, 100);
                   if (selected !== 'off') {
                       disableSlide('contrast-container');
                   } else {
                       enableSlide('contrast-container');
                   }
                   var span = $('#contrast-container').find('.slide-label');
                   $(span).text(Number(msg.contrast));
                });
            });
        });
    });

    $(dropdownMenu).html(ul);
}

function getEnergySavingVal() {
    getSystemSettings('commercial', ['easyBrightnessMode'], function (ret) {
        getPictureDBVal(['energySaving'], function(msg) {
            var option = msg.energySaving;
            changeDropdownLabel("saving-mode-dropdown", savingDisplayName[option]);
            var myIndex = savingValList.findIndex(function (item) {return item === option});
            changeDropdownAria("saving-mode-dropdown", myIndex);
            if (ret.easyBrightnessMode === 'on' || option !== 'off') {
                disableSlide('contrast-container');
            } else {
                enableSlide('contrast-container');
            }
            // $('#contrast').attr('disabled', (ret.easyBrightnessMode === 'on' || option != 'off'));
        });
    });
}

function gammaSelected(pictureMode, selected) {
    var setting = {};
    setting['gamma'] = selected;
    setting.pictureSettingModified = {};
    setting.pictureSettingModified[pictureMode] = true;
    setPictureDBVal(setting, name);
    refreshPictureDropDownVal();
    checkChanged();
}

function drawGammaSelect() {
    var isDisable = false;
    getPictureMode(function(msg) {
        myDropdownBinding('gamma-dropdown');
        if (msg.pictureMode === 'vivid') {
            isDisable = true;
            gammaDisplayName = {
                medium: getLanguageText(languageTable, 'Medium'),
            }
            gammaValList = ['medium'];
            myDropdownUnBinding('gamma-dropdown');
        } else if (msg.pictureMode === 'expert1' || msg.pictureMode === 'cinema') {
            gammaValList = ['low', 'medium', 'high1', 'high2'];
            gammaDisplayName = {
                low: '1.9',
                medium: '2.2',
                high1: '2.4',
                high2: 'BT.1886'
            }
            isDisable = false;
        } else {
            gammaValList = ['low', 'medium', 'high1', 'high2'];
            gammaDisplayName = {
                low: getLanguageText(languageTable, 'Low'),
                medium: getLanguageText(languageTable, 'Medium'),
                high1: getLanguageText(languageTable, 'High1'),
                high2: getLanguageText(languageTable, 'High2')
            }
            isDisable = false;
        }

        var gammaDropdown = $('#gamma-dropdown');
        var dropdownMenu = $(gammaDropdown).children(".dropdown-menu");

        var ul = createDropdownChildren(gammaValList, gammaDisplayName, 'gamma-dropdown');

        const lis = $(ul).children('li');
        lis.each(function (i) {
            var selected = gammaValList[i];
            $(this).on('click', function () {
                changeDropdownLabel("gamma-dropdown", gammaDisplayName[selected]);
                changeDropdownAria("gamma-dropdown", i);
                gammaSelected(msg.pictureMode, selected);
            });
        });

        $(dropdownMenu).html(ul);
    });
}

function drawColorTempSelect() {
    var colorDropdown = $('#colorTempSelect');
    var dropdownMenu = $(colorDropdown).children(".dropdown-menu");

    getPictureMode(function(msg) {
        if (msg.pictureMode !== 'expert1') {
            var nonUl = createDropdownChildren([], {});
            $(dropdownMenu).html(nonUl);
            disableDropButton('colorTempSelect', true);
        } else {
            var ul = createDropdownChildren(colorValList, colorDisplayName, 'colorTempSelect');

            var lis = $(ul).children('li');
            lis.each(function (i) {
                $(this).on('click', function () {
                    changeDropdownLabel("colorTempSelect", colorDisplayName[colorValList[i]]);
                    changeDropdownAria("colorTempSelect", i);
                    var setting = {};
                    setting.pictureSettingModified = {};
                    setting.pictureSettingModified[msg.pictureMode] = true;
                    setPictureDBVal(setting, name);
                    setSystemSettings('commercial', {
                        ledColorTempDegree: selected
                    });
                    refreshPictureDropDownVal();
                    checkChanged();
                });
            });
        }
    });
}

function drawBlackSelect() {
    getSystemSettingDesc('picture', ['blackLevel'], function(msg) {
        var active = msg.results[0].ui.active;
        var visible = msg.results[0].ui.visible;

        blackValList = [];
        blackDisplayName = {};
        msg.results[0].values.arrayExt.forEach(function(mode){
            if(mode.active && mode.visible){
                var name = mode.value[0].toUpperCase() + mode.value.slice(1, mode.value.length);
                blackValList.push(mode.value);
                // blackDisplayName.push(name);
                blackDisplayName[mode.value] = getLanguageText(languageTable, name);
            }
        });

        var blackDropdown = $('#black-level-dropdown');
        var dropdownMenu = $(blackDropdown).children(".dropdown-menu");

        if (active && visible) {

            var ul = createDropdownChildren(blackValList, blackDisplayName, 'black-level-dropdown');

            var lis = $(ul).children('li');
            lis.each(function (i) {
                $(this).on('click', function () {
                    var selected = blackValList[i];
                    changeDropdownLabel("black-level-dropdown", blackDisplayName[selected]);
                    changeDropdownAria("black-level-dropdown", i);
                    getPictureMode(function(msg) {
                        getSystemSettings('dimensionInfo', ['colorSystem'], function(msg2) {
                            var setting = {};
                            var selectedObject = {};
                            selectedObject[msg2.colorSystem] = selected;
                            setting['blackLevel'] = selectedObject;
                            setting.pictureSettingModified = {};
                            setting.pictureSettingModified[msg.pictureMode] = true;
                            setPictureDBVal(setting, name);
                            refreshPictureDropDownVal();
                            checkChanged();
                        });
                    });
                });
            });

            $(dropdownMenu).html(ul);
            setTimeout(function () {
                myDropdownBinding("black-level-dropdown");
            },500);

        } else if (!active && visible) {
            var nonUl = createDropdownChildren([], {});
            $(dropdownMenu).html(nonUl);
            changeDropdownLabel("black-level-dropdown", " ");
            setTimeout(function () {
                myDropdownUnBinding('black-level-dropdown');
            },500);

        } else {
            console.log('LOG: Invisible');
        }

    });
}

function checkChanged() {
    getPictureMode(function(msg) {
        var modified = msg.pictureSettingModified;
        var mode = msg.pictureMode;
        $('#pictureUser').html(mode !== 'expert1' && modified[mode] ? '(' + getLanguageText(languageTable, 'User') + ')' : '');
        $('#pictureResetBtn').prop('disabled', !modified[mode]);
    });
}

function pictureResetButton() {
    $('#pictureResetBtn-ok').on('click', function () {
        resetPictureDB(function (msg) {
            if (msg.returnValue) {
                pictureCallback();
                getPictureMode(function (msg) {
                    if (msg.pictureMode === 'expert1') {
                        setSystemSettings('commercial', {
                            ledColorTempDegree: '6500K'
                        });
                        refreshPictureDropDownVal();
                        checkChanged();
                    }
                });
            }
        });
    });
}

function setUpPictureSlider(name, start, end) {
    checkRangeUI(name, start, end);

    var browserType = '';

    if ( agent.indexOf("webkit") !== -1 ) {
        browserType = "webkit";
    } else if ( agent.indexOf("firefox") !== -1 ) {
        browserType = "firefox";
    } else{
        browserType = "msie";
    }

    if( browserType === "msie" ) {
        $('#' + name).mouseup(function() {
            var val = $(this).val();
            var myName = $(this).attr('name');
            if (myName === 'tint') {
                val -= 50;
            } else if (myName === 'sharpness' || myName === 'hSharpness' || myName === 'vSharpness') {
                val /= 2;
            }
            var setting = {};
            setting[name] = Number(val);
            getPictureMode(function(msg) {
                setting.pictureSettingModified = {};
                setting.pictureSettingModified[msg.pictureMode] = true;
                setPictureDBVal(setting, name);
                pictureCallback();
            });
        }).on('change', function () {
            checkRangeUI(name, start, end);
            if (name === 'tint') {
                var tintValue = $("#" + name).val();
                tintValue -= 50;

                if (tintValue !== 0) {
                    tintValue = (tintValue < 0 ? 'R' : 'G') + Math.abs(tintValue);
                }

                $("#" + name + "Val").text(tintValue);
            } else if (name === 'sharpness' || name === 'hSharpness' || name === 'vSharpness') {
                var myVal = $("#" + name).val() / 2;
                $("#" + name + "Val").text(myVal);
            } else {
                $("#" + name + "Val").text($("#" + name).val());
            }
        });
    } else {
        $('#' + name).on('change', function () {
            var val = $(this).val();
            var myName = $(this).attr('name');
            if (myName === 'tint') {
                val -= 50;
            } else if (myName === 'sharpness' || myName === 'hSharpness' || myName === 'vSharpness') {
                val /= 2;
            }
            var setting = {};
            setting[name] = Number(val);
            getPictureMode(function(msg) {
                setting.pictureSettingModified = {};
                setting.pictureSettingModified[msg.pictureMode] = true;
                setPictureDBVal(setting, name);
                pictureCallback();
            });
        }).on('input', function () {
            if (name === 'tint') {
                var tintValue = $("#" + name).val();
                tintValue -= 50;

                if (tintValue !== 0) {
                    tintValue = (tintValue < 0 ? 'R' : 'G') + Math.abs(tintValue);
                }

                $("#" + name + "Val").text(tintValue);
            } else if (name === 'sharpness' || name === 'hSharpness' || name === 'vSharpness') {
                var myVal = $("#" + name).val() / 2;
                $("#" + name + "Val").text(myVal);
            } else {
                $("#" + name + "Val").text($("#" + name).val());
            }
            checkRangeUI(name, start, end);
        });
    }
}

function setUpPicturePanel() {
    setUpPictureSlider('brightness', 0, 100);
    setUpPictureSlider('contrast', 0, 100);
    setUpPictureSlider('hSharpness', 0, 100);
    setUpPictureSlider('vSharpness', 0, 100);
    setUpPictureSlider('sharpness', 0, 100);
    setUpPictureSlider('color', 0, 100);
    setUpPictureSlider('tint', 0, 100);

    getPictureModeVal();
    getEnergySavingVal();
    pictureCallback();
    pictureResetButton();
}