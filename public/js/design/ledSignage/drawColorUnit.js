var statusScale = 1920 / 702;
var currControlUnit = undefined;

function drawColorUnit(config){
    $('#statusCanvasWrapper').css('position', 'relative');
    $('#statusCanvasWrapper').css('overflow', 'hidden');

    var layout = config.ledLayout;

    $('.statusUnit').remove();
    if( layout.senderList !== undefined ){
        layout.senderList.forEach(function(sender, senderNo, senderArr){
            var senderX = sender.x;
            var senderY = sender.y;
            sender.unitList.forEach(function(unit, unitNo, unitArr){
                var slave = parseInt(sender.senderId) * 2;
                var unitIdx = parseInt(unit.unitId) - 1;

                getReceiverStatus( slave, unitIdx, function(ret){
                    var style = 'width:'+ (unit.width / statusScale)+';';
                    style += ' height:'+ (unit.height / statusScale)+';';
                    style += ' left:'+ ( (senderX + unit.x) / statusScale)+';';
                    style += ' top:'+ ( (senderY + unit.y) / statusScale)+';';
                    var unitTxt = '<div class="statusUnit';
                    if(!ret.result.power){
                        unitTxt += ' powerOff';
                    }else if(!ret.result.signal){
                        unitTxt += ' noSignal';
                    }
                    unitTxt += '" name="'+sender.senderId + ':' + unit.unitId+'" style="'+style+'"></div>'
                    $('#statusCanvasWrapper').append(unitTxt);
                    if( unitNo === unitArr.length-1 ){
                        addUnitHdl();
                    }
                });
            });
        });
    }
}

function addUnitHdl(){
    $('.statusUnit').unbind('click')
    $('.statusUnit').click(function(){
        currControlUnit = $(this).attr('name');

        var senderIdx = parseInt(currControlUnit.split(':')[0]) - 1;
        var slave = ( parseInt(currControlUnit.split(':')[0]) * 2 );
        var unitIdx = parseInt(currControlUnit.split(':')[1]) - 1 ;

        $('.statusUnit').css('border', '1px solid black');
        $(this).css('border', '1px solid #cf0652');
        $('#controlPanel').hide();
        $('#detailPanel').show();

        getTempHumidityColor(senderIdx,unitIdx, function(ret){
            var temperature = parseInt(ret.result.temp);
            $('#unitTemp').html( temperature == -128 ? Locale.getText('NG') : temperature );
        });

        getReceiverStatus( slave, unitIdx, function(ret){
            if(ret.returnValue){
                $('#unitPower').html(ret.result.power ? Locale.getText('OK') : Locale.getText('NG') );
                $('#unitSignal').html(ret.result.signal ? Locale.getText('OK') : Locale.getText('NG') );
            }else{
                if(ret.result != undefined){
                    $('#unitPower').html(ret.result.power ? Locale.getText('OK') : Locale.getText('NG') );
                    $('#unitSignal').html(ret.result.signal ? Locale.getText('OK') : Locale.getText('NG') );
                }else{
                    $('#unitPower').html( Locale.getText('NG') );
                    $('#unitSignal').html( Locale.getText('NG') );
                }
            }
        });
    });

    if( currPath[1] == 'calibration' ){

        if( $('.statusUnit').length > 0 ){
            var name = $('.statusUnit').attr('name');
            var slave  = parseInt( name.split(':')[0] ) * 2;
            var unitIdx  = parseInt( name.split(':')[1] ) - 1;

            getReceiverStatus( slave, unitIdx, function(ret){
                var status = ret.result;

                if( status.power && status.signal ){
                    $('.statusUnit')[0].click();
                    setTimeout(function(){
                        $('#moduleCalBtn').click();
                    }, 300);
                }
            });
        }
    }
}