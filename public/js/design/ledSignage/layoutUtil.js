function unitSelectHandler(id, curUnit) {
    if(env.supportLedFilm){
        var width = curUnit.w / 16;
        var height = curUnit.h / 16;
        var tableStr = '';
        var moduleNum = 1;

        for(var i=0; i<height; i++){
            tableStr += '<tr>';
            for(var j=0; j<width; j++){
                tableStr +='<td>'+ getLanguageText(languageTable, "Modules") + moduleNum + '</td>';
                moduleNum++;
            }
            tableStr += '</tr>';
        }

        $('#moduleDetailTable').html(tableStr);

        getLedPowerState( (id-1), function(msg) {
            if(msg.powerState){
                $('#unitPower').html('OK');
                getLedSignalState( (id-1), function(msg){
                    $('#unitSignal').html(msg.signalState ? 'OK' : 'Not OK');
                });

                getLedFilmTempHumidity( (id-1), function(msg){
                    readTempType(function(tempType){
                        isCelsius = tempType.isCelsius;
                        if( isCelsius == 'true' ){
                            $('#unitTemp').html(Math.floor(msg.temperature) + '°C');
                        }else{
                            $('#unitTemp').html(Math.floor(msg.temperature*1.8+32) + '°F');
                        }
                    });
                });
                $('#unitCalBtn').prop('disabled', false);
                $('#moduleCalBtn').prop('disabled', false);
            }else{
                $('#unitPower').html('Not OK');
                $('#unitSignal').html('-');
                $('#unitTemp').html('-');
                $('#unitCalBtn').prop('disabled', true);
                $('#moduleCalBtn').prop('disabled', true);
            }
        });
    }else{
        getLedPowerStatus(id * 2, function(msg) {
            $('#unitPower').html(msg.isPowerOk ? 'OK' : 'Not OK');

            $('#moduleCalBtn').prop('disabled', !msg.isPowerOk);

            if (!msg.isPowerOk) {
                $('#unitTemp').html('-');

                $('#unitSignal').html('-');

                for (var i = 1; i != 5; ++i) {
                    $('#moduleTemp' + i).html('-');
                }

                for (var i = 0; i != 4; ++i) {
                    $('#moduleCurrent' + (i + 1)).html('-');
                }

                return;
            }

            getLedTemperature(id * 2, function(msg) {
                $('#unitTemp').html(msg.temperature[0] + '°C');
                for (var i = 1; i != 5; ++i) {
                    $('#moduleTemp' + i).html(msg.temperature[i] + '°C');
                }
            });

            getLedCurrent(id * 2, function(msg) {
                for (var i = 0; i != 4; ++i) {
                    $('#moduleCurrent' + (i + 1)).html(msg.current[i] + 'mA');
                }
            });

            getLedSignalStatus(id * 2, function(msg) {
                $('#unitSignal').html(msg.isSignalOk ? 'OK' : 'Not OK');
            });
        });
    }

}

function createCanvas(){}
function changeCanvas(){}

function drawUnitSelect() {

    var id = [];
    var val = [];
    mainCanvas.rectList.forEach(function(rect) {
        id.push(rect.id);
        val.push(rect.id);
    });

    /*$('#unitSelect').html('');
    drawDropDownBox('unitSelect', id, val, 'dropboxType3', function (selected) {
        curUnit = mainCanvas.rectList[selected - 1];
        refreshLed();
        unitSelectHandler(Number(selected), curUnit);
    }, 1);*/
}