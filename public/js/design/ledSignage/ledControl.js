var defaultControlItem = 'picture';

var controlValList = ['picture', 'external', 'controller', 'network', 'time'];
const cssMap = {
    picture: '/assets/css/control_led_w3/control_led_w3_video.css',
    controller: '/assets/css/control_led_w3/control_led_w3_virtual_controller.css',
}
const itemDisplayName = {
    picture: getLanguageText(languageTable, 'Picture'),
    external: getLanguageText(languageTable, 'Input'),
    controller: getLanguageText(languageTable, 'Virtual Controller'),
    network: getLanguageText(languageTable, 'Network'),
    time: getLanguageText(languageTable, 'Time'),
    scaler: getLanguageText(languageTable, 'Video Scaler'),
    input: getLanguageText(languageTable, 'Input')
};

document.addEventListener("DOMContentLoaded", function() {
    // init UI change
    const head = $('head');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/control_led_w3_control.css">');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/control_led_w3_unit.css">');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/popup_module_calibration.css">');
    // head.append('<link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">');

    let mem = localStorage.getItem('ledControlMenu');
    if (!mem) {
        head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/control_led_w3_video.css">');
    } else {
        switch (mem) {
            case 'picture':
                head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/control_led_w3_video.css">');
                break;
            case 'controller':
                head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/control_led_w3_virtual_controller.css">');
                break;
            case 'network':
                head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/control_led_w3_network.css">');
                break;
            case 'time':
                head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/control_led_w3_time.css">');
                break;
            default:
                head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w3/control_led_w3_video.css">');
                break;
        }
    }

    createCurrentMenu(1, "ledControl");

    drawControlItemSelect();

    if (!mem) {
        mem = 'picture';
    }

    // $('.content-middle-right').addClass("control-video");
    const menuIndex = controlValList.findIndex(function (item) {return item === mem});
    const ariaId = 'option-control-' + controlValList[menuIndex];
    changeDropdownLabel("control-item-dropdown", itemDisplayName[mem], ariaId);
    changeDropdownAria("control-item-dropdown", menuIndex);

    changeItemDisplay(menuIndex, mem);

    addMyClass(mem);

    $('#btn-edit-layout').on('click', function () {
        showMyModal('layoutEditorModal');
    });

    $('#network-switch').on('change', function () {
        const isChecked = $(this).prop('checked');
        $(this).attr('aria-checked', isChecked);
    });

    var external = $('[name=external-checkbox-type]');
    external.on('click', function () {
        var isChecked = $(this).prop("checked");
        $(this).attr("checked", isChecked);
    });

    if (mem === 'picture') {
        getPictureModeVal();
        pictureCallback();
    }

    addAlertModals('network-warning-modal', getLanguageText(languageTable, "Success"));
    addAlertModals('time-saved-modal', getLanguageText(languageTable, "Success"));

    $('#moduleCalibOK').on('click', function () {
        _closePopup('color-calibration-modal');
    });

    $('#moduleCalibCancel').on('click', function () {
        _closePopup('color-calibration-modal');
    });

    makeKeyDownDropdown('control-item-dropdown');
    makeKeyDownDropdown('type-dropdown');
    makeKeyDownDropdown('videoScalerSelect');
    makeKeyDownDropdown('canvasSelect');

    displayLoading(false);

    setInterval(function () {
        sendRemocon('left');
    },30000);

});

function init() {

}

function drawControlItemSelect() {
    // if (env.supportLedFilm) {
    //     if (env.numOfExtInputs > 1) {
    //         controlValList.unshift('input')
    //     }
    // } else {
    //     controlValList.push('scaler');
    // }

    const controlDropdown = $('#control-item-dropdown');
    const dropdownMenu = $(controlDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(controlValList, itemDisplayName, 'control-item-dropdown');

    const lis = $(ul).children('li');
    for (let i = 0; i < controlValList.length; i++) {
        $(lis[i]).on('click', function () {
            const item = controlValList[i];
            // changeDropdownLabel("control-item-dropdown", itemDisplayName[item]);
            // changeDropdownAria("control-item-dropdown", i);

            // changeItemDisplay(i, item);
            localStorage.setItem('ledControlMenu', item);
            window.location.reload();
        });
    }

    $(dropdownMenu).html(ul);
}

function setupInputList() {
    getInputList(function(msg) {
        for (var i = 0; i < msg.length; i++) {
            var itsId = msg[i].id;
            var checkbox = `<div class="checkbox">
                    <input type="radio" name="external-radio-type" id="${itsId}" value="${itsId}">
                    <label for="${itsId}">${msg[i].name}</label>
            </div>`;
            $(checkbox).appendTo('#ctrl-external');
        }

        var ctrlExternal = $('[name=external-radio-type]');
        ctrlExternal.on('click', function () {
            var thisVal = $(this).val();
            setHdmiInput(thisVal);

            ctrlExternal.each(function (j, elem) {
                var elemVal = $(elem).val();
                if (thisVal === elemVal) {
                    $(this).attr("checked", true);
                } else {
                    $(elem).attr("checked", false);
                }
            });
        });

        getHdmiInput(function (msg) {
            $("input:radio[name=external-radio-type][value='" + msg + "']").attr("checked", true);
            $("input:radio[name=external-radio-type][value='" + msg + "']").prop("checked", "checked");
        });
    });
}

function changeItemDisplay(index, item) {
    const dp = $('.ctrl-info');
    const bmList = $('#ctrl-bottom-list');
    const bmNtp = $('#ctrl-bottom-ntp');
    const reset = $('.btn-ctrl-reset');
    const network = $('#btn-network-save');
    const save = $('.btn-ctrl-save');
    const pc = $('.btn-pc-save');


    for (let i = 0; i < dp.length; i++) {
        if (i === index) {
            // mountCSS(item);
            $(dp[i]).css('display', item === 'controller' ? 'flex' : 'block');

        } else {
            $(dp[i]).css('display', 'none');
            // unmountCSS(item);
        }
    }

    switch (index) {
        case 0:
            $(reset).css('display', 'block');
            $(network).css('display', 'none');
            $(save).css('display', 'none');
            $(pc).css('display', 'none');
            $(bmList).show();
            $(bmNtp).hide();
            break;
        case 1:
            $(reset).css('display', 'none');
            $(network).css('display', 'none');
            $(save).css('display', 'none');
            $(pc).css('display', 'none');
            $(bmList).hide();
            $(bmNtp).hide();
            break;
        case 2:
            $(reset).css('display', 'none');
            $(network).css('display', 'none');
            $(save).css('display', 'none');
            $(pc).css('display', 'none');
            $(bmList).hide();
            $(bmNtp).hide();
            break;
        case 3:
            $(reset).css('display', 'none');
            $(network).css('display', 'flex');
            $(save).css('display', 'none');
            $(network).children('.btn-ctrl-save').css('display', 'block');
            $(pc).css('display', 'none');
            $(bmList).hide();
            $(bmNtp).hide();
            break;
        case 4:
            $(reset).css('display', 'none');
            $(network).css('display', 'none');
            $(save).css('display', 'block');
            $(pc).css('display', 'block');
            $(bmList).hide();
            $(bmNtp).show();
            break;
    }
    if (index === 0) {
        $(reset).css('display', 'block');
    } else {
        $(reset).css('display', 'none');
    }
}

function addMyClass(item) {
    const md = $('.content-middle-right')[0];

    for (let i = 0; i < controlValList.length; i++) {
        $(md).removeClass(`control-${i === 0 ? 'picture' : controlValList[i]}`);
    }

    $(md).addClass(`control-${item === 'picture' ? 'video' : item}`);
}

/**********************
 * Picture Mode
 */


function checkRangUISharpness() {
    getPictureMode(function(msg) {
        if (msg.pictureMode === 'expert1') {
            $('#sharpness-container').hide();
            $('#hSharpness-container').show();
            $('#vSharpness-container').show();
            checkRangeUI('hSharpness', 0, 100);
            checkRangeUI('vSharpness', 0, 100);
        } else {
            $('#sharpness-container').show();
            $('#hSharpness-container').hide();
            $('#vSharpness-container').hide();
            checkRangeUI('sharpness', 0, 100);
        }
    });
}

/**********************
 * network
 */
function checkIPAddress(ip) {
    var numbers = ip.split(".");

    if (numbers.length !== 4) {
        return false;
    }

    for (var i = 0; i !== 4; ++i) {
        if( (numbers[i].length>1) && (numbers[i][0] === 0) ){
            return false;
        }

        if (!$.isNumeric(numbers[i])) {
            return false;
        }
        var n = parseInt(numbers[i]);

        if (n < 0 || n > 255) {
            return false;
        }
    }

    return true;
}

function checkAddressValid(userData){
    var checkResult = {};
    checkResult.valid = true;
    checkResult.message = '';

    if(userData.dhcp){
        return checkResult;
    }

    if(!checkIPAddress(userData.ip)){
        checkResult.valid = false;
        checkResult.message += '<br>IP address';
    }

    if(!checkIPAddress(userData.netMask)){
        checkResult.valid = false;
        checkResult.message += '<br>Subnet Mask';
    }

    if(!checkIPAddress(userData.gateway)){
        checkResult.valid = false;
        checkResult.message += '<br>Gateway';
    }

    if(!checkIPAddress(userData.dns)){
        checkResult.valid = false;
        checkResult.message += '<br>DNS Server';
    }

    return checkResult;
}


function drawIpTypeSelect(wired) {
    var ipTypeList = [4];
    var ipTypeMap = new Map();
    ipTypeMap.set(4, 'IPv4');

    if( wired.ipv6 !== undefined ){
        ipTypeList.push(6);
        ipTypeMap.set(6, 'IPv6');
    }

    const typeDropdown = $('#type-dropdown');
    const dropdownMenu = $(typeDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(ipTypeList, ipTypeMap, 'type-dropdown');

    const lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            var selected = ipTypeList[i];
            changeDropdownLabel("type-dropdown", ipTypeMap.get(selected));
            changeDropdownAria("type-dropdown", i);
            if (selected === 4) {
                enableTextField(true);
                getNetworkStatus(function (ret) {
                    var wired = ret.wired;
                    $('#network-switch').prop('checked', wired.method === "dhcp");
                    $('#network-switch').trigger('change');
                    $('#network-mac').val(wired.macAddress);
                    $('#network-ip').val(wired.ipAddress);
                    $('.subnetMaskTxt').html(getLanguageText(languageTable, "Subnet Mask"));
                    $('#network-subnet').val(wired.netmask);
                    $('#network-gateway').val(wired.gateway);
                    $('#network-dns').val(wired.dns1);
                });
            } else {
                enableTextField(false);
                getNetworkStatus(function (ret) {
                    var wired = ret.wired;
                    var ipv6 = wired.ipv6;
                    if (ipv6 === undefined) {
                        return;
                    }
                    $('#network-mac').val(wired.macAddress);
                    $('#network-ip').val(ipv6.ipAddress);
                    $('.subnetMaskTxt').html(getLanguageText(languageTable, 'Subnet prefix length'));
                    $('#network-subnet').val(ipv6.prefixLenth);
                    $('#network-gateway').val(ipv6.gateway);
                    $('#network-dns').val(getDnsInfo(wired));

                    $('#network-ip').attr('disabled', true);
                    $('#network-subnet').attr('disabled', true);
                    $('#network-gateway').attr('disabled', true);
                    $('#network-dns').attr('disabled', true);
                });
            }
        });
    });

    $(dropdownMenu).html(ul);

    changeDropdownLabel('type-dropdown', ipTypeMap.get(4));
    changeDropdownAria('type-dropdown', 0);
}

function enableTextField(on) {
    $('#network-ip').attr('disabled', !on);
    $('#network-subnet').attr('disabled', !on);
    $('#network-gateway').attr('disabled', !on);
    $('#network-dns').attr('disabled', !on);
}

function getDnsInfo(data) {
    if (!data) {
        return "";
    }
    var dns = "";
    var repeat = true;
    var i = 1;
    while (repeat) {
        var key = "dns" + i;
        if (data[key]) {
            if (data[key].indexOf(":") >= 0) {
                dns = data[key];
                repeat = false;
            }
        } else {
            repeat = false;
        }
        i++;
    }
    return dns;
}

function setUpNetworkPanel() {
    getNetworkStatus(function (ret) {
        wired = ret.wired;
        originDns = wired.dns1;
        drawIpTypeSelect(wired);

        $('#network-switch').prop('checked', wired.method === "dhcp");
        $('#network-switch').trigger('change');
        $('#network-mac').val(wired.macAddress);
        $('#network-ip').val(wired.ipAddress);
        $('#network-subnet').val(wired.netmask);
        $('#network-gateway').val(wired.gateway);
        $('#network-dns').val(wired.dns1);
    });

    $('network-switch').on('change', function () {
        var checked = $(this).prop('checked');

        $('#network-ip').attr('disabled', checked);
        $('#network-subnet').attr('disabled', checked);
        $('#network-gateway').attr('disabled', checked);
        $('#network-dns').attr('disabled', checked);

        if (!checked) {
            $('#network-mac').val(wired.macAddress);
            $('#network-ip').val(wired.ipAddress);
            $('#network-subnet').val(wired.netmask);
            $('#network-gateway').val(wired.gateway);
            $('#network-dns').val(wired.dns1);
        } else {
            $('#network-ip').val('');
            $('#network-subnet').val('');
            $('#network-gateway').val('');
            $('#network-dns').val('');
        }
    });

    $('#btn-network-save').on('click', function () {
        var userData = {};
        userData.dhcp = $('#network-switch').prop('checked');
        userData.ip = $('#network-ip').val();
        userData.netMask = $('#network-subnet').val();
        userData.gateway = $('#network-gateway').val();
        userData.dns = $('#network-dns').val();

        var checkResult = checkAddressValid(userData);
        if(checkResult.valid){
            setNetworkStatus(userData.dhcp, userData.ip, userData.netMask, userData.gateway, undefined, function(retNetwork){
                if(retNetwork.returnValue){
                    setNetworkDNS(userData.dhcp ? originDns : userData.dns, undefined, function(retDNS){
                        if(retDNS.returnValue){
                            var myText = `${getLanguageText(languageTable, "Success")}<br>${checkResult.message}`;
                            showMyModal('network-warning-modal', myText);
                        }
                    });
                }
            });
        } else {
            var myInvalid = `${getLanguageText(languageTable, "Invalid value")}<br>${checkResult.message}`;
            showMyModal('network-warning-modal', myInvalid);
            $('#network-ip').val(wired.ipAddress);
            $('#network-subnet').val(wired.netmask);
            $('#network-gateway').val(wired.gateway);
            $('#network-dns').val(wired.dns1);
        }
    });
}

/**********************
 * time
 */


/**********************
 * Video Scaler
 */


function drawVideoScalerSelect() {
    var displayName = {
        on: 'On',
        off: 'Off'
    }
    var valList = ['on', 'off'];

    const videoDropdown = $('#videoScalerSelect');
    const dropdownMenu = $(videoDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(valList, displayName, 'videoScalerSelect');
    const lis = $(ul).children('li');
    lis.each(function (i) {
        var selected = valList[i];
        changeDropdownLabel("videoScalerSelect", displayName[selected]);
        changeDropdownAria("videoScalerSelect", i);
    });

    $(dropdownMenu).html(ul);

    changeDropdownLabel("videoScalerSelect", displayName[valList[0]]);
    changeDropdownAria("videoScalerSelect", 0);
}

function drawCanvasSelect() {
    var displayName = {
        create: 'Create New',
    }
    var valList = ['create'];

    const canvasDropdown = $('#canvasSelect');
    const dropdownMenu = $(canvasDropdown).children(".dropdown-menu");

    const ul = createDropdownChildren(valList, displayName, 'canvasSelect');
    const lis = $(ul).children('li');
    lis.each(function (i) {
        var selected = valList[i];
        changeDropdownLabel("canvasSelect", displayName[selected]);
        changeDropdownAria("canvasSelect", i);
        if (selected === 'create') {
            createCanvas();
        } else {
            changeCanvas();
        }
    });

    $(dropdownMenu).html(ul);

    changeDropdownLabel("canvasSelect", displayName[valList[0]]);
    changeDropdownAria("canvasSelect", 0);
}

function setUpScalerPanel() {
    drawVideoScalerSelect();
    drawCanvasSelect();
}



