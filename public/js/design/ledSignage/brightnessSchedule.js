var CUR_BRIGHTNESS_STATUS = false;
var CUR_BRIGHTNESS_SCHEDULE = 0;
var MAX_BRIGHTNESS_SCHEDULE = 6;
var scheduleList;
var brightnessNewSchedule = {
    hour: 12,
    minute: 0,
    backlight: 70	// use 'backlight' instead of 'brightness' in this function
};

function getBrightnessMode() {
    getSystemSettings('commercial', ['easyBrightnessMode'], function (ret) {
        if (ret.easyBrightnessMode === 'on') {
            CUR_BRIGHTNESS_STATUS = true;
            $('#brightnessScheduleEnable').prop('checked', true);
            $('#brightnessScheduleEnable').attr('aria-checked', true);
        } else {
            CUR_BRIGHTNESS_STATUS = false;
            $('#brightnessScheduleEnable').prop('checked', false);
            $('#brightnessScheduleEnable').attr('aria-checked', false);
        }
        setUpButtons();
    });
}

function initBrightnessSchedule() {
    const myContainer = document.getElementById('schedule-popup');
    const myFocusTrap = focusTrap.createFocusTrap('#schedule-popup', {
        onActivate: function () {myContainer.classList.add('is-active')},
        onDeactivate: function () {myContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });

    $('#schedule-popup').on('cssClassChanged', function (e) {
        // e.stopPropagation();
        myFocusTrap.activate();
    });

    getBrightnessSchedule();
    getBrightnessMode();
    setBrightnessMode();
    setBrightnessSchedule();

    // setBrightnessSchedule이 콜 된 이후에
    $('#btn-schedule-cancel').on('click', function () {
        myFocusTrap.deactivate();
        _closePopup('schedule-popup');
    });

    $('#btn-schedule-ok').on('click', function () {
        myFocusTrap.deactivate();
        displayLoading(true);
        var editIdx = $('#schedule-popup').data('id');
        if(checkScheduleDuplicated(editIdx, brightnessNewSchedule)){
            alert(getLanguageText(languageTable, "Invalid schedule.") + " " + getLanguageText(languageTable, "Please check the schedule again."));
        } else {
            if(editIdx >= 0){
                scheduleList.splice(editIdx, 1);
                $('#schedule-popup').removeData('id'); // edit mode
            }
            scheduleList.push(brightnessNewSchedule);
        }
        setSchedule(scheduleList);
        _closePopup('schedule-popup');
        setTimeout(function () {
            displayLoading(false);
            window.location.reload();
        }, 500);
    });

    makeKeyDownDropdown('schedule-hour-dropdown');
    makeKeyDownDropdown('schedule-min-dropdown');
}

function showTableContents() {
    var container = $('#brightnessScheduleList');
    container.empty();
    if (CUR_BRIGHTNESS_SCHEDULE > 0) {
        var ul = document.createElement('ul');
        ul.classList.add('list-item');
        for (var i = 0; i < CUR_BRIGHTNESS_SCHEDULE; i++) {
            var hour = scheduleList[i].hour + ":" + (scheduleList[i].minute < 10 ? "0" : "") + scheduleList[i].minute + " " + (scheduleList[i].hour >= 12 ? 'PM' : 'AM');
            var li = `<li data-id="${i}">
                        <div class="item-title scheduleTxt">${hour + ' ' + '(' + scheduleList[i].backlight + ')'}</div>
                        <button type="button" class="btn btn-delete brightnessScheduleDel">${getLanguageText(languageTable, 'Delete')}</button>
                    </li>`;
            $(ul).append(li);
        }
        container.html(ul);
    } else {
        var body = `<div class="no-data">${getLanguageText(languageTable, "There is no list.")}</div>`;
        container.html(body);
    }
}

function getBrightnessSchedule() {
    getSystemSettings('commercial', ['easyBrightnessSchedule'], function (ret) {
        scheduleList = JSON.parse(ret.easyBrightnessSchedule);
        CUR_BRIGHTNESS_SCHEDULE = scheduleList.length;
        showTableContents();
    })
}

function setUpButtons() {
    if (CUR_BRIGHTNESS_STATUS) { // ON Brightness Scheduling
        $('#brightnessScheduleAdd').removeAttr('disabled');
        setTimeout(function () {
            $('#contrast').attr('disabled', 'disabled');
            disableSlide('contrast-container');
        },500);

        if (CUR_BRIGHTNESS_SCHEDULE > 0) {
            $('.brightnessScheduleDel').attr('disabled', false);
            $('#brightnessScheduleDelAll').removeAttr('disabled');
            if (CUR_BRIGHTNESS_SCHEDULE === MAX_BRIGHTNESS_SCHEDULE) {
                $('#brightnessScheduleAdd').attr('disabled', true);
            }
        } else {
            $('#brightnessScheduleDelAll').attr('disabled', true);
        }
        myDropdownUnBinding('saving-mode-dropdown');
    } else { // OFF Brightness Scheduling
        $('#brightnessScheduleAdd').attr('disabled', true);
        $('#brightnessScheduleDelAll').attr('disabled', true);
        $('.brightnessScheduleDel').attr('disabled', true);
        $('#contrast').removeAttr('disabled');
        setTimeout(function () {
            enableSlide('contrast-container');
            checkRangeUI('contrast', 0, 100);
            myDropdownBinding('saving-mode-dropdown');
        },500);
    }
}

function setBrightnessMode() {
    $('#brightnessScheduleEnable').on('change',function () {
        // const isChecked = $(this).prop('checked');
        // $(this).attr('aria-checked', isChecked ? "true" : "false");
        // console.log('schedule checked', isChecked);
        var status = $("input:checkbox[id='brightnessScheduleEnable']").is(":checked") ? "on" : "off";
        // var status = isChecked ? "on" : "off";
        setSystemSettings('commercial', {
            'easyBrightnessMode': status
        }, function () {
            if (status === 'on') {
                $(this).attr('aria-checked', "true");
                setSystemSettings('picture', {'energySaving': 'off'}, function() {
                    changeDropdownLabel('saving-mode-dropdown', getLanguageText(languageTable, "Off"));
                    changeDropdownAria('saving-mode-dropdown', 1);
                    getPictureDBVal(['contrast'], function(msg) {
                        $('#contrast').val(Number(msg.contrast));
                        $('#contrastVal').html(msg.contrast);
                        checkRangeUI('contrast', 0, 100);
                    });
                });
            } else {
                $(this).attr('aria-checked', "false");
            }

            CUR_BRIGHTNESS_STATUS = (status === "on" ? true : false);
            setUpButtons();
        })
    });
}

function setSchedule(schedule) {
    setSystemSettings('commercial', {
        'easyBrightnessSchedule': JSON.stringify(schedule)
    }, function (ret) {
        if(ret.returnValue === true){
            scheduleList = schedule;
            CUR_BRIGHTNESS_SCHEDULE = scheduleList.length;
            setUpButtons();
            getBrightnessSchedule();
        }
    });
}

function setBrightnessSchedule() {
    addConfirmModals('#brightnessScheduleDelAll', 'brightnessScheduleDellAllModal', getLanguageText(languageTable, "information message"),
        getLanguageText(languageTable, 'Do you want to delete all schedules?'), false, function () {
            var schedule = [];
            setSchedule(schedule);
        });

    drawBrightnessHourSelect();
    drawBrightnessMinuteSelect();
    setUpBrightnessSchSlider('schBrightness', 0, 100);

    $('#brightnessScheduleAdd').on('click', function () {
        brightnessNewSchedule.hour = 0;
        brightnessNewSchedule.minute = 0;
        brightnessNewSchedule.backlight = 70;

        changeDropdownLabel("schedule-hour-dropdown", brightnessNewSchedule.hour, 'option-hour-' + brightnessNewSchedule.hour);
        changeDropdownAria("schedule-hour-dropdown", brightnessNewSchedule.hour);
        changeDropdownLabel("schedule-min-dropdown", brightnessNewSchedule.minute, 'option-min-' + brightnessNewSchedule.minute);
        changeDropdownAria("schedule-min-dropdown", brightnessNewSchedule.minute);
        initBrightnessSchSliderValue('schBrightness', 0, 100, brightnessNewSchedule.backlight);

        showMyModal('schedule-popup');
    });

    $('.scheduleTxt').on('click', function () {
        var id = $(this).closest('li').data('id');
        $('#schedule-popup').data('id', id);
        brightnessNewSchedule.hour = scheduleList[id].hour;
        brightnessNewSchedule.minute = scheduleList[id].minute;
        brightnessNewSchedule.backlight = scheduleList[id].backlight;

        changeDropdownLabel("schedule-hour-dropdown", scheduleList[id].hour, 'option-hour-' + scheduleList[id].hour);
        changeDropdownAria("schedule-hour-dropdown", scheduleList[id].hour);
        changeDropdownLabel("schedule-min-dropdown", scheduleList[id].minute, 'option-min-' + scheduleList[id].minute);
        changeDropdownAria("schedule-min-dropdown", scheduleList[id].minute);

        initBrightnessSchSliderValue('schBrightness', 0, 100, scheduleList[id].backlight);
    });

    setTimeout(function () {
        addConfirmModals('.brightnessScheduleDel', 'brightnessScheduleDelModal', getLanguageText(languageTable, "information message"),
            getLanguageText(languageTable, 'Do you want to delete the schedule?'), false, function () {
                var id = $('#brightnessScheduleDelModal').data('id');
                scheduleList.splice(id, 1);

                setSchedule(scheduleList);
            }, function () {
            }, function (btnThis) {
                var idx = $(btnThis).closest('li').data('id');
                $('#brightnessScheduleDelModal').data('id', idx);
            });
    },1000);
}

function checkScheduleDuplicated (editIdx, newSchedule) {
    var found = -1;

    scheduleList.some(function(elem, idx) {
        if (editIdx !== idx && newSchedule.hour === elem.hour && newSchedule.minute === elem.minute) {
            found = idx;
            return true;
        }
    });

    return found >= 0;
}

function drawBrightnessHourSelect() {
    var hourList = [];
    var hourMap = new Map();

    for(var i = 0; i <= 23; i++){
        hourList.push(i);
        hourMap.set(i, String(i));
    }

    var hourDropdown = $('#schedule-hour-dropdown');
    var dropdownMenu = $(hourDropdown).children(".dropdown-menu");
    var hourId = 'hour';
    var ul = createDropdownChildren(hourList, hourMap, hourId, 'schedule-hour-dropdown');

    var lis = $(ul).children('li');
    lis.each(function(i) {
        $(this).on('click', function () {
            var ariaId = 'option-' + hourId + '-' + i;
            changeDropdownLabel("schedule-hour-dropdown", i, ariaId);
            changeDropdownAria("schedule-hour-dropdown", i);
            brightnessNewSchedule.hour = i;
        });
    });

    $(dropdownMenu).html(ul);

    brightnessNewSchedule.hour = 0;
    changeDropdownLabel("schedule-hour-dropdown", 0, 'option-hour-0');
    changeDropdownAria("schedule-hour-dropdown", 0);

}

function drawBrightnessMinuteSelect() {
    var minList = [];
    var minMap = new Map();

    for (var i = 0; i <= 59; i++) {
        minList.push(i);
        minMap.set(i, String(i));
    }

    var minDropdown = $('#schedule-min-dropdown');
    var dropdownMenu = $(minDropdown).children(".dropdown-menu");
    var minId = 'min';
    var ul = createDropdownChildren(minList, minMap, minId, 'schedule-min-dropdown');

    var lis = $(ul).children('li');
    lis.each(function(i) {
        $(this).on('click', function () {
            var ariaId = 'option-' + minId + '-' + i;
            changeDropdownLabel("schedule-min-dropdown", i, ariaId);
            changeDropdownAria("schedule-min-dropdown", i);
            brightnessNewSchedule.minute = i;
        });
    });

    $(dropdownMenu).html(ul);

    brightnessNewSchedule.minute = 0;
    changeDropdownLabel("schedule-min-dropdown", 0, 'option-min-0');
    changeDropdownAria("schedule-min-dropdown", 0);
}

function initBrightnessSchSliderValue (name, start, end, value) {
    var initVal = value ? value : 70
    brightnessNewSchedule.backlight = initVal;

    $('#' + name).val(initVal);
    $('#' + name + 'Val').html(initVal);
    checkRangeUI(name, start, end);
}

function setUpBrightnessSchSlider(name, start, end, value) {
    initBrightnessSchSliderValue(name, start, end, value);

    var browserType;

    if (agent.indexOf("webkit") !== -1) {
        browserType = "webkit";
    } else if (agent.indexOf("firefox") !== -1) {
        browserType = "firefox";
    } else {
        browserType = "msie";
    }

    if (browserType === "msie") {
        $('#' + name).on('change',function() {
            brightnessNewSchedule.backlight = parseInt($(this).val());
            $('#' + name + 'Val').html($(this).val());
            checkRangeUI(name, start, end);
        });
    } else {
        $('#' + name).on('change',function() {
            brightnessNewSchedule.backlight = parseInt($(this).val());
        }).on('input', function() {
            $('#' + name + 'Val').html($(this).val());
            checkRangeUI(name, start, end);
        });
    }
}

