/**
 * Copyright (c) 2019 LG Electronics, Inc.
 */
var isUpdatingFailMsg = false;

function init() {
    $.ajax({
        type: "get",
        url: "/loginStatus",
        success: function (data) {
            switch (data.status) {
                case 'fail':
                    updateFailMessage(data.loginTryTimes, data.loginRestrictedTime);
                    break;
                default:
            }
        }
    });

    $('#loginButton').click(function () {
        handleLogin();
    });

    $('#login-error-ok').on('click', function () {
        loginErrorOK();
    });
}

function initDisplay() {

}

function handleLogin() {
    var password = $("#password").val();
    $.ajax({
        type: "post",
        url: "/login",
        data: {
            password: password
        },
        success: function (data) {
            onLoginResponse(data);
        }
    });
}

function onLoginResponse(data) {
    console.log('pbj/ onLoginResponse:', data);
    switch (data.status) {
        case 'success':
            location.replace('/dashboard');
            break;
        case 'first_login':
            alert(Locale.getText('For your security, you must change the default password.'));
            location.replace('/changePasswd');
            break;
        case 'fail':
            // if (!isUpdatingFailMsg) {
                updateFailMessage(data.loginTryTimes, data.loginRestrictedTime);
            // }
            break;
        default:
    }
}

function updateFailMessage(loginTryTimes, loginRestrictedTime) {
    var failMsg = '';

    if (loginTryTimes >= 5) {
        var min = Math.floor(loginRestrictedTime/ 1000 / 60);
        var sec = Math.floor((loginRestrictedTime / 1000) - (min * 60));
        if (min < 10) {
            min = '0' + min;
        }
        if (sec < 10) {
            sec = '0' + sec;
        }
        if (loginRestrictedTime > 0) {
            failMsg = Locale.getText('You have failed to login 5 times. Please try again after {time}.').replace('{time}', min + ':' + sec);
            $('#login-failed-text').text(failMsg);
            $('#login-error-modal').addClass('show');
            // setTimeout(function() {
            //     updateFailMessage(loginTryTimes, loginRestrictedTime - 1000);
            // }, 1000);
            isUpdatingFailMsg = true;
        } else {
            isUpdatingFailMsg = false;
        }
    } else if (loginTryTimes > 0 && loginTryTimes < 5) {
        failMsg = Locale.getText('You have failed to login 1/5 time.').replace('1', loginTryTimes);
    }

    $('#loginMessage').text(failMsg);
}

function loginErrorOK() {
    $('#login-error-modal').removeClass('show');
}

(function () {
    init();
})();
