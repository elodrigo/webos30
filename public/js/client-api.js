function callAPI(command, data, event, callback) {
	if (event && callback) {
		var id = Math.round(Math.random() * 1000000);
		socket.on(event + id, function(msg) {
			callback(msg);
			socket.removeAllListeners(event + id);
		});
		data.eventID = id;
	}
	data.command = command;
	socket.emit("api", data);
}

/**
 * on return, 'surevu' event is emitted
 */
function getSureVu(callback) {
	callAPI("getSureVu", {},
	"surevu", callback);
}

function setVolume(vol) {
	callAPI("setVolume", {
		volume: vol
	});
}

function getVolume(callback) {
	callAPI("getVolume", {},
	"volume", callback);
}

function setMuted(muted) {
	callAPI("setMuted", {
		muted: muted
	});
}

function setHdmiInput(val) {
	callAPI("setHdmiInput", {
		input: val
	});
}

function getHdmiInput(callback) {
	callAPI("getHdmiInput", {},
	"input", callback);
}

function setPortrait(settings) {
	callAPI("setPortrait", settings);
}

function getPortrait(callback) {
	callAPI("getPortrait", {},
	"portrait", callback);
}

function getCapture(height, callback) {
	callAPI("capture", {
		height: height
	},
	"capture", callback);
}

function sendRemocon(val) {
	callAPI("sendRemocon", {
		key: val
	});
}

function sendAlert(msg) {
	callAPI("sendAlert", {
		message: msg
	});
}

function sendScrollAlert(msg, location, textSize, textColor, bkColor, dir, textAlpha, bkAlpha) {
	function calcColor(alpha, hexStrColor) {
		var hexAlpha = Math.round(alpha * 0xff / 100);
		var ret = (hexAlpha << 24 | parseInt(hexStrColor, 16)) & 0xffffffff;
		return ret;
	}

	var textDecColor = calcColor(textAlpha, textColor);
	var bkDecColor = calcColor(bkAlpha, bkColor);

	callAPI("sendScrollAlert", {
		message: msg,
		location: location,
		textSize: textSize,
		textColor: textDecColor,
		backgroundColor: bkDecColor,
		scrollDir: dir
	});
}

function sendToast(msg) {
	callAPI("sendToast", {
		message: msg
	});
}

function sendMouseEvent(x, y) {
	callAPI("sendMouseEvent", {
		x: x,
		y: y
	});
}

function sendKeyEvent(keycode) {
	callAPI("sendKeyEvent", {
		keycode: keycode.toLowerCase()
	});
}

function deleteFile(which, fileName, callback) {
	callAPI("deleteFile", {
		which: which,
		fileName: fileName
	},
	"deleteFile", callback);
}

function swupdate(fileName, micomUpdate, callback) {
	callAPI("swupdate", {
		fileName: fileName,
		micomUpdate: micomUpdate
	}, "swupdate", callback);
}

function getMicomUpdateProgress(callback) {
	socket.on('micom', callback);
}

function listUpdates(callback) {
	callAPI("listUpdates", {},
			"listUpdates", callback);
}

function getUpdateStatus(callback) {
	callAPI("getUpdateStatus", {},
			"swupdate", callback);
}

function emptyThumbnail(){
	callAPI("emptyThumbnail", {});
}

function getThumbnail(id, fileName) {
	callAPI("thumbnail", {
		id: id,
		fileName: fileName
	});
}

function getNetworkStatus(callback) {
	callAPI("getNetworkStatus", {},
	"networkInfo", callback);
}

function getMacAddr(callback) {
	callAPI("getMacAddr", {},
	"networkInfo", callback);
}

function setNetworkStatus(dhcp, address, netmask, gateway, ssid, callback) {
	var method = dhcp ? "dhcp": "manual";
	callAPI("setNetworkStatus", {
		method: method,
		address: address,
		netmask: netmask,
		gateway: gateway,
		ssid: ssid
	}, "setNetworkStatus", callback);
}

function setNetworkDNS(dns, ssid, callback) {

	callAPI("setNetworkDNS", {
		dns: dns,
		ssid: ssid
	}, "setNetworkDNS", callback);
}

function getSignageName(callback) {
	callAPI("getSignageName", {},
	"signageName", callback);
}

function getNTPStatus(callback) {
	callAPI("getNTPStatus", {},
	"ntpStatus", callback);
}

function setNTPStatus(param, callback) {
	callAPI("setNTPStatus", param, "ntp", callback);
}

function setSignageName(signageName) {
	callAPI("setSignageName", {
		signageName: signageName
	});
}

function setPictureMode(mode) {
	callAPI("updatePictureMode", {
		pictureMode: mode,
	});
}

function getPictureMode(callback) {
	callAPI("getPictureMode", {},
	"pictureMode", callback);
}

function turnOffScreen() {
	callAPI("turnOffScreen", {},
	"turnOffScreen");
}

function turnOnScreen() {
	callAPI("turnOnScreen", {},
	"turnOnScreen");
}

function getPowerState(callback) {
	callAPI('getPowerState', {}, 'powerState', callback);
}

function rebootScreen() {
	callAPI("rebootScreen", {},
	"reboot");
}

function checkScreenCalib(eyekey, callback) {
	callAPI("calibrateCheckScreen", {eyekey: eyekey}, "checkScreenCalib", callback);
}

function getCheckScreenOn(callback) {
	callAPI("getCheckScreenOn", {},
	"isCheckScreenOn", callback);
}

function getNormalCheckScreenStatus(callback) {
	callAPI("getNormalCheckScreenStatus", {},
	"normalCheckScreen", callback);
}

function setCheckScreenOn(isOn) {
	callAPI("setCheckScreenOn", {isOn: isOn});
}

function getCheckScreenInfo(callback) {
	callAPI("getCheckScreenInfo", {},
	"checkScreenInfo", callback);
}

function calibrateCheckScreenEYEKey(callback) {
	callAPI("calibrateCheckScreenEYEKey", {},
	"calibrateCheckScreenEYEKey", callback);
}

function getCheckScreenColor(callback) {
	callAPI("getCheckScreenColor", {},
	"checkScreenColor", callback);
}

function setCheckScreenPos(position) {
	callAPI("setCheckScreenPos", {
		position: position
	});
}

function changeDoorMuteOpt(mute) {
	var param = {};
	if (mute) {
		param.mute = mute;
	}
	callAPI('changeDoorMuteOpt', param);
}

function getDoorStatus(callback) {
	callAPI('getDoorStatus', {}, 'curDoor', callback);
}

function resetDoorState() {
	callAPI('resetDoorState', {});
}

function getInputList(callback) {
	callAPI("getInputList", {},
		"inputList", callback);
}

function getFanCtlTable(id, micomId, callback) {
	callAPI("getFanControl", {
			id: id,
			micomId: micomId
		},
		"controlTable", callback);
}

function setFanCtlTable(id, data, micomId) {
	callAPI("setFanControl", {
		id: id,
		micomId: micomId,
		temp: data.temp,
		zone: data.zone,
		level: data.level
	});
}

function resetFanCtlTable() {
	callAPI("resetFanControl", {});
}

function stopFanControl() {
	callAPI("stopFanControl", {});
}

function restartFanControl() {
	callAPI("restartFanControl", {});
}

function setFanDuty(id, duty, micomId) {
	callAPI("setFanDuty", {
		id: id,
		duty: Number(duty),
		micomId: micomId
	});
}

function mpPressPowerBtn(msg) {
	callAPI("mpPressPowerBtn", msg);
}

function mpPressResetBtn(msg) {
	callAPI("mpPressResetBtn", msg);
}

function mpCheckPowerLed(callback) {
	callAPI("mpCheckPowerLed", {}, 'powerLed', callback);
}

function getTempHistory(id, start, end, callback) {
	callAPI("getTempHistory", {
		id: id,
		start: start,
		end: end
	}, 'tempHistory', callback);
}

function getFanRpmHistory(which, id, start, end, callback) {
	callAPI("getFanRpmHistory", {
		which: which,
		id: id,
		start: start,
		end: end
	}, 'fanRpmHistory', callback);
}

function getBacklightHistory(start, end, callback) {
	callAPI("getBacklightHistory", {
		start: start,
		end: end
	}, 'backlightHistory', callback);
}

function getEyeQSensorHistory(start, end, callback) {
	callAPI("getEyeQSensorHistory", {
		start: start,
		end: end
	}, 'illumHistory', callback);
}

function getHumidityHistory(start, end, callback) {
	callAPI("getHumidityHistory", {
		start: start,
		end: end
	}, 'humidityHistory', callback);
}

function getTemp(id, callback) {
	callAPI("getTemp", {id: id}, 'temperature', callback);
}

function getPictureDBVal(keys, callback) {
	callAPI("getPictureDBVal", {keys: keys}, 'pictureDB', callback);
}

function setPictureDBVal(settings, from) {
	callAPI("setPictureDBVal", {settings: settings, from: from});
}

function getBasicInfo(callback) {
	callAPI("getBasicInfo", {}, 'basicInfo', callback);
}

function getWebOSInfo(callback) {
	callAPI("getWebOSInfo", {}, 'webOSInfo', callback);
}

function getFanMicomInfo(callback) {
	callAPI("getFanMicomInfo", {}, 'FanMicomInfo', callback);
}

function getSystemProperties(keys, callback) {
	callAPI("getSystemProperties", {keys: keys}, 'systemProp', callback);
}

function setSystemProperties(properties, callback) {
	callAPI("setSystemProperties", {
		properties: properties
	});
}

function getSystemSettings(category, keys, callback, dimension) {
	var param = {
		category: category,
		keys: keys
	};

	if (dimension) {
		param.dimension = dimension;
	}

	callAPI("getSystemSettings", param, 'systemSettings', callback);
}

function setSystemSettings(category, settings, callback, dimension) {
	var param = {
		category: category,
		settings: settings
	};

	if (dimension) {
		param.dimension = dimension;
	}

	callAPI("setSystemSettings", param, 'systemSettings', callback);
}

function resetSystemSettings(category, keys, callback) {
	var param = {
		category: category,
		keys: keys
	};

	callAPI("resetSystemSettings", param, 'systemSettings', callback);
}

function getDowntimeIncident(callback) {
	callAPI("getDowntimeIncident", {}, 'downtimeIncident', callback);
}

function getFanStatus(callback) {
	callAPI("getFanStatus", {}, 'fanStatus', callback);
}

function getFanRPM(callback) {
	callAPI("getFanRPM", {}, 'fanRPM', callback);
}

function getNumFans(callback) {
	callAPI("getNumFans", {}, 'numFans', callback);
}

function isNoSignal(callback) {
	callAPI("isNoSignal", {}, 'isNoSignal', callback);
}

function getVideoSize(callback) {
	callAPI("getVideoSize", {}, 'videoSize', callback);
}

function getVideoStillStatus(callback) {
	callAPI("getVideoStillStatus", {}, 'videoStill', callback);
}

function resetStalledImage(minutes) {
	callAPI("resetStalledImage", {minutes: minutes});
}

function getEmergency(callback) {
	callAPI("getEmergency", {}, 'emergency', callback);
}

function setCurrentTime(time) {
	callAPI("setCurrentTime", {utc:time} , 'setCurrentTime' );
}

function getCurrentTime(callback) {
	callAPI("getCurrentTime", {}, 'currentTime', callback);
}

function setEmergency(lcmOffset, panelOffset, backlightOn, backlightOff, powerOff) {
	callAPI("setEmergency", {
		lcmOffset: lcmOffset,
		panelOffset: panelOffset,
		backlightOn: backlightOn,
		backlightOff: backlightOff,
		powerOff: powerOff
	});
}

function testsuiteForDIL(callback) {
	callAPI("testsuiteForDIL", {}, 'testDIL', callback);
}

function testsuiteForEyeQ(callback) {
	callAPI("testsuiteForEyeQ", {}, 'testEyeQ', callback);
}

function getLocaleInfo(callback) {
	callAPI("getLocaleInfo", {}, 'locale', callback);
}

function getHistoryInterval(callback) {
	callAPI("getHistoryInterval", {}, 'historyInterval', callback);
}

function setHistoryInterval(second) {
	callAPI("setHistoryInterval", {second: second});
}

function clearHistory() {
	callAPI("clearHistory", {});
}

function fanMicomUpdate(fileName, callback) {
	callAPI("fanMicomUpdate", {
		fileName: fileName
	}, 'fanMicom', callback);
}

function getPmMode(callback) {
	callAPI("getPmMode", {}, 'pmMode', callback);
}

function setPmMode(mode) {
	callAPI("setPmMode", {pmMode: mode});
}

function checkPasswd(currentPasswd, callback) {
	callAPI("checkPasswd", {currentPasswd: currentPasswd},'checkPass',callback);
}

function testPattern(enable, pattern) {
	callAPI("testPattern", {
		enable: enable,
		pattern: pattern
	});
}

function getCountryList(continent, callback) {
	callAPI("getCountryList", { continent:continent }, 'getCountryList', callback);
}

function getCityList(country, callback) {
	callAPI("getCityList", { country:country }, 'getCityList', callback);
}

function setContinent(continent, callback) {
	callAPI("setContinent", { continent:continent }, 'setContinent', callback);
}

function setCountry(country, callback) {
	callAPI("setCountry", { country:country }, 'setCountry', callback);
}

function setCity(timeZone, callback) {
	callAPI("setCity", { timeZone:timeZone }, 'setCity', callback);
}

function getlocaleContinent(callback){
	callAPI("getlocaleContinent", {}, 'getlocaleContinent', callback);
}

function getlocaleCountry(callback){
	callAPI("getlocaleCountry", {}, 'getlocaleCountry', callback);
}

function getTimeZone(callback){
	callAPI("getTimeZone", {}, 'getTimeZone', callback);
}

function setDstOnOff(dstOnOff){
	callAPI("setDstOnOff", {
		dstOnOff:dstOnOff
	});
}

function setDstStartTime(Month, Week, Weekday, Hour){
	callAPI("setDstStartTime", { 
		Month:Month, 
		Week:Week,
		Weekday:Weekday,
		Hour:Hour
	});
}

function setDstEndTime(Month, Week, Weekday, Hour){
	callAPI("setDstEndTime", { 
		Month:Month, 
		Week:Week,
		Weekday:Weekday,
		Hour:Hour
	});
}

function getDSTInfo(callback){
	callAPI("getDSTInfo", {}, "getDSTInfo", callback);
}

function readTempType(callback){
	callAPI("readTempType", {}, "readTempType", callback);
}

function writeTempType(temptype, callback){
	callAPI("writeTempType", {isCelsius:temptype}, "writeTempType", callback);
}

function getPanelErrorOut(callback){
	callAPI("getPanelErrorOut", {}, "getPanelErrorOut", callback);
}

function getBacklight(callback){
	callAPI("getBacklight", {}, "getBacklight", callback);
}

function setBacklight(id, val, callback){
	callAPI("setBacklight", {id:id, val:val}, "setBacklight", callback);
}

function getForegroundAppInfo(callback){
	callAPI("getForegroundAppInfo", {}, "getForegroundAppInfo", callback);
}

function getRunningAppInfo(callback){
	callAPI("getRunningAppInfo", {}, "getRunningAppInfo", callback);
}

function getRecentsAppInfo(callback){
	callAPI("getRecentsAppInfo", {}, "getRecentsAppInfo", callback);
}

function getMenuLanguage(callback) {
	callAPI('getMenuLanguage', {}, 'language', callback);
}

function setMenuLanguage(langCode, callback) {
	callAPI('setMenuLanguage', {langCode: langCode}, 'language', callback);
}

function getOSDLockMode(callback) {
	callAPI('getOSDLockMode', {}, 'OSDLockMode', callback);
}

function getSDMInfo(callback) {
	callAPI('getSDMInfo', {}, 'getSDMInfo', callback);
}

function resetPictureDB(callback) {
	callAPI('resetPictureDB', {}, 'resetPictureDB', callback);
}

function getVideoInfo(fileName, callback) {
	callAPI('getVideoInfo', { fileName : fileName }, 'getVideoInfo', callback);
}

function getSystemSettingDesc(category, keys, callback) {
	callAPI('getSystemSettingDesc', {
		category: category,
		keys: keys
	}, 'getSystemSettingDesc', callback);
}

function getHdmiPcMode(callback) {
	callAPI("getHdmiPcMode", {}, "getHdmiPcMode", callback);
}

function setInputSouce(appId, callback) {
	callAPI('setInputSouce', {
		appId: appId
	}, 'setInputSouce', callback);
}

function getPowerCurrent (callback) {
	callAPI('getPowerCurrent', {}, 'getPowerCurrent', callback);
}

function getBluMaintain(callback) {
	callAPI('getBluMaintain', {}, 'getBluMaintain', callback);
}

function getPowerCurrentHistory(start, end, callback) {
	var params = {
		start: start,
		end: end
	}
	callAPI('getPowerCurrentHistory', params, 'getPowerCurrentHistory', callback);
}

function getBluMaintainHistory (start, end, callback) {
	var params = {
		start: start,
		end: end
	}
	callAPI('getBluMaintainHistory', params, 'getBluMaintainHistory', callback);
}

function setNTPServerAddress (type, addr, callback) {
	var params = {
		type: type,
		address: addr
	}
	callAPI('setNTPServerAddress', params, 'setNTPServerAddress', callback);
}
