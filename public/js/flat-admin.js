$(function() {
  $(".navbar-expand-toggle").click(function() {
    return $(".app-container").toggleClass("expanded");
  });

  return $(".navbar-right-expand-toggle").click(function() {
    return $(".navbar-right").toggleClass("expanded");
  });
});

$(function() {
  return $(".side-menu .nav .dropdown").on('show.bs.collapse', function() {
    return $(".side-menu .nav .dropdown .collapse").collapse('hide');
  });
});

$(function() {
	var url = window.location;
	var element = $('ul.nav a').filter(function() {
		return this.href == url || url.href.indexOf(this.href) == 0;
	}); //.addClass('active').parent().parent().addClass('in').parent();
	element.parent().addClass('active');

	var upper = element.parent().parent().parent().parent();
	upper.addClass('in');

	if (window.location.pathname.indexOf('/mobile') >= 0) {
		return;
	}

	function toggleMenu() {
		$(".app-container").toggleClass("expanded");
	}

	var prevWidth = document.body.clientWidth;

	if (prevWidth < 1025) {
		toggleMenu();
	}

	window.addEventListener('resize', function(event){
		var curWidth = document.body.clientWidth;
		if ((prevWidth - 1025) * (curWidth - 1025) < 0) {
			toggleMenu();
		}

		prevWidth = curWidth;
	});
});
