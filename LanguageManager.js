var fs = require('fs');
// var web = require('./web');
var luna;
var clientLocale = "en-US";
var languageTable = [];
var languageTableClient = [];

exports.init = function (env, code, callback) {
    // luna = lunaService;

    setLanguageTable("en-US".split("-"));
    // setLanguageTable("ko-KR".split("-"));

    // api.js 에서 init을 콜하면서 아래 luna api를 이미 실행시킨 결과값이 code 파라미터이다. -elo
    // var language = luna.subscribe('luna://com.webos.settingsservice/getSystemSettings', {
    //     keys: ['localeInfo'],
    //     subscribe: true
    // });

    // language.on('response', function (ret) {
    //     var localeInfo = ret.payload.settings.localeInfo;
    //     var domain = localeInfo.locales.UI.split("-");
    //     var io = require("./api").io;
    //     setLanguageTable(domain);
    //     io.emit("locale", domain);
    // });

    var domain = code.split("-");
    setLanguageTable(domain);

    if (callback) {
        callback();
    }
};

function setLanguageTable(domain) {
    var fileName = "strings.json";
    var filePath = "/resources/";

    languageTable = [];
    for (var i = 0; i < domain.length; i++) {
        filePath = filePath + domain[i] + "/";
        try {
            languageTable[i] = JSON.parse(fs.readFileSync(__dirname + filePath + fileName));
        } catch (e) {
            languageTable[i] = {};
        }
    }
    exports.languageTable = languageTable;
}

exports.setLanguageTableClient = function (locale) {
    var fileName = "strings.json";
    var filePath = "/resources/";
    var localeArr = locale.split("-");

    languageTableClient = [];
    for (var i = 0; i < localeArr.length; i++) {
        filePath = filePath + localeArr[i] + "/";
        try {
            languageTableClient[i] = JSON.parse(fs.readFileSync(__dirname + filePath + fileName));
        } catch (e) {
            languageTableClient[i] = {};
        }
    }

    exports.clientLocale = locale;
    exports.languageTableClient = languageTableClient;
};

exports.getLanguageText = function (text) {
    var changedText = text + "";
    // var supportBrowserLocale = web.isSupportedBrowserLocale();
    // var table = supportBrowserLocale ? languageTableClient : languageTable;
    var table = languageTable;

    for (var i = table.length - 1; i >= 0; i--) {
        if (table[i][changedText]) {
            changedText = table[i][changedText];
        }
    }
    return changedText;
};

exports.clientLocale = clientLocale;
exports.languageTableClient = languageTableClient;
exports.languageTable = languageTable;