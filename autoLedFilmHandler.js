var ledFilmAPI = require('./ledFilmAPI');
var web = require('./web');

var p14Info = {
	sender: {
		width: 960,
		height: 540,
	},
	unit: {
		width: 48,
		height: 36,
		miniHeight: 9,
		maxRows: 15
	}
};

var p24Info = {
	sender: {
		width: 980,
		height: 540,
	},
	unit: {
		width: 28,
		height: 20,
		miniHeight: 5,
		maxRows: 27
	}
};

function getFilmPitch (callback) {
	ledFilmAPI.ledGetConfig('getP14Status', {}, function (res) {
		var pitch = 14;

		if (res.payload.result) {
			pitch = res.payload.result.p14Status === true ? 14 : 24;
		}

		callback(pitch);
	});
}

function getSenderSize (pitch) {
	return Number(pitch) === 14 ? p14Info.sender : p24Info.sender;
}

function getUnitSize (pitch) {
	return Number(pitch) === 14 ? p14Info.unit : p24Info.unit;
}

function adjustUnitSize (pitch, rows, mini, inputDirection, portSelection) {
	// CAUTION : no mini film for P14;
	var unitPixels = getUnitSize(pitch);

	var w = portSelection == 0 ? unitPixels.width * 2 : unitPixels.width;
	var h = (rows * unitPixels.height) + (mini * unitPixels.miniHeight);

	if (inputDirection < 2) {
		return {width: h, height: w}
	} else {
		return {width: w, height: h}
	}
}

function checkMACValid(mac) {
	var regex = new RegExp('[0-9a-fA-F]{2}');

	return (regex.test(mac[0]) && regex.test(mac[1]));
}

function checkNecessaryFields (layout) {
	var isOneBoxModel = web.env.supportLedFilm && web.env.numOfExtInputs === 1;
	var MAX_SENDER_CNT = isOneBoxModel ? 1 : 4;
	var errorMsg = '';

	// add pixel pitch
	if (!layout.pixelPitch) {
		errorMsg += 'pixelPitch error\n'
	} else {
		getFilmPitch(function (val) {
			var pitchIdentical = (Number(layout.pixelPitch) === val);
			if (!pitchIdentical) {
				errorMsg += 'pixelPitch not identical\n'
			}
		});
	}

	if (errorMsg.length > 0) {
		return errorMsg;
	}

	var pitch = layout.pixelPitch;
	var senderSize = getSenderSize(pitch);
	var unitSize = getUnitSize(pitch);

	var registeredSenderId = [];

	if (!layout.senderList || !Array.isArray(layout.senderList) || layout.senderList.length === 0 || layout.senderList.length > MAX_SENDER_CNT) {
		return 'senderList error\n';
	}

	layout.senderList.forEach(function (sender) {
		if (typeof sender.senderId === 'undefined' || sender.senderId < 1 || sender.senderId > MAX_SENDER_CNT
			|| registeredSenderId.indexOf(parseInt(sender.senderId)) >= 0 || isOneBoxModel && Number(sender.senderId) !== 1) {
			// sender ID should be set without duplication
			errorMsg += 'senderId error\n';
			return;
		} else {
			registeredSenderId.push(sender.senderId);
		}

		if (typeof sender.inputDirection === 'undefined' || sender.inputDirection < 0 || sender.inputDirection > 3) {
			// input direction should be set
			errorMsg += 'S#' + sender.senderId + ' inputDirection error\n';
		}

		if (typeof sender.x === 'undefined' || sender.x < 0 || sender.x > 1920 - senderSize.width) {
			// no sender.x or sender.x range over
			errorMsg += 'S#' + sender.senderId + ' sender:x error\n';
		}

		if (typeof sender.y === 'undefined'  || sender.y < 0 || sender.y > 540) {
			// no sender.y or sender.y range over
			errorMsg += 'S#' + sender.senderId + ' sender:y error\n';
		}

		var registeredUnitId = [];

		sender.unitList.forEach(function (unit) {
			if (typeof unit.unitId === 'undefined' || unit.unitId < 1 || unit.unitId > 18 || registeredUnitId.indexOf(parseInt(unit.unitId)) >= 0) {
				// unit ID should be set without duplication
				errorMsg += 'S#' + sender.id + ' unitId error\n';
				return;
			} else {
				registeredUnitId.push(unit.unitId);
			}

			if (!unit.mac || !Array.isArray(unit.mac) || !checkMACValid(unit.mac)) {
				errorMsg += 'S#' + sender.senderId + 'U#' + unit.unitId + ' mac error\n';
				return;
			}

			// if unit.mini exists
			if (typeof unit.mini !== 'undefined' && ((pitch === 24 && unit.mini < 0 || unit.mini > 3) || (pitch === 14 && unit.mini !== 0))) {
				// unit should be 0~3 (P24 only) / unit should be 0 (P14 only)
				errorMsg += 'S#' + sender.senderId + 'U#' + unit.unitId + ' mini unit error\n';
				return;
			}

			if (typeof unit.portSelection === 'undefined' || unit.portSelection < 0 || unit.portSelection > 2) {
				// port selection should be set
				errorMsg += 'S#' + sender.senderId + 'U#' + unit.unitId + ' unit_portSelection error\n';
			}

			if (typeof unit.rows === 'undefined') {
				errorMsg += 'S#' + sender.senderId + 'U#' + unit.unitId + ' unit_rows error\n';
			} else if (unit.rows && pitch === 14 && (unit.rows < 0 || unit.rows > 15)) {
				errorMsg += 'S#' + sender.senderId + 'U#' + unit.unitId + ' unit_rows error\n';
			} else if (unit.rows && pitch === 24 && (unit.rows < 0 || unit.rows > (unit.mini ? 26 : 27))) {
				errorMsg += 'S#' + sender.senderId + 'U#' + unit.unitId + ' unit_rows error\n';
			}

			if (unit.trimming) {
				if (unit.trimming.left < 0 || unit.trimming.left > unitSize.width - 1) {
					errorMsg += 'S#' + sender.senderId + 'U#' + unit.unitId + ' unit_trimming_left error\n';
				}

				if (unit.trimming.right < 0 || unit.trimming.right > unitSize.width - 1) {
					errorMsg += 'S#' + sender.senderId + 'U#' + unit.unitId + ' unit_trimming_right error\n';
				}
			}

			var trimming = {
				left: typeof unit.trimming !== 'undefined' && unit.trimming.left >= 0 ? unit.trimming.left : 0,
				right: typeof unit.trimming !== 'undefined' && unit.trimming.right >= 0 ? unit.trimming.right : 0
			};

			var adjustTrimmingXL = Number(unit.portSelection) !== 2 ? Number(trimming.left) : 0;
			var adjustTrimmingXR = Number(unit.portSelection) !== 1 ? Number(trimming.right) : 0;
			var adjustTrimmingYL = 0;
			var adjustTrimmingYR = 0;

			var unitWidth = Number(unit.portSelection) === 0 ? unitSize.width * 2 : unitSize.width;
			var unitHeight = (Number(unit.rows) + (unit.mini ? 1 : 0)) * unitSize.height;

			if (sender.inputDirection < 2) {
				var tmpVal = unitWidth;
				unitWidth = unitHeight;
				unitHeight = tmpVal;

				tmpVal = adjustTrimmingXL;
				adjustTrimmingXL = adjustTrimmingYR;
				adjustTrimmingYR = tmpVal;

				tmpVal = adjustTrimmingXR;
				adjustTrimmingXR = adjustTrimmingYL;
				adjustTrimmingYL = adjustTrimmingXR;
			}

			var minX = adjustTrimmingXL;
			var maxX = senderSize.width - unitWidth + adjustTrimmingXL + adjustTrimmingXR;
			var minY = adjustTrimmingYL;
			var maxY = senderSize.height - unitHeight + adjustTrimmingYL + adjustTrimmingYR;

			if (typeof unit.x === 'undefined' || unit.x > maxX || unit.x < minX) {
				errorMsg += 'S#' + sender.senderId + 'U#' + unit.unitId + ' unit:x error\n'
			}

			if (typeof unit.y === 'undefined' || unit.y > maxY || unit.y < minY) {
				errorMsg += 'S#' + sender.senderId + 'U#' + unit.unitId + ' unit:y error\n'
			}
		});
	});

	return errorMsg;
}

function adjustFilmLayout (layout, pitch) {
	// add physical layout
	if (!layout.physicalLayout) {
		layout.physicalLayout = {x: 0, y: 0}
	}

	var pitch = layout.pixelPitch;

	layout.senderList.forEach(function (sender) {
		var senderSize = getSenderSize(pitch);
		sender.width = senderSize.width;
		sender.height = senderSize.height;

		sender.unitList.forEach(function (unit) {
			if (typeof unit.mini === 'undefined') {
				unit.mini = 0;
			}

			if (typeof unit.trimming === 'undefined') {
				unit.trimming = {left: 0, right: 0}
			}

			if (sender.inputDirection < 2 && unit.y < unit.trimming.right && unit.portSelection != 1) {
				unit.y = Number(unit.trimming.right);
			} else if (sender.inputDirection >= 2 && unit.x < unit.trimming.left && unit.portSelection != 2) {
				unit.x = Number(unit.trimming.left);
			}

			var adjusted = adjustUnitSize(pitch, unit.rows, unit.mini, sender.inputDirection, unit.portSelection);
			unit.width = adjusted.width;
			unit.height = adjusted.height;
		});
	});
}

exports.adjustFilmLayout = adjustFilmLayout;
exports.checkNecessaryFields = checkNecessaryFields;