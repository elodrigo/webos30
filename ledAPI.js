var fs = require('fs');
var api = require('./api');
var lunaAPI = require('./luna');
var web = require('./web');

var signage365Care = require('./365careDownload');
var systemserviceURL = 'palm://com.palm.systemservice/'
var applicationManagerURL = 'luna://com.webos.applicationManager/';
var care365URL = 'luna://com.webos.app.commercial.care365.service/';
var appInstallServiceURL = 'luna://com.webos.appInstallService/';

var configFile = '/var/outdoorweb/ledConfig.json';
var newConfigFile = '/var/outdoorweb/ledUnitConfig.json';
var dbcGroupFile = '/var/outdoorweb/dbcGroup.txt';
var controlGroupFile = '/var/outdoorweb/controlGroup.txt';
var updateDir = "/media/update/";

var command = {};
var ledOption;

var dbcInterval = 30 * 1000; // 10 seconds
var lastDbcSetTime = 0;

var isFirstBooting = true;

function init() {
	initCommand();

	var envStr = fs.readFileSync(__dirname + '/env/ledOption.json');
	ledOption = JSON.parse(envStr);
	if(!web.env.supportLedFilm){
		setInterval(checkDBC, dbcInterval);
	}
}

function commandHandler(socket, msg) {
	var handler = command[msg.command];
	if (!handler) {
		console.log('No matching handler for ' + msg.command);
		return;
	}

	handler(socket, msg);
}

function getUnitResolutionList(socket, msg) {
	socket.emit("config" + msg.eventID, ledOption);
}

function getUnitConfigInternal(callback) {
	fs.readFile(configFile, function(err, data) {
		var config = {
			resolution: '384 x 360',
			size : {
				h: 0,
				v: 0
			},
			from: 0,
			direction: 0,
			regular: 0,
			outputWindow: "0",
			onUnits: {
				numOn: 0
			}
		}

		if (!err) {
			config = JSON.parse(String(data));
		}

		callback(config);
	});
}

function getUnitConfig(socket, msg) {
	getUnitConfigInternal(function(config) {
		socket.emit("config" + msg.eventID, config);
	});
}

function setUnitConfig(socket, msg) {
	var config = {
		resolution: msg.resolution,
		size: {
			h: msg.size.h,
			v: msg.size.v
		},
		from: msg.from,
		direction: msg.direction,
		onUnits: msg.onUnits,
		outputWindow: msg.outputWindow,
		regular: msg.regular
	};

	fs.writeFile(configFile, JSON.stringify(config, null, 2), function(err) {
		if (err) {
			console.log(err);
		}
		api.sendReturn(socket, err !== undefined, "config");
	});


	var resolution = msg.resolution.split('x');
	var settings = {
		moduleWidth: Number(resolution[0]) / 2,
		moduleHeight: Number(resolution[1]) / 2,
		rowOfModule: 2,
		columnOfModule: 2,
		rowOfUnit: msg.size.v,
		columnOfUnit: msg.size.h,
		numOfUnits: msg.onUnits.numOn
	};

	lunaAPI.setSystemSettings('commercial', settings, function(ret) {
		if (!ret.payload.returnValue) {
			console.log(ret.payload);
		}
	});

	lunaAPI.getSystemSettings('commercial', ['tileMode'], function(ret) {
		var orig = ret.payload.settings.tileMode;
		if (orig != 'on') {
			return;
		}

		lunaAPI.setSystemSettings('commercial', {
			tileMode: 'off'
		}, function(ret2) {
			lunaAPI.setSystemSettings('commercial', {
				tileMode: 'on'
			}, function(ret3) {
			});
		});
	});
}

var ledfpgaURL = 'luna://com.webos.service.tv.signage/ledfpga/';

function lunaCallConfig(url, command, param, callback) {
	api.luna().call(url, {
		command: command,
		parameter: param
	}, callback);
}

function lunaSetConfig(command, param, callback) {
	var setConfigURL = ledfpgaURL + 'setConfig';
	lunaCallConfig(setConfigURL, command, param, callback);
}

function lunaGetConfig(command, param, callback) {
	var getConfigURL = ledfpgaURL + 'getConfig';
	lunaCallConfig(getConfigURL, command, param, callback);
}

function saveConfigInternal(slave, callback) {
	setTimeout(function() {
		lunaSetConfig('showPattern', {
			slave: slave,
			on: false,
			r: 0,
			g: 0,
			b: 0
		}, function(ret) {
			lunaSetConfig('saveConfigData', {
				slave: slave
			}, function(ret) {
				callback(ret);
			});
		});
	}, 100);
}

function saveConfig(socket, msg) {
	saveConfigInternal(msg.slave, function(ret) {
		socket.emit("config" + msg.eventID, ret.payload.returnValue);
	});
}

function loadConfig(socket, msg) {
	lunaSetConfig('loadConfigData', {
		slave: msg.slave
	}, function(ret) {
		socket.emit("config" + msg.eventID, ret.payload.returnValue);
	});
}

function setSlaveAddr(socket, msg) {
	lunaSetConfig('setSlaveAddr', {
		numUnits: msg.numUnits
	}, function(ret) {
		saveConfigInternal(0, function(ret2) {
			socket.emit("config" + msg.eventID, ret.payload.returnValue);
		});
	});
}

function setPosition(socket, msg) {
	lunaSetConfig('setPosition', {
		slave: msg.slave,
		x: msg.x / 16,
		y: msg.y
	}, function(ret) {
		saveConfigInternal(msg.slave, function(ret2) {
			api.sendReturn(socket, ret.payload.returnValue, 'config');
		});
	});
}

function showPatternInternal(msg, callback) {
	lunaSetConfig('showPattern', {
		slave: msg.slave,
		on: msg.on,
		r: msg.r,
		g: msg.g,
		b: msg.b
	}, callback);
}

function showPattern(socket, msg) {
	showPatternInternal(msg, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'pattern');
	});
}

function setGamma(socket, msg) {
	lunaSetConfig('setGamma', {
		slave: msg.slave,
		gamma: msg.gamma
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'unit');
	});
}

function getGamma(socket, msg) {
	lunaGetConfig('getGamma', {
		slave: msg.slave
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload);
	});
}

function setGamut(socket, msg) {
	lunaSetConfig('setGamut', {
		slave: msg.slave,
		array: msg.array
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'unit');
	});
}

function getGamut(socket, msg) {
	lunaGetConfig('getGamut', {
		slave: msg.slave
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result.gamut);
	});
}

function setBC_CC(socket, msg) {
	lunaSetConfig('setBC_CC', {
		slave: msg.slave,
		bank: msg.bank,
		bc: msg.bc,
		r: msg.r,
		g: msg.g,
		b: msg.b
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'module');
	});
}

function getBC_CC(socket, msg) {
	lunaGetConfig('getBC_CC', {
		slave: msg.slave,
		bank: msg.bank
	}, function(ret) {
		socket.emit("module" + msg.eventID, ret.payload.result);
	});
}

function setCtrlDCDC_internal(slave, dcdc, callback) {
	lunaSetConfig('setCtrlDCDC', {
		slave: slave,
		dcdc: dcdc,
	}, callback);
}

function setCtrlDCDC(socket, msg) {
	setCtrlDCDC_internal(msg.slave, msg.dcdc, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'unit');
	});

	ledPowerChangeHandler(msg.dcdc);
}

function getCtrlDCDC(socket, msg) {
	lunaGetConfig('getCtrlDCDC', {
		slave: msg.slave,
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload);
	});
}

function getTemperature(socket, msg) {
	lunaGetConfig('getTemperature', {
		slave: msg.slave,
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function getCurrent(socket, msg) {
	lunaGetConfig('getCurrent', {
		slave: msg.slave,
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function getSignalStatus(socket, msg) {
	lunaGetConfig('getSignalStatus', {
		slave: msg.slave,
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function getPowerStatus(socket, msg) {
	lunaGetConfig('getPowerStatus', {
		slave: msg.slave,
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function getLOD(socket, msg) {
	lunaGetConfig('getLOD', {
		slave: msg.slave,
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function getLodDataInternal(param, callback) {
	lunaGetConfig('getLodData', {
		slave: param.slave
	}, callback);
}

function getLodData(socket, msg) {
	getLodDataInternal(msg, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function getOverallStatus(socket, msg) {
	lunaGetConfig('getOverallStatus', {
		slave: msg.slave,
	}, function(ret) {
		socket.emit("unit" + msg.eventID, ret.payload.result);
	});
}

function saveCalibrationData(socket, msg) {
	lunaSetConfig('saveCalibrationData', {
		slave: msg.slave,
		bank: msg.bank
	}, function(ret) {
		socket.emit("module" + msg.eventID, ret.payload.result);
	});
}

function loadCalibrationData(socket, msg) {
	lunaSetConfig('loadCalibrationData', {
		slave: msg.slave,
		from: msg.from
	}, function(ret) {
		socket.emit("module" + msg.eventID, ret.payload.result);
	});
}

function ledFpgaUpdate(socket, msg) {
	api.luna().call(ledfpgaURL + 'fpgaUpdate', {
		filePath: updateDir + msg.fileName
	}, function(ret) {
		socket.emit("ledFpga" + msg.eventID, ret.payload);
	});
}

function getFpgaUpdatePercent(socket, msg) {
	api.luna().call(ledfpgaURL + 'getFPGAUpdatePercent', {
	}, function(ret) {
		socket.emit("ledFpga" + msg.eventID, ret.payload);
	});
}

function getFpgaCheckSum(socket, msg) {
	var path = '/media/update/' + msg.name;
	if (msg.name == 'NULL') {
		path = 'NULL';
	}
	api.luna().call(ledfpgaURL + 'getFPGACheckSum', {
		filePath: path
	}, function(ret) {
		socket.emit("ledFpga" + msg.eventID, ret.payload);
	});
}

function setPMControl(socket, msg) {
	lunaSetConfig('setPMControl', {
		onOff: msg.onOff,
	}, function(ret) {
		api.sendReturn(socket, ret.payload.returnValue, 'factory');
	});
}

function setI2C(socket, msg) {
	lunaSetConfig('setI2cInfo', {
		slave: msg.slave,
		subAddr: msg.subAddr,
		byte: msg.byte
	}, function(ret) {
	});
}

function getI2C(socket, msg) {
	lunaGetConfig('getI2cInfo', {
		slave: msg.slave,
		subAddr: msg.subAddr
	}, function(ret) {
		socket.emit("ledFpga" + msg.eventID, ret.payload);
	});
}

function setPixelBrightness(socket, msg) {
	lunaSetConfig('setPixelBrightness', {
		slave: msg.slave,
		bank: msg.bank,
		v: msg.v,
		h: msg.h,
		coeff: msg.coeff
	}, function(ret) {
	});
}

function setEdgeBrightness(socket, msg) {
	lunaSetConfig('setEdgeBrightness', {
		slave: msg.slave,
		bank: msg.bank,
		side: msg.side,
		coeff: msg.coeff,
		rgbw: msg.rgbw
	}, function(ret) {
	});
}

function getLedConfig(socket, msg) {
	fs.readFile(newConfigFile, function(err, data) {
		var config = {
			from: 0,
			direction: 0,
			resolution: {
				w: 384,
				h: 360
			},
			viewPort: {
				x: 0,
				y: 0,
				w: 1920,
				h: 1080
			},
			unitList: []
		};
		
		if (!err) {
			config = JSON.parse(String(data));
		}
		
		socket.emit("config" + msg.eventID, config);
	});
}

function setLedConfig(socket, msg) {
	var config = msg.config;

	fs.writeFile(newConfigFile, JSON.stringify(config, null, 2), function(err) {
		if (err) {
			console.log(err);
		}
		api.sendReturn(socket, err !== undefined, "config");
	});

	//To Do : have to Set moduleWidth, moduleHeight, rowOfUnit, columnOfUnit
	var settings = {
		moduleWidth: config.resolution.w / 2,
		moduleHeight: config.resolution.h / 2,
		rowOfModule: 2,
		columnOfModule: 2,
		rowOfUnit: config.viewPort.h / config.resolution.h,
		columnOfUnit: config.viewPort.w / config.resolution.w,
		numOfUnits: config.unitList.length,
		ledLayout: config
	};

	if(web.env.supportLedFilm){
		settings.rowOfUnit = 1;
		settings.columnOfUnit = 1;
	}


	lunaAPI.setSystemSettings('commercial', settings, function(ret) {
		if (!ret.payload.returnValue) {
			console.log(ret.payload);
		}
	});
}

function getConfigs(socket, msg) {
	api.luna().call("luna://com.webos.service.config/getConfigs", {
		configNames: ["commercial.disableOsd"]
	}, function(m) {
		socket.emit('configs' + msg.eventID, m.payload);
	});
}

function setConfigs(socket, msg) {
	api.luna().call("luna://com.webos.service.config/setConfigs", {
		configs: {
			"commercial.disableOsd": msg.configs["commercial.disableOsd"]
		}
	}, function(m) {
		api.sendReturn(socket, m.payload.returnValue, "configs");
	});
}

function setMouseVisibility(socket, msg) {
	api.luna().call("luna://com.webos.service.config/setConfigs", {
		configs: {
			"com.webos.service.commercialinputdevice.enableMouseVisible": msg.configs["enableMouseVisible"]
		}
	}, function(m) {
		api.sendReturn(socket, m.payload.returnValue, "configs");
	});
}

function getSharedIpList(socket, msg) {
	lunaGetConfig('getSharedIpList', {
	}, function(ret) {
		socket.emit("config" + msg.eventID, ret.payload);
	});
}

function readGroupFile(type, callback) {
	var groupFile = type == 'dbc' ? dbcGroupFile : controlGroupFile;
	fs.readFile(groupFile, function(err, data) {
		if (err) {
			callback('error');
			return;
		}
		data = '' + data;
		var line = data.split('\n');

		if (line.length < 2) {
			callback('error');
			return;
		}

		var ret = {
			slaves: []
		};
		ret.master = line[0];
		ret.numSlaves = Number(line[1]);

		for (var i = 0; i < ret.numSlaves; ++i) {
			ret.slaves.push(line[2 + i]);
		}
		callback(ret);
	});

}

function getGroupFile(socket, msg) {
	var type = msg.type;

	readGroupFile(type, function(ret) {
		socket.emit("config" + msg.eventID, ret);
	});
}

function setGroupFile(socket, msg) {
	var groupFile = msg.type == 'dbc' ? dbcGroupFile : controlGroupFile;
	var str = msg.str;

	fs.writeFile(groupFile, str, function(err) {
		if (err) {
			console.log(err);
		}
		api.sendReturn(socket, err !== undefined, "config");
	});
}

var lodTimer = undefined;

var colorCode = ['-', 'R', 'G', 'RG', 'B', 'RB', 'GB', 'RGB'];
function mergeColor(c1, c2) {
	if (c1 == c2) {
		return c1;
	}
	var i1 = colorCode.indexOf(c1);
	var i2 = colorCode.indexOf(c2);

	var newIndex = i1 | i2;

	return colorCode[newIndex];
}

function packLodData(data) {
	var keys = Object.keys(data);
	var ret = {};
	keys.forEach(function(key) {
		var comma = key.lastIndexOf(',');
		var location = key.substring(0, comma);
		var color = key.substring(comma + 1, key.length);

		if (ret[location]) {
			color = mergeColor(ret[location], color);
		}

		ret[location] = color;
	});

	return ret;
}

function liveLOD(socket, msg) {
	var ret = {
		isRunning: lodTimer !== undefined
	};

	if (msg.start === undefined) {
		if (lodTimer) {
			ret.data = packLodData(LodData.data);
		}
		socket.emit('liveLOD' + msg.eventID, ret);
		return;
	}

	if (msg.start === false) {
		if (lodTimer) {
			LodData.stopDetection();
		}

		ret.isRunning = false;
		socket.emit('liveLOD' + msg.eventID, ret);
		return;
	}

	getUnitConfigInternal(function(config) {
		LodData.config = config;

		if (LodData.config.onUnits.numOn == 0) {
			return;
		}
	
		lodTimer = setInterval(LodData.detectionTimerCallback, 10 * 1000);

		ret.isRunning = true;
		socket.emit('liveLOD' + msg.eventID, ret);
	});
}

var LodData = {
	slave: 2,
	bank: 0,
	config: undefined,
	data: {},

	collectData: function (slave, now) {
		getLodDataInternal({
			slave: slave,
		}, function(ret) {
			ret.payload.result.defects.forEach(function(defect) {
				for (var i = 0; i != defect.color.length; ++i) {
					var key = '' + slave + ',' + defect.ldm + ',' + defect.v + ',' + defect.h + ',' + defect.color[i];
					LodData.data[key] = now;
				}
			});
		})
	},

	moveDetectionWindow: function (data, now) {
		for (var key in data) {
			if (data[key] < (now - 60 * 60 * 1000)) { // one hour
				delete data[key];
			}
		}
	},

	detectionTimerCallback: function () {
		var now = Date.now();

		LodData.moveDetectionWindow(LodData.data, now);
		LodData.collectData(LodData.slave, now);

		LodData.slave = ((LodData.slave / 2) % LodData.config.onUnits.numOn + 1) * 2;
	},

	stopDetection: function() {
		clearInterval(lodTimer);
		lodTimer = undefined;
		LodData.data = {};
		LodData.slave = 2;
	}
}

// Sync Master/slave
var https = require('https');

function createRequestOptions(ip, port, api, method) {
	return {
		host: ip,
		port: port,
		path: '/rest/' + api,
		rejectUnauthorized: false,
		requestCert: true,
		agent: false,
		method: method,
		headers: {
			"Content-Type": "application/json"
		}
	};
}

function setSharedPmForAll(min) {
	getTargetPmValue(function(ret) {
		min = Math.min(ret.payload.result.targetPmVal, min);

		restCallFunction('setSharedPmValue', {
			sharedPmVal: min
		}, true);

		setSharedPmValue(min, function() {});
	});
}

function runLocalDBC() {
	getTargetPmValue(function(ret) {
		val = ret.payload.result.targetPmVal;
		setSharedPmValue(val, function() {}, true);
	});
}

function checkDBCforSlave(group) {
	var cur = Date.now();

	// exception handling for connection error
	if (cur - lastDbcSetTime < dbcInterval * 5) {
		return;
	}

	// run DBC locally
	runLocalDBC();
}

function checkDBCforMaster(group) {
	var completed = 0;
	var min = 0xff;

	function setSharedPmHandler() {
		++completed;
		if (completed === group.slaves.length) {
			setSharedPmForAll(min);
		}
	}

	getTargetPmValue(function(ret) {
		min = ret.payload.result.targetPmVal;

		group.slaves.forEach(function(ip) {
			var options = createRequestOptions(ip, web.env.port, 'getTargetPmValue', 'GET');

			var req = https.request(options, function(res) {
				res.setEncoding('utf8');
				res.on('data', function (chunk) {
                    try {
					    var ret = JSON.parse(chunk);
					    if (ret.returnValue) {
						    min = Math.min(ret.result.targetPmVal, min);
					    }
                    } catch (e) {
                        console.log(e);
                    }
					setSharedPmHandler();
				});
			});

			req.on('error', function(e) {
				setSharedPmHandler();
				console.log('problem with request: ' + e.message);
			});

			req.end();
		});
	});
}

function checkDBC() {
	if (!web.env.supportLedSignage) {
		return;
	}

	lunaAPI.getSystemSettings("commercial", ["ledResolutionWindow"], function(ret) {
		if (!ret.payload.settings.ledResolutionWindow.on) {
			return;
		}

		readGroupFile('dbc', function(group) {
			if (group == 'error' || group.master == '0' || group.numSlaves == 0) {
				runLocalDBC();
				return;
			}

			lunaAPI.getNetworkStatus(function(network) {
				if (group.master !== network.payload.wired.ipAddress) {
					checkDBCforSlave(group);
					return;
				}

				checkDBCforMaster(group);
			});
		});
	});
}

function restCallFunction(api, param, isDBC) {
	if (!web.env.supportLedSignage) {
		return;
	}

	readGroupFile(isDBC ? 'dbc' : 'control', function(group) {
		if (group == 'error' || group.master == '0') {
			return;
		}
		lunaAPI.getNetworkStatus(function(network) {
			if (group.master !== network.payload.wired.ipAddress) {
				return;
			}

			group.slaves.forEach(function(ip) {
				var options = createRequestOptions(ip, web.env.port, api, 'PUT');

				var req = https.request(options, function(res) {
					res.setEncoding('utf8');
					res.on('data', function (chunk) {
					});
				});
			
				req.on('error', function(e) {
					console.log('problem with request: ' + e.message);
				});

				// write data to request body
				req.end(JSON.stringify(param));
			});
		});
	});
}

function pictureChangeHandler(dimension, settings) {
	restCallFunction('picture', {
		dimension: dimension,
		settings: settings
	});
}

function colorTempChangeHandler(temp) {
	restCallFunction('colorTemp', {
		temp: temp
	});
}

function inputChangeHandler(input) {
	restCallFunction('input', {
		id: input,
	});
}

function ledPowerChangeHandler(DCDC) {
	restCallFunction('ledPower', {
		ledPower: DCDC,
	});
}

function getLgseInterfereVal(socket, msg) {
	lunaGetConfig('getLgseInterfereVal', {
		slave: msg.slave,
		module: msg.module,
		type: msg.type,
		rgb: msg.rgb
	}, function(ret) {
		socket.emit('getLgseInterfereVal' + msg.eventID, ret.payload);
	});
}

function setLgseInterfereVal(socket, msg) {
	lunaSetConfig('setLgseInterfereVal', {
		slave: msg.slave,
		module: msg.module,
		type: msg.type,
		rgb: msg.rgb,
		setVal: msg.setVal
	}, function(ret) {
		socket.emit('setLgseInterfereVal' + msg.eventID, ret.payload.returnValue);
	});
}

function setAverageEdgeGain(socket, msg) {
	lunaSetConfig('setAverageEdgeGain', {
		slave: msg.slave,
		offset: msg.offset
	}, function(ret) {
		socket.emit('setAverageEdgeGain' + msg.eventID, ret.payload.returnValue);
	});
}

function setPixelCalibration(socket, msg) {
	lunaSetConfig('setPixelCalibration', {
		slave: msg.slave,
		bank: msg.bank,
		gain: msg.gain
	}, function(ret) {
		socket.emit('setPixelCalibration' + msg.eventID, ret.payload.returnValue);
	});
}

function setRandomPixelGain(socket, msg) {
	lunaSetConfig('setRandomPixelGain', {
		slave: msg.slave,
        bank: msg.bank,
        defaultGain: msg.defaultGain,
        from: msg.from,
        to: msg.to,
        color: msg.color,
        range: msg.range
	}, function(ret) {
		socket.emit('setRandomPixelGain' + msg.eventID, ret.payload.returnValue);
	});
}

function setSharedPmValue(sharedPmVal, callback, isFromLocal) {
	if (isFromLocal == undefined) {
		lastDbcSetTime = Date.now();
	}
	lunaSetConfig('setSharedPmValue', {
		sharedPmVal: sharedPmVal
	}, callback);
}

function getTargetPmValue(callback) {
	lunaGetConfig('getTargetPmValue', {}, function(ret) {
		callback(ret);
	});
}

function get365CareServiceMode(socket, msg) {
    api.luna().call("luna://com.webos.service.tv.systemproperty/getProperties", { keys:["commer365CareServiceMode"] }, function(ret) {
        socket.emit('signage365Care' + msg.eventID, ret.payload);
    });
}

function searchAccountName(socket, msg) {
    signage365Care.searchAccountName(msg.accountNumber, function(ret) {
        socket.emit('signage365Care' + msg.eventID, ret);
    });
}

function care365Enable(socket, msg) {
    api.luna().call(care365URL + 'start', {
	}, function(ret) {
        socket.emit('signage365Care' + msg.eventID, ret.payload);
    });
}

function care365Disable(socket, msg) {
    api.luna().call(care365URL + 'stop', {
	}, function(ret) {
        socket.emit('signage365Care' + msg.eventID, ret.payload);
    });
}

function appDownloadURL(socket, msg) {
    signage365Care.appDownloadURL(msg.serviceMode, function(ret) {
        socket.emit('signage365Care' + msg.eventID, ret);
    });
}

function getServerStatusCare365(socket, msg) {
    api.luna().call(care365URL + 'getServerStatus', {
	}, function(ret) {
        socket.emit('signage365Care' + msg.eventID, ret.payload);
    });
}

function getServerStatusCare365Subscribe(socket, msg) {
    serverStatus = api.luna().subscribe(care365URL + 'getServerStatus', {
        subscribe: true
	});

    serverStatus.on("response", function(m) {
        if (!m.payload.returnValue) {
            console.log("365 Care Server Subscribe End");
            serverStatus.cancel();
        } else {
            api.io.emit("serverStatus", m.payload);
        }
    });

}

function remove365CareApp(socket, msg) {
     api.luna().call(appInstallServiceURL + 'remove', {
        id: "com.webos.app.commercial.care365"
    }, function(ret) {
        socket.emit('signage365Care' + msg.eventID, ret.payload);
    });
}

function getListApps(socket, msg) {
    api.luna().call(applicationManagerURL + 'listApps', {
	}, function(ret) {
        socket.emit("getListAppsInfo" + msg.eventID, ret.payload);
    });
}

function getUseNetworkTime(socket, msg) {
    api.luna().call(systemserviceURL + 'getPreferences', {
        keys: ["useNetworkTime"]
    }, function(ret) {
        console.log(ret);
        socket.emit("signage365Care" + msg.eventID, ret.payload);
    });
}

function setAutomaticallyTime(socket, msg) {
    api.luna().subscribe(systemserviceURL + 'setPreferences', {
        useNetworkTime: true,
        subscribe: true
    });

    lunaAPI.setSystemSettings('time', {
        autoClock: 'on'
    }, function(ret) {
        socket.emit("signage365Care" + msg.eventID, ret.payload);
    });
}

function isFirstBoot(socket, msg) {
    socket.emit('signage365Care' + msg.eventID, isFirstBooting);
    isFirstBooting = false;
}

function installIpkDownload(socket, msg) {
    var ipkDownload = api.luna().subscribe("palm://com.palm.downloadmanager/download", {
        target: msg.downloadUrl,
        targetDir: "/media/internal/downloads/",
        targetFilename: "com.webos.app.commercial.care365.ipk",
		subscribe: true
	});

	ipkDownload.on("response", function(m) {
        if (m.payload.hasOwnProperty("completionStatusCode")) {
			console.log(m.payload.completionStatusCode);
			console.log(api);
			console.log(api.io);
            api.io.emit("installIpk", m.payload);
            ipkDownload.cancel();
        }
	})
}

function install365CareApp(socket, msg) {
    var careInstall = api.luna().subscribe("luna://com.webos.appInstallService/commercial/installInternal", {
        id: "com.webos.app.commercial.care365",
        ipkUrl: "/media/internal/downloads/" + msg.destFile,
        signature: msg.signature,
        subscribe: true
    });

    careInstall.on("response", function(m) {
        if (m.payload.hasOwnProperty("details") && (m.payload.details.state === "installed" || m.payload.details.state === "install failed")) {
            api.io.emit("careInstall", m.payload);
            careInstall.cancel();
        }
    });
}

function initCommand() {
	command['getUnitResolutionList'] = getUnitResolutionList;

	command['getUnitConfig'] = getUnitConfig;
	command['setUnitConfig'] = setUnitConfig;

	command['setSlaveAddr'] = setSlaveAddr;
	command['setPosition'] = setPosition;

	command['showPattern'] = showPattern;

	command['getGamma'] = getGamma;
	command['setGamma'] = setGamma;
	command['getGamut'] = getGamut;
	command['setGamut'] = setGamut;
	command['getBC_CC'] = getBC_CC;
	command['setBC_CC'] = setBC_CC;
	command['getCtrlDCDC'] = getCtrlDCDC;
	command['setCtrlDCDC'] = setCtrlDCDC;

	command['getTemperature'] = getTemperature;
	command['getCurrent'] = getCurrent;
	command['getSignalStatus'] = getSignalStatus;
	command['getPowerStatus'] = getPowerStatus;
	command['getLOD'] = getLOD;
	command['getOverallStatus'] = getOverallStatus;

	command['getLodData'] = getLodData;

	command['saveConfig'] = saveConfig;
	command['loadConfig'] = loadConfig;

	command['saveCalibrationData'] = saveCalibrationData;
	command['loadCalibrationData'] = loadCalibrationData;

	command['ledFpgaUpdate'] = ledFpgaUpdate;
	command['getFpgaUpdatePercent'] = getFpgaUpdatePercent;
	command['getFpgaCheckSum'] = getFpgaCheckSum;

	command['setPMControl'] = setPMControl;

	command['liveLOD'] = liveLOD;

	command['setI2C'] = setI2C;
	command['getI2C'] = getI2C;

	command['setPixelBrightness'] = setPixelBrightness;
	command['setEdgeBrightness'] = setEdgeBrightness;

	command['getLedConfig'] = getLedConfig;
	command['setLedConfig'] = setLedConfig;

	command['getConfigs'] = getConfigs;
	command['setConfigs'] = setConfigs;
	command['setMouseVisibility'] = setMouseVisibility;

	command['getSharedIpList'] = getSharedIpList;

	command['getGroupFile'] = getGroupFile;
	command['setGroupFile'] = setGroupFile;

	command['getLgseInterfereVal'] = getLgseInterfereVal;
	command['setLgseInterfereVal'] = setLgseInterfereVal;

	command['setAverageEdgeGain'] = setAverageEdgeGain;

	command['setPixelCalibration'] = setPixelCalibration;
	command['setRandomPixelGain'] = setRandomPixelGain;

	command["searchAccountName"] = searchAccountName;
	command["care365Enable"] = care365Enable;
	command["care365Disable"] = care365Disable;
	command["appDownloadURL"] = appDownloadURL;
	command["getServerStatusCare365"] = getServerStatusCare365;
	command["getServerStatusCare365Subscribe"] = getServerStatusCare365Subscribe;
	command["remove365CareApp"] = remove365CareApp;
	command["installIpkDownload"] = installIpkDownload;
	command["install365CareApp"] = install365CareApp;
	command['get365CareServiceMode'] = get365CareServiceMode;
	command['getListApps'] = getListApps;
	command['getUseNetworkTime'] = getUseNetworkTime;
	command['setAutomaticallyTime'] = setAutomaticallyTime;
	command['isFirstBoot'] = isFirstBoot;
}


exports.commandHandler = commandHandler;
exports.getUnitConfig = getUnitConfigInternal;
exports.ledGetConfig = lunaGetConfig;
exports.getLodData = getLodDataInternal;
exports.showPattern = showPatternInternal;

exports.readGroupFile = readGroupFile;
exports.pictureChangeHandler = pictureChangeHandler;
exports.colorTempChangeHandler = colorTempChangeHandler;
exports.inputChangeHandler = inputChangeHandler;
exports.ledPowerChangeHandler = ledPowerChangeHandler;

exports.setCtrlDCDC = setCtrlDCDC_internal;

exports.getTargetPmValue = getTargetPmValue;
exports.setSharedPmValue = setSharedPmValue;

exports.init = init;
